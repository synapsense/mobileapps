package com.synapsense.mobile.view;

import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.EmbossMaskFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

public class PieChartView extends LinearLayout {

	private static final int DEFAULT_RADIUS = 100;
	private static final PieChartItem[] EMPTY = new PieChartItem[0];
	private ChartView chartView;
	private LinearLayout legendView;
	private PieChartItem[] pieChartItems = EMPTY;
	private int radius = DEFAULT_RADIUS;
	private float totalCount;
	private int legendTextColor = getResources().getColor(android.R.color.background_light);
	private boolean legendShown = true;
	private boolean inverseOrder = false;

	public PieChartView(Context context) {
		super(context);
		init(context);
	}

	public PieChartView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context context) {
		chartView = new ChartView(context);
		LinearLayout.LayoutParams chartViewLayoutParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
			LayoutParams.WRAP_CONTENT);
		chartViewLayoutParams.gravity = Gravity.CENTER;
		legendView = new LinearLayout(context);
		legendView.setOrientation(LinearLayout.VERTICAL);
		LinearLayout.LayoutParams legendViewLayoutParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
			LayoutParams.WRAP_CONTENT);
		legendViewLayoutParams.gravity = Gravity.CENTER;
		if (inverseOrder) {
			addView(legendView, legendViewLayoutParams);
			addView(chartView, chartViewLayoutParams);
		} else {
			addView(chartView, chartViewLayoutParams);
			addView(legendView, legendViewLayoutParams);
		}
	}

	public LinearLayout getLegendView() {
		return legendView;
	}

	public boolean isInverseOrder() {
		return inverseOrder;
	}

	public void setInverseOrder(boolean inverseOrder) {
		this.inverseOrder = inverseOrder;
	}

	public PieChartItem[] getPieChartItems() {
		return pieChartItems.clone();
	}

	public void setPieChartItems(PieChartItem[] newPieChartItems) {
		if (newPieChartItems != null) {
			pieChartItems = newPieChartItems.clone();
			legendView.removeAllViews();
			totalCount = 0;
			for (PieChartItem pieChartItem : newPieChartItems) {
				totalCount += pieChartItem.count;
				legendView.addView(new LegendItemLayout(legendView.getContext(), pieChartItem));
			}
			chartView.invalidate();
		} else {
			legendView.removeAllViews();
			pieChartItems = EMPTY;
		}
	}

	public void setPieChartItems(List<PieChartItem> newPieChartItems) {
		setPieChartItems(newPieChartItems != null ?
			newPieChartItems.toArray(new PieChartItem[newPieChartItems.size()]) : null);
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
		chartView.setRadius(radius);
	}

	public boolean isLegendShown() {
		return legendShown;
	}

	public void setLegendShown(boolean legendShown) {
		this.legendShown = legendShown;
		legendView.setVisibility(legendShown ? VISIBLE : GONE);
	}

	public int getLegendTextColor() {
		return legendTextColor;
	}

	public void setLegendTextColor(int legendTextColor) {
		this.legendTextColor = legendTextColor;
	}

	public static class PieChartItem {

		public final float count; 
		public final String title; 
		public final int color; 

		public PieChartItem(float count, String title, int color) {
			this.count = count;
			this.title = title;
			this.color = color;
		}

		public String toString() {
			return "PieChartItem {title=" + title + ", count=" + count + ", color=0x" +
				Integer.toHexString(color).toUpperCase() + "}";
		}
	}

	private class ChartView extends View {

		private static final float DEGREES_360 = 360;
		private static final float AMBIENT = 0.4f;
		private static final float SPECULAR = 10;
		private static final float BLUR_RADIUS = 8.2f;
		private final float[] DIRECTION = {1, 1, 1};
		private final EmbossMaskFilter filter = new EmbossMaskFilter(DIRECTION, AMBIENT, SPECULAR, BLUR_RADIUS);
		private final RectF rect = new RectF();
		private Paint sectorPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		private Paint arcPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

		private ChartView(Context context) {
			super(context);
			sectorPaint.setStyle(Paint.Style.FILL);
			sectorPaint.setColor(getResources().getColor(android.R.color.background_light));
			sectorPaint.setStrokeWidth(0.5F);
			sectorPaint.setMaskFilter(filter);
			arcPaint.setStyle(Paint.Style.STROKE);
			arcPaint.setColor(getResources().getColor(android.R.color.background_dark));
			arcPaint.setStrokeWidth(0.5f);
			setRadius(DEFAULT_RADIUS);
		}

		private void setRadius(int radius) {
			setMinimumHeight(getPaddingTop() + radius + getPaddingBottom());
			setMinimumWidth(getPaddingLeft() + radius + getPaddingRight());
		}

		protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);
			rect.set(getPaddingLeft(), getPaddingTop(), getWidth() - getPaddingRight(), getHeight() - getPaddingBottom());
			if (isInEditMode()) {
				canvas.drawArc(rect, 0, DEGREES_360 - Float.MIN_VALUE, true, sectorPaint);
				canvas.drawArc(rect, 0, DEGREES_360 - Float.MIN_VALUE, false, arcPaint);
			} else {
				float startAngle = 0;
				for (PieChartItem pieChartItem : pieChartItems) {
					sectorPaint.setColor(pieChartItem.color);
					float sweepAngle = -DEGREES_360 * (pieChartItem.count / totalCount);
					canvas.drawArc(rect, startAngle, sweepAngle, true, sectorPaint);
					canvas.drawArc(rect, startAngle, sweepAngle, false, arcPaint);
					startAngle += sweepAngle;
				}
			}
		}

		protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
			setMeasuredDimension(getSuggestedMinimumWidth(), getSuggestedMinimumHeight());
		}
	}

	private class LegendItemLayout extends LabelView {

		private static final int DEFAULT_SIZE = 10;

		private LegendItemLayout(Context context, PieChartItem item) {
			super(context);
			image.setMinimumWidth(DEFAULT_SIZE);
			image.setMinimumHeight(DEFAULT_SIZE);
			image.setBackgroundColor(item.color);
			text.setTextColor(legendTextColor);
			text.setText(item.title);
		}
	}
}
