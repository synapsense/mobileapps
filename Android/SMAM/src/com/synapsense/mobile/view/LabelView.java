package com.synapsense.mobile.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class LabelView extends LinearLayout {

	public final ImageView image;
	public final TextView text;
	private static final int DEFAULT_MARGIN = 5;

	public LabelView(Context context) {
		super(context);
		image = new ImageView(context);
		LinearLayout.LayoutParams layoutParamsColor = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
			LayoutParams.WRAP_CONTENT);
		layoutParamsColor.leftMargin = DEFAULT_MARGIN;
		layoutParamsColor.rightMargin = DEFAULT_MARGIN;
		layoutParamsColor.gravity = Gravity.CENTER_VERTICAL;
		addView(image, layoutParamsColor);
		LinearLayout.LayoutParams layoutParamsTitle = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
			LayoutParams.WRAP_CONTENT);
		layoutParamsTitle.gravity = Gravity.CENTER_VERTICAL;
		text = new TextView(context);
		addView(text, layoutParamsTitle);
	}

	public LabelView(Context context, AttributeSet attrs) {
		super(context, attrs);
		image = new ImageView(context, attrs);
		LinearLayout.LayoutParams layoutParamsColor = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
			LayoutParams.WRAP_CONTENT);
		layoutParamsColor.leftMargin = DEFAULT_MARGIN;
		layoutParamsColor.rightMargin = DEFAULT_MARGIN;
		layoutParamsColor.gravity = Gravity.CENTER_VERTICAL;
		addView(image, layoutParamsColor);
		LinearLayout.LayoutParams layoutParamsTitle = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
			LayoutParams.WRAP_CONTENT);
		layoutParamsTitle.gravity = Gravity.CENTER_VERTICAL;
		text = new TextView(context, attrs);
		addView(text, layoutParamsTitle);
	}
}
