package com.synapsense.mobile.ui;

import android.os.Bundle;

import com.synapsense.mobile.R;

public class DataCenterTabActivity extends CustomTabActivity {

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addTab("FacilityDashboard", R.string.dashboardTabTitle, R.drawable.tab_dashboard,
			DataCenterDashboardActivity.class);
		addTab("Alerts", R.string.alertsTabTitle, R.drawable.tab_alerts, AlertListActivity.class);
		addTab("FloorPlan", R.string.floorplaneTabTitle, R.drawable.tab_floorplan, FloorPlanActivity.class);
		addTab("EmergencyContact", R.string.emergencyContactTabTitle, R.drawable.tab_contact,
			DataCenterContactActivity.class);
		tabHost.setCurrentTab(getIntent().getIntExtra("currentTabIndex", 0));
	}
}
