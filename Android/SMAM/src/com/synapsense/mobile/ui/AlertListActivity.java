package com.synapsense.mobile.ui;

import java.util.Arrays;
import java.util.Comparator;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.synapsense.mobile.R;
import com.synapsense.mobile.alert.Alert;
import com.synapsense.mobile.alert.AlertStatus;
import com.synapsense.mobile.service.AlertService;
import com.synapsense.mobile.service.DefaultAlertManager;
import com.synapsense.mobile.service.DefaultObjectManager;

// ESCA-JAVA0077:
public class AlertListActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener,
		DialogInterface.OnMultiChoiceClickListener, DialogInterface.OnDismissListener, AbsListView.OnScrollListener,
		View.OnTouchListener, DialogInterface.OnClickListener {

	private static final int START_DATA_REFRESH = 0;
	private static final int STOP_DATA_REFRESH = 1;
	private static final int STOP_DISMISS = 2;
	private static final String[] sotingItems = {"By name, ascending", "By name, descending",
		"By object, ascending", "By object, descending", "By priority, ascending",
		"By priority, descending", "By timestamp, ascending", "By timestamp, descending"};
	private static final String[] filterItems = {"Priority: Critical", "Priority: Major", "Priority: Minor",
		"Priority: Informational", "Status: Acknowledged", "Status: Opened"};
	private static final boolean[] filterSelections = {true, true, true, true, true, true};
	private static final int priorityCount = 4;
	private final NewAlertsReceiver newAlertsReceiver = new NewAlertsReceiver();
	private final GestureDetector gestureDetector = new GestureDetector(new OnGestureListener());
	private ListView alertListView;
	private TextView alertCountTextView;
	private EditText newCommentTextField;
	private ImageView refreshButton;
	private AlertListAdapter alertListAdapter;
	private AlertDialog filterDialog;
	private AlertDialog sortingDialog;
	private AlertDialog errorDialog;
	private AlertDialog newAlertsDialog;
	private AlertDialog newCommentDialog;
	private Button dismissButton = null;
	private MenuItem dismissAllAlertsMenuItem;
	private MenuItem sortMenuItem;
	private MenuItem filterMenuItem;
	private int totalAlertCount;
	private int totalActiveAlertCount;
	private int start = 0;
	private int count = AlertService.INIT_ALERT_COUNT;
	private boolean isFilterChanged = false;
	private int firstVisibleItemIndex = 0;
	private int lastVisibleItemIndex = Integer.MAX_VALUE;
	private String objectId;
	private String objectName;
	private Alert[] alerts;
	private Toast lastToast;
	private boolean dismissAllAlerts;
	private final AlertComparator alertComparator = new AlertComparator();
	private final Handler handler = new Handler() {

		public void handleMessage(Message msg) {
			if (isFinishing()) {
				return;
			}
			if (msg.what == START_DATA_REFRESH) {
				showProgressBar();
			} else if (msg.what == STOP_DATA_REFRESH) {
				setTitle(getString(R.string.customTitle) + " - " + objectName);
				if (alerts == null) {
					hideProgressBar();
					errorDialog.show();
				} else {
					// update alertListView
					if (alerts != null) {
						Arrays.sort(alerts, alertComparator);
					}
					alertListAdapter.setAlerts(alerts);
					// update alertCountTextView
					alertCountTextView.setText(getString(R.string.displayingMessage) + " " + totalAlertCount +
						" " + getString(R.string.outOfMessage) + " " + totalActiveAlertCount + " " + getString(R.string.alertsMessage));
					// update filterStatusTextView
					int priorities =
						PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("priorities",
							AlertService.DEFAULT_PRIORITIES);
					int statuses =
						PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("statuses",
							AlertService.DEFAULT_STATUSES);
					// update filter content
					for (int i = 0; i < priorityCount; ++i) {
						filterSelections[priorityCount - 1 - i] = (priorities & 1) == 1;
						priorities >>= 1;
					}
					for (int i = priorityCount; i < filterSelections.length - priorityCount; i++) {
						filterSelections[i] = (statuses & 1) == 1;
						statuses >>= 1;
					}
					hideProgressBar();
				}
			} else if (msg.what == STOP_DISMISS) {
				if (msg.arg1 > 0) {
					errorDialog.show();
				} else {
					Thread runner = new Thread() {

						public void run() {
							if (!isFinishing()) {
								refresh();
							}
						}
					};
					runner.start();
				}
			}
		}
	};

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inflateContent(R.layout.alerts);
		contentView = (ViewGroup) findViewById(R.id.alertListLayout);
		alertListView = (ListView) contentView.findViewById(R.id.alertList_alertList);
		alertListAdapter = new AlertListAdapter(this);
		alertListAdapter.add(null);
		alertListView.setAdapter(alertListAdapter);
		alertListView.setOnItemClickListener(this);
		alertListView.setOnScrollListener(this);
		alertListView.setOnTouchListener(this);
		alertListView.setDivider(null);
		objectId = getIntent().getStringExtra("objectId");
		if (objectId == null && getParent() != null) {
			objectId = getParent().getIntent().getStringExtra("objectId");
		}
		alertCountTextView = (TextView) contentView.findViewById(R.id.alertList_alertCount);
		refreshButton = (ImageView) contentView.findViewById(R.id.alertList_refreshButton);
		refreshButton.setOnClickListener(this);
		errorDialog = new AlertDialog.Builder(this).setTitle(R.string.communicationError).create();
		newAlertsDialog = new AlertDialog.Builder(this).create();
		newAlertsDialog.setOnDismissListener(this);
		View newCommentView =
			((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.comment, null);
		newCommentTextField = (EditText) newCommentView.findViewById(R.id.comment_newCommentTextField);
		newCommentDialog = new AlertDialog.Builder(this).setView(newCommentView).create();
		newCommentDialog.setButton("OK", this);
		newCommentDialog.setOnDismissListener(this);
		filterDialog = new AlertDialog.Builder(this).setMultiChoiceItems(filterItems, filterSelections, this).create();
		filterDialog.setTitle(R.string.alertFilterTitle);
		filterDialog.setOnDismissListener(this);
		sortingDialog = new AlertDialog.Builder(this).setSingleChoiceItems(sotingItems, alertComparator.sortingType,
			this).create();
		sortingDialog.setTitle(R.string.alertSortingTitle);
		sortingDialog.setOnDismissListener(this);
	}

	private Alert[] requestAlerts() {
		if (objectId != null) {
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			alertComparator.sortingType = prefs.getInt("sortingType", sotingItems.length - 1);
			//set beforePeriod to null to always retrieve alerts for all time
			Long beforePeriod = null;
			totalActiveAlertCount = 
				DefaultAlertManager.instance.getNumActiveAlerts(objectId, beforePeriod,
					AlertService.DEFAULT_PRIORITIES, AlertService.DEFAULT_STATUSES);
			totalAlertCount =
				DefaultAlertManager.instance.getNumActiveAlerts(objectId, beforePeriod,
					prefs.getInt("priorities", AlertService.DEFAULT_PRIORITIES),
					prefs.getInt("statuses", AlertService.DEFAULT_STATUSES));
			if (totalAlertCount > AlertService.INIT_ALERT_COUNT) {
				return DefaultAlertManager.instance.getActiveAlerts(objectId, beforePeriod,
					prefs.getInt("priorities", AlertService.DEFAULT_PRIORITIES),
					prefs.getInt("statuses", AlertService.DEFAULT_STATUSES), start, count);
			} else {
				return DefaultAlertManager.instance.getActiveAlerts(objectId, beforePeriod,
					prefs.getInt("priorities", AlertService.DEFAULT_PRIORITIES),
					prefs.getInt("statuses", AlertService.DEFAULT_STATUSES), null, null);
			}
		} else {
			return null;
		}
	}

	public void onResume() {
		super.onResume();
		registerReceiver(newAlertsReceiver, new IntentFilter(AlertService.NEW_ALERTS_ACTION));
		Thread runner = new Thread() {

			public void run() {
				if (!isFinishing()) {
					refresh();
				}
			}
		};
		runner.start();
	}

	public void onPause() {
		super.onPause();
		newAlertsDialog.dismiss();
		newCommentDialog.dismiss();
		errorDialog.dismiss();
		filterDialog.dismiss();
		sortingDialog.dismiss();
		unregisterReceiver(newAlertsReceiver);
		if (lastToast != null) {
			lastToast.cancel();
		}
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		sortMenuItem = menu.add(Menu.NONE, Menu.FIRST, Menu.NONE, getString(R.string.sort));
		sortMenuItem.setIcon(R.drawable.sort);
		filterMenuItem = menu.add(Menu.NONE, Menu.FIRST, Menu.NONE, getString(R.string.filter));
		filterMenuItem.setIcon(R.drawable.filter);
		dismissAllAlertsMenuItem = menu.add(Menu.NONE, Menu.FIRST, Menu.NONE, getString(R.string.dismissAllAlerts));
		dismissAllAlertsMenuItem.setIcon(R.drawable.dismiss_all);
		return super.onCreateOptionsMenu(menu);
	}

	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		if (item == dismissAllAlertsMenuItem) {
			dismissAllAlerts = true;
			newCommentTextField.setText("");
			newCommentDialog.setTitle(R.string.dismissAllAlertsTitle);
			newCommentDialog.show();
			return true;
		} else if (item == sortMenuItem) {
			sortingDialog.show();
		} else if (item == filterMenuItem) {
			filterDialog.show();
		}
		return super.onMenuItemSelected(featureId, item);
	}

	private void refresh() {
		Message msg = handler.obtainMessage();
		msg.what = START_DATA_REFRESH;
		handler.sendMessage(msg);
		if (objectId != null) {
			objectName = (String) DefaultObjectManager.instance.getPropertyValue(objectId, "name");
			objectName = objectName == null ? getString(R.string.notAvailableMessage) : objectName;
		}
		alerts = requestAlerts();
		msg = handler.obtainMessage();
		msg.what = STOP_DATA_REFRESH;
		handler.sendMessage(msg);
	}

	public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
		alertListAdapter.setSelectionIndex(position);
		Intent intent = new Intent(AlertListActivity.this, AlertDetailsActivity.class);
		intent.putExtra("alert", alertListAdapter.getItem(position));
		intent.putExtra("objectName", objectName);
		startActivity(intent);
	}

	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		firstVisibleItemIndex = firstVisibleItem;
		lastVisibleItemIndex = firstVisibleItem + visibleItemCount;
		if (alerts != null) {
			int checkedIndex = alerts.length - 1;
			if (alerts.length < totalAlertCount && checkedIndex >= firstVisibleItemIndex &&
					checkedIndex <= lastVisibleItemIndex) {
				if (alertListAdapter.getCount() == 0 ||
						alertListAdapter.getItem(alertListAdapter.getCount() - 1) != null) {
					alertListAdapter.add(null);
					Thread runner = new Thread() {

						public void run() {
							count += AlertService.ALERT_COUNT_INCREMENT;
							if (start + count > totalAlertCount) {
								count = totalAlertCount;
							}
							refresh();
						}
					};
					runner.start();
				}
			}
		}
	}

	public void onScrollStateChanged(AbsListView view, int scrollState) {}

	public void onClick(View view) {
		if (view == refreshButton) {
			Thread runner = new Thread() {

				public void run() {
					refresh();
				}
			};
			runner.start();
		} else if (view == dismissButton) {
			dismissAllAlerts = false;
			dismissButton.setOnClickListener(null);
			newCommentDialog.setTitle(R.string.dismissAlertTitle);
			newCommentDialog.show();
		}
	}

	public boolean onTouch(View view, MotionEvent event) {
		if (dismissButton != null) {
			dismissButton.setOnClickListener(null);
			dismissButton.setVisibility(View.GONE);
		}
		return gestureDetector.onTouchEvent(event);
	}

	public void onClick(DialogInterface dialogInterface, int itemIndex, boolean flag) {
		filterSelections[itemIndex] = flag;
		isFilterChanged = true;
	}

	public void onClick(DialogInterface dialog, int which) {
		if (dialog == sortingDialog) {
			alertComparator.sortingType = which;
		} else if (dialog == newCommentDialog && which == DialogInterface.BUTTON_POSITIVE) {
			final String newComment = newCommentTextField.getText().toString().trim();
			if (newComment.isEmpty()) {
				lastToast = Toast.makeText(this, R.string.commentRequiredError, Toast.LENGTH_LONG);
				lastToast.show();
				Thread runner = new Thread() {

					public void run() {
						contentView.post(new Runnable() {

							public void run() {
								newCommentDialog.show();
							}
						});
					}
				};
				runner.start();
			} else {
				newCommentTextField.setText("");
				if (dismissButton != null) {
					Thread runner = new Thread() {

						public void run() {
							Message msg = handler.obtainMessage();
							msg.what = START_DATA_REFRESH;
							handler.sendMessage(msg);
							boolean result = dismissAllAlerts ?
								DefaultAlertManager.instance.dismissAllAlerts(objectId, newComment) :
								DefaultAlertManager.instance.dismiss(alerts[(Integer) dismissButton.getTag()].getId(),
								newComment);
							msg = handler.obtainMessage();
							msg.what = STOP_DISMISS;
							msg.arg1 = result ? 0 : 1;
							handler.sendMessage(msg);
						}
					};
					runner.start();
				}
			}
		}
	}

	public void onDismiss(DialogInterface dialog) {
		if (dialog == filterDialog) {
			int prioritySelectionCount = 0;
			int priorities = 0;
			for (int i = 0; i < priorityCount; ++i) {
				priorities <<= 1;
				if (filterSelections[i]) {
					++prioritySelectionCount;
					++priorities;
				}
			}
			int statusSelectionCount = 0;
			int statuses = 0;
			for (int i = priorityCount; i < filterSelections.length; ++i) {
				statuses <<= 1;
				if (filterSelections[i]) {
					++statusSelectionCount;
					++statuses;
				}
			}
			if (prioritySelectionCount == 0 || statusSelectionCount == 0) {
				filterDialog.show();
				Toast.makeText(this, R.string.filterError, Toast.LENGTH_LONG).show();
			} else {
				if (isFilterChanged) {
					final int priorities1 = priorities;
					final int statuses1 = statuses;
					Thread runner = new Thread() {

						public void run() {
							isFilterChanged = false;
							SharedPreferences.Editor prefsEditor =
								PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
							prefsEditor.putInt("priorities", priorities1);
							prefsEditor.putInt("statuses", statuses1);
							prefsEditor.commit();
							start = 0;
							refresh();
						}
					};
					runner.start();
				}
			}
		} else if (dialog == sortingDialog) {
			SharedPreferences.Editor prefsEditor =
				PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
			prefsEditor.putInt("sortingType", alertComparator.sortingType);
			prefsEditor.commit();
			if (alerts != null) {
				Arrays.sort(alerts, alertComparator);
			}
			alertListAdapter.setAlerts(alerts);
		} else if (dialog == newAlertsDialog) {
			Thread runner = new Thread() {

				public void run() {
					refresh();
				}
			};
			runner.start();
		} else if (dialog == newCommentDialog) {
			if (dismissButton != null) {
				dismissButton.setOnClickListener(null);
				dismissButton.setVisibility(View.GONE);
			}
		}
	}

	private class OnGestureListener extends GestureDetector.SimpleOnGestureListener {

		private static final float updateGestureVelocityThreshold = 2000;
		private static final float swipeGestureVelocityThreshold = 1000;

		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
			if (Math.abs(velocityY) < updateGestureVelocityThreshold &&
					Math.abs(velocityX) > swipeGestureVelocityThreshold) {
				dismissButton = (Button) alertListAdapter.getDismissButton();
				if (dismissButton != null) {
					Alert alert = alerts[(Integer) dismissButton.getTag()];
					if (alert != null && (alert.getStatus() == AlertStatus.OPENED ||
							alert.getStatus() == AlertStatus.ACKNOWLEDGED)) {
						dismissButton.setVisibility(View.VISIBLE);
						dismissButton.setOnClickListener(AlertListActivity.this);
					}
				}
			}
			return false;
		}
	}

	private static class AlertComparator implements Comparator<Alert> {

		private int sortingType = sotingItems.length - 1;

		public int compare(Alert alert1, Alert alert2) {
			switch (sortingType) {
			case 0:
				return alert1.getName().compareTo(alert2.getName());
			case 1:
				return alert2.getName().compareTo(alert1.getName());
			case 2:
				return alert1.getObjectName().compareTo(alert2.getObjectName());
			case 3:
				return alert2.getObjectName().compareTo(alert1.getObjectName());
			case 4:
				return alert1.getPriority() - alert2.getPriority();
			case 5:
				return alert2.getPriority() - alert1.getPriority();
			case 6:
				long delta = alert1.getTimestamp() - alert2.getTimestamp();
				return delta > 0 ? 1 : (delta < 0 ? -1 : 0);
			case 7:
				delta = alert2.getTimestamp() - alert1.getTimestamp();
				return delta > 0 ? 1 : (delta < 0 ? -1 : 0);
			default:
				return 0;
			}
		}
	}

	private class NewAlertsReceiver extends BroadcastReceiver {

		public void onReceive(Context context, final Intent intent) {
			newAlertsDialog.setTitle(intent.getStringExtra("message"));
			newAlertsDialog.setIcon(intent.getIntExtra("iconId", R.drawable.major_small));
			newAlertsDialog.show();
			Ringtone ringtone = RingtoneManager.getRingtone(AlertListActivity.this,
				RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
			if (ringtone != null) {
				ringtone.play();
			}
		}
	}
}
