package com.synapsense.mobile.ui;

import android.os.Bundle;

import com.synapsense.mobile.R;

public class MainTabActivity extends CustomTabActivity {

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addTab("Facilities", R.string.facilitiesTabTitle, R.drawable.tab_data_centers, DataCenterListActivity.class);
		addTab("TotalDashboard", R.string.dashboardTabTitle, R.drawable.tab_dashboard, TotalDashboardActivity.class);
		if (getIntent().getBooleanExtra("showSystemHealth", false)) {
			addTab("SystemHealth", R.string.systemHealthTabTitle, R.drawable.tab_system_health,
				SystemHealthActivity.class);
		}
		tabHost.setCurrentTab(getIntent().getIntExtra("currentTabIndex", 0));
	}
}
