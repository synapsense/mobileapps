package com.synapsense.mobile.ui;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;

import com.synapsense.mobile.R;
import com.synapsense.mobile.tree.ObjectStatus;

// ESCA-JAVA0076: ESCA-JAVA0256: ESCA-JAVA0259:
public class FloorPlanView extends WebView {

	public static final String RACK_TYPE = "RACK";
	public static final String CRAC_TYPE = "CRAC";
	public static final String PRESSURE_TYPE = "PRESSURE";
	public static final String DOOR_TYPE = "DOOR";
	public static final String GENERIC_TEMPERATURE_TYPE = "GENERICTEMPERATURE";
	public static final String LEAK_TYPE = "LEAK";
	public static final String EQUIPMENT_STATUS_TYPE = "EQUIPMENTSTATUS";
	public static final String VERTICAL_TEMPERATURE_TYPE = "VERTICALTEMPERATURE";
	public static final String ION_DELTA_TYPE = "ION_DELTA";
	public static final String ION_WYE_TYPE = "ION_WYE";
	public static final String ION_WIRELESS_TYPE = "ION_WIRELESS";
	public final Set<FloorPlanObjectInfo> floorPlanObjectInfoList = new TreeSet<FloorPlanObjectInfo>(comparator);
	private static final int DEFAULT_SHIFT = 10;
	private boolean newPicture = false;
	private final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private float floorPlanWidth = 1;
	private float lastX = 0;
	private float lastY = 0;
	private int floorPlanImageWidth = 1;
	private boolean touchStart = false;
	private OnFloorPlanObjectClickListener onFloorPlanObjectClickListener;
	private Bitmap rack_gray, rack_red, rack_red_open, rack_yellow, rack_yellow_open, rack_green, rack_green_open;
	private Bitmap crac_gray, crac_red, crac_yellow, crac_green;
	private Bitmap pressure_gray, pressure_red, pressure_yellow, pressure_green;
	private Bitmap door_gray, door_red, door_red_open, door_yellow, door_yellow_open, door_green, door_green_open;
	private Bitmap generic_temp_gray, generic_temp_red, generic_temp_yellow, generic_temp_green;
	private Bitmap leak_gray, leak_red, leak_yellow, leak_green;
	private Bitmap equipment_status_gray, equipment_status_red, equipment_status_yellow, equipment_status_green;
	private Bitmap vertical_temp_gray, vertical_temp_red, vertical_temp_yellow, vertical_temp_green;
	private Bitmap energy_gray, energy_red, energy_yellow, energy_green;
	private final Rect bounds = new Rect();
	private static float floorPlanScale = 0;
	public static Point scrollXY = new Point();
	private ScaleGestureDetector scaleDetector;
	private FloorPlanObjectClickListenerDelay floorPlanObjectClickListenerDelay;
	private static final Comparator<FloorPlanObjectInfo> comparator = new Comparator<FloorPlanObjectInfo>() {
		
	    public int compare(FloorPlanObjectInfo treeObjectInfo1, FloorPlanObjectInfo treeObjectInfo2) {
	    	if(treeObjectInfo1 == null) {
	    		return treeObjectInfo2 == null ? 0 : 1;
	    	} else {
	    		return treeObjectInfo2 == null ? 1 : (treeObjectInfo1.y > treeObjectInfo2.y ? 1 : -1);
	    	}
	    }
	};

	public FloorPlanView(Context context) {
		super(context);
		init();
	}

	public FloorPlanView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public FloorPlanView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {
		scaleDetector = new ScaleGestureDetector(getContext(), new ScaleListener());
		this.setPictureListener(new FloorPlanImageListener());
		getSettings().setSupportZoom(true);
		getSettings().setBuiltInZoomControls(true);
		getSettings().setUseWideViewPort(true);
		getSettings().setLoadWithOverviewMode(true);
		getSettings().setDefaultZoom(ZoomDensity.FAR);
		rack_gray = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.rack_gray);
		rack_red = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.rack_red);
		rack_red_open = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.rack_red_open);
		rack_yellow = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.rack_yellow);
		rack_yellow_open = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.rack_yellow_open);
		rack_green = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.rack_green);
		rack_green_open = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.rack_green_open);
		crac_gray = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.crac_gray);
		crac_red = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.crac_red);
		crac_yellow = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.crac_yellow);
		crac_green = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.crac_green);
		pressure_gray = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.pressure_gray);
		pressure_red = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.pressure_red);
		pressure_yellow = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.pressure_yellow);
		pressure_green = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.pressure_green);
		door_gray = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.door_gray);
		door_red = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.door_red);
		door_red_open = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.door_red_open);
		door_yellow = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.door_yellow);
		door_yellow_open = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.door_yellow_open);
		door_green = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.door_green);
		door_green_open = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.door_green_open);
		generic_temp_gray = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.generic_temp_gray);
		generic_temp_red = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.generic_temp_red);
		generic_temp_yellow = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.generic_temp_yellow);
		generic_temp_green = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.generic_temp_green);
		leak_gray = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.leak_gray);
		leak_red = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.leak_red);
		leak_yellow = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.leak_yellow);
		leak_green = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.leak_green);
		equipment_status_gray =
			BitmapFactory.decodeResource(getContext().getResources(), R.drawable.equipment_status_gray);
		equipment_status_red =
			BitmapFactory.decodeResource(getContext().getResources(), R.drawable.equipment_status_red);
		equipment_status_yellow =
			BitmapFactory.decodeResource(getContext().getResources(), R.drawable.equipment_status_yellow);
		equipment_status_green =
			BitmapFactory.decodeResource(getContext().getResources(), R.drawable.equipment_status_green);
		vertical_temp_gray = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.vertical_temp_gray);
		vertical_temp_red = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.vertical_temp_red);
		vertical_temp_yellow =
			BitmapFactory.decodeResource(getContext().getResources(), R.drawable.vertical_temp_yellow);
		vertical_temp_green = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.vertical_temp_green);
		energy_gray = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.energy_gray);
		energy_red = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.energy_red);
		energy_yellow = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.energy_yellow);
		energy_green = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.energy_green);
	}

	public OnFloorPlanObjectClickListener getOnFloorPlanObjectClickListener() {
		return onFloorPlanObjectClickListener;
	}

	public void setOnFloorPlanObjectClickListener(OnFloorPlanObjectClickListener listener) {
		onFloorPlanObjectClickListener = listener;
	}

	public int getFloorPlanImageWidth() {
		return floorPlanImageWidth;
	}

	public void setFloorPlanImageWidth(int newFloorPlanImageWidth) {
		floorPlanImageWidth = newFloorPlanImageWidth;
	}

	public float getFloorPlanWidth() {
		return floorPlanWidth;
	}

	public void setFloorPlanWidth(float newFloorPlanWidth) {
		this.floorPlanWidth = newFloorPlanWidth;
	}
	
	public void setNewPicture(boolean newPicture) {
		this.newPicture = newPicture;
	}

	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (!isInEditMode()) {
			canvas.getClipBounds(bounds);
			float k = getScale() * floorPlanImageWidth / floorPlanWidth;
			for (FloorPlanObjectInfo info : floorPlanObjectInfoList) {
				if (info != null) {
					float x = k * info.x - ((float) info.icon.getWidth()) / 2;
					float y = k * info.y - ((float) info.icon.getHeight()) / 2;
					canvas.drawBitmap(info.icon, x, y, paint);
					info.xOffset = x - bounds.left;
					info.yOffset = y - bounds.top;
				}
			}
		}
	}

	public boolean onTouchEvent(MotionEvent event) {
		scaleDetector.onTouchEvent(event);
		boolean consumed = super.onTouchEvent(event);
		if (onFloorPlanObjectClickListener != null) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				lastX = event.getX();
				lastY = event.getY();
				touchStart = true;
				if (floorPlanObjectClickListenerDelay != null && floorPlanObjectClickListenerDelay.notStarted) {
					floorPlanObjectClickListenerDelay.notStarted = false;
				}
			} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
				float x = event.getX();
				float y = event.getY();
				if (Math.abs(lastX - x) > DEFAULT_SHIFT || Math.abs(lastY - y) > DEFAULT_SHIFT) {
					touchStart = false;
				}
				updateScrollXY();
			} else if (touchStart && event.getAction() == MotionEvent.ACTION_UP) {
				float size = Math.max(event.getTouchMajor(), event.getToolMajor()) / 2;
				float x = event.getX();
				float y = event.getY();
				for (FloorPlanObjectInfo info : floorPlanObjectInfoList) {
					int width = info.icon.getWidth();
					int heigth = info.icon.getHeight();
					if ((size < width || size < heigth) && x >= info.xOffset && x <= info.xOffset + width &&
							y >= info.yOffset && y <= info.yOffset + heigth) {
						floorPlanObjectClickListenerDelay = new FloorPlanObjectClickListenerDelay(info);
						floorPlanObjectClickListenerDelay.start();
					} else if (info.xOffset >= x - size && info.xOffset + width <= x + size &&
							info.yOffset >= y - size && info.yOffset + heigth <= y + size) {
						floorPlanObjectClickListenerDelay = new FloorPlanObjectClickListenerDelay(info);
						floorPlanObjectClickListenerDelay.start();
					}
				}
			}
		}
		return consumed;
	}
	
	public void updateFloorPlanScale() {
		floorPlanScale = getScale();		
	}
	
	public void updateScrollXY() {
		scrollXY.x = getScrollX();
		scrollXY.y = getScrollY();
	}
	
	public float getFloorPlanScale() {
		return floorPlanScale;
	}
	
	public void setFloorPlanScale(float scale) {
		floorPlanScale = scale;
	}

	public FloorPlanObjectInfo createFloorPlanObjectInfo(String id, String type, Number status, Number alertCount,
			Number x, Number y, boolean openDoor) {
		return new FloorPlanObjectInfo(id, type, status, alertCount, x, y, openDoor);
	}

	// ESCA-JAVA0007:
	public class FloorPlanObjectInfo {

		public final String id;
		public final String type;
		public final int status;
		public final int alertCount;
		public final float x;
		public final float y;
		public final Bitmap icon;
		public final boolean openDoor;
		public float xOffset;
		public float yOffset;

		private FloorPlanObjectInfo(String id, String type, Number status, Number alertCount, Number x,
				Number y, boolean openDoor) {
			if (id == null) {
				throw new IllegalArgumentException("Wrong id");
			}
			this.id = id;
			this.type = type;
			this.status = status == null ? 0 : status.intValue();
			this.alertCount = alertCount == null ? 0 : alertCount.intValue();
			this.x = x == null ? 0 : x.floatValue();
			this.y = y == null ? 0 : y.floatValue();
			this.openDoor = openDoor;
			if (RACK_TYPE.equals(this.type)) {
				if (this.status == ObjectStatus.DISABLED) {
					icon = rack_gray;
				} else if (this.alertCount > 0) {
					icon = openDoor ? rack_red_open : rack_red;
				} else if (this.status == ObjectStatus.NOT_RECEIVING_DATA) {
					icon = openDoor ? rack_yellow_open : rack_yellow;
				} else {
					icon = openDoor ? rack_green_open : rack_green;
				}
			} else if (CRAC_TYPE.equals(this.type)) {
				if (this.status == ObjectStatus.DISABLED) {
					icon = crac_gray;
				} else if (this.alertCount > 0) {
					icon = crac_red;
				} else if (this.status == ObjectStatus.NOT_RECEIVING_DATA) {
					icon = crac_yellow;
				} else {
					icon = crac_green;
				}
			} else if (PRESSURE_TYPE.equals(this.type)) {
				if (this.status == ObjectStatus.DISABLED) {
					icon = pressure_gray;
				} else if (this.alertCount > 0) {
					icon = pressure_red;
				} else if (this.status == ObjectStatus.NOT_RECEIVING_DATA) {
					icon = pressure_yellow;
				} else {
					icon = pressure_green;
				}
			} else if (DOOR_TYPE.equals(this.type)) {
				if (this.status == ObjectStatus.DISABLED) {
					icon = door_gray;
				} else if (this.alertCount > 0) {
					icon = openDoor ? door_red_open : door_red;
				} else if (this.status == ObjectStatus.NOT_RECEIVING_DATA) {
					icon = openDoor ? door_yellow_open : door_yellow;
				} else {
					icon = openDoor ? door_green_open : door_green;
				}
			} else if (GENERIC_TEMPERATURE_TYPE.equals(this.type)) {
				if (this.status == ObjectStatus.DISABLED) {
					icon = generic_temp_gray;
				} else if (this.alertCount > 0) {
					icon = generic_temp_red;
				} else if (this.status == ObjectStatus.NOT_RECEIVING_DATA) {
					icon = generic_temp_yellow;
				} else {
					icon = generic_temp_green;
				}
			} else if (LEAK_TYPE.equals(this.type)) {
				if (this.status == ObjectStatus.DISABLED) {
					icon = leak_gray;
				} else if (this.alertCount > 0) {
					icon = leak_red;
				} else if (this.status == ObjectStatus.NOT_RECEIVING_DATA) {
					icon = leak_yellow;
				} else {
					icon = leak_green;
				}
			} else if (EQUIPMENT_STATUS_TYPE.equals(this.type)) {
				if (this.status == ObjectStatus.DISABLED) {
					icon = equipment_status_gray;
				} else if (this.alertCount > 0) {
					icon = equipment_status_red;
				} else if (this.status == ObjectStatus.NOT_RECEIVING_DATA) {
					icon = equipment_status_yellow;
				} else {
					icon = equipment_status_green;
				}
			} else if (VERTICAL_TEMPERATURE_TYPE.equals(this.type)) {
				if (this.status == ObjectStatus.DISABLED) {
					icon = vertical_temp_gray;
				} else if (this.alertCount > 0) {
					icon = vertical_temp_red;
				} else if (this.status == ObjectStatus.NOT_RECEIVING_DATA) {
					icon = vertical_temp_yellow;
				} else {
					icon = vertical_temp_green;
				}
			} else if (ION_DELTA_TYPE.equals(this.type) || ION_WYE_TYPE.equals(this.type) ||
				ION_WIRELESS_TYPE.equals(this.type)) {
				if (this.status == ObjectStatus.DISABLED) {
					icon = energy_gray;
				} else if (this.alertCount > 0) {
					icon = energy_red;
				} else if (this.status == ObjectStatus.NOT_RECEIVING_DATA) {
					icon = energy_yellow;
				} else {
					icon = energy_green;
				}
			} else {
				throw new IllegalArgumentException("Unsupported object type: " + type);
			}
		}
	}

	private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

		public boolean onScale(ScaleGestureDetector detector) {
        	updateFloorPlanScale();
            return true;
        }
    }

	private class FloorPlanImageListener implements PictureListener {

	    public void onNewPicture(WebView view, Picture arg1) {
	    	// Temporary hack to avoid extra calls on scrolling and scaling.
	    	// To be removed when Google fix the internal issues (now the method is deprecated)
	    	if (newPicture) {
	    		scrollTo(scrollXY.x, scrollXY.y);
	    		newPicture = false;
	    	}
	    }
	} 

	private class FloorPlanObjectClickListenerDelay extends Thread {

		private final FloorPlanObjectInfo info;
		private boolean notStarted = true;

		private FloorPlanObjectClickListenerDelay(FloorPlanObjectInfo info) {
			this.info = info;
		}

		public void run() {
			synchronized (this) {
				try {
					wait(1000);
					if (notStarted && info != null) {
						notStarted = false;
						onFloorPlanObjectClickListener.onFloorPlanObjectClick(info);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public interface OnFloorPlanObjectClickListener {
		void onFloorPlanObjectClick(FloorPlanObjectInfo floorPlanObjectInfo);
	}
}
