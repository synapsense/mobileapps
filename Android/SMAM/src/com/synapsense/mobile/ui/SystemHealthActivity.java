package com.synapsense.mobile.ui;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.synapsense.mobile.R;
import com.synapsense.mobile.service.AlertService;
import com.synapsense.mobile.service.DefaultSystemStatusManager;
import com.synapsense.mobile.status.ComponentStatus;
import com.synapsense.mobile.status.Components;
import com.synapsense.mobile.status.Status;

public class SystemHealthActivity extends BaseActivity implements DialogInterface.OnDismissListener,
	DialogInterface.OnClickListener, OnClickListener {

	private static final int START_DATA_REFRESH = 0;
	private static final int STOP_DATA_REFRESH = 1;
	private final UpdateReceiver updateReceiver = new UpdateReceiver();
	private final NewAlertsReceiver newAlertsReceiver = new NewAlertsReceiver();
	private ViewGroup activeControlLayout;
	private TextView hostTextField;
	private TextView deviceManagerTextField;
	private TextView imagingServerTextField;
	private TextView environmentalServerTextField;
	private TextView activeControlTextField;
	private AlertDialog newAlertsDialog;
	private AlertDialog errorDialog;
	private AlertDialog detailsDialog;
	private AlertDialog quitDialog;
	private ComponentStatus[] componentStatuses;
	private final Handler handler = new Handler() {

		public void handleMessage(Message msg) {
			if (isFinishing()) {
				return;
			}
			if (msg.what == START_DATA_REFRESH) {
				showProgressBar();
			} else if (msg.what == STOP_DATA_REFRESH) {
				if (componentStatuses != null) {
					hostTextField.setText(PreferenceManager.getDefaultSharedPreferences(getApplicationContext())
						.getString("serverName", getString(R.string.notAvailableMessage)));
					activeControlLayout.setVisibility(View.GONE);
					for (ComponentStatus componentStatus : componentStatuses) {
						final String message = componentStatus.getMessage();
						if (componentStatus.getComponent() == Components.DEVICE_MANAGER) {
							if (message != null && message.trim().length() > 0) {
								deviceManagerTextField.setTextColor(getResources().getColor(R.color.blue));
								SpannableString content =
									new SpannableString(componentStatusToString(componentStatus.getStatus()));
								content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
								deviceManagerTextField.setText(content);
								deviceManagerTextField.setOnClickListener(SystemHealthActivity.this);
							} else {
								deviceManagerTextField.setTextColor(getResources().getColor(R.color.disableLink));
								deviceManagerTextField.setText(componentStatusToString(componentStatus.getStatus()));
								deviceManagerTextField.setOnClickListener(null);
							}
						} else if (componentStatus.getComponent() == Components.IMAGING_SERVER) {
							if (message != null && message.trim().length() > 0) {
								imagingServerTextField.setTextColor(getResources().getColor(R.color.blue));
								SpannableString content =
									new SpannableString(componentStatusToString(componentStatus.getStatus()));
								content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
								imagingServerTextField.setText(content);
								imagingServerTextField.setOnClickListener(SystemHealthActivity.this);
							} else {
								imagingServerTextField.setTextColor(getResources().getColor(R.color.disableLink));
								imagingServerTextField.setText(componentStatusToString(componentStatus.getStatus()));
								imagingServerTextField.setOnClickListener(null);
							}
						} else if (componentStatus.getComponent() == Components.ENVIRONMENTAL_SERVER) {
							if (message != null && message.trim().length() > 0) {
								environmentalServerTextField.setTextColor(getResources().getColor(R.color.blue));
								SpannableString content =
									new SpannableString(componentStatusToString(componentStatus.getStatus()));
								content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
								environmentalServerTextField.setText(content);
								environmentalServerTextField.setOnClickListener(SystemHealthActivity.this);
							} else {
								environmentalServerTextField.setTextColor(getResources().getColor(R.color.disableLink));
								environmentalServerTextField.setText(componentStatusToString(componentStatus.getStatus()));
								environmentalServerTextField.setOnClickListener(null);
							}
						} else if (componentStatus.getComponent() == Components.CONTROL_PROCESS) {
							if (message != null && message.trim().length() > 0) {
								activeControlTextField.setTextColor(getResources().getColor(R.color.blue));
								SpannableString content =
									new SpannableString(componentStatusToString(componentStatus.getStatus()));
								content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
								activeControlTextField.setText(content);
								activeControlTextField.setOnClickListener(SystemHealthActivity.this);
							} else {
								activeControlTextField.setTextColor(getResources().getColor(R.color.disableLink));
								activeControlTextField.setText(componentStatusToString(componentStatus.getStatus()));
								activeControlTextField.setOnClickListener(null);
							}
							activeControlLayout.setVisibility(View.VISIBLE);
						}
					}
					hideProgressBar();
				} else {
					hideProgressBar();
					errorDialog.show();
				}
			}
		}
	};

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inflateContent(R.layout.system_health);
		contentView = (ViewGroup) findViewById(R.id.systemHealthLayout);
		activeControlLayout = (ViewGroup) findViewById(R.id.systemHealth_activeControlLayout);
		hostTextField = (TextView) findViewById(R.id.systemHealth_hostTextField);
		deviceManagerTextField = (TextView) findViewById(R.id.systemHealth_deviceManagerTextField);
		imagingServerTextField = (TextView) findViewById(R.id.systemHealth_imagingServerTextField);
		environmentalServerTextField = (TextView) findViewById(R.id.systemHealth_environmentalServerTextField);
		activeControlTextField = (TextView) findViewById(R.id.systemHealth_activeControlTextField);
		errorDialog = new AlertDialog.Builder(this).setTitle(R.string.communicationError).create();
		newAlertsDialog = new AlertDialog.Builder(this).create();
		newAlertsDialog.setOnDismissListener(this);
		detailsDialog = new AlertDialog.Builder(this).create();
		quitDialog = new AlertDialog.Builder(this).setTitle(R.string.quitTitle).create();
		quitDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.quitButton), this);
		quitDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.logOutButton), this);
		quitDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancelButton), this);
	}

	public void onResume() {
		super.onResume();
		registerReceiver(updateReceiver, new IntentFilter(AlertService.UPDATE_ACTION));
		registerReceiver(newAlertsReceiver, new IntentFilter(AlertService.NEW_ALERTS_ACTION));
		Thread runner = new Thread() {

			public void run() {
				if (!isFinishing()) {
					refresh(true);
				}
			}
		};
		runner.start();
	}

	public void onPause() {
		super.onPause();
		detailsDialog.dismiss();
		newAlertsDialog.dismiss();
		errorDialog.dismiss();
		quitDialog.dismiss();
		unregisterReceiver(updateReceiver);
		unregisterReceiver(newAlertsReceiver);
	}

	public void onBackPressed() {
		quitDialog.show();
	}

	private void refresh(boolean showProgressDialog) {
		Message msg = handler.obtainMessage();
		if (showProgressDialog) {
			msg.what = START_DATA_REFRESH;
			handler.sendMessage(msg);
		}
		componentStatuses = DefaultSystemStatusManager.instance.getEnvironmentalServerState();
		msg = handler.obtainMessage();
		msg.what = STOP_DATA_REFRESH;
		handler.sendMessage(msg);
	}

	private String componentStatusToString(int status) {
		if (status == Status.RUNNING) {
			return getString(R.string.runningComponentStatus);
		} else if (status == Status.STOPPED) {
			return getString(R.string.stoppedComponentStatus);
		} else if (status == Status.PARTIAL) {
			return getString(R.string.partialComponentStatus);
		} else if (status == Status.UNKNOWN) {
			return getString(R.string.unknownComponentStatus);
		}
		return "";
	}

	private void showDetailsDialog(String title, String message) {
		detailsDialog.setTitle(title);
		detailsDialog.setMessage(message);
		detailsDialog.show();
	}

	public void onDismiss(DialogInterface dialog) {
		Thread runner = new Thread() {

			public void run() {
				refresh(true);
			}
		};
		runner.start();
	}

	public void onClick(DialogInterface dialog, int which) {
		if (dialog == quitDialog) {
			if (which == DialogInterface.BUTTON_POSITIVE) {
				// quit
				if (getParent() != null) {
					getParent().setResult(RESULT_OK);
				} else {
					setResult(RESULT_OK);
				}
				finish();
			} else if (which == DialogInterface.BUTTON_NEUTRAL) {
				// log out
				finish();
			} else {
				// cancel
				quitDialog.dismiss();
			}
		}
	}

	public void onClick(View view) {
		for (ComponentStatus componentStatus : componentStatuses) {
			String message = componentStatus.getMessage();
			if (message != null && message.trim().length() > 0) {
				if (view == deviceManagerTextField && componentStatus.getComponent() == Components.DEVICE_MANAGER) {
					showDetailsDialog(getString(R.string.deviceManagerLabel), message.trim());
				} else if (view == imagingServerTextField &&
						componentStatus.getComponent() == Components.IMAGING_SERVER) {
					showDetailsDialog(getString(R.string.imagingServerLabel), message.trim());
				} else if (view == environmentalServerTextField &&
						componentStatus.getComponent() == Components.ENVIRONMENTAL_SERVER) {
					showDetailsDialog(getString(R.string.environmentServerLabel), message.trim());
				} else if (view == activeControlTextField &&
						componentStatus.getComponent() == Components.CONTROL_PROCESS) {
					showDetailsDialog(getString(R.string.activeControlLabel), message.trim());
				}
			}
		}
	}

	private class UpdateReceiver extends BroadcastReceiver {

		public void onReceive(Context context, Intent intent) {
			Thread runner = new Thread() {

				public void run() {
					refresh(false);
				}
			};
			runner.start();
		}
	}

	private class NewAlertsReceiver extends BroadcastReceiver {

		public void onReceive(Context context, final Intent intent) {
			newAlertsDialog.setTitle(intent.getStringExtra("message"));
			newAlertsDialog.setIcon(intent.getIntExtra("iconId", R.drawable.major_small));
			newAlertsDialog.show();
			Ringtone ringtone = RingtoneManager.getRingtone(SystemHealthActivity.this,
				RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
			if (ringtone != null) {
				ringtone.play();
			}
		}
	}
}
