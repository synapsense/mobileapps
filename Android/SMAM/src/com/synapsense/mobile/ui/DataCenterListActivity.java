package com.synapsense.mobile.ui;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.synapsense.mobile.R;
import com.synapsense.mobile.interaction.ObjectRequest;
import com.synapsense.mobile.interaction.ResponseElement;
import com.synapsense.mobile.service.AlertService;
import com.synapsense.mobile.service.DefaultObjectManager;
import com.synapsense.mobile.service.ObjectUtils;
import com.synapsense.mobile.ui.DataCenterListAdapter.DataCenterInfo;

public class DataCenterListActivity extends BaseActivity implements AdapterView.OnItemClickListener,
		DialogInterface.OnDismissListener, DialogInterface.OnClickListener,OnMarkerClickListener {

	private static final int GRID_VIEW = 0;
	private static final int MAP_VIEW = 1;
	private static final int START_DATA_REFRESH = 0;
	private static final int STOP_DATA_REFRESH = 1;
	private static final int ERROR = 2;
	private final NewAlertsReceiver newAlertsReceiver = new NewAlertsReceiver();
	private final UpdateReceiver siteListUpdateReceiver = new UpdateReceiver();
	private Map<Marker, DataCenterInfo> markers = new HashMap<Marker, DataCenterInfo>();
	private ListView dataCentersListView;
	private MapView dataCentersMapView;
	private GoogleMap map;
	private DataCenterListAdapter siteListAdapter;
	private AlertDialog errorDialog;
	private AlertDialog newAlertsDialog;
	private AlertDialog quitDialog;
	private DataCenterInfo[] infos = null;
	private CameraPosition initCameraPosition = null;
	private MenuItem switchViewMenuItem;
	private int viewType = GRID_VIEW;
	private boolean firstTime = true;
	private final ObjectRequest zoneRequest = new ObjectRequest();
	private final ObjectRequest dcRequest = new ObjectRequest();
	private final Handler handler = new Handler() {

		public void handleMessage(Message msg) {
			if (isFinishing()) {
				return;
			}
			if (msg.what == START_DATA_REFRESH) {
				showProgressBar();
			} else if (msg.what == STOP_DATA_REFRESH) {
				siteListAdapter.setTreeObjectInfos(infos);
				for(Marker m :markers.keySet()){
					m.remove();
				}
				markers.clear();
				if(map != null){
					for (DataCenterInfo info : infos) {
						if (info.latitude != null && info.longitude != null) {
							Marker m = map.addMarker(new MarkerOptions().position(new LatLng(info.latitude, info.longitude))
									.title(info.name).snippet(info.description).icon(BitmapDescriptorFactory.fromResource(info.iconId))); 
							markers.put(m, info);
						}
					}
					if (firstTime && infos.length > 0) {
						firstTime = false;
						if (initCameraPosition != null) {
							 map.animateCamera(CameraUpdateFactory.newCameraPosition(initCameraPosition));
						} else {	
							LatLng newLatLang = new LatLng(infos[0].latitude == null ? 45.773924 : infos[0].latitude, infos[0].longitude == null ? -98.709294 : infos[0].longitude);
							CameraUpdate newLocation = CameraUpdateFactory.newLatLng(newLatLang);
							map.animateCamera(newLocation);
						}
					}
					hideProgressBar();
				} else if (msg.what == ERROR) {
					siteListAdapter.setTreeObjectInfos(null);
					hideProgressBar();
					errorDialog.show();
					
				}
			}
		}
	};

	{
		zoneRequest.setType("ZONE");
		zoneRequest.setRequestChildrenNumberOnly(true);
		zoneRequest.setRequestDirectChildrenOnly(false);
		dcRequest.setType("DC");
		dcRequest.setProperties(new String []{"name", "status", "numAlerts", "latitude", "longitude"});
		dcRequest.setChildrenRequests(new ObjectRequest[]{zoneRequest});
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inflateContent(R.layout.data_centers);
		siteListAdapter = new DataCenterListAdapter(this);
		dataCentersListView = (ListView) contentView.findViewById(R.id.dataCenters_listView);
		dataCentersListView.setAdapter(siteListAdapter);
		dataCentersListView.setOnItemClickListener(this);
		dataCentersListView.setDivider(null);

		dataCentersMapView = (MapView) contentView.findViewById(R.id.dataCenters_mapView);
		dataCentersMapView.onCreate(savedInstanceState);
		errorDialog = new AlertDialog.Builder(this).setTitle(R.string.communicationError).create();
		newAlertsDialog = new AlertDialog.Builder(this).create();
		newAlertsDialog.setOnDismissListener(this);
        quitDialog = new AlertDialog.Builder(this).setTitle(R.string.quitTitle).create();
		quitDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.quitButton), this);
		quitDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.logOutButton), this);
		quitDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancelButton), this);
	}

	public void onResume() {
		super.onResume();
		dataCentersMapView.onResume();
		setUpMapIfNeeded();
		registerReceiver(siteListUpdateReceiver, new IntentFilter(AlertService.UPDATE_ACTION));
		registerReceiver(newAlertsReceiver, new IntentFilter(AlertService.NEW_ALERTS_ACTION));
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		setViewType(prefs.getInt("faciliriesViewType", GRID_VIEW));
		if (prefs.contains("latitude") && prefs.contains("longitude")) {
			initCameraPosition = CameraPosition.fromLatLngZoom(new LatLng(prefs.getFloat("latitude", 0), prefs.getFloat("longitude", 0)),prefs.getFloat("zoom", 8));
		}
		Thread runner = new Thread() {

			public void run() {
				if (!isFinishing()) {
					refresh(true);
				}
			}
		};
		runner.start();
	}
	private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (map == null) {
        	int checkGooglePlayServices =    GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
            if (checkGooglePlayServices != ConnectionResult.SUCCESS) {
            // google play services is missing!!!!
            /* Returns status code indicating whether there was an error. 
            Can be one of following in ConnectionResult: SUCCESS, SERVICE_MISSING, SERVICE_VERSION_UPDATE_REQUIRED, SERVICE_DISABLED, SERVICE_INVALID.
            */
               GooglePlayServicesUtil.getErrorDialog(checkGooglePlayServices, this, 1122).show();
            }
            else{
            	map = dataCentersMapView.getMap();
            	try {
            		MapsInitializer.initialize(this);
            	} catch (GooglePlayServicesNotAvailableException e) {
            		e.printStackTrace();
        	 }
        	if(map != null){
        		 map.setOnMarkerClickListener(this);
        	}
            }
        }
    }
	

	public void onPause() {
		super.onPause();
		dataCentersMapView.onPause();
		newAlertsDialog.dismiss();
		errorDialog.dismiss();
		quitDialog.dismiss();
		unregisterReceiver(siteListUpdateReceiver);
		unregisterReceiver(newAlertsReceiver);
		SharedPreferences.Editor prefsEditor =
			PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
		prefsEditor.putInt("faciliriesViewType", viewType);
		if (!firstTime && map != null) {
			CameraPosition cameraPosition = map.getCameraPosition();
			prefsEditor.putFloat("latitude", (float)cameraPosition.target.latitude);
			prefsEditor.putFloat("longitude", (float)cameraPosition.target.longitude);
			prefsEditor.putFloat("zoom", (float)cameraPosition.zoom);
		}
		prefsEditor.commit();
	}

	public void onBackPressed() {
		quitDialog.show();
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		dataCentersMapView.onDestroy();
	}
	@Override
	 protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		dataCentersMapView.onSaveInstanceState(outState);
	}
	@Override
	public void onLowMemory() {
		super.onLowMemory();
		dataCentersMapView.onLowMemory();
	}
	

	public boolean onCreateOptionsMenu(Menu menu) {
		switchViewMenuItem = menu.add(Menu.NONE, Menu.FIRST, Menu.NONE,
			getString(viewType == MAP_VIEW ? R.string.listView : R.string.mapView));
		switchViewMenuItem.setIcon(viewType == MAP_VIEW ? R.drawable.list_view : R.drawable.map_view);
		return super.onCreateOptionsMenu(menu);
	}

	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		if (item.getItemId() == Menu.FIRST) {
			if (viewType == GRID_VIEW) {
				setViewType(MAP_VIEW);
				switchViewMenuItem.setTitle(R.string.listView);
				switchViewMenuItem.setIcon(R.drawable.list_view);
			} else {
				setViewType(GRID_VIEW);
				switchViewMenuItem.setTitle(R.string.mapView);
				switchViewMenuItem.setIcon(R.drawable.map_view);
			}
			return true;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	private void setViewType(int newViewType) {
		viewType = newViewType;
		if (viewType == GRID_VIEW) {
			dataCentersMapView.setVisibility(View.GONE);
			dataCentersListView.setVisibility(View.VISIBLE);
		} else {
			dataCentersListView.setVisibility(View.GONE);
			dataCentersMapView.setVisibility(View.VISIBLE);
		}
	}
	
	private void refresh(boolean showProgressDialog) {
		
		Message msg = handler.obtainMessage();
		if (showProgressDialog) {
			msg.what = START_DATA_REFRESH;
			handler.sendMessage(msg);
		}
		ResponseElement response = DefaultObjectManager.instance.getObjectData(dcRequest);
		if (response != null) {
			List<ResponseElement> dcList = ObjectUtils.getObjects(response, "DC");
			infos = new DataCenterInfo[dcList.size()];
			for (int i = 0; i < infos.length; ++i) {
				ResponseElement dcResponse = dcList.get(i);
				int zoneCount = ObjectUtils.getChildrenNumber(dcResponse, "ZONE");
				String description = getResources().getQuantityString(R.plurals.zoneCountMessage, zoneCount, zoneCount);
				String id = String.valueOf(ObjectUtils.getAttr(dcResponse, "id"));
				String name =
					String.valueOf(ObjectUtils.getAttr(ObjectUtils.getObjectProperty(dcResponse, "name"), "v"));
				int status = (Integer) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(dcResponse, "status"), "v");
				int numAlerts =
					(Integer) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(dcResponse, "numAlerts"), "v");
				int iconId = DefaultObjectManager.getDataCenterResourceId(numAlerts, status);
				Double latitude =
					(Double) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(dcResponse, "latitude"), "v");
				Double longitude =
					(Double) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(dcResponse, "longitude"), "v");
				infos[i] = new DataCenterInfo(id, name, description, iconId, latitude, longitude, numAlerts, status);
				
			}
			Arrays.sort(infos);
			
			msg = handler.obtainMessage();
			msg.what = STOP_DATA_REFRESH;
			handler.sendMessage(msg);
		} else {
			msg = handler.obtainMessage();
			msg.what = ERROR;
			handler.sendMessage(msg);
		}
	}

	private void startDataCenterTabActivity(String objectId, String objectName) {
		Intent intent = new Intent(DataCenterListActivity.this, DataCenterTabActivity.class);
		intent.putExtra("objectId", objectId);
		intent.putExtra("objectName", objectName);
		startActivity(intent);
	}

	public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
		startDataCenterTabActivity(infos[position].objectId, infos[position].name);
	}
	@Override
	public boolean onMarkerClick(final Marker marker) {
	DataCenterInfo info = markers.get(marker);
	if(info != null){
		startDataCenterTabActivity(info.objectId, info.name);
		}
	return true;
	}
	public void onDismiss(DialogInterface dialog) {
		Thread runner = new Thread() {

			public void run() {
				refresh(true);
			}
		};
		runner.start();
	}

	public void onClick(DialogInterface dialog, int which) {
		if (dialog == quitDialog) {
			if (which == DialogInterface.BUTTON_POSITIVE) {
				// quit
				if (getParent() != null) {
					getParent().setResult(RESULT_OK);
				} else {
					setResult(RESULT_OK);
				}
				finish();
			} else if (which == DialogInterface.BUTTON_NEUTRAL) {
				// log out
				finish();
			} else {
				// cancel
				quitDialog.dismiss();
			}
		}
	}
	

	private class UpdateReceiver extends BroadcastReceiver {

		public void onReceive(Context context, Intent intent) {
			Thread runner = new Thread() {

				public void run() {
					refresh(false);
				}
			};
			runner.start();
		}
	}
	
	private class NewAlertsReceiver extends BroadcastReceiver {

		public void onReceive(Context context, final Intent intent) {
			newAlertsDialog.setTitle(intent.getStringExtra("message"));
			newAlertsDialog.setIcon(intent.getIntExtra("iconId", R.drawable.major_small));
			newAlertsDialog.show();
			Ringtone ringtone = RingtoneManager.getRingtone(DataCenterListActivity.this,
				RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
			if (ringtone != null) {
				ringtone.play();
			}
		}
	}
}
