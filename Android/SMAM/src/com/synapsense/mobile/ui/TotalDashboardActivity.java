package com.synapsense.mobile.ui;

import java.util.List;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.ViewGroup;
import android.widget.TextView;

import com.synapsense.mobile.R;
import com.synapsense.mobile.interaction.ObjectRequest;
import com.synapsense.mobile.interaction.ResponseElement;
import com.synapsense.mobile.service.AlertService;
import com.synapsense.mobile.service.DefaultAlertManager;
import com.synapsense.mobile.service.DefaultObjectManager;
import com.synapsense.mobile.service.DefaultSystemStatusManager;
import com.synapsense.mobile.service.ObjectUtils;
import com.synapsense.mobile.service.UserPreferencesUtils;

public class TotalDashboardActivity extends BaseActivity implements DialogInterface.OnDismissListener,
		DialogInterface.OnClickListener {

	private static final int START_DATA_REFRESH = 0;
	private static final int STOP_DATA_REFRESH = 1;
	private final UpdateReceiver updateReceiver = new UpdateReceiver();
	private final NewAlertsReceiver newAlertsReceiver = new NewAlertsReceiver();
	private TextView totalDataCentersTextField;
	private TextView totalAlertsTextField;
	private TextView totalWsnWarningsTextField;
	private TextView totalRacksTextField;
	private TextView totalCracsTextField;
	private TextView maxPueTextField;
	private TextView minPueTextField;
	private TextView totalSquareFootageTextField;
	private AlertDialog newAlertsDialog;
	private AlertDialog errorDialog;
	private AlertDialog quitDialog;
	private int totalDCs;
	private int totalRacks;
	private int totalCoolingUnits;
	private double minPue = -1;
	private double maxPue = -1;
	private double totoalSquareFootage = -1;
	private int alertWarnings;
	private int alertCount;
	private final ObjectRequest dcRequest = new ObjectRequest();
	private final ObjectRequest rackRequest = new ObjectRequest();
	private final ObjectRequest cracRequest = new ObjectRequest();
	private final ObjectRequest pueRequest = new ObjectRequest();
	private final ObjectRequest rootRequest = new ObjectRequest();
	private final Handler handler = new Handler() {

		public void handleMessage(Message msg) {
			if (isFinishing()) {
				return;
			}
			if (msg.what == START_DATA_REFRESH) {
				showProgressBar();
			} else if (msg.what == STOP_DATA_REFRESH) {
				if (msg.arg1 > 0) {
					hideProgressBar();
					errorDialog.show();
				} else {
					totalDataCentersTextField.setText(String.valueOf(totalDCs));
					totalAlertsTextField.setText(String.valueOf(alertCount));
					totalWsnWarningsTextField.setText(String.valueOf(alertWarnings));
					totalRacksTextField.setText(String.valueOf(totalRacks));
					totalCracsTextField.setText(String.valueOf(totalCoolingUnits));
					maxPueTextField.setText(maxPue < 0 ? getString(R.string.notAvailableMessage) :
						UserPreferencesUtils.formatNumber(maxPue));
					minPueTextField.setText(minPue < 0 ? getString(R.string.notAvailableMessage) :
						UserPreferencesUtils.formatNumber(minPue));
					totalSquareFootageTextField.setText(totoalSquareFootage < 0 ?
						getString(R.string.notAvailableMessage) :
						UserPreferencesUtils.formatSquare((long) totoalSquareFootage, TotalDashboardActivity.this));
					hideProgressBar();
				}
			}
		}
	};

	{
		dcRequest.setType("DC");
		dcRequest.setProperties(new String []{"area"});
		rackRequest.setType("RACK");
		rackRequest.setRequestChildrenNumberOnly(true);
		rackRequest.setRequestDirectChildrenOnly(false);
		cracRequest.setType("CRAC");
		cracRequest.setRequestChildrenNumberOnly(true);
		cracRequest.setRequestDirectChildrenOnly(false);
		pueRequest.setType("PUE");
		pueRequest.setRequestDirectChildrenOnly(false);
		pueRequest.setProperties(new String []{"pue"});
		rootRequest.setType("ROOT");
		rootRequest.setProperties(new String [0]);
		rootRequest.setChildrenRequests(new ObjectRequest[]{dcRequest, rackRequest, cracRequest, pueRequest});
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inflateContent(R.layout.total_dashboard);
		contentView = (ViewGroup) findViewById(R.id.totalDashboardLayout);
		totalDataCentersTextField = (TextView) findViewById(R.id.totalDashboard_totalDataCentersTextField);
		totalAlertsTextField = (TextView) findViewById(R.id.totalDashboard_totalAlertsTextField);
		totalWsnWarningsTextField = (TextView) findViewById(R.id.totalDashboard_totalWsnWarningsTextField);
		totalRacksTextField = (TextView) findViewById(R.id.totalDashboard_totalRacksTextField);
		totalCracsTextField = (TextView) findViewById(R.id.totalDashboard_totalCollingUnitsTextField);
		maxPueTextField = (TextView) findViewById(R.id.totalDashboard_maxPueTextField);
		minPueTextField = (TextView) findViewById(R.id.totalDashboard_minPueTextField);
		totalSquareFootageTextField = (TextView) findViewById(R.id.totalDashboard_totalSquareFootageTextField);
		
		errorDialog = new AlertDialog.Builder(this).setTitle(R.string.communicationError).create();
		newAlertsDialog = new AlertDialog.Builder(this).create();
		newAlertsDialog.setOnDismissListener(this);
		quitDialog = new AlertDialog.Builder(this).setTitle(R.string.quitTitle).create();
		quitDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.quitButton), this);
		quitDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.logOutButton), this);
		quitDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancelButton), this);
	}

	public void onResume() {
		super.onResume();
		registerReceiver(updateReceiver, new IntentFilter(AlertService.UPDATE_ACTION));
		registerReceiver(newAlertsReceiver, new IntentFilter(AlertService.NEW_ALERTS_ACTION));
		Thread runner = new Thread() {

			public void run() {
				if (!isFinishing()) {
					refresh(true);
				}
			}
		};
		runner.start();
	}

	public void onPause() {
		super.onPause();
		newAlertsDialog.dismiss();
		errorDialog.dismiss();
		quitDialog.dismiss();
		unregisterReceiver(updateReceiver);
		unregisterReceiver(newAlertsReceiver);
	}

	public void onBackPressed() {
		quitDialog.show();
	}

	private void refresh(boolean showProgressDialog) {
		Message msg = handler.obtainMessage();
		if (showProgressDialog) {
			msg.what = START_DATA_REFRESH;
			handler.sendMessage(msg);
		}
		msg = handler.obtainMessage();
		msg.what = STOP_DATA_REFRESH;
		alertWarnings = DefaultSystemStatusManager.instance.getNumWarnings();
		alertCount = DefaultAlertManager.instance.getTotalNumActiveAlerts();
		ResponseElement response = DefaultObjectManager.instance.getObjectData(rootRequest);
		if (alertWarnings >= 0 && alertCount >= 0 && response != null) {
			totalRacks = ObjectUtils.getChildrenNumber(response, "RACK");
			totalCoolingUnits = ObjectUtils.getChildrenNumber(response, "CRAC");
			for (ResponseElement pueResponse : ObjectUtils.getObjects(response, "PUE")) {
				Double pue = (Double) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(pueResponse, "pue"), "v");
				if (pue != null) {
					minPue = minPue < 0 ? pue : (minPue > pue ? pue : minPue);
					maxPue = maxPue < 0 ? pue : (maxPue < pue ? pue : maxPue);
				}
			}
			List<ResponseElement> dcList = ObjectUtils.getObjects(response, "DC");
			totalDCs = dcList.size();
			double value = -1;
			for (ResponseElement dcResponse : dcList) {
				Double squareFootage =
					(Double) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(dcResponse, "area"), "v");
				if (squareFootage != null) {
					value = value < 0 ? squareFootage : value + squareFootage;
				}
			}
			if (value > 0) {
				totoalSquareFootage = value;
			}
			msg.arg1 = 0;
		} else {
			msg.arg1 = 1;
		}
		handler.sendMessage(msg);
	}

	public void onDismiss(DialogInterface dialog) {
		Thread runner = new Thread() {

			public void run() {
				refresh(true);
			}
		};
		runner.start();
	}

	public void onClick(DialogInterface dialog, int which) {
		if (dialog == quitDialog) {
			if (which == DialogInterface.BUTTON_POSITIVE) {
				// quit
				if (getParent() != null) {
					getParent().setResult(RESULT_OK);
				} else {
					setResult(RESULT_OK);
				}
				finish();
			} else if (which == DialogInterface.BUTTON_NEUTRAL) {
				// log out
				finish();
			} else {
				// cancel
				quitDialog.dismiss();
			}
		}
	}

	private class UpdateReceiver extends BroadcastReceiver {

		public void onReceive(Context context, Intent intent) {
			Thread runner = new Thread() {

				public void run() {
					refresh(false);
				}
			};
			runner.start();
		}
	}

	private class NewAlertsReceiver extends BroadcastReceiver {

		public void onReceive(Context context, final Intent intent) {
			newAlertsDialog.setTitle(intent.getStringExtra("message"));
			newAlertsDialog.setIcon(intent.getIntExtra("iconId", R.drawable.major_small));
			newAlertsDialog.show();
			Ringtone ringtone = RingtoneManager.getRingtone(TotalDashboardActivity.this,
				RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
			if (ringtone != null) {
				ringtone.play();
			}
		}
	}
}
