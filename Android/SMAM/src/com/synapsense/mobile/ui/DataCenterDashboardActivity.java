package com.synapsense.mobile.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.synapsense.mobile.R;
import com.synapsense.mobile.interaction.ObjectRequest;
import com.synapsense.mobile.interaction.ResponseElement;
import com.synapsense.mobile.service.AlertService;
import com.synapsense.mobile.service.DefaultConfigManager;
import com.synapsense.mobile.service.DefaultObjectManager;
import com.synapsense.mobile.service.ObjectUtils;
import com.synapsense.mobile.service.UserPreferencesUtils;
import com.synapsense.mobile.view.PieChartView;
import com.synapsense.mobile.view.PieChartView.PieChartItem;

public class DataCenterDashboardActivity extends BaseActivity implements DialogInterface.OnDismissListener {

	private static final int START_DATA_REFRESH = 0;
	private static final int STOP_DATA_REFRESH = 1;
	private final UpdateReceiver updateReceiver = new UpdateReceiver();
	private final NewAlertsReceiver newAlertsReceiver = new NewAlertsReceiver();
	private TextView totalAlertsTextField;
	private TextView totalRacksTextField;
	private TextView totalCracsTextField;
	private TextView energyUsageTextField;
	private TextView pueTextField;
	private PieChartView pieChart;
	private ViewGroup pueLayout;
	private AlertDialog newAlertsDialog;
	private AlertDialog errorDialog;
	private String objectId;
	private int totalRacks = 0;
	private int totalCoolingUnits = 0;
	private Double energyUsage;
	private Double pue;
	private Double lightingPower;
	private Double powerLoss;
	private Double coolingPower;
	private Double itPower;
	private Double infrastructurePower;
	private String pueType;
	private boolean showPue;
	private double summaryPower = 1;
	private String objectName;
	private int alertCount = 0;
	private final ObjectRequest rackRequest = new ObjectRequest();
	private final ObjectRequest cracRequest = new ObjectRequest();
	private final ObjectRequest pueRequest = new ObjectRequest();
	private final ObjectRequest dcRequest = new ObjectRequest();
	private final Handler handler = new Handler() {

		private static final int NUMBER_100 = 100;
		private static final int IT_POWER_COLOR = 0xFFD32529;
		private static final int COOLING_POWER_COLOR = 0xFF1F3668;
		private static final int POWER_LOSS_COLOR = 0xFFF09902;
		private static final int LIGHTING_POWER_COLOR = 0xFF00793E;
		private static final int INFRASTRUVTURE_POWER_COLOR = 0xFF9595FF;
		private final List<PieChartItem> pieChartItems = new ArrayList<PieChartItem>();

		public void handleMessage(Message msg) {
			if (isFinishing()) {
				return;
			}
			if (msg.what == START_DATA_REFRESH) {
				if (objectId != null) {
					showProgressBar();
				} else {
					errorDialog.show();
				}
			} else if (msg.what == STOP_DATA_REFRESH) {
				if (msg.arg1 > 0) {
					hideProgressBar();
					errorDialog.show();
				} else {
					setTitle(getString(R.string.customTitle) + " - " + objectName);
					totalAlertsTextField.setText(String.valueOf(alertCount));
					totalRacksTextField.setText(totalRacks < 0 ? getString(R.string.notAvailableMessage) :
						String.valueOf(totalRacks));
					totalCracsTextField.setText(totalCoolingUnits < 0 ? getString(R.string.notAvailableMessage) :
						String.valueOf(totalCoolingUnits));
					if (energyUsage != null) {
						if (energyUsage < 1) {
							energyUsageTextField.setText(UserPreferencesUtils.formatEnergy(energyUsage,
								DataCenterDashboardActivity.this));
						} else if (energyUsage > 10000) {
							energyUsage /= 1000;
							energyUsageTextField.setText(UserPreferencesUtils.formatEnergy(energyUsage.longValue(),
								DataCenterDashboardActivity.this).replace("k", "M").replace('�', '�'));
						} else {
							energyUsageTextField.setText(UserPreferencesUtils.formatEnergy(energyUsage.longValue(),
								DataCenterDashboardActivity.this));
						}
					} else {
						energyUsageTextField.setText(getString(R.string.notAvailableMessage));
					}
					pueTextField.setText(pue == null ? getString(R.string.notAvailableMessage) :
						UserPreferencesUtils.formatNumber(pue));
					pueLayout.setVisibility(showPue ? View.VISIBLE : View.GONE);
					if ("full".equals(pueType)) {
						if (lightingPower != null || powerLoss != null || coolingPower != null || itPower != null) {
							pieChartItems.clear();
							if (itPower != null) {
								pieChartItems.add(new PieChartItem(itPower.floatValue(),
									getString(R.string.itLoadMessage) + " " +
									UserPreferencesUtils.formatPower(itPower.longValue(),
									DataCenterDashboardActivity.this) + " (" +
									Math.round(NUMBER_100 * itPower.floatValue() / summaryPower) + "%)",
									IT_POWER_COLOR));
							}
							if (coolingPower != null) {
								pieChartItems.add(new PieChartItem(coolingPower.floatValue(),
									getString(R.string.coolingLoadMessage) + " " +
									UserPreferencesUtils.formatPower(coolingPower.longValue(),
									DataCenterDashboardActivity.this) + " (" +
									Math.round(NUMBER_100 * coolingPower / summaryPower) + "%)",
									COOLING_POWER_COLOR));
							}
							if (powerLoss != null) {
								pieChartItems.add(new PieChartItem(powerLoss.floatValue(),
									getString(R.string.powerLossesMessage) + " " +
									UserPreferencesUtils.formatPower(powerLoss.longValue(),
									DataCenterDashboardActivity.this) + " (" +
									Math.round(NUMBER_100 * powerLoss / summaryPower) + "%)",
									POWER_LOSS_COLOR));
							}
							if (lightingPower != null) {
								pieChartItems.add(new PieChartItem(lightingPower.floatValue(),
									getString(R.string.miscLoadMessage) + " " +
									UserPreferencesUtils.formatPower(lightingPower.longValue(),
									DataCenterDashboardActivity.this) + " (" +
									Math.round(NUMBER_100 * lightingPower / summaryPower) + "%)",
									LIGHTING_POWER_COLOR));
							}
							pieChart.setPieChartItems(pieChartItems);
							pieChart.setVisibility(View.VISIBLE);
						}
					} else if ("lite".equals(pueType)) {
						if (lightingPower != null || infrastructurePower != null) {
							pieChartItems.clear();
							if (itPower != null) {
								pieChartItems.add(new PieChartItem(itPower.floatValue(),
									getString(R.string.itLoadMessage) + " " +
									UserPreferencesUtils.formatPower(itPower.longValue(),
									DataCenterDashboardActivity.this) + " (" +
									Math.round(NUMBER_100 * itPower.floatValue() / summaryPower) + "%)",
									IT_POWER_COLOR));
							}
							if (infrastructurePower != null) {
								pieChartItems.add(new PieChartItem(infrastructurePower.floatValue(),
									getString(R.string.itLoadMessage) + " " +
									UserPreferencesUtils.formatPower(infrastructurePower.longValue(),
									DataCenterDashboardActivity.this) + " (" +
									Math.round(NUMBER_100 * infrastructurePower / summaryPower) + "%)",
									INFRASTRUVTURE_POWER_COLOR));
							}
							pieChart.setPieChartItems(pieChartItems);
							pieChart.setVisibility(View.VISIBLE);
						}
					} else {
						pieChart.setVisibility(View.GONE);
					}
					hideProgressBar();
				}
			}
		}
	};

	{
		rackRequest.setType("RACK");
		rackRequest.setRequestChildrenNumberOnly(true);
		rackRequest.setRequestDirectChildrenOnly(false);
		cracRequest.setType("CRAC");
		cracRequest.setRequestChildrenNumberOnly(true);
		cracRequest.setRequestDirectChildrenOnly(false);
		pueRequest.setType("PUE");
		pueRequest.setProperties(new String []{"pue", "lightingPower", "powerLoss", "coolingPower", "itPower",
			"estimatedAnEnergyUsageToDate", "type", "infrastructurePower"});
		dcRequest.setProperties(new String []{"name", "numAlerts"});
		dcRequest.setChildrenRequests(new ObjectRequest[]{rackRequest, cracRequest, pueRequest});
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inflateContent(R.layout.data_center_dashboard);
		contentView = (ViewGroup) findViewById(R.id.dataCenterDashboardLayout);
		objectId = getIntent().getStringExtra("objectId");
		if (objectId == null && getParent() != null) {
			objectId = getParent().getIntent().getStringExtra("objectId");
		}
		totalAlertsTextField = (TextView) findViewById(R.id.dataCenterDashboard_totalAlertsTextField);
		totalRacksTextField = (TextView) findViewById(R.id.dataCenterDashboard_totalRacksTextField);
		totalCracsTextField = (TextView) findViewById(R.id.dataCenterDashboard_totalCollingUnitsTextField);
		energyUsageTextField = (TextView) findViewById(R.id.dataCenterDashboard_energyUsageTextField);
		pueTextField = (TextView) findViewById(R.id.dataCenterDashboard_pueTextField);
		pieChart = (PieChartView) findViewById(R.id.dataCenterDashboard_pieChart);
		pieChart.setLegendTextColor(getResources().getColor(R.color.white));
		pieChart.getLegendView().setPadding(10, 0, 0, 0);
		errorDialog = new AlertDialog.Builder(this).setTitle(R.string.communicationError).create();
		newAlertsDialog = new AlertDialog.Builder(this).create();
		newAlertsDialog.setOnDismissListener(this);
		pueLayout = (ViewGroup) findViewById(R.id.dataCenterDashboard_pueLayout);
	}

	public void onResume() {
		super.onResume();
		registerReceiver(updateReceiver, new IntentFilter(AlertService.UPDATE_ACTION));
		registerReceiver(newAlertsReceiver, new IntentFilter(AlertService.NEW_ALERTS_ACTION));
		Thread runner = new Thread() {

			public void run() {
				if (!isFinishing()) {
					refresh(true);
				}
			}
		};
		runner.start();
	}

	public void onPause() {
		super.onPause();
		newAlertsDialog.dismiss();
		unregisterReceiver(updateReceiver);
		unregisterReceiver(newAlertsReceiver);
	}

	private void refresh(boolean showProgressDialog) {
		if (objectId != null) {
			Message msg = handler.obtainMessage();
			if (showProgressDialog) {
				msg.what = START_DATA_REFRESH;
				handler.sendMessage(msg);
			}
			msg = handler.obtainMessage();
			msg.what = STOP_DATA_REFRESH;
			dcRequest.setId(objectId);
			ResponseElement response = DefaultObjectManager.instance.getObjectData(dcRequest);
			if (response != null) {
				ResponseElement dcResponse = ObjectUtils.getObject(response, objectId);
				objectName =
					String.valueOf(ObjectUtils.getAttr(ObjectUtils.getObjectProperty(dcResponse, "name"), "v"));
				objectName = objectName == null ? getString(R.string.notAvailableMessage) : objectName;
				alertCount = (Integer) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(dcResponse, "numAlerts"), "v");
				totalRacks = ObjectUtils.getChildrenNumber(dcResponse, "RACK");
				totalCoolingUnits = ObjectUtils.getChildrenNumber(dcResponse, "CRAC");
				for (ResponseElement pueResponse : ObjectUtils.getObjects(response, "PUE")) {
					energyUsage = (Double) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(pueResponse,
						"estimatedAnEnergyUsageToDate"), "v");
					showPue = false;
					String[] uiPrivileges = DefaultConfigManager.instance.getUIPrivileges();
					if (uiPrivileges != null) {
						for (String uiPrivilege : uiPrivileges) {
							if ("showPUE".equals(uiPrivilege)) {
								showPue = true;
								break;
							}
						}
					}
					pue = (Double) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(pueResponse, "pue"), "v");
					lightingPower =
						(Double) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(pueResponse, "lightingPower"), "v");
					powerLoss =
						(Double) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(pueResponse, "powerLoss"), "v");
					coolingPower =
						(Double) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(pueResponse, "coolingPower"), "v");
					itPower = (Double) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(pueResponse, "itPower"), "v");
					infrastructurePower = (Double) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(pueResponse,
						"infrastructurePower"), "v");
					pueType = String.valueOf(ObjectUtils.getAttr(ObjectUtils.getObjectProperty(pueResponse, "type"),
						"v"));
					summaryPower = 0;
					if ("full".equals(pueType)) {
						if (lightingPower != null) {
							summaryPower += lightingPower;
						}
						if (powerLoss != null) {
							summaryPower += powerLoss;
						}
						if (coolingPower != null) {
							summaryPower += coolingPower;
						}
						if (itPower != null) {
							summaryPower += itPower;
						}
					} else if ("lite".equals(pueType)) {
						if (itPower != null) {
							summaryPower += itPower;
						}
						if (infrastructurePower != null) {
							summaryPower += infrastructurePower;
						}
					}
					break;
				}
				msg.arg1 = 0;
			} else {
				msg.arg1 = 1;
			}
			handler.sendMessage(msg);
		}
	}

	public void onDismiss(DialogInterface dialog) {
		Thread runner = new Thread() {

			public void run() {
				refresh(true);
			}
		};
		runner.start();
	}

	private class UpdateReceiver extends BroadcastReceiver {

		public void onReceive(Context context, Intent intent) {
			Thread runner = new Thread() {

				public void run() {
					refresh(false);
				}
			};
			runner.start();
		}
	}

	private class NewAlertsReceiver extends BroadcastReceiver {

		public void onReceive(Context context, final Intent intent) {
			newAlertsDialog.setTitle(intent.getStringExtra("message"));
			newAlertsDialog.setIcon(intent.getIntExtra("iconId", R.drawable.major_small));
			newAlertsDialog.show();
			Ringtone ringtone = RingtoneManager.getRingtone(DataCenterDashboardActivity.this,
				RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
			if (ringtone != null) {
				ringtone.play();
			}
		}
	}
}
