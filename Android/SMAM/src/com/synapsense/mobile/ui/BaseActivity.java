package com.synapsense.mobile.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.synapsense.mobile.R;
import com.synapsense.mobile.service.UserPreferencesUtils;

public class BaseActivity extends Activity {

	protected View contentView;
	private LayoutInflater layoutInflater;
	private TextView titleTextView;
	private ProgressBar progressBar;
	private LinearLayout contentLayout;
	private AlertDialog aboutDialog;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		setContentView(R.layout.base);
		titleTextView = (TextView) findViewById(R.id.base_title);
		progressBar = (ProgressBar) findViewById(R.id.base_progressBar);
		contentLayout = (LinearLayout) findViewById(R.id.content);
		aboutDialog = new AlertDialog.Builder(this).setView(layoutInflater.inflate(R.layout.about, null)).create();
		aboutDialog.setTitle(R.string.aboutTitle);
		setTitle(R.string.customTitle);
		hideProgressBar();
	}

	public void onResume() {
		super.onResume();
		if (PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("lastActivityHashCode", 0) == hashCode()) {
			Intent intent = new Intent(this, LoginActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}
		SharedPreferences.Editor prefsEditor =
			PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
		prefsEditor.putBoolean("isApplicationInForeground", true);
		prefsEditor.commit();

		if (!getBaseContext().getResources().getConfiguration().locale.equals(UserPreferencesUtils.getCurrentLocale())) {
			Configuration config = new Configuration();
			config.locale = UserPreferencesUtils.getCurrentLocale();
			getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());
			if (getParent() != null) {
				Intent intent = getParent().getIntent().addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
				if(getParent() instanceof TabActivity) {
					intent.putExtra("currentTabIndex", ((TabActivity) getParent()).getTabHost().getCurrentTab());
				}
				startActivity(intent);
			} else {
				startActivity(getIntent().addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT));
			}
			finish();
		}
	}

	public void onPause() {
		super.onPause();
		SharedPreferences.Editor prefsEditor =
			PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
		prefsEditor.putInt("lastActivityHashCode", hashCode());
		prefsEditor.putBoolean("isApplicationInForeground", false);
		prefsEditor.commit();
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		menu.findItem(R.id.item_settings).setIntent(new Intent(this,
			SettingsActivity.class)).setIcon(R.drawable.settings);
		menu.findItem(R.id.item_about).setIntent(new Intent(this, SettingsActivity.class)).setIcon(R.drawable.about);
		return true;
	}

	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		if (item.getItemId() == R.id.item_about) {
			aboutDialog.show();
			return true;
		}
		return false;
	}

	protected void inflateContent(int id) {
		contentView = layoutInflater.inflate(id, contentLayout);
	}

	public void setTitle(CharSequence title) {
		super.setTitle(title);
		titleTextView.setText(title);
	}

	public void showProgressBar() {
		progressBar.setVisibility(View.VISIBLE);
	}

	public void hideProgressBar() {
		progressBar.setVisibility(View.GONE);
	}
}
