package com.synapsense.mobile.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

import com.synapsense.mobile.R;

public class SettingsActivity extends PreferenceActivity implements OnPreferenceChangeListener {

	private ListPreference requestPeriodPrefs;
	private EditTextPreference serverPortPrefs;
	private CheckBoxPreference secureConnectionPrefs;
	private SharedPreferences prefs;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getListView().setBackgroundColor(getResources().getColor(R.color.darkGray));
		addPreferencesFromResource(R.xml.preferences);
		requestPeriodPrefs = (ListPreference) findPreference("requestPeriod");
		requestPeriodPrefs.setOnPreferenceChangeListener(this);
		requestPeriodPrefs.setSummary(requestPeriodPrefs.getEntry());
		serverPortPrefs = (EditTextPreference) findPreference("serverPort");
		serverPortPrefs.setOnPreferenceChangeListener(this);
		serverPortPrefs.setSummary(serverPortPrefs.getText());
		secureConnectionPrefs = (CheckBoxPreference) findPreference("secureConnection");
		secureConnectionPrefs.setOnPreferenceChangeListener(this);
		prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	}

	public void onResume() {
		super.onResume();
		if (prefs.getInt("lastActivityHashCode", 0) == hashCode()) {
			Intent intent = new Intent(this, LoginActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}
		SharedPreferences.Editor prefsEditor = prefs.edit();
		prefsEditor.putBoolean("isApplicationInForeground", true);
		prefsEditor.commit();
	}

	public void onPause() {
		super.onPause();
		SharedPreferences.Editor prefsEditor = prefs.edit();
		prefsEditor.putInt("lastActivityHashCode", hashCode());
		prefsEditor.putBoolean("isApplicationInForeground", false);
		prefsEditor.commit();
	}

	public boolean onPreferenceChange(Preference preference, Object newValue) {
		if (preference == serverPortPrefs) {
			String str = String.valueOf(newValue);
			// ESCA-JAVA0008:
			try {
				int n = Integer.parseInt(str);
				if (n > 0 && n <65536) {
					serverPortPrefs.setSummary(str);
					SharedPreferences.Editor prefsEditor = prefs.edit();
					if (prefs.getBoolean("secureConnection", false)) {
						prefsEditor.putString("openConnectionServerPort", str);
					} else {
						prefsEditor.putString("secureConnectionServerPort", str);
					}
					prefsEditor.commit();
					return true;
				}
			} catch (NumberFormatException e) {}
		} else if (preference == secureConnectionPrefs) {
			SharedPreferences.Editor prefsEditor = prefs.edit();
			String serverPortValue;
			if ((Boolean) newValue) {
				serverPortValue = prefs.getString("secureConnectionServerPort", "8443");
			} else {
				serverPortValue = prefs.getString("openConnectionServerPort", "8080");
			}
			prefsEditor.putString("serverPort", serverPortValue);
			serverPortPrefs.setSummary(serverPortValue);
			serverPortPrefs.setText(serverPortValue);
			prefsEditor.commit();
			return true;
		} else if (preference == requestPeriodPrefs) {
			int index = requestPeriodPrefs.findIndexOfValue(String.valueOf(newValue));
			CharSequence[] enties = requestPeriodPrefs.getEntries();
			if (index >= 0 && index < enties.length) {
				requestPeriodPrefs.setSummary(enties[index]);
				return true;
			}
		}
		return false;
	}
}
