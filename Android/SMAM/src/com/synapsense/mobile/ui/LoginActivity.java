package com.synapsense.mobile.ui;

import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.synapsense.mobile.R;
import com.synapsense.mobile.service.AlertService;
import com.synapsense.mobile.service.DefaultConfigManager;
import com.synapsense.mobile.service.DefaultUserManager;
import com.synapsense.mobile.user.LoginStatus;
import com.synapsense.mobile.user.UserInfo;

public class LoginActivity extends Activity implements View.OnClickListener {

	private View contentView;
	private EditText serverNameTextField;
	private EditText userNameField;
	private EditText passwordTextField;
	private Button loginButton;
	private AlertDialog autenticationErrorDialog;
	private AlertDialog communicationErrorDialog;
	private AlertDialog registrationDialog;
	private AlertDialog accessErrorDialog;
	private AlertDialog errorDialog;
	private AlertDialog aboutDialog;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		contentView = (ViewGroup) findViewById(R.id.loginLayout);
		serverNameTextField = (EditText) findViewById(R.id.login_serverNameTextField);
		serverNameTextField.setText(prefs.getString("serverName", ""));
		userNameField = (EditText) findViewById(R.id.login_userNameTextField);
		userNameField.setText(prefs.getString("userName", ""));
		passwordTextField = (EditText) findViewById(R.id.login_passwordTextField);
		loginButton = (Button) findViewById(R.id.login_logInButton);
		loginButton.setOnClickListener(this);
		autenticationErrorDialog = new AlertDialog.Builder(this).setTitle(R.string.authenticationError).create();
		communicationErrorDialog = new AlertDialog.Builder(this).setTitle(R.string.communicationError).create();
		registrationDialog = new AlertDialog.Builder(this).setTitle(R.string.deviceRegistrationTitle).create();
		registrationDialog.setMessage(getString(R.string.registrationRequestedMessage));
		accessErrorDialog = new AlertDialog.Builder(this).setTitle(R.string.accessErrorTitle).create();
		accessErrorDialog.setMessage(getString(R.string.accessNotAllowedError));
		errorDialog = new AlertDialog.Builder(this).setTitle(R.string.errorTitle).create();
		LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		aboutDialog = new AlertDialog.Builder(this).setView(layoutInflater.inflate(R.layout.about, null)).create();
		aboutDialog.setTitle(R.string.aboutTitle);
	}

	public void onResume() {
		super.onResume();
		passwordTextField.setText("");
		// FOR DEBUG (BEGIN)
//		serverNameTextField.setText("dendrite.merann.ru:8080");
//		serverNameTextField.setText("axon.merann.ru");
//		serverNameTextField.setText("n102569.merann.ru:443");
//		serverNameTextField.setText("n102569.merann.ru");
//		userNameField.setText("admin"); // "user"
//		passwordTextField.setText("admin"); // "user"
		// FOR DEBUG (END)
		if(serverNameTextField.getText().toString().trim().length() == 0) {
			serverNameTextField.requestFocus();
		} else if(userNameField.getText().toString().trim().length() == 0) {
			userNameField.requestFocus();
		} else {
			passwordTextField.requestFocus();
		}
	}

	public void onPause() {
    	super.onPause();
    	autenticationErrorDialog.dismiss();
    	communicationErrorDialog.dismiss();
    	registrationDialog.dismiss();
    	accessErrorDialog.dismiss();
    	errorDialog.dismiss();
    	aboutDialog.dismiss();
    }

	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		menu.findItem(R.id.item_settings).setIntent(new Intent(this,
			SettingsActivity.class)).setIcon(R.drawable.settings);
		menu.findItem(R.id.item_about).setIntent(new Intent(this, SettingsActivity.class)).setIcon(R.drawable.about);
		return true;
	}

	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		if (item.getItemId() == R.id.item_about) {
			aboutDialog.show();
			return true;
		}
		return false;
	}

	public void onClick(View view) {
		final String serverName = serverNameTextField.getText().toString().trim();
		final String userName = userNameField.getText().toString().trim();
		final String password = passwordTextField.getText().toString().trim();
		if (serverName == null || serverName.trim().length() == 0) {
			errorDialog.setMessage(getString(R.string.serverNameError));
			errorDialog.show();
			return;
		} else if (userName == null || userName.trim().length() == 0) {
			errorDialog.setMessage(getString(R.string.userNameError));
			errorDialog.show();
			return;
		} else if (password == null || password.trim().length() == 0) {
			errorDialog.setMessage(getString(R.string.passwordError));
			errorDialog.show();
			return;
		}
		final ProgressDialog progressDialog =
			ProgressDialog.show(LoginActivity.this, "", getString(R.string.loggingInMessage), true);
		Thread runner = new Thread() {

			public void run() {
				String deviceName = android.os.Build.MODEL;
				//A hardware serial number, if available. Alphanumeric only, case-insensitive.
				String deviceId = android.os.Build.SERIAL;
				if (deviceId == null || android.os.Build.UNKNOWN.equals(deviceId)) {
					//A 64-bit number (as a hex string) that is randomly generated on the device's first boot and should remain constant for the lifetime of the device. (The value may change if a factory reset is performed on the device.) 
					deviceId = Settings.System.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
				}
				if (deviceId == null) {
					TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
					//Returns the unique device ID, for example, the IMEI for GSM and the MEID or ESN for CDMA phones. Return null if device ID is not available. 
					deviceId = telephonyManager != null ? telephonyManager.getDeviceId() : null;
				}
				if (deviceId == null) {
					WifiManager wifiMan = (WifiManager) getSystemService(Context.WIFI_SERVICE);
					WifiInfo wifiInf = wifiMan.getConnectionInfo();
					deviceId = wifiInf.getMacAddress();
				}
				final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
				final UserInfo userInfo = DefaultUserManager.instance.login(serverName + ':' +
					prefs.getString("serverPort", "8080"), userName, password, deviceName, deviceId,
					prefs.getBoolean("secureConnection", false));
				if (userInfo != null && userInfo.getLoginStatus() == LoginStatus.OK) {
					SharedPreferences.Editor prefsEditor = prefs.edit();
					prefsEditor.putString("serverName", serverName);
					prefsEditor.putString("userName", userName);
					prefsEditor.commit();
					contentView.post(new Runnable() {

						public void run() {
							progressDialog.dismiss();
						}
					});
					Intent intent = new Intent(LoginActivity.this, MainTabActivity.class);
					String[] uiPrivileges = DefaultConfigManager.instance.getUIPrivileges();
					if (uiPrivileges != null) {
						for (String uiPrivilege : uiPrivileges) {
							if ("showSystemHealth".equals(uiPrivilege)) {
								intent.putExtra("showSystemHealth", true);
								break;
							}
						}
					}
					startActivityForResult(intent, 1);
					startService(new Intent(LoginActivity.this, AlertService.class));
				} else if (userInfo != null && userInfo.getLoginStatus() == LoginStatus.DEVICE_REGISTRATION_REQUESTED) {
					contentView.post(new Runnable() {

						public void run() {
							progressDialog.dismiss();
							registrationDialog.show();
						}
					});
				} else if (userInfo != null && userInfo.getLoginStatus() == LoginStatus.DEVICE_NOT_ALLOWED) {
					contentView.post(new Runnable() {

						public void run() {
							progressDialog.dismiss();
							accessErrorDialog.show();
						}
					});
				} else {
					if (DefaultUserManager.instance.isLoggedIn()) {
						stopService(new Intent(LoginActivity.this, AlertService.class));
					}
					contentView.post(new Runnable() {

						public void run() {
							progressDialog.dismiss();
							if (userInfo != null) {
								autenticationErrorDialog.show();
							} else {
								communicationErrorDialog.show();
							}
						}
					});
				}
			}
		};
		runner.start();
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (resultCode == RESULT_OK) {
			stopService(new Intent(this, AlertService.class));
			finish();
		}
		// delete all private files (cached floorplans)
		for (File file : getFilesDir().listFiles()) {
			if (!file.delete()) {
				Log.d("SMAM", "File " + file.getAbsolutePath() + " cannot be deleted!");
			}
		}
	}
}
