package com.synapsense.mobile.ui;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.synapsense.mobile.R;
import com.synapsense.mobile.interaction.ObjectRequest;
import com.synapsense.mobile.interaction.RequestInfo;
import com.synapsense.mobile.interaction.ResponseElement;
import com.synapsense.mobile.service.AlertService;
import com.synapsense.mobile.service.DefaultConfigManager;
import com.synapsense.mobile.service.DefaultObjectManager;
import com.synapsense.mobile.service.ObjectUtils;
import com.synapsense.mobile.service.UserPreferencesUtils;
import com.synapsense.mobile.tree.LiveImagingType;
import com.synapsense.mobile.ui.FloorPlanView.FloorPlanObjectInfo;
import com.synapsense.mobile.ui.FloorPlanView.OnFloorPlanObjectClickListener;

// ESCA-JAVA0077:
public class FloorPlanActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener,
		DialogInterface.OnDismissListener, CompoundButton.OnCheckedChangeListener, OnFloorPlanObjectClickListener {

	private static final int START_DATA_REFRESH = 0;
	private static final int STOP_DATA_REFRESH = 1;
	private static final int SHOW_OBJECT_DETAILS = 2;
	private static final int MAX_LIVE_IMAGING_IMAGE_WIDTH = 1800;
	private static final int MAX_LIVE_IMAGING_IMAGE_HEIGHT = 1200;
	private static final int MAX_FLOOR_PLAN_IMAGE_SIZE = 2879;
	private static final int REAL_RACK_HEIGHT = 24;
	private static final String[] linkPropertyNames = {"lastValue"};
	private static final Set<String> objectTypes = new HashSet<String>();
	private final UpdateReceiver updateReceiver = new UpdateReceiver();
	private final NewAlertsReceiver newAlertsReceiver = new NewAlertsReceiver();
	private FloorPlanWebViewClient floorPlanWebViewClient = new FloorPlanWebViewClient();
	private FloorPlanView floorPlanView;
	private View objectFilterLayout;
	private View objectDetailsLayout;
	private CheckBox cracTypeCheckBox;
	private CheckBox pressureTypeCheckBox;
	private CheckBox doorTypeCheckBox;
	private CheckBox genericTemperatureTypeCheckBox;
	private CheckBox leakDetectorCheckBox;
	private CheckBox equipmentStatusCheckBox;
	private CheckBox verticalTemperatureCheckBox;
	private CheckBox rackTypeCheckBox;
	private CheckBox energyMeterTypeCheckBox;
	private CheckBox energyMeterWirelessCheckBox;
	private View environmentalsLayout;
	private TextView objectNameTextField;
	private TextView timestampTextField;
	private ViewGroup propertiesTable;
	private View liveImagingLayout;
	private RadioGroup liveImageTypesRadioGroup;
	private AlertDialog objectDetailsDialog;
	private AlertDialog objectFilterDialog;
	private AlertDialog liveImagingDialog;
	private AlertDialog errorDialog;
	private AlertDialog newAlertsDialog;
	private static String objectId = "";
	private LiveImagingType[] liveImagingTypes;
	private List<String> allowedObjectTypes;
	private static int lastLiveImagingTypes = 0;
	private String dcName;
	private String imageUrl;
	private String objectName;
	private String serverName;
	private long timeStamp;
	private boolean optionsChanged;
	private MenuItem objectFilterMenuItem;
	private MenuItem liveImagingMenuItem;
	private boolean liveImagingChanged;
	private int liveImagingImageWidth;
	private int floorPlanImageWidth;
	private static float initialScale = 0;
	private int rackIconHeight = 20;
	private final ObjectRequest objectRequest = new ObjectRequest();
	private final ObjectRequest dcRequest = new ObjectRequest();
	private final ObjectRequest wsnSensorRequest = new ObjectRequest();
	private final ObjectRequest IpmiSensorRequest = new ObjectRequest();
	private final ObjectRequest modBusPropertyRequest = new ObjectRequest();
	private final Handler handler = new Handler() {

		public void handleMessage(Message msg) {
			if (isFinishing()) {
				return;
			}
			if (msg.what == START_DATA_REFRESH) {
				showProgressBar();
			} else if (msg.what == STOP_DATA_REFRESH) {
				setTitle(getString(R.string.customTitle) + " - " + dcName);
				if (imageUrl != null && !imageUrl.equals(floorPlanView.getUrl())) {
					floorPlanView.loadUrl(imageUrl);
					floorPlanView.setNewPicture(true);
				} else {
					floorPlanView.invalidate();
				}
				if (msg.arg1 > 0) {
					hideProgressBar();
					errorDialog.show();
				} else if (msg.obj != null) {
					@SuppressWarnings("unchecked")
					List<FloorPlanObjectInfo> objectInfos = (List<FloorPlanObjectInfo>) msg.obj;
					floorPlanView.floorPlanObjectInfoList.clear();
					floorPlanView.floorPlanObjectInfoList.addAll(objectInfos);
					hideProgressBar();
				}
			} else if (msg.what == SHOW_OBJECT_DETAILS) {
				objectNameTextField.setText(objectName);
				timestampTextField.setText(UserPreferencesUtils.formatDate(timeStamp));
				propertiesTable.removeAllViews();
				if (msg.arg1 > 0) {
					hideProgressBar();
					errorDialog.show();
				} else if (msg.obj != null) {
					@SuppressWarnings("unchecked")
					Map<String, CharSequence> objectProperties = (Map<String, CharSequence>) msg.obj;
					if (objectProperties.size() > 0) {
						for (String name : objectProperties.keySet()) {
							LinearLayout row = new LinearLayout(FloorPlanActivity.this);
							row.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,
								LayoutParams.WRAP_CONTENT));
							TextView nameTextView = new TextView(FloorPlanActivity.this);
							nameTextView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT, 1));
							nameTextView.setTextColor(Color.BLACK);
							nameTextView.setPadding(0, 0, 10, 0);
							nameTextView.setText(name);
							row.addView(nameTextView);
							TextView valueTextView = new TextView(FloorPlanActivity.this);
							valueTextView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));
							valueTextView.setTextColor(Color.BLACK);
							valueTextView.setGravity(Gravity.RIGHT);
							valueTextView.setText(objectProperties.get(name));
							row.addView(valueTextView);
							propertiesTable.addView(row);
						}
						objectDetailsDialog.show();
					}
					hideProgressBar();
				}
			}
		}
	};

	{
		dcRequest.setProperties(new String []{"name", "height", "scale", "width"});
		objectRequest.setAdditionalInfo(RequestInfo.HISTORICAL | RequestInfo.LINK | RequestInfo.TIMESTAMP |
			RequestInfo.UNITS | RequestInfo.MAPPED_NAME | RequestInfo.MAPPED_VALUE);
		wsnSensorRequest.setType("WSNSENSOR");
		wsnSensorRequest.setProperties(linkPropertyNames);
		wsnSensorRequest.setAdditionalInfo(RequestInfo.HISTORICAL | RequestInfo.TIMESTAMP |
			RequestInfo.UNITS | RequestInfo.MAPPED_NAME | RequestInfo.MAPPED_VALUE);
		IpmiSensorRequest.setType("IPMISENSOR");
		IpmiSensorRequest.setProperties(linkPropertyNames);
		IpmiSensorRequest.setAdditionalInfo(RequestInfo.HISTORICAL | RequestInfo.TIMESTAMP |
			RequestInfo.UNITS | RequestInfo.MAPPED_NAME | RequestInfo.MAPPED_VALUE);
		modBusPropertyRequest.setType("MODBUSPROPERTY");
		modBusPropertyRequest.setProperties(linkPropertyNames);
		modBusPropertyRequest.setAdditionalInfo(RequestInfo.HISTORICAL | RequestInfo.TIMESTAMP |
			RequestInfo.UNITS | RequestInfo.MAPPED_NAME | RequestInfo.MAPPED_VALUE);
		objectRequest.setLinkRequests(new ObjectRequest[]{wsnSensorRequest, IpmiSensorRequest, modBusPropertyRequest});
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inflateContent(R.layout.floorplan);
		contentView = (ViewGroup) findViewById(R.id.floorPlanLayout);
		String objectid = getIntent().getStringExtra("objectId");
		if (objectid == null && getParent() != null) {
			objectid = getParent().getIntent().getStringExtra("objectId");
		}
		floorPlanView = (FloorPlanView) findViewById(R.id.floorPlan_webView);
		floorPlanView.setOnFloorPlanObjectClickListener(this);
		if (!objectId.equals(objectid)) {
			lastLiveImagingTypes = 0;
			objectTypes.clear();
			FloorPlanView.scrollXY = new Point();
			floorPlanView.setFloorPlanScale(0);
			initialScale = 0;
			objectId = objectid;
		}
		objectFilterLayout =
			((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.object_filter, null);
		environmentalsLayout = objectFilterLayout.findViewById(R.id.floorPlanProperties_environmentalsLayout);
		cracTypeCheckBox = (CheckBox) objectFilterLayout.findViewById(R.id.floorPlanProperties_cracTypeCheckBox);
		cracTypeCheckBox.setOnCheckedChangeListener(this);
		pressureTypeCheckBox =
			(CheckBox) objectFilterLayout.findViewById(R.id.floorPlanProperties_pressureTypeCheckBox);
		pressureTypeCheckBox.setOnCheckedChangeListener(this);
		doorTypeCheckBox = (CheckBox) objectFilterLayout.findViewById(R.id.floorPlanProperties_doorTypeCheckBox);
		doorTypeCheckBox.setOnCheckedChangeListener(this);
		genericTemperatureTypeCheckBox =
			(CheckBox) objectFilterLayout.findViewById(R.id.floorPlanProperties_genericTemperatureTypeCheckBox);
		genericTemperatureTypeCheckBox.setOnCheckedChangeListener(this);
		leakDetectorCheckBox =
			(CheckBox) objectFilterLayout.findViewById(R.id.floorPlanProperties_leakDetectorCheckBox);
		leakDetectorCheckBox.setOnCheckedChangeListener(this);
		equipmentStatusCheckBox =
			(CheckBox) objectFilterLayout.findViewById(R.id.floorPlanProperties_equipmentStatusCheckBox);
		equipmentStatusCheckBox.setOnCheckedChangeListener(this);
		verticalTemperatureCheckBox =
			(CheckBox) objectFilterLayout.findViewById(R.id.floorPlanProperties_verticalTemperatureCheckBox);
		verticalTemperatureCheckBox.setOnCheckedChangeListener(this);
		rackTypeCheckBox = (CheckBox) objectFilterLayout.findViewById(R.id.floorPlanProperties_rackTypeCheckBox);
		rackTypeCheckBox.setOnCheckedChangeListener(this);
		energyMeterTypeCheckBox =
			(CheckBox) objectFilterLayout.findViewById(R.id.floorPlanProperties_energyMeterTypeCheckBox);
		energyMeterTypeCheckBox.setOnCheckedChangeListener(this);
		energyMeterWirelessCheckBox =
			(CheckBox) objectFilterLayout.findViewById(R.id.floorPlanProperties_energyMeterWirelessCheckBox);
		energyMeterWirelessCheckBox.setOnCheckedChangeListener(this);
		liveImagingLayout =
			((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.live_imaging, null);
		liveImageTypesRadioGroup = (RadioGroup) liveImagingLayout.findViewById(R.id.liveImaging_liveImagingTypes);
		objectDetailsLayout =
			((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.object_details, null);
		objectNameTextField = (TextView) objectDetailsLayout.findViewById(R.id.objectDetails_objectName);
		timestampTextField = (TextView) objectDetailsLayout.findViewById(R.id.objectDetails_timestamp);
		propertiesTable = (ViewGroup) objectDetailsLayout.findViewById(R.id.objectDetails_propertiesTable);
		errorDialog = new AlertDialog.Builder(this).setTitle(R.string.communicationError).create();
		newAlertsDialog = new AlertDialog.Builder(this).create();
		newAlertsDialog.setOnDismissListener(this);
		objectFilterDialog = new AlertDialog.Builder(this).setTitle(R.string.objectFilter).create();
		objectFilterDialog.setView(objectFilterLayout);
		objectFilterDialog.setOnDismissListener(this);
		liveImagingDialog = new AlertDialog.Builder(this).setTitle(R.string.liveImaging).create();
		liveImagingDialog.setView(liveImagingLayout);
		liveImagingDialog.setOnDismissListener(this);
		objectDetailsDialog = new AlertDialog.Builder(this).setTitle(R.string.objectDetailsTitle).create();
		objectDetailsDialog.setView(objectDetailsLayout);
		serverName = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("serverName", "");
	}

	public void onResume() {
		super.onResume();
		floorPlanView.setWebViewClient(floorPlanWebViewClient);
		registerReceiver(updateReceiver, new IntentFilter(AlertService.UPDATE_ACTION));
		registerReceiver(newAlertsReceiver, new IntentFilter(AlertService.NEW_ALERTS_ACTION));
		Thread runner = new Thread() {

			public void run() {
				refresh(lastLiveImagingTypes, true);
			}
		};
		runner.start();
	}
	
	public void onPause() {
		super.onPause();
		objectDetailsDialog.dismiss();
		newAlertsDialog.dismiss();
		objectFilterDialog.dismiss();
		liveImagingDialog.dismiss();
		unregisterReceiver(updateReceiver);
		unregisterReceiver(newAlertsReceiver);
		floorPlanView.setWebViewClient(null);
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		objectFilterMenuItem = menu.add(Menu.NONE, Menu.FIRST, Menu.NONE, getString(R.string.filter));
		objectFilterMenuItem.setIcon(R.drawable.filter);
		liveImagingMenuItem = menu.add(Menu.NONE, Menu.FIRST, Menu.NONE, getString(R.string.liveImaging));
		liveImagingMenuItem.setIcon(R.drawable.live_imaging);
		return super.onCreateOptionsMenu(menu);
	}

	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		if (item == objectFilterMenuItem && (allowedObjectTypes != null && allowedObjectTypes.size() > 0)) {
			optionsChanged = false;
			objectFilterDialog.show();
			return true;
		} else if (item == liveImagingMenuItem && (liveImagingTypes != null && liveImagingTypes.length > 0)) {
			optionsChanged = false;
			liveImagingDialog.show();
			return true;
		}
		return super.onMenuItemSelected(featureId, item);
	}
	
	// ESCA-JAVA0084:
	public void onFloorPlanObjectClick(final FloorPlanObjectInfo info) {
		Thread runner = new Thread() {

			public void run() {
				Message msg = handler.obtainMessage();
				msg.what = START_DATA_REFRESH;
				handler.sendMessage(msg);
				msg = handler.obtainMessage();
				msg.what = SHOW_OBJECT_DETAILS;
				objectRequest.setId(info.id);
				ResponseElement response = DefaultObjectManager.instance.getObjectData(objectRequest);
				if (response != null) {
					ResponseElement objectResponse = ObjectUtils.getObject(response, info.id);
					if (objectResponse != null && objectResponse.getSubElements() != null) {
						objectName = String.valueOf(ObjectUtils.getAttr(ObjectUtils.getObjectProperty(objectResponse,
							"name"), "v"));
						Map<String, CharSequence> objectProperties = new LinkedHashMap<String, CharSequence>();
						for (ResponseElement childResponse : objectResponse.getSubElements()) {
							String mn = (String) ObjectUtils.getAttr(childResponse, "mn");
							if (mn == null) {
								mn = childResponse.getName();
							}
							Number t = (Number) ObjectUtils.getAttr(childResponse, "t");
							Boolean h = (Boolean) ObjectUtils.getAttr(childResponse, "h");
							Boolean isLink = (Boolean) ObjectUtils.getAttr(childResponse, "isLink");
							CharSequence value = "";
							if (t != null && t.longValue() > 0 && h != null && h) {
								timeStamp = t.longValue();
								Object v = ObjectUtils.getAttr(childResponse, "v");
								String u = (String) ObjectUtils.getAttr(childResponse, "u");
								if (v != null) {
									if (v instanceof Number) {
										value = UserPreferencesUtils.formatNumber(((Number) v).doubleValue());
									} else {
										value = String.valueOf(v);
									}
									value = value + (u != null ? " " + u : "");
								} else {
									value = Html.fromHtml("<font color=\"red\">x</font>" + (u != null ? " " + u : ""));
								}
								objectProperties.put(mn, value);
							} else if (isLink != null && isLink && childResponse.getSubElements() != null) {
								for (ResponseElement linkChildRespons : childResponse.getSubElements()) {
									if ("lastValue".equals(linkChildRespons.getName())) {
										t = (Number) ObjectUtils.getAttr(linkChildRespons, "t");
										h = (Boolean) ObjectUtils.getAttr(linkChildRespons, "h");
										if (t != null && t.longValue() > 0 && h != null && h) {
											timeStamp = t.longValue();
											Object v = ObjectUtils.getAttr(linkChildRespons, "v");
											String u = (String) ObjectUtils.getAttr(linkChildRespons, "u");
											String mv = (String) ObjectUtils.getAttr(linkChildRespons, "mv");
											if (mv != null) {
												value = mv;
											} else if (v != null) {
												if (v instanceof Number) {
													Number number = (Number) v;
													if (number.intValue() == -5000 || number.intValue() == -3000) {
														value = Html.fromHtml("<font color=\"red\">x</font>" +
															(u != null ? " " + u : ""));
													} else if (number.intValue() == -2000) {
														value = Html.fromHtml("<font color=\"yellow\">x</font>" +
															(u != null ? " " + u : ""));
													} else {
														value = UserPreferencesUtils.formatNumber(number.doubleValue()) +
															(u != null ? " " + u : "");
													}
												} else {
													value = String.valueOf(v) + (u != null ? " " + u : "");
												}
											} else {
												value = Html.fromHtml("<font color=\"red\">x</font>" +
													(u != null ? " " + u : ""));
											}
											objectProperties.put(mn, value);
											break;
										}
									}
								}
							}
						}
						msg.obj = objectProperties;
					}
				} else {
					msg.arg1++;
				}
				handler.sendMessage(msg);
			}
		};
		runner.start();
	}

	private void refresh(int liveImagingType, boolean showProgressDialog) {
		Message msg = handler.obtainMessage();
		if (showProgressDialog) {
			msg.what = START_DATA_REFRESH;
			handler.sendMessage(msg);
		}
		msg = handler.obtainMessage();
		msg.what = STOP_DATA_REFRESH;
		if (liveImagingTypes == null) {
			if (DefaultObjectManager.instance.isLiveImagingAvailable(objectId)) {
				liveImagingTypes = DefaultObjectManager.instance.getLiveImagingTypes();
			} else {
				liveImagingTypes = new LiveImagingType[0];
			}
			liveImageTypesRadioGroup.removeAllViews();
			if (liveImagingTypes.length > 0) {
				liveImageTypesRadioGroup.setOnCheckedChangeListener(null);
				RadioButton offRadioButton = new RadioButton(FloorPlanActivity.this);
				offRadioButton.setText(R.string.offLabel);
				offRadioButton.setTextColor(getResources().getColor(R.color.black));
				offRadioButton.setId(0);
				liveImageTypesRadioGroup.addView(offRadioButton);
				for (int i = 0; i < liveImagingTypes.length; ++i) {
					RadioButton radioButton = new RadioButton(FloorPlanActivity.this);
					radioButton.setText(liveImagingTypes[i].getName());
					radioButton.setTextColor(getResources().getColor(R.color.black));
					radioButton.setId(i + 1);
					liveImageTypesRadioGroup.addView(radioButton);
				}
				if (liveImagingType == 0) {
					offRadioButton.toggle();
				} else {
					liveImageTypesRadioGroup.check(liveImagingType);
				}
				liveImageTypesRadioGroup.setOnCheckedChangeListener(FloorPlanActivity.this);
				liveImagingLayout.setVisibility(View.VISIBLE);
			} else {
				liveImageTypesRadioGroup.setOnCheckedChangeListener(null);
				liveImagingLayout.setVisibility(View.GONE);
			}
			msg.arg1 = liveImagingTypes == null ? 1 : 0;
		}
		if (allowedObjectTypes == null) {		
			String[] floorplanTypes = DefaultConfigManager.instance.getFloorplanTypes();
			if (floorplanTypes != null && floorplanTypes.length > 0) {
				allowedObjectTypes = Arrays.asList(floorplanTypes);
				if (objectTypes.size() == 0) {
					objectTypes.addAll(allowedObjectTypes);
				}
				cracTypeCheckBox.setVisibility(allowedObjectTypes.contains(FloorPlanView.CRAC_TYPE) ? View.VISIBLE :
					View.GONE);
				cracTypeCheckBox.setChecked(pressureTypeCheckBox.getVisibility() == View.VISIBLE && objectTypes.contains(FloorPlanView.CRAC_TYPE));
				pressureTypeCheckBox.setVisibility(allowedObjectTypes.contains(FloorPlanView.PRESSURE_TYPE) ? View.VISIBLE :
					View.GONE);
				pressureTypeCheckBox.setChecked(pressureTypeCheckBox.getVisibility() == View.VISIBLE && objectTypes.contains(FloorPlanView.PRESSURE_TYPE));
				doorTypeCheckBox.setVisibility(allowedObjectTypes.contains(FloorPlanView.DOOR_TYPE) ? View.VISIBLE :
					View.GONE);
				doorTypeCheckBox.setChecked(doorTypeCheckBox.getVisibility() == View.VISIBLE && objectTypes.contains(FloorPlanView.DOOR_TYPE));
				genericTemperatureTypeCheckBox.setVisibility(allowedObjectTypes.contains(FloorPlanView.GENERIC_TEMPERATURE_TYPE) ?
					View.VISIBLE : View.GONE);
				genericTemperatureTypeCheckBox.setChecked(genericTemperatureTypeCheckBox.getVisibility() == View.VISIBLE && objectTypes.contains(FloorPlanView.GENERIC_TEMPERATURE_TYPE));
				leakDetectorCheckBox.setVisibility(allowedObjectTypes.contains(FloorPlanView.LEAK_TYPE) ? View.VISIBLE :
					View.GONE);
				leakDetectorCheckBox.setChecked(leakDetectorCheckBox.getVisibility() == View.VISIBLE && objectTypes.contains(FloorPlanView.LEAK_TYPE));
				equipmentStatusCheckBox.setVisibility(allowedObjectTypes.contains(FloorPlanView.EQUIPMENT_STATUS_TYPE) ?
					View.VISIBLE : View.GONE);
				equipmentStatusCheckBox.setChecked(equipmentStatusCheckBox.getVisibility() == View.VISIBLE && objectTypes.contains(FloorPlanView.EQUIPMENT_STATUS_TYPE));
				verticalTemperatureCheckBox.setVisibility(allowedObjectTypes.contains(FloorPlanView.VERTICAL_TEMPERATURE_TYPE) ?
					View.VISIBLE : View.GONE);
				verticalTemperatureCheckBox.setChecked(verticalTemperatureCheckBox.getVisibility() == View.VISIBLE && objectTypes.contains(FloorPlanView.VERTICAL_TEMPERATURE_TYPE));
				rackTypeCheckBox.setVisibility(allowedObjectTypes.contains(FloorPlanView.RACK_TYPE) ? View.VISIBLE :
					View.GONE);
				rackTypeCheckBox.setChecked(rackTypeCheckBox.getVisibility() == View.VISIBLE && objectTypes.contains(FloorPlanView.RACK_TYPE));
				energyMeterTypeCheckBox.setVisibility(allowedObjectTypes.contains(FloorPlanView.ION_DELTA_TYPE) ||
					objectTypes.contains(FloorPlanView.ION_WYE_TYPE) ? View.VISIBLE : View.GONE);
				energyMeterTypeCheckBox.setChecked(energyMeterTypeCheckBox.getVisibility() == View.VISIBLE && objectTypes.contains(FloorPlanView.ION_DELTA_TYPE));
				energyMeterWirelessCheckBox.setVisibility(allowedObjectTypes.contains(FloorPlanView.ION_WIRELESS_TYPE) ?
					View.VISIBLE : View.GONE);
				energyMeterWirelessCheckBox.setChecked(energyMeterWirelessCheckBox.getVisibility() == View.VISIBLE && objectTypes.contains(FloorPlanView.ION_WIRELESS_TYPE));
				environmentalsLayout.setVisibility(View.VISIBLE);
			} else {
				environmentalsLayout.setVisibility(View.GONE);
			}
		}
		if (objectId != null) {
			// request objects
			dcRequest.setId(objectId);
			ResponseElement response = DefaultObjectManager.instance.getObjectData(dcRequest);
			if (response != null) {
				ResponseElement dcResponse = ObjectUtils.getObject(response, objectId);
				if (dcResponse != null) {
					dcName =
						String.valueOf(ObjectUtils.getAttr(ObjectUtils.getObjectProperty(dcResponse, "name"), "v"));
					Number width =
						(Number) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(dcResponse, "width"), "v");
					Number height = (Number) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(dcResponse, "height"),
						"v");
					if (width != null && height != null) {
						floorPlanView.setFloorPlanWidth(width.floatValue());
						Number scale =
							(Number) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(dcResponse, "scale"), "v");
						if (scale != null) {
							int imageHeight = (int) (height.floatValue() / scale.floatValue());
							int imageWidth = (int) (width.floatValue() / scale.floatValue());
							if (liveImagingType == 0) {
								float exportScale = 1;
								if (imageWidth > MAX_FLOOR_PLAN_IMAGE_SIZE) {
									exportScale = (float) MAX_FLOOR_PLAN_IMAGE_SIZE / imageWidth;
								}
								if (imageHeight * exportScale > MAX_FLOOR_PLAN_IMAGE_SIZE) {
									exportScale = (float) MAX_FLOOR_PLAN_IMAGE_SIZE / imageHeight;
								}								
								int resultImageWidth = (int) (imageWidth * exportScale);
								floorPlanImageWidth = resultImageWidth;
								float resultImageHeight = imageHeight * exportScale;
								// ESCA-JAVA0078:
								if (initialScale == 0) {
									initialScale = (height.floatValue() / resultImageHeight) *
										((float) rackIconHeight / REAL_RACK_HEIGHT);
									floorPlanView.setFloorPlanScale(initialScale);
								}
								floorPlanView.setFloorPlanImageWidth(resultImageWidth);
							} else {
								int resultImageWidth = MAX_LIVE_IMAGING_IMAGE_WIDTH;
								if (imageWidth > MAX_LIVE_IMAGING_IMAGE_WIDTH ||
										imageHeight > MAX_LIVE_IMAGING_IMAGE_HEIGHT) {
									if (imageWidth < imageHeight) {
										resultImageWidth = MAX_LIVE_IMAGING_IMAGE_HEIGHT * imageWidth / imageHeight;
									}
								} else {
									resultImageWidth = imageWidth;
								}
								liveImagingImageWidth = resultImageWidth;
								floorPlanView.setFloorPlanImageWidth(resultImageWidth);
							}
						}
					}
					Object[] objectsData = DefaultObjectManager.instance.getObjectsData(objectId,
						objectTypes.toArray(new String[objectTypes.size()]));
					if (objectsData != null) {
						List<FloorPlanObjectInfo> objectInfos = new ArrayList<FloorPlanObjectInfo>();
						for (Object objectData : objectsData) {
							if (objectData instanceof List) {
								@SuppressWarnings("unchecked")
								List<Object> properties = (List<Object>) objectData;
								boolean openDoor = false;
								String id = String.valueOf(properties.get(0));
								String objectType = id.substring(0, id.indexOf(':'));
								int status = (Integer) properties.get(1);
								int numAlerts = (Integer) properties.get(2);
								Number x = (Number) properties.get(3);
								Number y = (Number) properties.get(4);
								if (properties.size() > 5) {
									Number v = (Number) properties.get(5);
									openDoor = v != null && v.intValue() > 0;
								}
								objectInfos.add(floorPlanView.createFloorPlanObjectInfo(id, objectType, status,
									numAlerts, x, y, openDoor));
							}
						}
						msg.obj = objectInfos;
					}

				}
			}
			String floorplanFileName = (serverName + "_" + objectId).replace(':', '_') + ".png";
			File floorplanFile = getFileStreamPath(floorplanFileName);
			if (!floorplanFile.exists()) {
				// floorplan caching
				try {
					byte[] image = DefaultObjectManager.instance.getFloorplan(objectId);
					if (image != null) {
						FileOutputStream fos = openFileOutput(floorplanFileName, MODE_PRIVATE);
						fos.write(image);
						fos.close();
					} else {
						msg.arg1++;
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (liveImagingType == 0) {
				imageUrl = "file://" + floorplanFile.getAbsolutePath();
			} else {
				imageUrl = DefaultObjectManager.instance.getFloorplan(objectId,
					liveImagingTypes[liveImagingType - 1].getDataClass(),
					liveImagingTypes[liveImagingType - 1].getLayer());
			}
		} else {
			msg.arg1++;
		}
		handler.sendMessage(msg);
	}

	public void onCheckedChanged(RadioGroup radioGroup, final int checkedId) {
		if(radioGroup == liveImageTypesRadioGroup) {
			int prevSelection = lastLiveImagingTypes;
			lastLiveImagingTypes = checkedId;
			optionsChanged = true;
			if ((prevSelection == 0) || (lastLiveImagingTypes == 0)) {
				liveImagingChanged = true;
			}			
		}
	}

	public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
		if (compoundButton == cracTypeCheckBox) {
			if (isChecked) {
				objectTypes.add(FloorPlanView.CRAC_TYPE);
			} else {
				objectTypes.remove(FloorPlanView.CRAC_TYPE);
			}
		} else if (compoundButton == pressureTypeCheckBox) {
			if (isChecked) {
				objectTypes.add(FloorPlanView.PRESSURE_TYPE);
			} else {
				objectTypes.remove(FloorPlanView.PRESSURE_TYPE);
			}
		} else if (compoundButton == doorTypeCheckBox) {
			if (isChecked) {
				objectTypes.add(FloorPlanView.DOOR_TYPE);
			} else {
				objectTypes.remove(FloorPlanView.DOOR_TYPE);
			}
		} else if (compoundButton == genericTemperatureTypeCheckBox) {
			if (isChecked) {
				objectTypes.add(FloorPlanView.GENERIC_TEMPERATURE_TYPE);
			} else {
				objectTypes.remove(FloorPlanView.GENERIC_TEMPERATURE_TYPE);
			}
		} else if (compoundButton == leakDetectorCheckBox) {
			if (isChecked) {
				objectTypes.add(FloorPlanView.LEAK_TYPE);
			} else {
				objectTypes.remove(FloorPlanView.LEAK_TYPE);
			}
		} else if (compoundButton == equipmentStatusCheckBox) {
			if (isChecked) {
				objectTypes.add(FloorPlanView.EQUIPMENT_STATUS_TYPE);
			} else {
				objectTypes.remove(FloorPlanView.EQUIPMENT_STATUS_TYPE);
			}
		} else if (compoundButton == verticalTemperatureCheckBox) {
			if (isChecked) {
				objectTypes.add(FloorPlanView.VERTICAL_TEMPERATURE_TYPE);
			} else {
				objectTypes.remove(FloorPlanView.VERTICAL_TEMPERATURE_TYPE);
			}
		} else if (compoundButton == rackTypeCheckBox) {
			if (isChecked) {
				objectTypes.add(FloorPlanView.RACK_TYPE);
			} else {
				objectTypes.remove(FloorPlanView.RACK_TYPE);
			}
		} else if (compoundButton == energyMeterTypeCheckBox) {
			if (allowedObjectTypes.contains(FloorPlanView.ION_DELTA_TYPE)) {
				if (isChecked) {
					objectTypes.add(FloorPlanView.ION_DELTA_TYPE);
				} else {
					objectTypes.remove(FloorPlanView.ION_DELTA_TYPE);
				}
			}
			if (allowedObjectTypes.contains(FloorPlanView.ION_WYE_TYPE)) {
				if (isChecked) {
					objectTypes.add(FloorPlanView.ION_WYE_TYPE);
				} else {
					objectTypes.remove(FloorPlanView.ION_WYE_TYPE);
				}
			}
		} else if (compoundButton == energyMeterWirelessCheckBox) {
			if (isChecked) {
				objectTypes.add(FloorPlanView.ION_WIRELESS_TYPE);
			} else {
				objectTypes.remove(FloorPlanView.ION_WIRELESS_TYPE);
			}
		}
		optionsChanged = true;
	}

	public void onDismiss(final DialogInterface dialog) {
		Thread runner = new Thread() {

			public void run() {
				if (((dialog == objectFilterDialog || dialog == liveImagingDialog) && optionsChanged) ||
						dialog == newAlertsDialog) {
					refresh(lastLiveImagingTypes, true);
				}
			}
		};
		runner.start();
	}

	private class FloorPlanWebViewClient extends WebViewClient {

		private boolean firstLoadStarted = false;

		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			return true;
		}

		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			showProgressBar();
			firstLoadStarted = true;
		}

		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			hideProgressBar();
			errorDialog.show();
		}

		public void onPageFinished(WebView view, String url) {
			hideProgressBar();
			float viewWidth = floorPlanView.getWidth();
			float scale = floorPlanView.getFloorPlanScale();
			if (liveImagingChanged) {
				float scaleFactor = 0;
				if (lastLiveImagingTypes == 0) {
					scaleFactor = scale / (viewWidth / liveImagingImageWidth);
					scale = (scaleFactor * viewWidth) / floorPlanImageWidth;
				} else {
					scaleFactor = scale / (viewWidth / floorPlanImageWidth);
					scale = (scaleFactor * viewWidth) / liveImagingImageWidth;
				}
				liveImagingChanged = false;
			}
			floorPlanView.setInitialScale((int) (100 * scale));
		}

		public void onScaleChanged(WebView view, float oldScale, float newScale) {
			if (firstLoadStarted) {
				floorPlanView.updateFloorPlanScale();	
			}			
		}
		
		public void onLoadResource(WebView view, String url) {
			floorPlanView.setNewPicture(true);
		}
	}

	private class UpdateReceiver extends BroadcastReceiver {

		public void onReceive(Context context, Intent intent) {
			Thread runner = new Thread() {

				public void run() {
					refresh(lastLiveImagingTypes, false);
				}
			};
			runner.start();
		}
	}

	private class NewAlertsReceiver extends BroadcastReceiver {

		public void onReceive(Context context, final Intent intent) {
			newAlertsDialog.setTitle(intent.getStringExtra("message"));
			newAlertsDialog.setIcon(intent.getIntExtra("iconId", R.drawable.major_small));
			newAlertsDialog.show();
			Ringtone ringtone = RingtoneManager.getRingtone(FloorPlanActivity.this,
				RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
			if (ringtone != null) {
				ringtone.play();
			}
		}
	}
}
