package com.synapsense.mobile.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.synapsense.mobile.R;
import com.synapsense.mobile.tree.ObjectStatus;
import com.synapsense.mobile.ui.DataCenterListAdapter.DataCenterInfo;

public class DataCenterListAdapter extends ArrayAdapter<DataCenterInfo> {

	private LayoutInflater layoutInflater;
	private DataCenterInfo[] empty = new DataCenterInfo[0];
	private DataCenterInfo[] infos = empty;

	public DataCenterListAdapter(Context context) {
		super(context, R.layout.data_center);
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setTreeObjectInfos(DataCenterInfo[] treeObjectInfos) {
		infos = treeObjectInfos != null ? treeObjectInfos.clone() : empty;
		notifyDataSetChanged();
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.data_center, null);
		}
		// TODO investigate problem of infos[position] == null
		if (infos[position] != null) {
			ImageView alertsImageView = (ImageView) convertView.findViewById(R.id.dataCenter_alerts);
			alertsImageView.setVisibility(infos[position].numAlerts > 0 ? View.VISIBLE : View.GONE);
			ImageView warningsImageView = (ImageView) convertView.findViewById(R.id.dataCenter_warnings);
			warningsImageView.setVisibility(infos[position].status == ObjectStatus.NOT_RECEIVING_DATA ? View.VISIBLE :
				View.INVISIBLE);
			((TextView) convertView.findViewById(R.id.dataCenter_dataCenterName)).setText(infos[position].name);
			((TextView) convertView.findViewById(R.id.dataCenter_zonesCount)).setText(infos[position].description);
			TextView alertsCountTextView = (TextView) convertView.findViewById(R.id.dataCenter_alertsCount);
			alertsCountTextView.setText(String.valueOf(infos[position].numAlerts));
			alertsCountTextView.setVisibility(infos[position].numAlerts > 0 ? View.VISIBLE : View.GONE);
			convertView.setTag(infos[position].objectId);
		}
		return convertView;
	}

	public int getCount() {
		return infos.length;
	}

	public DataCenterInfo getItem(int position) {
		return infos[position];
	}

	public long getItemId(int position) {
		return position;
	}

	public static class DataCenterInfo implements Comparable<DataCenterInfo> {

		public final String objectId;
		public final String name;
		public final String description;
		public final int iconId;
		public final Double latitude;
		public final Double longitude;
		public final int numAlerts;
		public final int status;

		public DataCenterInfo(String objectId, String name, String description, int iconId, Double latitude,
				Double longitude, int numAlerts, int status) {
			this.objectId = objectId;
			this.name = name;
			this.description = description;
			this.iconId = iconId;
			this.latitude = latitude;
			this.longitude = longitude;
			this.numAlerts = numAlerts;
			this.status = status;
		}

		public int compareTo(DataCenterInfo info) {
			return info != null ? name.compareTo(info.name) : 1;
		}
	}
}
