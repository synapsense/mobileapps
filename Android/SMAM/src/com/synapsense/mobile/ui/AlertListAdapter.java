package com.synapsense.mobile.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.synapsense.mobile.R;
import com.synapsense.mobile.alert.Alert;
import com.synapsense.mobile.alert.AlertStatus;
import com.synapsense.mobile.service.DefaultAlertManager;
import com.synapsense.mobile.service.UserPreferencesUtils;

public class AlertListAdapter extends ArrayAdapter<Alert> implements OnTouchListener {

	private int selectionIndex = -1;
	private Button dismissButton;
	private LayoutInflater layoutInflater;
	private View updatingItemView;

	public AlertListAdapter(Context context) {
		super(context, R.layout.alert);
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		updatingItemView = layoutInflater.inflate(R.layout.updating, null);
	}

	public void setAlerts(Alert[] newAlerts) {
		setNotifyOnChange(false);
		clear();
		if (newAlerts != null) {
			for (Alert alert : newAlerts) {
				add(alert);
			}
		}
		setNotifyOnChange(true);
		notifyDataSetChanged();
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		Alert alert = getItem(position);
		if (alert != null) {
			if (convertView == null || convertView == updatingItemView) {
				convertView = layoutInflater.inflate(R.layout.alert, null);
			}
			((TextView) convertView.findViewById(R.id.alert_name)).setText(alert.getName());
			((TextView) convertView.findViewById(R.id.alert_objectName)).setText(alert.getObjectName());
			LinearLayout secondLayout = (LinearLayout) convertView.findViewById(R.id.alert_secondLayout);
			TextView timestamp = (TextView) convertView.findViewById(R.id.alert_timestamp);
			ImageView imageView = (ImageView) convertView.findViewById(R.id.alert_priorityIcon);
			if (alert.getStatus() == AlertStatus.OPENED) {
				timestamp.setText(UserPreferencesUtils.formatDate(alert.getTimestamp()).replace(' ', '\n'));
				imageView.setImageDrawable(convertView.getResources().getDrawable(
					DefaultAlertManager.getPriorityResourceId(alert.getPriority())));
				secondLayout.setVisibility(View.VISIBLE);
			} else {
				secondLayout.setVisibility(View.GONE);
			}
			convertView.setOnTouchListener(this);
			((Button) convertView.findViewById(R.id.alert_dismissButton)).setTag(position);
			return convertView;
		} else {
			return updatingItemView;
		}
	}

	public void setSelectionIndex(int newSelectionIndex) {
		selectionIndex = newSelectionIndex;
		notifyDataSetInvalidated();
	}

	public int getSelectionIndex() {
		return selectionIndex;
	}

	public View getDismissButton() {
		return dismissButton;
	}

	public boolean onTouch(View view, MotionEvent event) {
		dismissButton = (Button) view.findViewById(R.id.alert_dismissButton);
		return false;
	}
}
