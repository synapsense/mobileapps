package com.synapsense.mobile.ui;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.TabHost.OnTabChangeListener;

import com.synapsense.mobile.R;


public class CustomTabActivity extends TabActivity implements OnTabChangeListener {

	protected TabHost tabHost;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		tabHost = getTabHost();
		tabHost.setOnTabChangedListener(this);
	}

	protected void addTab(String id, int titleId, int iconId, Class<?> activityClass) {
		TabHost.TabSpec spec = tabHost.newTabSpec(id);
		View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab, null);
		TextView textView = (TextView) tabIndicator.findViewById(R.id.tabLayout_tabTitle);
		textView.setTextColor(getResources().getColor(R.color.lightLightGray));
		textView.setText(titleId);
		ImageView imageView = (ImageView) tabIndicator.findViewById(R.id.tabLayout_tabIcon);
		imageView.setImageResource(iconId);
		spec.setIndicator(tabIndicator);
		spec.setContent(new Intent(this, activityClass));
		tabHost.addTab(spec);
	}

	public void onTabChanged(String tabId) {
		// workaround because of MOTOROLA devices problem
		TabWidget tabWidget = tabHost.getTabWidget();
		for (int i = 0; i < tabWidget.getTabCount(); ++i) {
			TextView textView = (TextView) tabWidget.getChildAt(i).findViewById(R.id.tabLayout_tabTitle);
			textView.setTextColor(getResources().getColor(i == tabHost.getCurrentTab() ? R.color.white :
				R.color.lightLightGray));
		}
	}
}