package com.synapsense.mobile.ui;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.synapsense.mobile.R;
import com.synapsense.mobile.alert.Alert;
import com.synapsense.mobile.alert.AlertStatus;
import com.synapsense.mobile.service.AlertService;
import com.synapsense.mobile.service.DefaultAlertManager;
import com.synapsense.mobile.service.UserPreferencesUtils;

public class AlertDetailsActivity extends BaseActivity implements View.OnClickListener, DialogInterface.OnClickListener {

	private static final int START_STATUS_CHANGE = 0;
	private static final int STOP_STATUS_CHANGE = 2;
	private final NewAlertsReceiver newAlertsReceiver = new NewAlertsReceiver();
	private TextView alertNameTextView;
	private TextView objectNameTextView;
	private TextView priorytyTextView;
	private TextView statusTextView;
	private TextView lastActionByTextView;
	private TextView lastActionDateTextView;
	private TextView locationTextView;
	private TextView timestampTextView;
	private EditText descriptionTextField;
	private EditText messageTextField;
	private EditText commentTextField;
	private EditText newCommentTextField;
	private Button acknowledgeButton;
	private Button resolveButton;
	private Button dismissButton;
	private AlertDialog errorDialog;
	private AlertDialog newAlertsDialog;
	private AlertDialog newCommentDialog;
	private int newState;
	private Alert alert;
	private Toast lastToast;
	// ESCA-JAVA0077:
	private final Handler handler = new Handler() {

		public void handleMessage(Message msg) {
			if (isFinishing()) {
				return;
			}
			if (msg.what == START_STATUS_CHANGE) {
				showProgressBar();
			} else if (msg.what == STOP_STATUS_CHANGE) {
				hideProgressBar();
				if (msg.arg1 > 0) {
					errorDialog.show();
				} else {
					AlertDetailsActivity.this.finish();
				}
			}
		}
	};

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inflateContent(R.layout.alert_details);
		alert = (Alert) getIntent().getSerializableExtra("alert");
		contentView = (ViewGroup) findViewById(R.id.alertDetailsLayout);
		alertNameTextView = (TextView) findViewById(R.id.alertDetails_alertNameTextField);
		objectNameTextView = (TextView) findViewById(R.id.alertDetails_objectNameTextField);
		priorytyTextView = (TextView) findViewById(R.id.alertDetails_priorityTextField);
		statusTextView = (TextView) findViewById(R.id.alertDetails_statusTextField);
		lastActionByTextView = (TextView) findViewById(R.id.alertDetails_lastActionByTextField);
		lastActionDateTextView = (TextView) findViewById(R.id.alertDetails_lastActionDateTextField);
		locationTextView = (TextView) findViewById(R.id.alertDetails_locationTextField);
		timestampTextView = (TextView) findViewById(R.id.alertDetails_timestampTextField);
		descriptionTextField = (EditText) findViewById(R.id.alertDetails_descriptionTextField);
		messageTextField = (EditText) findViewById(R.id.alertDetails_messageTextField);
		commentTextField = (EditText) findViewById(R.id.alertDetails_commentTextField);
		acknowledgeButton = (Button) findViewById(R.id.acknowledgeButton);
		acknowledgeButton.setOnClickListener(this);
		resolveButton = (Button) findViewById(R.id.resolveButton);
		resolveButton.setOnClickListener(this);
		dismissButton = (Button) findViewById(R.id.dismissButton);
		dismissButton.setOnClickListener(this);
		errorDialog = new AlertDialog.Builder(this).setTitle(R.string.changeStatusError).create();
		newAlertsDialog = new AlertDialog.Builder(this).create();
		View newCommentView =
			((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.comment, null);
		newCommentTextField = (EditText) newCommentView.findViewById(R.id.comment_newCommentTextField);
		newCommentDialog = new AlertDialog.Builder(this).setView(newCommentView).create();
		newCommentDialog.setButton("OK", this);
		if (alert != null) {
			setTitle(getString(R.string.customTitle) + " - " + getIntent().getStringExtra("objectName"));
			alertNameTextView.setText(alert.getName());
			objectNameTextView.setText(alert.getObjectName());
			locationTextView.setText(alert.getObjectLocation());
			priorytyTextView.setText(DefaultAlertManager.priorityToStringResourceId(alert.getPriority()));
			int status = alert.getStatus();
			statusTextView.setText(DefaultAlertManager.statusToStringResourceId(status));
			lastActionByTextView.setText(alert.getLastActionUser() != null ? alert.getLastActionUser() : " ");
			lastActionDateTextView.setText(alert.getLastActionTime() != null ?
				UserPreferencesUtils.formatDate(alert.getLastActionTime()) : " ");
			timestampTextView.setText(UserPreferencesUtils.formatDate(alert.getTimestamp()));
			descriptionTextField.setText(alert.getDescription());
			messageTextField.setText(alert.getMessage());
			commentTextField.setText(alert.getComment() != null ? alert.getComment() : "");
			if (status == AlertStatus.ACKNOWLEDGED) {
				acknowledgeButton.setEnabled(false);
			} else if (status == AlertStatus.DISMISSED || status == AlertStatus.RESOLVED) {
				acknowledgeButton.setEnabled(false);
				resolveButton.setEnabled(false);
				dismissButton.setEnabled(false);
			}
		}
	}

	public void onResume() {
		super.onResume();
		registerReceiver(newAlertsReceiver, new IntentFilter(AlertService.NEW_ALERTS_ACTION));
	}

	public void onPause() {
		super.onPause();
		newAlertsDialog.dismiss();
		newCommentDialog.dismiss();
		unregisterReceiver(newAlertsReceiver);
		if (lastToast != null) {
			lastToast.cancel();
		}
	}

	public void onClick(View v) {
		if (v == acknowledgeButton) {
			newState = AlertStatus.ACKNOWLEDGED;
			newCommentTextField.setText("");
			newCommentDialog.setTitle(R.string.acknowledgeAlertTitle);
			newCommentDialog.show();
		} else if (v == resolveButton) {
			newState = AlertStatus.RESOLVED;
			newCommentTextField.setText("");
			newCommentDialog.setTitle(R.string.resolveAlertTitle);
			newCommentDialog.show();
		} else if (v == dismissButton) {
			newState = AlertStatus.DISMISSED;
			newCommentTextField.setText("");
			newCommentDialog.setTitle(R.string.dismissAlertTitle);
			newCommentDialog.show();
		}
	}

	public void onClick(DialogInterface dialog, int which) {
		if (which == DialogInterface.BUTTON_POSITIVE) {
			final String newComment = newCommentTextField.getText().toString().trim();
			if (newComment.isEmpty()) {
				lastToast = Toast.makeText(this, R.string.commentRequiredError, Toast.LENGTH_LONG);
				lastToast.show();
				Thread runner = new Thread() {

					public void run() {
						contentView.post(new Runnable() {

							public void run() {
								newCommentDialog.show();
							}
						});
					}
				};
				runner.start();
			} else {
				Thread runner = new Thread() {

					public void run() {
						Message msg = handler.obtainMessage();
						msg.what = START_STATUS_CHANGE;
						handler.sendMessage(msg);
						boolean result;
						if (newState == AlertStatus.ACKNOWLEDGED) {
							result = DefaultAlertManager.instance.acknowledge(alert.getId(), newComment);
						} else if (newState == AlertStatus.RESOLVED) {
							result = DefaultAlertManager.instance.resolve(alert.getId(), newComment);
						} else {
							result = DefaultAlertManager.instance.dismiss(alert.getId(), newComment);
						}
						msg = handler.obtainMessage();
						msg.what = STOP_STATUS_CHANGE;
						msg.arg1 = result ? 0 : 1;
						handler.sendMessage(msg);
					}
				};
				runner.start();
			}
		}
	}

	private class NewAlertsReceiver extends BroadcastReceiver {

		public void onReceive(Context context, final Intent intent) {
			newAlertsDialog.setTitle(intent.getStringExtra("message"));
			newAlertsDialog.setIcon(intent.getIntExtra("iconId", R.drawable.major_small));
			newAlertsDialog.show();
			Ringtone ringtone = RingtoneManager.getRingtone(AlertDetailsActivity.this,
				RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
			if (ringtone != null) {
				ringtone.play();
			}
		}
	}
}
