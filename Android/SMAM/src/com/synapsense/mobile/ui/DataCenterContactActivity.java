package com.synapsense.mobile.ui;

import java.util.regex.Pattern;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.util.Linkify;
import android.view.ViewGroup;
import android.widget.TextView;

import com.synapsense.mobile.R;
import com.synapsense.mobile.interaction.ObjectRequest;
import com.synapsense.mobile.interaction.ResponseElement;
import com.synapsense.mobile.service.AlertService;
import com.synapsense.mobile.service.DefaultObjectManager;
import com.synapsense.mobile.service.ObjectUtils;

public class DataCenterContactActivity extends BaseActivity {

	private static final int START_DATA_REFRESH = 0;
	private static final int STOP_DATA_REFRESH = 1;
	private final NewAlertsReceiver newAlertsReceiver = new NewAlertsReceiver();
	private TextView contactNameTextField;
	private TextView contactTitleTextField;
	private TextView contactOfficePhoneTextField;
	private TextView contactMobilePhoneTextField;
	private TextView contactEmailTextField;
	private AlertDialog newAlertsDialog;
	private AlertDialog errorDialog;
	private String objectId;
	private String objectName;
	private String contactName;
	private String contactTitle;
	private String contactOfficePhone;
	private String contactMobilePhone;
	private String contactEmail;
	private final ObjectRequest dcRequest = new ObjectRequest();
	private final Handler handler = new Handler() {

		private final Pattern PHONE_PATTERN = Pattern.compile("[-+0-9 ]+");
		private final Pattern EMAIL_PATTERN = Pattern.compile("[^ ]+@[^ ]+");
		private static final String PHONE_PREFIX = "tel:";
		private static final String EMAIL_PREFIX = "mailto:";

		public void handleMessage(Message msg) {
			if (isFinishing()) {
				return;
			}
			if (msg.what == START_DATA_REFRESH) {
				if (objectId != null) {
					showProgressBar();
				} else {
					errorDialog.show();
				}
			} else if (msg.what == STOP_DATA_REFRESH) {
				if (msg.arg1 > 0) {
					hideProgressBar();
					errorDialog.show();
				} else {
					setTitle(getString(R.string.customTitle) + " - " + objectName);
					contactNameTextField.setText(contactName == null ? getString(R.string.notAvailableMessage) :
						contactName);
					contactTitleTextField.setText(contactTitle == null ? getString(R.string.notAvailableMessage) :
						contactTitle);
					if (contactOfficePhone != null && contactOfficePhone.trim().length() > 0) {
						contactOfficePhoneTextField.setText(contactOfficePhone);
						Linkify.addLinks(contactOfficePhoneTextField, PHONE_PATTERN, PHONE_PREFIX);
					} else {
						contactOfficePhoneTextField.setText(R.string.notAvailableMessage);
					}
					if (contactMobilePhone != null && contactMobilePhone.trim().length() > 0) {
						contactMobilePhoneTextField.setText(contactMobilePhone);
						Linkify.addLinks(contactMobilePhoneTextField, PHONE_PATTERN, PHONE_PREFIX);
					} else {
						contactMobilePhoneTextField.setText(R.string.notAvailableMessage);
					}
					if (contactEmail != null && contactEmail.trim().length() > 0) {
						contactEmailTextField.setText(contactEmail);
						Linkify.addLinks(contactEmailTextField, EMAIL_PATTERN, EMAIL_PREFIX);
					} else {
						contactEmailTextField.setText(R.string.notAvailableMessage);
					}
				}
				hideProgressBar();
			}
		}
	};

	{
		dcRequest.setProperties(new String []{"name", "emergencyName", "emergencyTitle", "emergencyOfficePhone",
			"emergencyMobilePhone", "emergencyEmail"});
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inflateContent(R.layout.data_center_contact);
		contentView = (ViewGroup) findViewById(R.id.dataCenterContactLayout);
		objectId = getIntent().getStringExtra("objectId");
		if (objectId == null && getParent() != null) {
			objectId = getParent().getIntent().getStringExtra("objectId");
		}
		contactNameTextField = (TextView) contentView.findViewById(R.id.dataCenterContact_contactNameTextField);
		contactTitleTextField = (TextView) findViewById(R.id.dataCenterContact_contactTitleTextField);
		contactOfficePhoneTextField = (TextView) findViewById(R.id.dataCenterContact_contactOfficePhoneTextField);
		contactOfficePhoneTextField.setLinkTextColor(getResources().getColor(R.color.blue));
		contactMobilePhoneTextField = (TextView) findViewById(R.id.dataCenterContact_contactMobilePhoneTextField);
		contactMobilePhoneTextField.setLinkTextColor(getResources().getColor(R.color.blue));
		contactEmailTextField = (TextView) findViewById(R.id.dataCenterContact_contactEmailTextField);
		contactEmailTextField.setLinkTextColor(getResources().getColor(R.color.blue));
		errorDialog = new AlertDialog.Builder(this).setTitle(R.string.communicationError).create();
		newAlertsDialog = new AlertDialog.Builder(this).create();
	}

	public void onResume() {
		super.onResume();
		registerReceiver(newAlertsReceiver, new IntentFilter(AlertService.NEW_ALERTS_ACTION));
		Thread runner = new Thread() {

			public void run() {
				if (!isFinishing()) {
					refresh();
				}
			}
		};
		runner.start();
	}

	public void onPause() {
		super.onPause();
		newAlertsDialog.dismiss();
		unregisterReceiver(newAlertsReceiver);
	}

	private void refresh() {
		if (objectId != null) {
			Message msg = handler.obtainMessage();
			msg.what = START_DATA_REFRESH;
			handler.sendMessage(msg);
			msg = handler.obtainMessage();
			msg.what = STOP_DATA_REFRESH;
			dcRequest.setId(objectId);
			ResponseElement response = DefaultObjectManager.instance.getObjectData(dcRequest);
			if (response != null) {
				ResponseElement dcResponse = ObjectUtils.getObject(response, objectId);
				objectName = String.valueOf(ObjectUtils.getAttr(ObjectUtils.getObjectProperty(dcResponse,
					"name"), "v"));
				objectName = objectName == null ? getString(R.string.notAvailableMessage) : objectName;
				contactName = (String) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(dcResponse,
					"emergencyName"), "v");
				contactTitle = (String) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(dcResponse,
					"emergencyTitle"), "v");
				contactOfficePhone = (String) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(dcResponse,
					"emergencyOfficePhone"), "v");
				contactMobilePhone = (String) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(dcResponse,
					"emergencyMobilePhone"), "v");
				contactEmail = (String) ObjectUtils.getAttr(ObjectUtils.getObjectProperty(dcResponse,
					"emergencyEmail"), "v");
				msg.arg1 = 0;
			} else {
				msg.arg1 = 1;
			}
			handler.sendMessage(msg);
		}
	}

	private class NewAlertsReceiver extends BroadcastReceiver {

		public void onReceive(Context context, final Intent intent) {
			newAlertsDialog.setTitle(intent.getStringExtra("message"));
			newAlertsDialog.setIcon(intent.getIntExtra("iconId", R.drawable.major_small));
			newAlertsDialog.show();
			Ringtone ringtone = RingtoneManager.getRingtone(DataCenterContactActivity.this,
				RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
			if (ringtone != null) {
				ringtone.play();
			}
		}
	}
}
