package com.synapsense.mobile.service;

import java.io.IOException;
import java.lang.reflect.UndeclaredThrowableException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.googlecode.jsonrpc4j.ProxyUtil;
import com.synapsense.mobile.user.LoginStatus;
import com.synapsense.mobile.user.UserInfo;
import com.synapsense.mobile.user.UserPreference;

public class DefaultUserManager {

	public static final DefaultUserManager instance = new DefaultUserManager();
	private static final String openUrlBegin = "http://";
	private static final String secureUrlBegin = "https://";
	private static final String urlEndPrefix = "/mobile/";
	private static final String urlEndUserManager = "userManager";
	private static final String urlEndAlertManager = "alertManager";
	private static final String urlEndObjectManager = "objectManager";
	private static final String urlEndSystemStatusManager = "statusManager";
	private static final String urlEndConfigManager = "configManager";
	private UserManager userManager;
	private AlertManager alertManager;
	private ObjectManager objectManager;
	private SystemStatusManager systemStatusManager;
	private ConfigManager configManager;
	private String lastSessionId;
	private String lastServerName;
	private String lastUserName;
	private String lastPassword;
	private String urlPrefix;

	private DefaultUserManager() {
		// Install the all-trusting trust manager and all-verifying host-name verifier
		try {
			SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, new TrustManager[] {new EmptyX509TrustManager()}, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier(new EmptyHostnameVerifier());
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}
	}

	public UserInfo login(String serverName, String userName, String password, String deviceName, String deviceId,
			boolean isSecureConnection) {
		try {
			String urlBegin = isSecureConnection ? secureUrlBegin : openUrlBegin;
			urlPrefix = urlBegin + serverName + urlEndPrefix;
			JsonRpcHttpClient userManagerClient = new ExtJsonRpcHttpClient(new URL(urlPrefix + urlEndUserManager));
			userManager = ProxyUtil.createClientProxy(UserManager.class.getClassLoader(),
				UserManager.class, userManagerClient);
			if (userManager != null) {
				UserInfo userInfo = userManager.login(userName, password, deviceId, deviceName,
					UserPreferencesUtils.USER_PREFERENCE_NAMES);
				if (userInfo != null && userInfo.getLoginStatus() == LoginStatus.OK) {
					lastSessionId = userInfo.getjSessionId();
					lastServerName = serverName;
					lastUserName = userName;
					lastPassword = password;
					HashMap<String, String> sessionCookies = new HashMap<String, String>();
					sessionCookies.put("Cookie", "JSESSIONID=" + userInfo.getjSessionId());
					JsonRpcHttpClient alertManagerClient =
						new ExtJsonRpcHttpClient(new URL(urlPrefix + urlEndAlertManager), sessionCookies);
					alertManager = ProxyUtil.createClientProxy(AlertManager.class.getClassLoader(),
						AlertManager.class, false, alertManagerClient, ExtJsonRpcHttpClient.REQUEST_COOKIES);
					JsonRpcHttpClient objectManagerClient =
						new ExtJsonRpcHttpClient(new URL(urlPrefix + urlEndObjectManager), sessionCookies);
					objectManager = ProxyUtil.createClientProxy(ObjectManager.class.getClassLoader(),
						ObjectManager.class, false, objectManagerClient, ExtJsonRpcHttpClient.REQUEST_COOKIES);
					JsonRpcHttpClient systemStatusManagerClient =
						new ExtJsonRpcHttpClient(new URL(urlPrefix + urlEndSystemStatusManager),
						sessionCookies);
					systemStatusManager = ProxyUtil.createClientProxy(SystemStatusManager.class.getClassLoader(),
						SystemStatusManager.class, false, systemStatusManagerClient, ExtJsonRpcHttpClient.REQUEST_COOKIES);
					JsonRpcHttpClient configManagerManagerClient =
						new ExtJsonRpcHttpClient(new URL(urlPrefix + urlEndConfigManager), sessionCookies);
					configManager = ProxyUtil.createClientProxy(ConfigManager.class.getClassLoader(),
						ConfigManager.class, false, configManagerManagerClient, ExtJsonRpcHttpClient.REQUEST_COOKIES);
					// re-retrieve user manager for specified session id
					userManagerClient =
						new ExtJsonRpcHttpClient(new URL(urlPrefix + urlEndUserManager), sessionCookies);
					userManager = ProxyUtil.createClientProxy(UserManager.class.getClassLoader(),
						UserManager.class, false, userManagerClient, ExtJsonRpcHttpClient.REQUEST_COOKIES);
					UserPreferencesUtils.setUserPreferences(userInfo.getUserPreferences());
				}
				return userInfo;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean logout() {
		try {
			if (userManager != null && lastSessionId != null) {
				userManager.logout();
				userManager = null;
				alertManager = null;
				objectManager = null;
				systemStatusManager = null;
				lastSessionId = null;
				urlPrefix = null;
				return true;
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
			System.out.println("Cause:");
			e.getCause().printStackTrace();
		}
		return false;
	}

	public UserPreference getPreference(String userPreferenceName) {
		try {
			if (userManager != null) {
				return userManager.getPreference(userPreferenceName);
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return null;
	}

	public UserPreference[] getPreferences(String[] userPreferenceNames) {
		try {
			if (userManager != null) {
				return userManager.getPreferences(userPreferenceNames);
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return null;
	}

	public AlertManager getAlertManager() {
		return alertManager;
	}

	public ObjectManager getObjectManager() {
		return objectManager;
	}

	public SystemStatusManager getSystemStatusManager() {
		return systemStatusManager;
	}

	public ConfigManager getConfigManager() {
		return configManager;
	}

	public String getLastServerName() {
		return lastServerName;
	}

	public String getLastUserName() {
		return lastUserName;
	}

	public String getLastPassword() {
		return lastPassword;
	}

	public boolean isLoggedIn() {
		return lastSessionId != null;
	}

	public String getUrlPrefix() {
		return urlPrefix;
	}

	String getLastSessionId() {
		return lastSessionId;
	}

	static class EmptyHostnameVerifier implements HostnameVerifier {

		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	}

	static class EmptyX509TrustManager implements X509TrustManager {

		private static final X509Certificate[] empty = new X509Certificate[0];

		public X509Certificate[] getAcceptedIssuers() {
			// ESCA-JAVA0259:
			return empty;
		}

		public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {}

		public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {}
	}
}
