package com.synapsense.mobile.service;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;

import com.synapsense.mobile.R;
import com.synapsense.mobile.alert.Alert;
import com.synapsense.mobile.alert.AlertPriority;
import com.synapsense.mobile.alert.AlertStatus;
import com.synapsense.mobile.ui.LoginActivity;

// ESCA-JAVA0077:
public class AlertService extends Service {

	public static final String UPDATE_ACTION = "AlertManager.update";
	public static final String NEW_ALERTS_ACTION = "AlertManager.newAlerts";
	public static final long DEFAULT_REQUEST_PERIOD = 30 * 1000L;
	public static final int INIT_ALERT_COUNT = 20;
	public static final int ALERT_COUNT_INCREMENT = 20;
	public static final int ALL_PRIORITIES = AlertPriority.CRITICAL | AlertPriority.MAJOR | AlertPriority.MINOR |
		AlertPriority.INFORMATIONAL;
	public static final int DEFAULT_PRIORITIES = ALL_PRIORITIES;
	public static final int ALL_STATUSES = AlertStatus.OPENED | AlertStatus.ACKNOWLEDGED | AlertStatus.DISMISSED |
		AlertStatus.RESOLVED;
	public static final int DEFAULT_STATUSES = AlertStatus.OPENED | AlertStatus.ACKNOWLEDGED;
	public static final int NEW_ALERTS_NOTIFICATION_ID = 1;
	// private static final
	private final Timer alertServiceTimer = new Timer("AlertServiceTimer", true);
	private NotificationManager notificationManager;
	private PendingIntent notificationIntent;
	private AlarmServiceTimerTask alarmServiceTimerTask;
	private SharedPreferences prefs;

	public IBinder onBind(Intent intent) {
		return null;
	}

	public void onCreate() {
		super.onCreate();
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationIntent =
			PendingIntent.getActivity(this, 0,
				new Intent(this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP), 0);
		prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	}

	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStart(intent, startId);
		if (alarmServiceTimerTask != null) {
			stopLastTimerTask();
		}
		alarmServiceTimerTask = new AlarmServiceTimerTask();
		long period = requestPeriodToLong(prefs.getString("requestPeriod", null));
		alertServiceTimer.schedule(alarmServiceTimerTask, period, period);
		SharedPreferences.Editor prefsEditor = prefs.edit();
		prefsEditor.putBoolean("isAlertServiceRunning", true);
		prefsEditor.commit();
		return START_STICKY;
	}

	private void stopLastTimerTask() {
		if (alarmServiceTimerTask != null) {
			alarmServiceTimerTask.cancel();
			alarmServiceTimerTask = null;
		}
		alertServiceTimer.purge();
	}

	public static long requestPeriodToLong(String requestPeriod) {
		try {
			return requestPeriod != null ? Long.valueOf(requestPeriod.trim()) : DEFAULT_REQUEST_PERIOD;
		} catch (NumberFormatException e) {
			return DEFAULT_REQUEST_PERIOD;
		}
	}

	public void onDestroy() {
		super.onDestroy();
		Thread runner = new Thread() {

			public void run() {
				stopLastTimerTask();
				DefaultUserManager.instance.logout();
				SharedPreferences.Editor prefsEditor = prefs.edit();
				prefsEditor.putBoolean("isAlertServiceRunning", false);
				prefsEditor.commit();
			}
		};
		runner.start();
	}

	private class AlarmServiceTimerTask extends TimerTask {

		private static final long lag = 1000;
		private final Intent updateIntent = new Intent(UPDATE_ACTION);
		private final Intent newAlertsIntent = new Intent(NEW_ALERTS_ACTION);

		public void run() {
			Alert[] alerts =
				DefaultAlertManager.instance.getActiveAlerts(null,
					requestPeriodToLong(prefs.getString("requestPeriod", null)) + lag, DEFAULT_PRIORITIES,
					DEFAULT_STATUSES, null, null);
			if (alerts != null && alerts.length > 0) {
				String title = alerts.length > 1 ? getString(R.string.notificationTitle2) +
					" (" + alerts.length + ")" : getString(R.string.notificationTitle1);
				int priority = 0;
				for (Alert alert : alerts) {
					if (alert.getPriority() > priority) {
						priority = alert.getPriority();
					}
				}
				int miniIconId = DefaultAlertManager.getPriorityMiniResourceId(priority);
				int iconId = DefaultAlertManager.getPriorityResourceId(priority);
				if (!prefs.getBoolean("isApplicationInForeground", false)) {
					RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.notification);
					remoteViews.setImageViewResource(R.id.notificationIcon, miniIconId);
					remoteViews.setTextViewText(R.id.notificationTitle, title);
					remoteViews.setTextViewText(R.id.notificationText,
						getString(R.string.notificationNessage));
					Notification notification = new Notification();
					notification.contentView = remoteViews;
					notification.icon = miniIconId;
					notification.when = System.currentTimeMillis();
					notification.tickerText = title;
					notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
					notification.flags = Notification.FLAG_AUTO_CANCEL;
					notification.contentIntent = notificationIntent;
					notification.number = alerts.length;
					notificationManager.notify(NEW_ALERTS_NOTIFICATION_ID, notification);
				} else if (prefs.getBoolean("isPopupNotificationOn", true)) {
					newAlertsIntent.putExtra("message", title);
					newAlertsIntent.putExtra("iconId", iconId);
					AlertService.this.sendBroadcast(newAlertsIntent);
				}
			}
			AlertService.this.sendBroadcast(updateIntent);
		}
	};
}
