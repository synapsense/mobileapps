package com.synapsense.mobile.service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.lang.reflect.UndeclaredThrowableException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.googlecode.jsonrpc4j.JsonRpcHttpClient;

public class ExtJsonRpcHttpClient extends JsonRpcHttpClient {

	static final String CSRF_NONCE = "CSRF-Nonce";
	static final HashMap<String, String> REQUEST_COOKIES = new HashMap<String, String>();
	static final Map<String, List<String>> LAST_HEADER_FIELDS = new HashMap<String, List<String>>();

	public ExtJsonRpcHttpClient(URL serviceUrl) {
		this(serviceUrl, new HashMap<String, String>());
	}

	public ExtJsonRpcHttpClient(URL serviceUrl, Map<String, String> headers) {
		super(serviceUrl, headers);
		setConnectionTimeoutMillis(10000);
	}

	public Object invoke(String methodName, Object argument, Type returnType,
			Map<String, String> extraHeaders) throws Throwable {
		synchronized (LAST_HEADER_FIELDS) {
			List<String> headers = LAST_HEADER_FIELDS.get(CSRF_NONCE);
			if (headers != null && headers.size() > 0) {
				REQUEST_COOKIES.put(CSRF_NONCE, headers.get(0));
			}
			HttpURLConnection connection = openConnection(extraHeaders);
			// ESCA-JAVA0166: ESCA-JAVA0170:
			try {
				super.invoke(methodName, argument, connection.getOutputStream());
				LAST_HEADER_FIELDS.clear();
				Map<String, List<String>> headerFields = connection.getHeaderFields();
				if (headerFields != null) {
					LAST_HEADER_FIELDS.putAll(headerFields);
				}
				return super.readResponse(returnType, connection.getInputStream());
			} catch (Throwable e) {
				throw new UndeclaredThrowableException(e, "Remote method '" + methodName + "' cannot be executed");
			}
		}
	}

	private HttpURLConnection openConnection(Map<String, String> extraHeaders) throws IOException {
		HttpURLConnection con = (HttpURLConnection) getServiceUrl().openConnection(getConnectionProxy());
		con.setConnectTimeout(getConnectionTimeoutMillis());
		con.setReadTimeout(getReadTimeoutMillis());
		con.setAllowUserInteraction(false);
		con.setDefaultUseCaches(false);
		con.setDoInput(true);
		con.setDoOutput(true);
		con.setUseCaches(false);
		con.setInstanceFollowRedirects(true);
		con.setRequestMethod("POST");
		Map<String, String> headers = getHeaders();
		for (String key : headers.keySet()) {
			con.setRequestProperty(key, headers.get(key));
		}
		for (String key : extraHeaders.keySet()) {
			con.setRequestProperty(key, extraHeaders.get(key));
		}
		con.setRequestProperty("Content-Type", "application/json-rpc");
		con.connect();
		return con;
	}
}
