package com.synapsense.mobile.service;

import java.lang.reflect.UndeclaredThrowableException;

import com.synapsense.mobile.status.ComponentStatus;

public class DefaultSystemStatusManager {

	public static final DefaultSystemStatusManager instance = new DefaultSystemStatusManager();

	private DefaultSystemStatusManager() {}

	public ComponentStatus[] getEnvironmentalServerState() {
		try {
			SystemStatusManager systemStatusManager = DefaultUserManager.instance.getSystemStatusManager();
			if (systemStatusManager != null) {
				return systemStatusManager.getEnvironmentalServerState();
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return null;
	}

	public int getNumWarnings() {
		try {
			SystemStatusManager systemStatusManager = DefaultUserManager.instance.getSystemStatusManager();
			if (systemStatusManager != null) {
				return systemStatusManager.getNumWarnings();
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return -1;
	}
}
