package com.synapsense.mobile.service;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.UndeclaredThrowableException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

import com.synapsense.mobile.R;
import com.synapsense.mobile.interaction.ObjectRequest;
import com.synapsense.mobile.interaction.ResponseElement;
import com.synapsense.mobile.tree.LiveImagingType;
import com.synapsense.mobile.tree.ObjectStatus;
import com.synapsense.mobile.tree.TreeObject;

public class DefaultObjectManager {

	public static final DefaultObjectManager instance = new DefaultObjectManager();
	private static final int TIMEOUT = 10000;

	private DefaultObjectManager() {}

	public TreeObject[] getSiteTree() {
		try {
			ObjectManager objectManager = DefaultUserManager.instance.getObjectManager();
			if (objectManager != null) {
				return objectManager.getObjectTree();
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return null;
	}

	public TreeObject[] getChildren(String objectId, String[] types, boolean fullSubtree) {
		try {
			ObjectManager objectManager = DefaultUserManager.instance.getObjectManager();
			if (objectManager != null) {
				return objectManager.getChildren(objectId, types, fullSubtree);
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Object getPropertyValue(String objectId, String propertyName) {
		try {
			ObjectManager objectManager = DefaultUserManager.instance.getObjectManager();
			if (objectManager != null) {
				return objectManager.getPropertyValue(objectId, propertyName);
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Object[] getPropertyValues(String objectId, String[] propertyNames) {
		try {
			ObjectManager objectManager = DefaultUserManager.instance.getObjectManager();
			if (objectManager != null) {
				return objectManager.getPropertyValues(objectId, propertyNames);
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Object[][] getManyObjectsPropertyValues(String[] objectIds, String[] propertyNames) {
		try {
			ObjectManager objectManager = DefaultUserManager.instance.getObjectManager();
			if (objectManager != null) {
				return objectManager.getManyObjectsPropertyValues(objectIds, propertyNames);
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ResponseElement getObjectData(ObjectRequest objectRequest) {
		try {
			ObjectManager objectManager = DefaultUserManager.instance.getObjectManager();
			if (objectManager != null) {
				return objectManager.getObjectData(objectRequest);
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ResponseElement getObjectsData(ObjectRequest[] objectRequest) {
		try {
			ObjectManager objectManager = DefaultUserManager.instance.getObjectManager();
			if (objectManager != null) {
				return objectManager.getObjectsData(objectRequest);
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Object[] getObjectsData(String objectId, String[] objectTypes) {
		try {
			ObjectManager objectManager = DefaultUserManager.instance.getObjectManager();
			if (objectManager != null) {
				return objectManager.getFloorPlanData(objectId, objectTypes);
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return null;
	}

    public LiveImagingType[] getLiveImagingTypes() {
		try {
			ObjectManager objectManager = DefaultUserManager.instance.getObjectManager();
			if (objectManager != null) {
				return objectManager.getLiveImagingTypes();
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return null;
    }

	public String getFloorplan(String objectId, int dataClassId, int layer) {
		try {
			ObjectManager objectManager = DefaultUserManager.instance.getObjectManager();
			if (objectManager != null) {
				return objectManager.getFloorplan(objectId, dataClassId, layer);
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return null;
	}

	public byte[] getFloorplan(String objectId) {
		try {
			URL url = new URL(DefaultUserManager.instance.getUrlPrefix() + "getImage?id=" + objectId);
			URLConnection connection = url.openConnection();
			if (connection != null) {
				connection.setConnectTimeout(TIMEOUT);
				connection.setReadTimeout(TIMEOUT);
				connection.setRequestProperty("Cookie", "JSESSIONID=" + DefaultUserManager.instance.getLastSessionId());
				List<String> headers = ExtJsonRpcHttpClient.LAST_HEADER_FIELDS.get(ExtJsonRpcHttpClient.CSRF_NONCE);
				if (headers != null && headers.size() > 0) {
					connection.setRequestProperty(ExtJsonRpcHttpClient.CSRF_NONCE, headers.get(0));
				}
				connection.connect();
				int length = connection.getContentLength();
				byte[] buffer = new byte[length];
				InputStream is = connection.getInputStream();
				int index = 0, n;
				if (is != null) {
					do {
						n = is.read(buffer, index, buffer.length - index);
						index += n;
					} while (n >= 0 && index < length);
					is.close();
				}
				ExtJsonRpcHttpClient.LAST_HEADER_FIELDS.clear();
				Map<String, List<String>> headerFields = connection.getHeaderFields();
				if (headerFields != null) {
					ExtJsonRpcHttpClient.LAST_HEADER_FIELDS.putAll(headerFields);
				}
				return buffer;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}			
		return null;
	}

	public boolean isLiveImagingAvailable(String objectId) {
		try {
			ObjectManager objectManager = DefaultUserManager.instance.getObjectManager();
			if (objectManager != null) {
				return objectManager.isLiveImagingAvailable(objectId);
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static int getDataCenterResourceId(int alertNumber, int status) {
		if (alertNumber > 0) {
			return R.drawable.data_center_alert;
		} else if (status == ObjectStatus.NOT_RECEIVING_DATA) {
			return R.drawable.data_center_warning;
		} else {
			return R.drawable.data_center_normal;
		}
	}
}
