package com.synapsense.mobile.service;

import java.lang.reflect.UndeclaredThrowableException;

import com.synapsense.mobile.R;
import com.synapsense.mobile.alert.Alert;
import com.synapsense.mobile.alert.AlertPriority;
import com.synapsense.mobile.alert.AlertStatus;

public class DefaultAlertManager {

	public static final DefaultAlertManager instance = new DefaultAlertManager();

	private DefaultAlertManager() {}

	public int getTotalNumActiveAlerts() {
		return getNumActiveAlerts(null, null, null, null);
	}

	public int getNumActiveAlerts(String objId, Long period, Integer priority, Integer status) {
		try {
			AlertManager alertManager = DefaultUserManager.instance.getAlertManager();
			if (alertManager != null) {
				int alertCount = alertManager.getNumActiveAlerts(objId, period, priority, status);
				// System.out.println("Number of alerts: " + alertCount); // FOR
				// DEBUG
				return alertCount;
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public Alert[] getActiveAlerts(String objId, Long period, Integer priority, Integer status, Integer start,
		Integer count) {
		try {
			AlertManager alertManager = DefaultUserManager.instance.getAlertManager();
			if (alertManager != null) {
//				System.out.println("objId=" + objId); // FOR DEBUG (BEGIN)
//				System.out.println("priority=" + priority);
//				System.out.println("status=" + status); // FOR DEBUG (END)
				Alert[] alerts = alertManager.getActiveAlerts(objId, period, priority, status, start, count);
//				System.out.println("alertCount=" + (alerts != null ? alerts.length : "null")); // FOR DEBUG (BEGIN)
//				if (alerts != null) {
//					for (Alert alert : alerts) {
//						System.out.println(alert);
//					}
//				} // FOR DEBUG (END)
				return alerts;
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Long beforePeriodToLong(String beforePeriod) {
		if (beforePeriod == null) {
			return null;
		}
		try {
			long n = Long.valueOf(beforePeriod.trim());
			return n > 0 ? n : null;
		} catch (NumberFormatException e) {
			return null;
		}
	}

	public boolean acknowledge(int alertId, String comment) {
		AlertManager alertManager = DefaultUserManager.instance.getAlertManager();
		if (alertManager != null) {
			return alertManager.acknowledge(alertId, comment);
		}
		return false;
	}

	public boolean resolve(int alertId, String comment) {
		AlertManager alertManager = DefaultUserManager.instance.getAlertManager();
		if (alertManager != null) {
			return alertManager.resolve(alertId, comment);
		}
		return false;
	}

	public boolean dismiss(int alertId, String comment) {
		AlertManager alertManager = DefaultUserManager.instance.getAlertManager();
		if (alertManager != null) {
			return alertManager.dismiss(alertId, comment);
		}
		return false;
	}

	public boolean dismissAllAlerts(String objectId, String comment) {
		AlertManager alertManager = DefaultUserManager.instance.getAlertManager();
		if (alertManager != null) {
			alertManager.dismissAllAlerts(objectId, comment);
			return true;
		}
		return false;
	}

	public static int getPriorityResourceId(int priority) {
		if (priority == AlertPriority.INFORMATIONAL) {
			return R.drawable.info;
		} else if (priority == AlertPriority.MINOR) {
			return R.drawable.minor;
		} else if (priority == AlertPriority.MAJOR) {
			return R.drawable.major;
		} else {
			return R.drawable.critical;
		}
	}

	public static int getPriorityMiniResourceId(int priority) {
		if (priority == AlertPriority.INFORMATIONAL) {
			return R.drawable.info_small;
		} else if (priority == AlertPriority.MINOR) {
			return R.drawable.minor_small;
		} else if (priority == AlertPriority.MAJOR) {
			return R.drawable.major_small;
		} else {
			return R.drawable.critical_small;
		}
	}

	public static int priorityToStringResourceId(int priority) {
		if (priority == AlertPriority.CRITICAL) {
			return R.string.criticalAlertPrioroty;
		} else if (priority == AlertPriority.MAJOR) {
			return R.string.majorAlertPrioroty;
		} else if (priority == AlertPriority.MINOR) {
			return R.string.manorAlertPrioroty;
		} else {
			return R.string.informatinalAlertPrioroty;
		}
	}

	public static int statusToStringResourceId(int status) {
		if (status == AlertStatus.OPENED) {
			return R.string.openedAlertStatus;
		} else if (status == AlertStatus.ACKNOWLEDGED) {
			return R.string.acknowledgedAlertStatus;
		} else if (status == AlertStatus.DISMISSED) {
			return R.string.dismissedAlertStatus;
		} else {
			return R.string.resolvedAlertStatus;
		}
	}
}
