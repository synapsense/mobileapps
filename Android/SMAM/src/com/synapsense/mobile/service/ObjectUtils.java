package com.synapsense.mobile.service;

import java.util.ArrayList;
import java.util.List;

import com.synapsense.mobile.interaction.ElementAttribute;
import com.synapsense.mobile.interaction.ResponseElement;

public abstract class ObjectUtils {

	private ObjectUtils() {}

	public static List<ResponseElement> getChildren(ResponseElement response, String objectId) {
		List<ResponseElement> children = new ArrayList<ResponseElement>();
		ResponseElement treeObjectResponse = getObject(response, objectId);
		if (treeObjectResponse != null && treeObjectResponse.getSubElements() != null) {
			for (ResponseElement subElementResponse : treeObjectResponse.getSubElements()) {
				if ("children".equals(subElementResponse.getName()) && subElementResponse.getSubElements() != null) {
					for (ResponseElement childObjectResponse : subElementResponse.getSubElements()) {
						children.add(childObjectResponse);
					}
				}
			}
		}
		return children;
	}

	public static int getChildrenNumber(ResponseElement response, String typeName) {
		return getChildrenNumber(response, typeName, 0);
	}

	public static List<ResponseElement> getObjects(ResponseElement response, String typeName) {
		return getObjects(response, typeName, new ArrayList<ResponseElement>());
	}

	public static ResponseElement getObject(ResponseElement response, String objectId) {
		if (response != null && objectId != null) {
			if (objectId.equals(getAttr(response, "id"))) {
				return response;
			} else if (response.getSubElements() != null) {
				for (ResponseElement subElementResponse : response.getSubElements()) {
					ResponseElement treeObject = getObject(subElementResponse, objectId);
					if (treeObject != null) {
						return treeObject;
					}
				}
			}
		}
		return null;
	}

	public static ResponseElement getObjectProperty(ResponseElement response, String propertyName) {
		if (response != null && response.getSubElements() != null) {
			for (ResponseElement subElementResponse : response.getSubElements()) {
				if (subElementResponse.getName().equals(propertyName)) {
					return subElementResponse;
				}
			}
		}
		return null;
	}

	public static Object getAttr(ResponseElement response, String attrName) {
		if (response != null && response.getAttributes() != null) {
			for (ElementAttribute attr : response.getAttributes()) {
				if (attr.getName().equals(attrName)) {
					return attr.getValue();
				}
			}
		}
		return null;
	}

	private static int getChildrenNumber(ResponseElement response, String typeName, int count) {
		if (response != null && response.getSubElements() != null && typeName != null) {
			for (ResponseElement subElementResponse : response.getSubElements()) {
				if ("children".equals(subElementResponse.getName()) &&
						typeName.equals(getAttr(subElementResponse, "typeName"))) {
					if (subElementResponse.getSubElements() != null) {
						for (ResponseElement childResponse : subElementResponse.getSubElements()) {
							if (childResponse.getName().equals(typeName)) {
								count++;
								count += getChildrenNumber(childResponse, typeName, 0);
							}
						}
					} else {
						count += (Integer) getAttr(subElementResponse, "totalNumber");
					}
				} else {
					count += getChildrenNumber(subElementResponse, typeName, 0);
				}
			}
		}
		return count;
	}

	private static List<ResponseElement> getObjects(ResponseElement response, String typeName,
			List<ResponseElement> children) {
		if (response != null && typeName != null) {
			if (response.getName().equals(typeName)) {
				children.add(response);
			}
			if (response.getSubElements() != null) {
				for (ResponseElement subElementResponse : response.getSubElements()) {
					if ("children".equals(subElementResponse.getName()) &&
							typeName.equals(getAttr(subElementResponse, "typeName"))) {
						if (subElementResponse.getSubElements() != null) {
							for (ResponseElement childResponse : subElementResponse.getSubElements()) {
								if (childResponse.getName().equals(typeName)) {
									children.add(childResponse);
								}
							}
						}
					} else {
						getObjects(subElementResponse, typeName, children);
					}
				}
			}
		}
		return children;
	}
}
