package com.synapsense.mobile.service;

import java.lang.reflect.UndeclaredThrowableException;

public class DefaultConfigManager {

	public static final DefaultConfigManager instance = new DefaultConfigManager();

	private DefaultConfigManager() {}

	public String[] getFloorplanTypes() {
		try {
			ConfigManager configManager = DefaultUserManager.instance.getConfigManager();
			if (configManager != null) {
				return configManager.getFloorplanTypes();
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String[] getUIPrivileges() {
		try {
			ConfigManager configManager = DefaultUserManager.instance.getConfigManager();
			if (configManager != null) {
				return configManager.getUIPrivileges();
			}
		} catch (UndeclaredThrowableException e) {
			e.printStackTrace();
		}
		return null;
	}
}
