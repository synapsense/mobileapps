package com.synapsense.mobile.service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;

import com.synapsense.mobile.R;
import com.synapsense.mobile.user.UserPreference;

public abstract class UserPreferencesUtils {

	public static final String[] USER_PREFERENCE_NAMES = {"DateFormat", "DecimalSeparator", "DecimalFormat",
		"UnitsSystem", "SavedLanguage"};
	private static final String[] unitTypeIds = {"SI", "Server"};
	private static final DecimalFormat decimalFormat = new DecimalFormat("##################");
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static int unitsSystemId;
	private static Locale serverLocale = Locale.getDefault();
	private static Locale currentLocale = Locale.getDefault();

	static {
		decimalFormat.setMinimumIntegerDigits(1);
	}

	private UserPreferencesUtils() {}

	public static String formatDate(long timeStamp) {
		return dateFormat.format(new Date(timeStamp));
	}

	public static String formatNumber(double number) {
		return decimalFormat.format(number);
	}

	public static String formatLength(double length, Context context) {
		return context != null ? decimalFormat.format(length) + " " +
			context.getString(R.string.unit_length).split(";")[unitsSystemId] : decimalFormat.format(length);
	}

	public static String formatSquare(double square, Context context) {
		return context != null ? decimalFormat.format(square) + " " +
			context.getString(R.string.unit_square).split(";")[unitsSystemId] : decimalFormat.format(square);
	}

	public static String formatSquare(long energy, Context context) {
		return context != null ? energy + " " + context.getString(R.string.unit_square).split(";")[unitsSystemId] :
			String.valueOf(energy);
	}

	public static String formatTeperature(double teperature, Context context) {
		return context != null ? decimalFormat.format(teperature) + " " +
			context.getString(R.string.unit_teperature).split(";")[unitsSystemId] : decimalFormat.format(teperature);
	}

	public static String formatPresure(double presure, Context context) {
		return context != null ? decimalFormat.format(presure) + " " +
			context.getString(R.string.unit_presure).split(";")[unitsSystemId] : decimalFormat.format(presure);
	}

	public static String formatPower(double power, Context context) {
		return context != null ? decimalFormat.format(power) + " " +
			context.getString(R.string.unit_power).split(";")[unitsSystemId] : decimalFormat.format(power);
	}

	public static String formatPower(long power, Context context) {
		return context != null ? power + " " + context.getString(R.string.unit_power).split(";")[unitsSystemId] :
			String.valueOf(power);
	}

	public static String formatEnergy(double energy, Context context) {
		return context != null ? decimalFormat.format(energy) + " " +
			context.getString(R.string.unit_energy).split(";")[unitsSystemId] : decimalFormat.format(energy);
	}

	public static String formatEnergy(long energy, Context context) {
		return context != null ? energy + " " + context.getString(R.string.unit_energy).split(";")[unitsSystemId] :
			String.valueOf(energy);
	}

	public static void refresh() {
		if (DefaultUserManager.instance != null) {
			UserPreference[] prefs = DefaultUserManager.instance.getPreferences(USER_PREFERENCE_NAMES);
			if (prefs != null && prefs.length > 4) {
				dateFormat = new SimpleDateFormat(trimDoubleQuotes(prefs[0].getValue()));
				decimalFormat.getDecimalFormatSymbols().setDecimalSeparator(prefs[1].getValue().charAt(0));
				decimalFormat.setMaximumFractionDigits(prefs[2] != null ? Integer.valueOf(prefs[2].getValue()) : 2);
				decimalFormat.setMinimumFractionDigits(decimalFormat.getMaximumFractionDigits());
				String unitsSystem = trimDoubleQuotes(prefs[3].getValue());
				for (unitsSystemId = 0 ; unitsSystemId < unitTypeIds.length; ++unitsSystemId) {
					if (unitTypeIds[unitsSystemId].equals(unitsSystem)) {
						break;
					}
				}
				String[] localeParts = trimDoubleQuotes(prefs[4].getValue()).split("_");
				serverLocale = localeParts.length > 2 ? new Locale(localeParts[0], localeParts[1], localeParts[2]) :
					new Locale(localeParts[0], localeParts[1]);
			}
		}
	}

	public static void setUserPreferences(UserPreference[] userPreferences) {
		if (userPreferences != null) {
			for (UserPreference userPreference : userPreferences) {
				if (userPreference != null) {
					if (USER_PREFERENCE_NAMES[0].equals(userPreference.getPreferenceName())) {
						dateFormat = new SimpleDateFormat(trimDoubleQuotes(userPreference.getValue()));
					}
					if (USER_PREFERENCE_NAMES[1].equals(userPreference.getPreferenceName())) {
						decimalFormat.getDecimalFormatSymbols().setDecimalSeparator(userPreference.getValue().charAt(0));
					}
					if (USER_PREFERENCE_NAMES[2].equals(userPreference.getPreferenceName())) {
						decimalFormat.setMaximumFractionDigits(Integer.valueOf(userPreference.getValue()));
						decimalFormat.setMinimumFractionDigits(decimalFormat.getMaximumFractionDigits());
					}
					if (USER_PREFERENCE_NAMES[3].equals(userPreference.getPreferenceName())) {
						String unitsSystem = trimDoubleQuotes(userPreference.getValue());
						for (unitsSystemId = 0 ; unitsSystemId < unitTypeIds.length; ++unitsSystemId) {
							if (unitTypeIds[unitsSystemId].equals(unitsSystem)) {
								break;
							}
						}
					}
					if (USER_PREFERENCE_NAMES[4].equals(userPreference.getPreferenceName())) {
						String[] localeParts = trimDoubleQuotes(userPreference.getValue()).split("_");
						serverLocale =
							localeParts.length > 2 ? new Locale(localeParts[0], localeParts[1], localeParts[2]) :
								new Locale(localeParts[0], localeParts[1]);
						setCurrentLocale(serverLocale);
					}
				}
			}
		}
	}

	private static String trimDoubleQuotes(String str) {
		if (str != null) {
			if (str.length() > 0 && str.charAt(0) == '"') {
				str = str.substring(1);
			}
			if (str.length() > 0 && str.charAt(str.length() - 1) == '"') {
				str = str.substring(0, str.length() - 1);
			}
		}
		return str;
	}

	public static Locale getServerLocale() {
		return serverLocale;
	}

	public static Locale getCurrentLocale() {
		return currentLocale;
	}

	public static void setCurrentLocale(Locale newCurrentLocale) {
		UserPreferencesUtils.currentLocale = newCurrentLocale != null ? newCurrentLocale : Locale.getDefault();
	}
}
