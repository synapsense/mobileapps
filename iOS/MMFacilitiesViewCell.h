//
//  MMFacilitiesViewCell.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMBadgeView.h"
#import "MMTitleLabel.h"

@interface MMFacilitiesViewCell : UITableViewCell
{
    MMBadgeView* _numAlertsBadge;
    MMBadgeView* _numWarningsBadge;
    UIImageView* _activeAlertsView;
    UIImageView* _warningsView;
    MMTitleLabel*     _dcNameLabel;
    UILabel*     _zoneNumLabel;
    UILabel*     _separator;
}

- (void)setNumAlerts:(NSUInteger)numAlerts;
- (void)setNumWarnings:(NSUInteger)numWarnings;
- (void)setDCName:(NSString*)dcName;
- (void)setZoneNumber:(NSUInteger)zoneNum;

@end
