//
//  MMFacilitiesViewCell.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMFacilitiesViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"

#define STATUS_IMAGE_SIZE 35
#define SEPARATOR_HEIGHT  1
#define CELL_HEIGHT 60.0f
#define INDENTATION 10.0f

@implementation MMFacilitiesViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self) 
    {
        //data center name label
        _dcNameLabel = [[MMTitleLabel alloc] init];
        [self addSubview:_dcNameLabel];
        [_dcNameLabel release];

        //draw separator
        _separator = [[UILabel alloc] initWithFrame:CGRectMake(HORIZONTAL_INDENT, CELL_HEIGHT - SEPARATOR_HEIGHT, self.frame.size.width - 2*HORIZONTAL_INDENT, SEPARATOR_HEIGHT)];
        [_separator setBackgroundColor:[UIColor darkGrayColor]];
        _separator.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self addSubview:_separator];
        [_separator release];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)setNumAlerts:(NSUInteger)numAlerts
{
    if (numAlerts > 0)
    {
        if (!_numAlertsBadge)
        {
            _numAlertsBadge = [[MMBadgeView alloc] initWithInteger:numAlerts];
            _activeAlertsView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Alert-red.png"]];
            [_activeAlertsView setBounds:CGRectMake(0, 0, STATUS_IMAGE_SIZE, STATUS_IMAGE_SIZE)];
            [_activeAlertsView addSubview:_numAlertsBadge];
            [_numAlertsBadge setBadgeFrameColor:[UIColor whiteColor]];
            [_numAlertsBadge setBadgeTextColor:[UIColor whiteColor]];
            [_numAlertsBadge release];
            [self addSubview:_activeAlertsView];
            [_activeAlertsView release];
            
            //active alerts layout
            [_activeAlertsView setCenter:CGPointMake(self.frame.size.width - STATUS_IMAGE_SIZE * 3 / 2 - HORIZONTAL_INDENT - INDENTATION, self.frame.size.height / 2)];
            _activeAlertsView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
            [_numAlertsBadge setCenter:CGPointMake(_activeAlertsView.frame.size.width - _numAlertsBadge.frame.size.width / 2,_numAlertsBadge.frame.size.height / 6)];
            _numAlertsBadge.layer.shadowColor = [UIColor blackColor].CGColor;
            _numAlertsBadge.layer.shadowOffset = CGSizeMake(0, 0);
            _numAlertsBadge.layer.shadowOpacity = 0.5;
        }
        else
        {
            [_numAlertsBadge setBadgeNumber:numAlerts];
        }
    }
}

- (void)setNumWarnings:(NSUInteger)numWarnings
{
    //comment untill will be created interface by server side
    if (numWarnings > 0) 
    {
        if (!_numWarningsBadge)
        {
            _numWarningsBadge = [[MMBadgeView alloc] initWithInteger:numWarnings];
            _warningsView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Warning-yellow.png"]];
            [_warningsView setBounds:CGRectMake(0, 0, STATUS_IMAGE_SIZE, STATUS_IMAGE_SIZE)];
            [_warningsView addSubview:_numWarningsBadge];
            [_numWarningsBadge release];
            [_numWarningsBadge setBadgeFrameColor:[UIColor whiteColor]];
            [_numWarningsBadge setBadgeTextColor:[UIColor whiteColor]];
            [self addSubview:_warningsView];
            [_warningsView release];
            
            //warnings status layout
            [_warningsView setCenter:CGPointMake(self.frame.size.width / 4 * 3 + STATUS_IMAGE_SIZE, self.frame.size.height / 2)];
            _warningsView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
            [_numWarningsBadge setCenter:CGPointMake(_warningsView.frame.size.width - _numWarningsBadge.frame.size.width / 2, _numWarningsBadge.frame.size.height / 2)];
        }
        else
        {
            [_numWarningsBadge setBadgeNumber:numWarnings];
        }
    }
    else
    {
        _warningsView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Warning-yellow.png"]];
        [_warningsView setBounds:CGRectMake(0, 0, STATUS_IMAGE_SIZE, STATUS_IMAGE_SIZE)];
        //        _warningsView.layer.shadowColor = [UIColor colorWithRed:51.0f/255.0f green:153.0f/255.0f blue:204.f/255.0f alpha:1.0f].CGColor;
        //        _warningsView.layer.shadowOffset = CGSizeMake(2, 2);
        //        _warningsView.layer.shadowOpacity = 10;
        [self addSubview:_warningsView];
        [_warningsView release];
        [_warningsView setCenter:CGPointMake(self.frame.size.width - STATUS_IMAGE_SIZE / 2.0f - HORIZONTAL_INDENT, self.frame.size.height / 2.0f)];
        _warningsView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    }
}

- (void)setDCName:(NSString *)dcName
{    
    //dc name label
    _dcNameLabel.text = dcName;
    [_dcNameLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [_dcNameLabel sizeToFit];
}

- (void)setZoneNumber:(NSUInteger)zoneNum
{
    //number of zones label
    self.detailTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"zones", @""),zoneNum];
    [self.detailTextLabel setFont:[UIFont systemFontOfSize:15]];
    [self.detailTextLabel setTextColor:[UIColor colorWithRed:51.0f/255.0f green:153.0f/255.0f blue:204.f/255.0f alpha:1.0f]];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [_dcNameLabel setFrame:CGRectMake(HORIZONTAL_INDENT, self.frame.size.height / 2 - _dcNameLabel.frame.size.height, self.frame.size.width - STATUS_IMAGE_SIZE -  2 * HORIZONTAL_INDENT, _dcNameLabel.frame.size.height)];
    [self.detailTextLabel setFrame:CGRectMake(HORIZONTAL_INDENT, self.frame.size.height / 2, self.detailTextLabel.frame.size.width, self.detailTextLabel.frame.size.height)];
//    [_separator setCenter:CGPointMake(_separator.center.x, self.frame.size.height - SEPARATOR_HEIGHT*2)];
    [_activeAlertsView setCenter:CGPointMake(_activeAlertsView.center.x, self.frame.size.height / 2)];
    [_warningsView setCenter:CGPointMake(_warningsView.center.x, self.frame.size.height / 2)];
}

@end