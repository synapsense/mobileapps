//
//  AppUtility.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppUtility : NSObject

+ (UIImage*)getStretchableImage:(NSString*)fileName;
+ (NSString*)safeString:(NSString*)string;

@end


@interface CustomButtonWithIncreasedHitArea : UIButton 
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event;
@property (nonatomic, assign) float zoom;
@property (nonatomic, assign) float minCoordinate;
@property (nonatomic, assign) float maxCoordinate;

@end