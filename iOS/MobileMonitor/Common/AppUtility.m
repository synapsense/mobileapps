//
//  AppUtility.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "AppUtility.h"
#import "Constants.h"

@implementation AppUtility

+ (UIImage*)getStretchableImage:(NSString*)fileName
{
    UIImage* resultImage = nil;
    UIImage* image = [UIImage imageNamed:fileName];
    if (image)
    {
        UIImage* imageWithScale = [UIImage imageWithCGImage:image.CGImage scale:2.0 orientation:UIImageOrientationUp];
        resultImage = [imageWithScale stretchableImageWithLeftCapWidth:imageWithScale.size.width/2 topCapHeight:0.0];
    }
    
    return resultImage;
}

+ (NSString*)safeString:(NSString*)string
{
    if (!string) 
    {
        return @"";
    }
    return string;
}

@end


@implementation CustomButtonWithIncreasedHitArea

@synthesize zoom;
@synthesize minCoordinate;
@synthesize maxCoordinate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.zoom = 1;
    [self setShowsTouchWhenHighlighted:YES];
    
    if(self.frame.size.width < REAL_HIT_AREA_FOR_BUTTONS)
    {
        self.minCoordinate = (self.frame.size.width - REAL_HIT_AREA_FOR_BUTTONS)/2;
        self.maxCoordinate = self.frame.size.width + (REAL_HIT_AREA_FOR_BUTTONS - self.frame.size.width)/2;
    }
    else
    {
        self.minCoordinate = 0;
        self.maxCoordinate = REAL_HIT_AREA_FOR_BUTTONS;
    }
       
    if(self)
    {
        return self;
    }
    return nil;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    [super pointInside:point withEvent:UIEventTypeTouches];
    if((((point.x) >= (self.minCoordinate)/zoom) && 
        ((point.x) <=(self.maxCoordinate)/zoom)) 
       &&
       (((point.y) >= (self.minCoordinate)/zoom) && 
        ((point.y) <=(self.maxCoordinate)/zoom)))
    {
        return YES;
    }
    return NO;
}

@end


