//
//  AppDelegate.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "AppDelegate.h"
#import "MMSDK.h"
#import "LoginViewController.h"
#import "BaseViewController.h"

@interface AppDelegate ()

- (void)initializeProgressPopup;

@end

@implementation AppDelegate

@synthesize window = _window;
@synthesize navigaionController;
@synthesize timerDataUpdater;
@synthesize applicationPreferences;
@synthesize alertStatusFilter;
@synthesize alertPriorityFilter;
@synthesize alertSorting;
@synthesize floorPlanFilter;
@synthesize userPreferences;
@synthesize uiPrivileges;
@synthesize securedPort;
@synthesize unsecuredPort;
@synthesize mapCoordinateRegion = _mapCoordinateRegion;

#define TRANSITION_DURATION        0.5
#define DEFAULT_REQUEST_FREQUENCY  30
#define DEFAULT_TIME_PERIOD        1
#define DEFAULT_USER_SERVICE       @"192.168.102.201:8080"
#define DEFAULT_UNSECURED_PORT     8080
#define DEFAULT_SECURED_PORT       8443
#define DEFAULT_LATITUDE           0
#define DEFAULT_LONGITUDE           0

+ (AppDelegate *)sharedInstance
{
    return [[UIApplication sharedApplication] delegate];
}

- (void)setDefaultPreferencesIfNecessary
{
    if (![applicationPreferences integerForKey:REQUEST_FREQUENCY]) 
    {
        [applicationPreferences setInteger:DEFAULT_REQUEST_FREQUENCY forKey:REQUEST_FREQUENCY];
    }
    
    if (![applicationPreferences integerForKey:REQUEST_TIME_PERIOD]) 
    {
        [applicationPreferences setInteger:DEFAULT_TIME_PERIOD forKey:REQUEST_TIME_PERIOD];
    }
    if (![applicationPreferences objectForKey:POP_UP_NOTIFICATION]) {
        [applicationPreferences setBool:YES forKey:POP_UP_NOTIFICATION];
    }
    if (![applicationPreferences objectForKey:MAP_PRESENTATION]) {
        
        [applicationPreferences setBool:NO forKey:MAP_PRESENTATION];
    }
    if (![applicationPreferences integerForKey:PRIORITY_FILTER]) {
        [applicationPreferences setInteger:MMAlertPriorityCritical | MMAlertPriorityInformational | MMAlertPriorityMajor | MMAlertPriorityMinor forKey:PRIORITY_FILTER];
    }
    if (![applicationPreferences integerForKey:STATUS_FILTER]) {
        [applicationPreferences setInteger:MMAlertStatusAcknowledged | MMAlertStatusOpened forKey:STATUS_FILTER];
    }
    if (![applicationPreferences integerForKey:ALERT_SORTING_CONDITION]) {
        [applicationPreferences setInteger:MMAlertDescendingSortTimestamp forKey:ALERT_SORTING_CONDITION];
    }
    if (![applicationPreferences objectForKey:USE_HTTPS]) {
        [applicationPreferences setBool:YES forKey:USE_HTTPS];
    }
    if (![applicationPreferences integerForKey:FLOORPLAN_FILTER]) {
        [applicationPreferences setInteger:~floorPlanFilter forKey:FLOORPLAN_FILTER];
    }
    if (![applicationPreferences integerForKey:UNSECURED_PORT]) {
        [applicationPreferences setInteger:DEFAULT_UNSECURED_PORT forKey:UNSECURED_PORT];
    }
    if (![applicationPreferences integerForKey:SECURED_PORT]) {
        [applicationPreferences setInteger:DEFAULT_SECURED_PORT forKey:SECURED_PORT];
    }
    if (![applicationPreferences integerForKey:UI_THEME]) {
        [applicationPreferences setInteger:UIThemeNightMode forKey:UI_THEME];
    }
    if (![applicationPreferences doubleForKey:MAP_VIEW__REGION_DELTA_LATITUDE]) {
        [applicationPreferences setDouble:MAP_VIEW_REGION_UNKNOWN forKey:MAP_VIEW__REGION_DELTA_LATITUDE];
    }
    if (![applicationPreferences doubleForKey:MAP_VIEW__REGION_DELTA_LONGITUDE]) {
        [applicationPreferences setDouble:MAP_VIEW_REGION_UNKNOWN forKey:MAP_VIEW__REGION_DELTA_LONGITUDE];
    }
    if (![applicationPreferences doubleForKey:MAP_VIEW__REGION_LONGITUDE]) {
        [applicationPreferences setDouble:DEFAULT_LONGITUDE forKey:MAP_VIEW__REGION_LONGITUDE];
    }
    if (![applicationPreferences doubleForKey:MAP_VIEW_REGION_LATITUDE]) {
        [applicationPreferences setDouble:DEFAULT_LATITUDE forKey:MAP_VIEW_REGION_LATITUDE];
    }
    //Uncomment to use "192.168.102.201:8080" as a default server
    /*if (![applicationPreferences objectForKey:@"Server URL"]) {
        [applicationPreferences setObject:DEFAULT_USER_SERVICE forKey:@"Server URL"];
    }*/
}

- (void)dealloc
{
    if ([timerDataUpdater isValid]) {
        [timerDataUpdater invalidate];
    }
    self.userPreferences = nil;
    timerDataUpdater = nil;
    [applicationPreferences release];
    [navigaionController release];
    [_window release];
    [super dealloc];
}

- (void)showUpdatingPopup
{
    [_popupWindow makeKeyAndVisible];
}

- (void)hideUpdatingPopup
{
    [self.window makeKeyAndVisible];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    _loginViewController = [[LoginViewController alloc] init];
    navigaionController = [[UINavigationController alloc] initWithRootViewController:_loginViewController];
    if ([navigaionController.navigationBar respondsToSelector:@selector(setBarTintColor:)]) {
        [[navigaionController navigationBar] setBarTintColor:[UIColor darkGrayColor]];
        [[navigaionController navigationBar] setTitleTextAttributes:@{UITextAttributeTextColor : [UIColor whiteColor]}];
        [[navigaionController navigationBar] setTintColor:[UIColor whiteColor]];
        [[navigaionController navigationBar] setTranslucent:NO];
        
    } else {
        [navigaionController.navigationBar setTintColor:[UIColor darkGrayColor]];
    }
    [_loginViewController release];
    applicationPreferences = [[NSUserDefaults alloc] init];
    [self setDefaultPreferencesIfNecessary];
    self.alertSorting = [applicationPreferences integerForKey:ALERT_SORTING_CONDITION];
    self.alertPriorityFilter = [applicationPreferences integerForKey:PRIORITY_FILTER];
    self.alertStatusFilter = [applicationPreferences integerForKey:STATUS_FILTER];
    self.floorPlanFilter = [applicationPreferences integerForKey:FLOORPLAN_FILTER];
    self.securedPort = [applicationPreferences integerForKey:SECURED_PORT];
    self.unsecuredPort = [applicationPreferences integerForKey:UNSECURED_PORT];
    _mapCoordinateRegion.center.longitude = [applicationPreferences doubleForKey:MAP_VIEW__REGION_LONGITUDE];
    _mapCoordinateRegion.center.latitude = [applicationPreferences doubleForKey:MAP_VIEW_REGION_LATITUDE];
    _mapCoordinateRegion.span.latitudeDelta = [applicationPreferences doubleForKey:MAP_VIEW__REGION_DELTA_LATITUDE];
    _mapCoordinateRegion.span.longitudeDelta = [applicationPreferences doubleForKey:MAP_VIEW__REGION_DELTA_LONGITUDE];
    self.window.rootViewController = navigaionController;
    [self.window makeKeyAndVisible];
    [self initializeProgressPopup];

    return YES;
}

- (void)updateCurrentVCData
{   
    if (navigaionController.topViewController)
    {
        if ([navigaionController.topViewController isKindOfClass:[UITabBarController class]])
        {
            if ([[(UITabBarController*)navigaionController.topViewController selectedViewController] respondsToSelector:@selector(updateScreenInfo)])
            {
                [(BaseViewController*)[(UITabBarController*)navigaionController.topViewController selectedViewController] updateScreenInfo];
            }
        }
        else
        {
            if ([navigaionController.topViewController respondsToSelector:@selector(updateScreenInfo)])
            {
                [(BaseViewController*)navigaionController.topViewController updateScreenInfo];
            }
        }
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [self checkViews:application.windows];
    if (navigaionController.topViewController != _loginViewController)
    {
        [navigaionController popToRootViewControllerAnimated:NO];
        MMSDK* sdk = [MMSDK sharedInstance];
        [sdk logoutWithSuccess:^(BOOL isLogout)
         {
             if (!isLogout)
             {
                 NSLog(@"logout is OK");
             }
         } failure:^(NSError *error)
         {
             NSLog(@"logout failed");
         }];
    }
}

-(void)checkViews:(NSArray *)subviews
{
    Class AVClass = [UIAlertView class];
    for (UIView * subview in subviews){
        if ([subview isKindOfClass:AVClass]){
            [(UIAlertView *)subview dismissWithClickedButtonIndex:[(UIAlertView *)subview cancelButtonIndex] animated:NO];
        } else {
            [self checkViews:subview.subviews];
        }
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    [self stopTimerUpdate];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [self startTimerUpdate];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

- (void)startTimerUpdate
{
    timerDataUpdater = [ NSTimer scheduledTimerWithTimeInterval:
                        [ applicationPreferences integerForKey:REQUEST_FREQUENCY ]
                                                         target:self
                                                       selector:@selector(updateCurrentVCData)
                                                       userInfo:nil 
                                                        repeats:YES ];
}

- (void)stopTimerUpdate
{
    [timerDataUpdater invalidate];
}

#pragma mark -
#pragma mark Protected Methods

- (void)initializeProgressPopup
{
    _popupWindow = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    MMActivityIndicatorViewController* vc = [[MMActivityIndicatorViewController alloc] init];
    _popupWindow.rootViewController = vc;
    [vc release];
}

@end
