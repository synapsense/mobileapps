//
//  OptionsSettingsViewController.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "OptionsSettingsViewController.h"
#import "MMTableViewCell.h"

@implementation OptionsSettingsViewController

@synthesize settingTitle;

#define REQUEST_FREQUENCY_30_SECONDS 30
#define REQUEST_FREQUENCY_1_MINUTE   60
#define REQUEST_FREQUENCY_2_MINUTES  180
#define REQUEST_FREQUENCY_5_MINUTES  300
#define REQUEST_FREQUENCY_10_MINUTES 600

#define REQUEST_TIME_PERIOD_LAST_HOUR     1
#define REQUEST_TIME_PERIOD_LAST_12_HOURS 12
#define REQUEST_TIME_PERIOD_LAST_DAY      24
#define REQUEST_TIME_PERIOD_LAST_WEEK     168
#define REQUEST_TIME_PERIOD_LAST_30_DAYS  720
#define REQUEST_TIME_PERIOD_ALL_TIME      999         //????

#define HEIGHT_OF_A_CELL                  60

#pragma mark - View lifecycle

- (void)updateScreenInfo
{
    //do nothing
    //[super updateScreenInfo];
    //implement updating data here
}

- (id)initWithSettingString:(NSString *)settingString
{
    self = [super init];
    if (self != nil) 
    {
        settingTitle = [[NSString alloc] initWithString:settingString];
        if ([settingTitle isEqualToString:NSLocalizedString(@"tableViewSettingRequestFrequency", @"")])
        {
            [self.view addSubview:[self loadHeaderWithTitle:NSLocalizedString(@"tableViewSettingRequestFrequency", @"")]];
            optionsTableDataArray = [[NSArray alloc] initWithObjects: NSLocalizedString(@"requestFrequencyOption_1", @""),
                                                                      NSLocalizedString(@"requestFrequencyOption_2", @""),
                                                                      NSLocalizedString(@"requestFrequencyOption_3", @""), 
                                                                      NSLocalizedString(@"requestFrequencyOption_4", @""),
                                                                      NSLocalizedString(@"requestFrequencyOption_5", @""), nil];
        }
        else if ([settingTitle isEqualToString:NSLocalizedString(@"tableViewSettingRequestTimePeriod", @" ")])
        {
            [self.view addSubview:[self loadHeaderWithTitle:NSLocalizedString(@"tableViewSettingRequestTimePeriod", @"")]];
            optionsTableDataArray = [[NSArray alloc] initWithObjects: NSLocalizedString(@"requestTimePeriodOption_1", @""),
                                                                      NSLocalizedString(@"requestTimePeriodOption_2", @""),
                                                                      NSLocalizedString(@"requestTimePeriodOption_3", @""),
                                                                      NSLocalizedString(@"requestTimePeriodOption_4", @""),
                                                                      NSLocalizedString(@"requestTimePeriodOption_5", @""),
                                                                      NSLocalizedString(@"requestTimePeriodOption_6", @""), nil];
        }

        atableView = [[MMTableView alloc]initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 35,
                                                                   self.view.frame.size.width, self.view.frame.size.height-35)
                                                                    style:UITableViewStyleGrouped];
        [atableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [atableView setDelegate:self];
        [atableView setDataSource:self];
        [self.view addSubview:atableView];
        [atableView release];
    }
    return self;
}

- (void)loadView
{
    self.view = [[[MMView alloc] initWithFrame:[UIScreen mainScreen].bounds]autorelease];
    self.navigationItem.title = NSLocalizedString(@"applicationTitle", @"");
    //self.navigationItem.backBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"backButtonTitle", @"") style:UIBarButtonItemStyleBordered target:nil action:nil]autorelease];
}


- (void)dealloc
{
    [optionsTableDataArray release];
    [settingTitle release];
    [super dealloc];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    return [optionsTableDataArray count];	
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    static NSString *CellIdentifier = @"Cell";
    MMTableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[MMTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        [cell setTableviewStyle:UITableViewStyleGrouped];
    }
    cell.textLabel.text = [optionsTableDataArray objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    if ([settingTitle isEqualToString:NSLocalizedString(@"tableViewSettingRequestFrequency", @" ")])
    {
        switch ([[[AppDelegate sharedInstance] applicationPreferences] integerForKey:REQUEST_FREQUENCY]) 
        {
            case REQUEST_FREQUENCY_30_SECONDS:
                if (indexPath.row == 0) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    currentCell = cell;
                }
                break;
            case REQUEST_FREQUENCY_1_MINUTE:
                if (indexPath.row == 1) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    currentCell = cell;
                }
                break;
            case REQUEST_FREQUENCY_2_MINUTES:
                if (indexPath.row == 2) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    currentCell = cell;
                }
                break;
            case REQUEST_FREQUENCY_5_MINUTES:
                if (indexPath.row == 3) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    currentCell = cell;
                }
                break;
            case REQUEST_FREQUENCY_10_MINUTES:
                if (indexPath.row == 4) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    currentCell = cell;
                }
                break;
            default:
                break;
        }
    }
    if ([settingTitle isEqualToString:NSLocalizedString(@"tableViewSettingRequestTimePeriod", @" ")])
    {
        switch ([[[AppDelegate sharedInstance] applicationPreferences] integerForKey:REQUEST_TIME_PERIOD]) 
        {
            case REQUEST_TIME_PERIOD_LAST_HOUR:
                if (indexPath.row == 0) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    currentCell = cell;
                }
                break;
            case REQUEST_TIME_PERIOD_LAST_12_HOURS:
                if (indexPath.row == 1) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    currentCell = cell;
                }
                break;
            case REQUEST_TIME_PERIOD_LAST_DAY:
                if (indexPath.row == 2) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    currentCell = cell;
                }
                break;
            case REQUEST_TIME_PERIOD_LAST_WEEK:
                if (indexPath.row == 3) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    currentCell = cell;
                }
                break;
            case REQUEST_TIME_PERIOD_LAST_30_DAYS:
                if (indexPath.row == 4) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    currentCell = cell;
                }
            case REQUEST_TIME_PERIOD_ALL_TIME:
                if (indexPath.row == 5) {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                    currentCell = cell;
                }
                break;
            default:
                break;
        }
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
    if (newCell.accessoryType == UITableViewCellAccessoryNone) 
    {
        currentCell.accessoryType = UITableViewCellAccessoryNone;
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        currentCell = newCell;
    }
    if ([settingTitle isEqualToString:NSLocalizedString(@"tableViewSettingRequestFrequency", @" ")])
    {
        switch (indexPath.row) {
            case 0:
                [[[AppDelegate sharedInstance] applicationPreferences] setInteger:REQUEST_FREQUENCY_30_SECONDS forKey:REQUEST_FREQUENCY];
                break;                
            case 1:
                [[[AppDelegate sharedInstance] applicationPreferences] setInteger:REQUEST_FREQUENCY_1_MINUTE forKey:REQUEST_FREQUENCY];
                break;
            case 2:
                [[[AppDelegate sharedInstance] applicationPreferences] setInteger:REQUEST_FREQUENCY_2_MINUTES forKey:REQUEST_FREQUENCY];
                break;
            case 3:
                [[[AppDelegate sharedInstance] applicationPreferences] setInteger:REQUEST_FREQUENCY_5_MINUTES forKey:REQUEST_FREQUENCY];
                break;
            case 4:
                [[[AppDelegate sharedInstance] applicationPreferences] setInteger:REQUEST_FREQUENCY_10_MINUTES forKey:REQUEST_FREQUENCY];
                break;
            default:
                break;
        }
        [[[AppDelegate sharedInstance] timerDataUpdater] invalidate];
        [[AppDelegate sharedInstance] startTimerUpdate];
    }
    
    if ([settingTitle isEqualToString:NSLocalizedString(@"tableViewSettingRequestTimePeriod", @" ")])
    {
        switch (indexPath.row) {
            case 0:
                [[[AppDelegate sharedInstance] applicationPreferences] setInteger:REQUEST_TIME_PERIOD_LAST_HOUR forKey:REQUEST_TIME_PERIOD];
                break;
            case 1:
                [[[AppDelegate sharedInstance] applicationPreferences] setInteger:REQUEST_TIME_PERIOD_LAST_12_HOURS forKey:REQUEST_TIME_PERIOD];
                break;
            case 2:
                [[[AppDelegate sharedInstance] applicationPreferences] setInteger:REQUEST_TIME_PERIOD_LAST_DAY forKey:REQUEST_TIME_PERIOD];
                break;
            case 3:
                [[[AppDelegate sharedInstance] applicationPreferences] setInteger:REQUEST_TIME_PERIOD_LAST_WEEK forKey:REQUEST_TIME_PERIOD];
                break;
            case 4:
                [[[AppDelegate sharedInstance] applicationPreferences] setInteger:REQUEST_TIME_PERIOD_LAST_30_DAYS forKey:REQUEST_TIME_PERIOD];
                break;
            case 5:
                [[[AppDelegate sharedInstance] applicationPreferences] setInteger:REQUEST_TIME_PERIOD_ALL_TIME forKey:REQUEST_TIME_PERIOD];
                break;
            default:
                break;
        }
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end
