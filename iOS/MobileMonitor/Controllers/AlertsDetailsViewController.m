//
//  AlertsDetailsAndResolutionViewController.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "AlertsDetailsViewController.h"
#import "MMAlertInfo.h"
#import "MMSDK.h"
#import "AppUtility.h"
#import <QuartzCore/QuartzCore.h>
#import "MMTextView.h"
#import "MMTitleLabel.h"
#import "MMSubtitleLabel.h"


@implementation AlertsDetailsViewController

#define HEADER                  50
#define HEIGHT_GAP              5
#define IDENTATION_X            20
#define ROW_HEIGHT              19
#define LABEL_HEIGHT            15
#define FIELD_HEIGHT            47
#define BUTTON_HEIGHT           35
#define HEIGHT_BOTTOM           3
#define BUTTONS_GAP             5

@synthesize currentDataCenter;
@synthesize arrayObject;
@synthesize sectionTableView;
@synthesize alertInfo;
@synthesize parent;

-(void)updateScreenInfo
{
    //do nothing
//    [super updateScreenInfo];
//    MMSDK *sdk = [MMSDK sharedInstance];
//    [sdk getActiveAlerts:alertInfo.object.id withPeriod:[NSNull null] withPriority:[NSNull null] withStatus:[NSNumber numberWithInt:15] withStart:[NSNull null] withCount:[NSNull null] success:^(NSArray *activeAlerts)
//     {
//         for (int i = 0; i < activeAlerts.count; i++)
//         {
//             if ([(MMAlertInfo*)[activeAlerts objectAtIndex:i] alertId] == alertInfo.alertId)
//             {
//                 self.alertInfo = [activeAlerts objectAtIndex:i]; 
//                 continue;
//             }    
//         }
//     } failure:^(NSError *error) {
//         NSLog(@"%@", error);
//     }];
//    [self updateHasFinished];
}

-(void)viewDidLoad
{
    if (self.view.frame.size.width > self.view.frame.size.height) {
        self.view = [[[MMView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width) viewLevel:ViewLevelSecond] autorelease];
    } else if (self.view.frame.size.width < self.view.frame.size.height) {
        self.view = [[[MMView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) viewLevel:ViewLevelSecond] autorelease];
    } else {
        self.view = [[[MMView alloc] initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, VIEW_HEIGHT) viewLevel:ViewLevelSecond] autorelease];
        NSLog(@"bug!!!!!!!!!!!!!!");
    }

    self.navigationItem.title = NSLocalizedString(@"applicationTitle", @"");
    self.navigationItem.backBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"backButtonTitle", @"") style:UIBarButtonItemStyleBordered target:nil action:nil] autorelease];
    
    //if this alert viewed from list of alerts
    //todo: this code should be used in described above case
    if(1)
    {
        //swipe gesture (right)
        UISwipeGestureRecognizer * swipeRightDirection = [[UISwipeGestureRecognizer alloc] 
                                                          initWithTarget:self
                                                          action:@selector(handleSwipeRightDirection)];
        swipeRightDirection.direction = UISwipeGestureRecognizerDirectionRight;
        swipeRightDirection.numberOfTouchesRequired = 1;
        
        [self.view addGestureRecognizer:swipeRightDirection];
        [swipeRightDirection release];
        
        //swipe gesture (left)
        UISwipeGestureRecognizer * swipeLeftDirection = [[UISwipeGestureRecognizer alloc] 
                                                          initWithTarget:self
                                                          action:@selector(handleSwipeLeftDirection)];
        swipeLeftDirection.direction = UISwipeGestureRecognizerDirectionLeft;
        swipeLeftDirection.numberOfTouchesRequired = 1;
        
        [self.view addGestureRecognizer:swipeLeftDirection];
        [swipeLeftDirection release];
    }
    [self loadSections];
}

-(void)viewDidUnload
{
    self.sectionTableView = nil;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self updateScreenInfo];
}

-(id)initWithAlertInfo:(MMAlertInfo *)alertInformation
{
    self = [super init];
    if (self)
    {
        self.alertInfo = alertInformation;
    }
    return self;
}

-(void)loadSections
{    
	sectionTableView = [[MMTableView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y,
                                                                     self.view.frame.size.width,
                                                                     self.view.frame.size.height)
                                                    style:UITableViewStylePlain];
    
	sectionTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    sectionTableView.showsVerticalScrollIndicator = YES;
    [sectionTableView setScrollEnabled:UIInterfaceOrientationIsLandscape(self.interfaceOrientation)];
    sectionTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
	[self.view addSubview:sectionTableView];
    
    sectionTableView.delegate = self;
    sectionTableView.dataSource = self;
    //NSLog(@"self.alertInfo = %@",self.alertInfo);
    
    NSMutableArray *arraySection = [[NSMutableArray alloc] init];
    
    [arraySection addObject:NSLocalizedString(@"alertNameWithColon", @"")];
    [arraySection addObject:NSLocalizedString(@"objectNameWithColon", @"")];
    [arraySection addObject:NSLocalizedString(@"priorityWithColon", @"")];
    [arraySection addObject:NSLocalizedString(@"statusWithColon", @"")];
    [arraySection addObject:NSLocalizedString(@"lastActionByWithColon", @"")];
    [arraySection addObject:NSLocalizedString(@"lastActionDateWithColon", @"")];
    [arraySection addObject:NSLocalizedString(@"locationWithColon", @"")];
    [arraySection addObject:NSLocalizedString(@"timestampWithColon", @"")];
    
    self.arrayObject = arraySection;
    [arraySection release];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
	return [self.arrayObject count] + 2;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *)indexPath
{
    if (indexPath.row == 8) {
        if (([self interfaceOrientation] == UIInterfaceOrientationLandscapeLeft) || ([self interfaceOrientation] == UIInterfaceOrientationLandscapeRight)) {
            return FIELD_HEIGHT*3 + HEIGHT_GAP*3;
        } else {
            return LABEL_HEIGHT*3 + FIELD_HEIGHT*3 + HEIGHT_GAP*7;
        }
    } else if (indexPath.row == 9) {
        return BUTTON_HEIGHT + HEIGHT_BOTTOM*2;
    } else {
        return ROW_HEIGHT;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [sectionTableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    UITableViewCell *cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyIdentifier"] autorelease];
    if (indexPath.row == 8) {
        UIView* view = [[[UIView alloc] init] autorelease];
        view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width , self.view.frame.size.height - [self.arrayObject count]*ROW_HEIGHT);
        view.backgroundColor = [UIColor clearColor];
        
        CGFloat descriptionLabelX = 0;
        CGFloat descriptionLabelY = 0;
        CGFloat descriptionLabelWidth = 0;
        CGFloat descriptionLabelHeight = 0;
        CGFloat descriptionTextViewX = 0;
        CGFloat descriptionTextViewY = 0;
        CGFloat descriptionTextViewWidth = 0;
        CGFloat descriptionTextViewHeight = 0;
        CGFloat messageLabelX = 0;
        CGFloat messageLabelY = 0;
        CGFloat messageLabelWidth = 0;
        CGFloat messageLabelHeight = 0;
        CGFloat messageTextViewX = 0;
        CGFloat messageTextViewY = 0;
        CGFloat messageTextViewWidth = 0;
        CGFloat messageTextViewHeight = 0;
        CGFloat commentLabelX = 0;
        CGFloat commentLabelY = 0;
        CGFloat commentLabelWidth = 0;
        CGFloat commentLabelHeight = 0;
        CGFloat commentTextViewX = 0;
        CGFloat commentTextViewY = 0;
        CGFloat commentTextViewWidth = 0;
        CGFloat commentTextViewHeight = 0;
        
        if (([self interfaceOrientation] == UIInterfaceOrientationLandscapeLeft) || ([self interfaceOrientation] == UIInterfaceOrientationLandscapeRight)) {
            // Landscape
            descriptionLabelX = view.frame.origin.x + IDENTATION_X;
            descriptionLabelY = view.frame.origin.y;
            descriptionLabelWidth = sectionTableView.frame.size.width/3 - IDENTATION_X;
            descriptionLabelHeight = FIELD_HEIGHT;
            
            descriptionTextViewX = sectionTableView.frame.size.width/3 - IDENTATION_X;
            descriptionTextViewY = view.frame.origin.y;
            descriptionTextViewWidth = sectionTableView.frame.size.width*2/3 - view.frame.origin.x;
            descriptionTextViewHeight = FIELD_HEIGHT;
            
            messageLabelX = view.frame.origin.x + IDENTATION_X;
            messageLabelY = view.frame.origin.y + FIELD_HEIGHT + HEIGHT_GAP;
            messageLabelWidth = sectionTableView.frame.size.width/3 - IDENTATION_X;
            messageLabelHeight = FIELD_HEIGHT;
            
            messageTextViewX = sectionTableView.frame.size.width/3 - IDENTATION_X;
            messageTextViewY = view.frame.origin.y + FIELD_HEIGHT + HEIGHT_GAP;
            messageTextViewWidth = sectionTableView.frame.size.width*2/3 - view.frame.origin.x;
            messageTextViewHeight = FIELD_HEIGHT;
            
            commentLabelX = view.frame.origin.x + IDENTATION_X;
            commentLabelY = view.frame.origin.y + FIELD_HEIGHT*2 + HEIGHT_GAP*2;
            commentLabelWidth = sectionTableView.frame.size.width/3 - IDENTATION_X;
            commentLabelHeight = FIELD_HEIGHT;
            
            commentTextViewX = sectionTableView.frame.size.width/3 - IDENTATION_X;
            commentTextViewY = view.frame.origin.y + FIELD_HEIGHT*2 + HEIGHT_GAP*2;
            commentTextViewWidth = sectionTableView.frame.size.width*2/3 - view.frame.origin.x;
            commentTextViewHeight = FIELD_HEIGHT;
        } else {
            // Portrait
            descriptionLabelX = view.frame.origin.x + IDENTATION_X;
            descriptionLabelY = view.frame.origin.y + HEIGHT_GAP;
            descriptionLabelWidth = sectionTableView.frame.size.width/2 - IDENTATION_X;
            descriptionLabelHeight = LABEL_HEIGHT;
            
            descriptionTextViewX = view.frame.origin.x + IDENTATION_X;
            descriptionTextViewY = view.frame.origin.y + LABEL_HEIGHT + HEIGHT_GAP*2;
            descriptionTextViewWidth = self.view.frame.size.width - IDENTATION_X * 2;
            descriptionTextViewHeight = FIELD_HEIGHT;
            
            messageLabelX = view.frame.origin.x + IDENTATION_X;
            messageLabelY = view.frame.origin.y + LABEL_HEIGHT + FIELD_HEIGHT + HEIGHT_GAP*3;
            messageLabelWidth = sectionTableView.frame.size.width/2 - IDENTATION_X;
            messageLabelHeight = LABEL_HEIGHT;
            
            messageTextViewX = view.frame.origin.x + IDENTATION_X;
            messageTextViewY = view.frame.origin.y + LABEL_HEIGHT*2 + FIELD_HEIGHT + HEIGHT_GAP*4;
            messageTextViewWidth = self.view.frame.size.width - IDENTATION_X * 2;
            messageTextViewHeight = FIELD_HEIGHT;
            
            commentLabelX = view.frame.origin.x + IDENTATION_X;
            commentLabelY = view.frame.origin.y + LABEL_HEIGHT*2 + FIELD_HEIGHT*2 + HEIGHT_GAP*5;
            commentLabelWidth = sectionTableView.frame.size.width/2 - IDENTATION_X;
            commentLabelHeight = LABEL_HEIGHT;
            
            commentTextViewX = view.frame.origin.x + IDENTATION_X;
            commentTextViewY = view.frame.origin.y + LABEL_HEIGHT*3 + FIELD_HEIGHT*2 + HEIGHT_GAP*6;
            commentTextViewWidth = self.view.frame.size.width - IDENTATION_X * 2;
            commentTextViewHeight = FIELD_HEIGHT;
        }
        MMTitleLabel *descriptionLabel = [[[MMTitleLabel alloc] initWithFrame:CGRectMake(descriptionLabelX,
                                                                               descriptionLabelY,
                                                                               descriptionLabelWidth,
                                                                               descriptionLabelHeight)] autorelease];
        descriptionLabel.text = NSLocalizedString(@"alertDescriptionWithColon", @"");
        descriptionLabel.font = [UIFont systemFontOfSize:14.0f];
        descriptionLabel.textAlignment = UITextAlignmentLeft;
        
        MMTextView *descriptionTextView = [[[MMTextView alloc] initWithFrame:CGRectMake(descriptionTextViewX,
                                                                                        descriptionTextViewY,
                                                                                        descriptionTextViewWidth,
                                                                                        descriptionTextViewHeight)] autorelease];
        descriptionTextView.backgroundColor = [UIColor colorWithRed:0.173 green:0.216 blue:0.230 alpha:0.1];
        descriptionTextView.font = [UIFont systemFontOfSize:12.0f];
        descriptionTextView.textAlignment = UITextAlignmentLeft;
        descriptionTextView.editable = NO;
        descriptionTextView.layer.cornerRadius = 5;
        descriptionTextView.clipsToBounds = YES;
        
        descriptionTextView.text = self.alertInfo.alertDescription != nil ? self.alertInfo.alertDescription : @"";
        
        MMTitleLabel *messageLabel = [[[MMTitleLabel alloc] initWithFrame:CGRectMake(messageLabelX,
                                                                           messageLabelY,
                                                                           messageLabelWidth,
                                                                           messageLabelHeight)] autorelease];
        messageLabel.text = NSLocalizedString(@"alertMessageWithColon", @"");
        messageLabel.font = [UIFont systemFontOfSize:14.0f];
        messageLabel.textAlignment = UITextAlignmentLeft;
        
        MMTextView *messageTextView = [[[MMTextView alloc] initWithFrame:CGRectMake(messageTextViewX,
                                                                                    messageTextViewY,
                                                                                    messageTextViewWidth,
                                                                                    messageTextViewHeight)] autorelease];
        messageTextView.backgroundColor = [UIColor colorWithRed:0.173 green:0.216 blue:0.230 alpha:0.1];
        messageTextView.font = [UIFont systemFontOfSize:12.0f];
        messageTextView.textAlignment = UITextAlignmentLeft;
        messageTextView.editable = NO;
        messageTextView.layer.cornerRadius = 5;
        messageTextView.clipsToBounds = YES;
        messageTextView.text = self.alertInfo.alertMessage != nil ? self.alertInfo.alertMessage : @"";
        
        MMTitleLabel *commentLabel = [[[MMTitleLabel alloc] initWithFrame:CGRectMake(commentLabelX,
                                                                           commentLabelY,
                                                                           commentLabelWidth,
                                                                           commentLabelHeight)] autorelease];
        commentLabel.text = NSLocalizedString(@"commentWithColon", @"");
        commentLabel.font = [UIFont systemFontOfSize:14.0f];
        commentLabel.textAlignment = UITextAlignmentLeft;
        
        MMTextView *commentTextView = [[[MMTextView alloc] initWithFrame:CGRectMake(commentTextViewX,
                                                                                    commentTextViewY,
                                                                                    commentTextViewWidth,
                                                                                    commentTextViewHeight)] autorelease];
        commentTextView.backgroundColor = [UIColor colorWithRed:0.173 green:0.216 blue:0.230 alpha:0.1];
        commentTextView.font = [UIFont systemFontOfSize:12.0f];
        commentTextView.textAlignment = UITextAlignmentLeft;
        commentTextView.editable = NO;
        commentTextView.layer.cornerRadius = 5;
        commentTextView.clipsToBounds = YES;
        commentTextView.text = self.alertInfo.alertComment != nil ? self.alertInfo.alertComment : @"";
        
        [view addSubview:descriptionLabel];
        [view addSubview:descriptionTextView];
        [view addSubview:messageLabel];
        [view addSubview:messageTextView];
        [view addSubview:commentLabel];
        [view addSubview:commentTextView];
        
        [view setNeedsLayout];
        [cell.contentView addSubview:view];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
    } else if (indexPath.row == 9) {
        UIView* view = [[[UIView alloc] init] autorelease];
        view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + HEIGHT_BOTTOM, self.view.frame.size.width , BUTTON_HEIGHT + HEIGHT_BOTTOM);
        view.backgroundColor = [UIColor clearColor];
        
        CGFloat acknowledgeButtonX = 0;
        CGFloat acknowledgeButtonY = 0;
        CGFloat resolveButtonX = 0;
        CGFloat resolveButtonY = 0;
        CGFloat dismissButtonX = 0;
        CGFloat dismissButtonY = 0;
        CGFloat buttonWidth = 0;
        CGFloat acknowledgeButtonWidth = 0;
        CGFloat otherButtonWidth = 0;
        CGFloat buttonHeight = 0;
        
        if (([self interfaceOrientation] == UIInterfaceOrientationLandscapeLeft) || ([self interfaceOrientation] == UIInterfaceOrientationLandscapeRight)) {
            // Landscape
            buttonWidth = (view.frame.size.width - BUTTONS_GAP * 2 - IDENTATION_X * 2) / 3;
            acknowledgeButtonWidth = buttonWidth;
            otherButtonWidth = buttonWidth;
            buttonHeight = BUTTON_HEIGHT;
            acknowledgeButtonX = view.frame.origin.x + IDENTATION_X;
            acknowledgeButtonY = view.frame.origin.y;
            resolveButtonX = IDENTATION_X + buttonWidth + BUTTONS_GAP;
            resolveButtonY = view.frame.origin.y;
            dismissButtonX = IDENTATION_X + buttonWidth * 2 + BUTTONS_GAP * 2;
            dismissButtonY = view.frame.origin.y;
        } else {
            // Portrait
            buttonWidth = (view.frame.size.width - BUTTONS_GAP * 2 - IDENTATION_X * 2) / 3;
            acknowledgeButtonWidth = buttonWidth + 10;
            otherButtonWidth = buttonWidth - 5;
            buttonHeight = BUTTON_HEIGHT;
            acknowledgeButtonX = view.frame.origin.x + IDENTATION_X;
            acknowledgeButtonY = view.frame.origin.y;
            resolveButtonX = IDENTATION_X + acknowledgeButtonWidth + BUTTONS_GAP;
            resolveButtonY = view.frame.origin.y;
            dismissButtonX = IDENTATION_X + acknowledgeButtonWidth + otherButtonWidth + BUTTONS_GAP * 2;
            dismissButtonY = view.frame.origin.y;
        }
        
        UIButton *acknowledgeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [acknowledgeButton setBackgroundImage:[UIImage imageNamed:@"blueButton1pxl.png"] forState:UIControlStateNormal];
//        [acknowledgeButton setBackgroundImage:[AppUtility getStretchableImage:@"btn_bg_dsbl.png"] forState:UIControlStateDisabled];
        [acknowledgeButton setBackgroundColor:[UIColor clearColor]];
        acknowledgeButton.enabled = NO;
        [acknowledgeButton.titleLabel setShadowOffset:CGSizeMake(0.0, -0.75)];
        [acknowledgeButton setTitleShadowColor:[UIColor blackColor] forState:UIControlStateNormal];
        [acknowledgeButton setTitle:NSLocalizedString(@"acknowledgeButtonTitle", @"") forState:UIControlStateNormal];
        [acknowledgeButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:14.0f]];
        [acknowledgeButton setFrame:CGRectMake(acknowledgeButtonX,
                                               0,
                                               acknowledgeButtonWidth,
                                               buttonHeight)];
        acknowledgeButton.layer.cornerRadius = 8.0f;
        acknowledgeButton.layer.masksToBounds = YES;
        
        UIButton *resolveButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [resolveButton setBackgroundImage:[UIImage imageNamed:@"blueButton1pxl.png"] forState:UIControlStateNormal];
//        [resolveButton setBackgroundImage:[AppUtility getStretchableImage:@"btn_bg_dsbl.png"] forState:UIControlStateDisabled];
        resolveButton.enabled = NO;
        [resolveButton.titleLabel setShadowOffset:CGSizeMake(0.0, -0.75)];
        [resolveButton setTitleShadowColor:[UIColor blackColor] forState:UIControlStateNormal];
        [resolveButton setTitle:NSLocalizedString(@"resolveButtonTitle", @"") forState:UIControlStateNormal];
        [resolveButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:14.0f]];
        [resolveButton setFrame:CGRectMake(resolveButtonX,
                                           0,
                                           otherButtonWidth,
                                           buttonHeight)];
        resolveButton.layer.cornerRadius = 8.0f;
        resolveButton.layer.masksToBounds = YES;
        
        UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [dismissButton setBackgroundImage:[UIImage imageNamed:@"blueButton1pxl.png"] forState:UIControlStateNormal];
//        [dismissButton setBackgroundImage:[AppUtility getStretchableImage:@"btn_bg_dsbl.png"] forState:UIControlStateDisabled];
        dismissButton.enabled = NO;
        [dismissButton.titleLabel setShadowOffset:CGSizeMake(0.0, -0.75)];
        [dismissButton setTitleShadowColor:[UIColor blackColor] forState:UIControlStateNormal];
        [dismissButton setTitle:NSLocalizedString(@"dismissButtonTitle", @"") forState:UIControlStateNormal];
        [dismissButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:14.0f]];
        [dismissButton setFrame:CGRectMake(dismissButtonX,
                                           0,
                                           otherButtonWidth,
                                           buttonHeight)];
        dismissButton.layer.cornerRadius = 8.0f;
        dismissButton.layer.masksToBounds = YES;
        
        if (self.alertInfo.alertStatus == MMAlertStatusOpened) {
            [acknowledgeButton addTarget:self action:@selector(acknowledgeButtonPressed) forControlEvents:UIControlEventTouchUpInside];
            [resolveButton addTarget:self action:@selector(resolveButtonPressed) forControlEvents:UIControlEventTouchUpInside];
            [dismissButton addTarget:self action:@selector(dismissButtonPressed) forControlEvents:UIControlEventTouchUpInside];
            acknowledgeButton.enabled = YES;
            resolveButton.enabled = YES;
            dismissButton.enabled = YES;
        } else if (self.alertInfo.alertStatus == MMAlertStatusAcknowledged) {
            [resolveButton addTarget:self action:@selector(resolveButtonPressed) forControlEvents:UIControlEventTouchUpInside];
            [dismissButton addTarget:self action:@selector(dismissButtonPressed) forControlEvents:UIControlEventTouchUpInside];
            resolveButton.enabled = YES;
            dismissButton.enabled = YES;
        }
        
        [view addSubview:acknowledgeButton];
        [view addSubview:resolveButton];
        [view addSubview:dismissButton];
        
        [view setNeedsLayout];
        [cell.contentView addSubview:view];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
    } else {
        MMSubtitleLabel* label = nil;
        MMTitleLabel* sectionLabel = nil;
        
        NSDateFormatter* dateFormater = [[NSDateFormatter alloc] init];
        dateFormater.dateFormat = [[AppDelegate sharedInstance].userPreferences dateFormat];
        
        if (([self interfaceOrientation] == UIInterfaceOrientationLandscapeLeft) || ([self interfaceOrientation] == UIInterfaceOrientationLandscapeRight)) {
            sectionLabel = [[[MMTitleLabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x + IDENTATION_X, cell.frame.origin.y, sectionTableView.frame.size.width/3 - 2 * IDENTATION_X, ROW_HEIGHT)] autorelease];
            label = [[[MMSubtitleLabel alloc] initWithFrame:CGRectMake(sectionTableView.frame.size.width/3, cell.frame.origin.y, sectionTableView.frame.size.width*2/3  - IDENTATION_X, 19.0)] autorelease];
        } else {
            sectionLabel = [[[MMTitleLabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x + IDENTATION_X, cell.frame.origin.y, sectionTableView.frame.size.width/2 -  2 * IDENTATION_X, ROW_HEIGHT)] autorelease];
            label = [[[MMSubtitleLabel alloc] initWithFrame:CGRectMake(sectionTableView.frame.size.width/2, cell.frame.origin.y, sectionTableView.frame.size.width/2 - IDENTATION_X, 19.0)] autorelease];
        }
        sectionLabel.lineBreakMode = UILineBreakModeTailTruncation;
        sectionLabel.font = [UIFont systemFontOfSize:14.0f];
        sectionLabel.textAlignment = UITextAlignmentLeft;
        
        label.lineBreakMode = UILineBreakModeTailTruncation;
        label.font = [UIFont systemFontOfSize:12.0f];
        label.textAlignment = UITextAlignmentLeft;
        
        [cell.contentView addSubview:sectionLabel];	
        [cell.contentView addSubview:label];
        sectionLabel.text = [self.arrayObject objectAtIndex:indexPath.row];
        switch ([indexPath row])
        {
            case 0:
            {
                label.text = alertInfo.name != nil ? alertInfo.name : @"";
            }
                break;
            case 1:
            {
                label.text = alertInfo.objectName != nil ? alertInfo.objectName : @"";
            }
                break;
            case 2:
            {
                label.text = [AlertsDetailsViewController toStringAlertPriority:alertInfo.alertPriority];
            }
                break;
            case 3:
            {
                label.text = [AlertsDetailsViewController toStringAlertStatus:alertInfo.alertStatus];
            }
                break;
            case 4:
            {
                label.text = alertInfo.lastActionUser != nil ? alertInfo.lastActionUser : @"";
            }
                break;
            case 5:
            {
                label.text = alertInfo.lastActionTime != nil ? [dateFormater stringFromDate:alertInfo.lastActionTime] : @"";
            }
                break;
            case 6:
            {
                label.text = alertInfo.location != nil ? alertInfo.location : NSLocalizedString(@"locationUnknown", @"");
            }
                break;
            case 7:
            {
                label.text = alertInfo.alertTimestamp != nil ? [dateFormater stringFromDate:alertInfo.alertTimestamp] : @"";
            }
                break;
        }
        
        sectionTableView.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        
        [dateFormater release];
    }
    return cell;
    
}

- (void)acknowledgeButtonPressed
{
    [self showPopup:MMAlertStatusAcknowledged];
}

- (void)resolveButtonPressed
{
    [self showPopup:MMAlertStatusResolved];
}

- (void)dismissButtonPressed
{
    [self showPopup:MMAlertStatusDismissed];
}

-(void)showPopup:(NSInteger)tag
{
    UIAlertView *alert = nil;
    switch (tag) {
        case MMAlertStatusAcknowledged:
            alert = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"acknowledgeButtonTitle", @"")
                                                message:NSLocalizedString(@"alertViewMessage", @"")
                                               delegate:self
                                      cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                      otherButtonTitles:nil] autorelease];
            [alert addButtonWithTitle:NSLocalizedString(@"okButtonTitle", @"")];
            break;
        case MMAlertStatusResolved:
            alert = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"resolveButtonTitle", @"")
                                                message:NSLocalizedString(@"alertViewMessage", @"")
                                               delegate:self
                                      cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                      otherButtonTitles:nil] autorelease];
            [alert addButtonWithTitle:NSLocalizedString(@"okButtonTitle", @"")];
            break;
        case MMAlertStatusDismissed:
            alert = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"dismissButtonTitle", @"")
                                                message:NSLocalizedString(@"alertViewMessage", @"")
                                               delegate:self
                                      cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                      otherButtonTitles:nil] autorelease];
            [alert addButtonWithTitle:NSLocalizedString(@"okButtonTitle", @"")];
            break;
        default:
        {
            UIAlertView *errorAlert = [[[UIAlertView alloc] initWithTitle:nil
                                                                  message:NSLocalizedString(@"noComment", @"")
                                                                 delegate:self
                                                        cancelButtonTitle:NSLocalizedString(@"okButtonTitle", @"")
                                                        otherButtonTitles:nil] autorelease];
            [errorAlert setTag:tag];
            [errorAlert show];
        }            
            break;
    }
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert setTag:tag];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //[acceptedAlertMessage setString:[[alertView textFieldAtIndex:0]text]];
    //NSLog(@"%@",acceptedAlertMessage);
    if (alertView.tag == 10) {
        return;
    } else if (alertView.tag == -1) {
        [self showPopup:prevTag];
        return;
    }
    if (buttonIndex == 1) {
        // OK button pressed
        
        NSString *message = [[alertView textFieldAtIndex:0] text];
        if ([message length] != 0) {
            MMSDK *sdk = [MMSDK sharedInstance];
            [sdk changeAlertState:alertInfo.alertId withState:alertView.tag comment:message success:^(BOOL isChanged)
             {
                 if (isChanged)
                 {
                     alertInfo.alertStatus = alertView.tag;
                 }
                 else 
                 {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil 
                                                                     message:NSLocalizedString(@"alertStateWasnotChanged", @"") 
                                                                    delegate:self 
                                                           cancelButtonTitle:NSLocalizedString(@"okButtonTitle", @"")
                                                           otherButtonTitles:nil];
                     [alert show];
                     [alert setTag:10];
                     [alert release];
                 }
                 [[[AppDelegate sharedInstance] navigaionController] popViewControllerAnimated:YES];
             } failure:^(NSError *error) {
                 NSLog(@"%@", error);
             }];
        } else {
            prevTag = alertView.tag;
            [self showPopup:-1];
        }
    } else {
        // Cancel button pressed
    }
    
}

- (void)dealloc
{
    self.sectionTableView = nil;
    self.alertInfo = nil;
    self.arrayObject = nil;
    [super dealloc];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [sectionTableView setScrollEnabled:UIInterfaceOrientationIsLandscape(self.interfaceOrientation)];
    [sectionTableView reloadData];
}

+(NSString *)toStringAlertPriority:(int)valueOfProirity
{
    switch (valueOfProirity) {
        case 1:
            return NSLocalizedString(@"informational", @"");
            break;
        case 2:
            return NSLocalizedString(@"minor", @"");
            break;
        case 4:
            return NSLocalizedString(@"major", @"");
            break;
        case 8:
            return NSLocalizedString(@"critical", @"");
            break;
        default:
            return NSLocalizedString(@"notAvailable", @"");
            break;
    }
}

+(NSString *)toStringAlertStatus:(int)valueOfStatus
{
    switch (valueOfStatus) {
        case 1:
            return NSLocalizedString(@"opened", @"");
            break;
        case 2:
            return NSLocalizedString(@"acknowledged", @"");
            break;
        case 4:
            return NSLocalizedString(@"dismissed", @"");
            break;
        case 8:
            return NSLocalizedString(@"resolved", @"");
            break;
        default:
            return NSLocalizedString(@"notAvailable", @"");
            break;
    }
}

- (void)handleSwipeRightDirection
{
    [self.parent getPreviousAlert];
}

- (void)handleSwipeLeftDirection
{
    [self.parent getNextAlert];
}

@end
