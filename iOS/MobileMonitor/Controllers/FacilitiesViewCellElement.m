//
//  FacilitiesViewCellElement.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "FacilitiesViewCellElement.h"
#import "MMDataCenterInfo.h"

@implementation FacilitiesViewCellElement

@synthesize delegate;
@synthesize dataCenterInfo;

- (id)initWithFrame:(CGRect)frame andFacility:(MMDataCenterInfo *)dcInfo andNumOfActiveAlerts:(int)numOfActiveAlerts
{
    self = [super initWithFrame:frame];
    if (self) {
        self.dataCenterInfo = dcInfo;
        
        NSMutableString *facilityImageName = [NSMutableString stringWithCapacity:20];
        [facilityImageName appendString:@"data_center"];
        
        if(numOfActiveAlerts > 0)
        {
            [facilityImageName appendString:@"_alert"];
        }
        else if(dcInfo.status.value == 0)
        {
            [facilityImageName appendString:@"_warning"];
        }
        else
        {
            [facilityImageName appendString:@"_normal"];
        }
        
        [facilityImageName appendString:@".png"];
        UIImage *img = [UIImage imageNamed:facilityImageName];
        UIImageView * facilityImage = [[UIImageView alloc]initWithImage:img];
        [facilityImage setCenter:CGPointMake(frame.size.width / 2, frame.size.height / 3)];
        [self addSubview:facilityImage];
        
        UILabel *facilityName = [[UILabel alloc]init];
        [facilityName setText:dcInfo.name];
        [facilityName setBackgroundColor:[UIColor clearColor]];
        [facilityName sizeToFit];
        [facilityName setBounds:CGRectMake(0, 0, 90, facilityName.font.lineHeight)];
        [facilityName setCenter:CGPointMake(frame.size.width / 2, facilityImage.center.y + facilityImage.frame.size.height / 2  + facilityName.font.lineHeight / 2)];
        [facilityName setTextAlignment:UITextAlignmentCenter];
        [self addSubview:facilityName];
        
        UILabel *zonesLabel = [[UILabel alloc]init];
        NSString* facilityZone = dcInfo.totalZoneNumber > 1 ? NSLocalizedString(@"zones", @"") : NSLocalizedString(@"zone", @"");
        [zonesLabel setText:[NSString stringWithFormat:facilityZone,dcInfo.totalZoneNumber]];
        [zonesLabel setTextColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1]];
        [zonesLabel setFont:[UIFont systemFontOfSize:15]];
        [zonesLabel setBackgroundColor:[UIColor clearColor]];
        [zonesLabel sizeToFit];
        [zonesLabel setCenter:CGPointMake(frame.size.width/2, facilityName.center.y + facilityName.frame.size.height / 2 + zonesLabel.font.lineHeight / 2)];
        [self addSubview:zonesLabel];
        [zonesLabel release];
        
        [facilityName release];
        [facilityImage release];
        
        
        UITapGestureRecognizer* singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap)];
        singleTap.numberOfTapsRequired = 1;
        singleTap.numberOfTouchesRequired = 1;
        singleTap.delegate = self;
        [self addGestureRecognizer:singleTap];
        [singleTap release];
    }
    return self;
}

-(void)handleSingleTap
{
    [delegate handleFacilityTap:dataCenterInfo];
}

@end
