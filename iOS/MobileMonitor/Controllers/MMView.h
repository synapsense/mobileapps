//
//  MMView.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ThemeManager.h"

typedef enum {
    ViewLevelFirst = 0,
    ViewLevelSecond
}ViewLevel;

@interface MMView : UIView<ThemeListenerControl>

@property (nonatomic)ViewLevel viewLevel;

- (id)initWithFrame:(CGRect)frame viewLevel:(ViewLevel)viewLevel;

@end
