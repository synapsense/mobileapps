//
//  FloorPlanObjectDetailsViewController.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "MMTableView.h"

@interface FloorPlanObjectDetailsViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) NSString *objectID;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *objectType;
@property (nonatomic, retain) UILabel *ObjectName;
@property (nonatomic, retain) NSMutableArray *listOfParameters;
@property (nonatomic, retain) IBOutlet MMTableView *tableView;

-(id)initWithObjectID:(NSString *)ID name:(NSString*) elementName andType:(NSString*) type;

@end
