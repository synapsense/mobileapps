//
//  DashboardViewController.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "EGORefreshTableHeaderView.h"
#import "MMTableView.h"

@interface DashboardViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, EGORefreshTableHeaderDelegate>
{
    NSMutableArray *values;
    MMTableView *atableView;
    UIAlertView *errorAlert;
    EGORefreshTableHeaderView *_refreshHeaderView;
	BOOL _reloading;
}

@property (nonatomic, retain) NSMutableArray *values;
@property (nonatomic,retain) NSDate *lastUpdateTime;

- (id)initWithData:(NSMutableArray *)dataValues;

@end
