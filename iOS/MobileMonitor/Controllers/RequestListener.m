//
//  RequestListener.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "RequestListener.h"

@implementation RequestListener

@synthesize taskIds = _taskIds;

- (id)init
{
    if (self = [super init])
    {
        _taskIds = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)dealloc
{
    self.taskIds = nil;
    [super dealloc];
}

- (void)taskFinished:(int)taskId
{
    for (id taskIdObject in _taskIds)
    {
        if (taskId == [taskIdObject intValue])
        {
            [_taskIds removeObject:taskIdObject];
            break;
        }
    }
}

@end
