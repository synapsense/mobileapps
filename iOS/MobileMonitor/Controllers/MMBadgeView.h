//
//  MMBadgeView.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMBadgeView : UIView
{
	NSString *badgeText;
	UIColor *badgeTextColor;
	UIColor *badgeFrameColor;
	CGFloat badgeCornerRoundness;
}

- (id)initWithInteger:(int)number;
- (void)setBadgeNumber:(int)number;

@property(nonatomic,retain) NSString *badgeText;
@property(nonatomic,retain) UIColor *badgeTextColor;
@property(nonatomic,retain) UIColor *badgeFrameColor;
@property(nonatomic,readwrite) CGFloat badgeCornerRoundness;


@end