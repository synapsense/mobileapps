//
//  SortViewController.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewController.h"
#import "MMAlertInfo.h"
#import "MMTableView.h"

@interface SortViewController : BaseViewController < UITableViewDelegate, UITableViewDataSource >
{
    NSDictionary* sortTableDataDictionary;
    UITableViewCell* currentCell;
    MMTableView* sectionTableView;
}

@property(retain, nonatomic) NSArray* sortTableDataArray;
@property(nonatomic, assign) MMAlertSort sortingAlerts;

@end
