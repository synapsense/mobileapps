//
//  FloorPlanObjectDetailsViewController.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "FloorPlanObjectDetailsViewController.h"
#import "TransferringAlertView.h"
#import "MMSDK.h"
#import "MMTableViewCell.h"
#import "MMTitleLabel.h"
#import "MMSubtitleLabel.h"

@implementation FloorPlanObjectDetailsViewController

#define LANDSCAPE_HIEGHT_CELL   50
#define LABEL_HEIGHT            39.0f

#define HEIGHT_HEADER           35

#define MMDataClassDoor 211
#define MMDataClassSomthing1 213
#define MMDataClassSomthing2 214


@synthesize objectID;
@synthesize name;
@synthesize objectType;
@synthesize ObjectName;
@synthesize listOfParameters;
@synthesize tableView;

- (void)updateScreenInfo
{
    //do nothing
}

-(id)initWithObjectID:(NSString*)ID name:(NSString*) elementName andType:(NSString*) type
{
    self = [super init];
    if(self != nil)
    {
        name = elementName;
        objectID = ID;
        objectType = type;
    }
    
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view = [[[MMView alloc] 
                  initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, VIEW_HEIGHT) viewLevel:ViewLevelSecond]autorelease];
    self.navigationItem.title = NSLocalizedString(@"applicationTitle", @"");
    self.tabBarController.navigationItem.backBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"backButtonTitle", @"") style:UIBarButtonItemStyleBordered target:nil action:nil]autorelease];

    //current time
    UIView *currentTime = [[UIView alloc]
                    initWithFrame:
                    CGRectMake(0, HEIGHT_HEADER, self.view.frame.size.width, HEIGHT_HEADER)];
    currentTime.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    MMTitleLabel *labelTime = [[MMTitleLabel alloc] init];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = [[AppDelegate sharedInstance].userPreferences dateFormat];
    labelTime.text = [dateFormatter stringFromDate:[NSDate date]];
    [dateFormatter release];
    [labelTime sizeToFit];
    [labelTime setCenter:CGPointMake(currentTime.frame.size.width / 2.0f, HEIGHT_HEADER / 2.0f)];
    labelTime.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [currentTime addSubview:labelTime];
    [labelTime release];
    [self.view addSubview: currentTime];
    [currentTime release];

    MMSDK *sdk = [MMSDK sharedInstance];
    [sdk getFloorPlanObjectInfo:self.objectID withType:self.objectType success:^(NSMutableArray *details, NSString* objectName)
     {
         //header
         UIView *headerview = [self loadHeaderWithTitle: objectName titleWidth:self.view.frame.size.width viewLevel:ViewLevelSecond];
         [self.view addSubview:headerview];

         //UITableView

         tableView = [[MMTableView alloc] initWithFrame:
                      CGRectMake(0,
                                 HEIGHT_HEADER * 2.0f,
                                 self.view.frame.size.width,
                                 self.view.frame.size.height-HEIGHT_HEADER * 2.0f)
                                                  style:UITableViewStylePlain];

         tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
         tableView.showsVerticalScrollIndicator = YES;
         tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
         [tableView setScrollEnabled:YES];
         
         [self.view addSubview:tableView];
         
         tableView.delegate = self;
         tableView.dataSource = self;

         listOfParameters = details;
         [listOfParameters retain];
         [self updateHasFinished];
          
     } failure:^(NSError *error) {
         [self updateHasFinished];
         NSLog(@"%@", error);
     }];

}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.listOfParameters count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MMSubtitleLabel* valueOfCell = nil;
    MMTitleLabel* nameOfCell = nil;
    NSString *reuseIdentifier = [NSString stringWithFormat:@"row %d", indexPath.row];
	MMTableViewCell *cell = (MMTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (!cell)
    {
        cell = [[[MMTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        nameOfCell = [[MMTitleLabel alloc] initWithFrame:CGRectMake(HORIZONTAL_INDENT, 0.0f, 190.0f, 39.0f)];
        nameOfCell.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        nameOfCell.lineBreakMode = UILineBreakModeTailTruncation;
        nameOfCell.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f];
        nameOfCell.textAlignment = UITextAlignmentLeft;
        float portraitWidth = [UIScreen mainScreen].bounds.size.width;
        valueOfCell = [[MMSubtitleLabel alloc] initWithFrame:CGRectMake(portraitWidth - HORIZONTAL_INDENT - 60.0f, 0.0f, 60.0f, LABEL_HEIGHT)];
        valueOfCell.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        valueOfCell.lineBreakMode = UILineBreakModeTailTruncation;
        valueOfCell.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f];
        valueOfCell.textAlignment = UITextAlignmentRight;
        FPObjectDetails *currentData = [listOfParameters objectAtIndex:indexPath.row];
        
        [nameOfCell setText:currentData.valueName];
        NSString *valuestring = nil;
        switch (currentData.dataClass)
        {
            case MMDataClassDoor:
            {
                if ([currentData.lastValue boolValue])
                {
                    valuestring = NSLocalizedString(@"open", @"");
                }
                else
                {
                    valuestring =NSLocalizedString( @"closed", @"");
                }
                break;
            }
            case MMDataClassSomthing1:
            {
                if ([currentData.lastValue boolValue])
                {
                    valuestring = NSLocalizedString(@"present", @"");
                }
                else
                {
                    valuestring = NSLocalizedString(@"absent", @"");
                }
                break;
            }
            case MMDataClassSomthing2:
            {
                if ([currentData.lastValue boolValue])
                {
                    valuestring = NSLocalizedString(@"on", @"");
                }
                else
                {
                    valuestring =NSLocalizedString( @"off", @"");
                }
                break;
            }
                
            default:
            {
                if(currentData.lastValue)
                {
                    if ([currentData.lastValue doubleValue] == -5000 || [currentData.lastValue doubleValue] == -3000) {
                        valuestring = @"x";
                        valueOfCell.textColor = [UIColor redColor];
                    }
                    else if ([currentData.lastValue doubleValue] == -2000)
                    {
                        valuestring = @"x";
                        valueOfCell.textColor = [UIColor yellowColor];
                    }
                    else
                    {
                        valuestring = [NSString stringWithFormat:@"%@", [[AppDelegate sharedInstance].userPreferences formatDouble:[currentData.lastValue doubleValue]]];
                    }
                }
                else
                {
                    valuestring = @"x";
                    valueOfCell.textColor = [UIColor redColor];
                }
                break;
            }
        
        }
        [valueOfCell setText:valuestring];
        [cell.contentView addSubview:nameOfCell];
        [cell.contentView addSubview:valueOfCell];
        [nameOfCell release];
        [valueOfCell release];
        if(currentData.valueUnits && currentData.valueUnits.length > 0)
        {
            MMSubtitleLabel *units = [[MMSubtitleLabel alloc] init];
            units.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f];
            [units setText:currentData.valueUnits];
            [units sizeToFit];
            [units setFrame:CGRectMake(portraitWidth - HORIZONTAL_INDENT - units.frame.size.width, 0.0f, units.frame.size.width, LABEL_HEIGHT)];
            [cell.contentView addSubview:units];
            [units release];
            [valueOfCell setFrame:CGRectMake(units.frame.origin.x - valueOfCell.frame.size.width - 5.0f, 0.0f, valueOfCell.frame.size.width, LABEL_HEIGHT)];
            units.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        }
    }

	return cell;
}


#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return LANDSCAPE_HIEGHT_CELL;
}

- (void)dealloc
{
    [tableView release];
    if (listOfParameters)
    {
        [listOfParameters release];        
    }
    [super dealloc];
}


@end