//
//  BaseViewController.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "BaseViewController.h"
#import "MMTitleLabel.h"
#import "Constants.h"

#define HEIGHT_SEPARETER_LINE   2

@implementation BaseViewController
@synthesize timeStamp;
@synthesize viewDidAppear;

#define DERIVED_TIMEOUT 30.0 //Change accordings to user preferences

- (id)init
{
    if (self = [super init])
    {
        _requestListener = [[RequestListener alloc] init];
    }
    return self;
}

#pragma mark - View lifecycle

- (void)updateScreenInfo
{
    if (!viewDidAppear)
    {
        [[AppDelegate sharedInstance] showUpdatingPopup];
    }
    [self setTimeStamp:[NSDate date]];
    [[AppDelegate sharedInstance] stopTimerUpdate];
    [[AppDelegate sharedInstance] startTimerUpdate];
    NSLog(@"Data is updating");
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)updateHasFinished
{
    [[AppDelegate sharedInstance] hideUpdatingPopup];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    viewDidAppear = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self cancelCurrentConnections];
}

- (void) cancelCurrentConnections
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    MMSDK *sdk = [MMSDK sharedInstance];
    [sdk cancelRequests:_requestListener.taskIds];
}

-(void) viewDidAppear:(BOOL)animated
{
    NSMutableArray* controllerArray = [[[NSMutableArray alloc] init] autorelease];

    for (int i = 0; i<[[[AppDelegate sharedInstance] navigaionController].viewControllers count]; i++)
    {
        if ([[[[AppDelegate sharedInstance] navigaionController].viewControllers objectAtIndex:i] isKindOfClass:[UITabBarController class]])
        {
            UITabBarController* tabBarController = [[[AppDelegate sharedInstance] navigaionController].viewControllers objectAtIndex:i];
            [controllerArray addObject:tabBarController.selectedViewController];
        }
        else
        {
            [controllerArray addObject:[[[AppDelegate sharedInstance] navigaionController].viewControllers objectAtIndex:i]];
        }
    }
    NSLog(@"\nPushView Stack= %@",controllerArray);
    [super viewDidAppear:YES];
}

- (MMView *) loadHeaderWithTitle:(NSString *)currentScreenTitle
{
    return [self loadHeaderWithTitle:currentScreenTitle titleWidth:self.view.frame.size.width viewLevel:ViewLevelFirst];
}

- (MMView *) loadHeaderWithTitle:(NSString *)currentScreenTitle titleWidth:(float)titleWidth
{
    return [self loadHeaderWithTitle:currentScreenTitle titleWidth:titleWidth viewLevel:ViewLevelFirst];
}
- (MMView *) loadHeaderWithTitle:(NSString *)currentScreenTitle titleWidth:(float)titleWidth viewLevel:(ViewLevel)level
{
    MMView *headerView = [[[MMView alloc]
                           initWithFrame:CGRectMake(self.view.frame.origin.x,
                                                    self.view.frame.origin.y,
                                                    self.view.frame.size.width,
                                                    HEIGHT_HEADER) viewLevel:level]
                          autorelease];
    headerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    MMTitleLabel *label = [[MMTitleLabel alloc] initWithFrame:CGRectMake(HORIZONTAL_INDENT, self.view.frame.origin.y, 
                                                               titleWidth - 2 * HORIZONTAL_INDENT, HEIGHT_HEADER)];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:17.0f];
    label.textAlignment = UITextAlignmentLeft;
    label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    label.text = currentScreenTitle;
    label.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UILabel *lineTop = [[UILabel alloc]
                        initWithFrame:CGRectMake(self.view.frame.origin.x,
                                                 HEIGHT_HEADER - HEIGHT_SEPARETER_LINE,
                                                 headerView.frame.size.width,
                                                 HEIGHT_SEPARETER_LINE)];
    lineTop.backgroundColor = [UIColor darkGrayColor];
    lineTop.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    [headerView addSubview:label];
    [headerView addSubview:lineTop];

    [label release];
    [lineTop release];
    return headerView;
}

- (void)taskFinished:(int)taskId
{
    [_requestListener taskFinished:taskId];
}

- (void)addTaskId:(int)taskId
{
    [_requestListener.taskIds addObject:[NSNumber numberWithInt:taskId]];
}

- (void)dealloc
{
    self.timeStamp = nil;
    [_requestListener release];
    [super dealloc];
}

@end
