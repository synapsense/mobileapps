//
//  MMUnderlinedLabel.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMUnderlinedLabel.h"

#define SPACING -2

@implementation MMUnderlinedLabel

@synthesize showUnderline;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [[ThemeManager sharedInstance] registerObserver:self];
        [self adjustUI:[ThemeManager sharedInstance].currentTheme];
        UITapGestureRecognizer* singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLabelTap)];
        singleTap.numberOfTapsRequired = 1;
        singleTap.numberOfTouchesRequired = 1;
        singleTap.delegate = self;
        [self setUserInteractionEnabled:YES];
        [self addGestureRecognizer:singleTap];
        [singleTap release];
    }
    return self;
}

- (void)dealloc
{
    [[ThemeManager sharedInstance]unregisterObserver:self];
    [super dealloc];
}

- (void)adjustUI:(MMDefaultTheme *)currentTheme
{
    self.textColor = currentTheme.underlinedLabelColor;
    self.backgroundColor = [UIColor clearColor];
}

- (void)drawRect:(CGRect)rect
{
    if (showUnderline)
    {
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        CGContextSetStrokeColorWithColor(ctx, self.textColor.CGColor); // RGBA
        CGContextSetLineWidth(ctx, 1.0f);
        
        CGSize textSize = [self.text sizeWithFont:self.font];
        if (self.textAlignment == UITextAlignmentRight)
        {
            CGContextMoveToPoint(ctx, rect.size.width, (self.bounds.size.height + textSize.height) / 2 + SPACING);
            CGContextAddLineToPoint(ctx, rect.size.width - textSize.width, (self.bounds.size.height + textSize.height) / 2 + SPACING);
        }
        else
        {
            CGContextMoveToPoint(ctx, 0, (self.bounds.size.height + textSize.height) / 2 + SPACING);
            CGContextAddLineToPoint(ctx, textSize.width, (self.bounds.size.height + textSize.height) / 2 + SPACING);
        }
        
        CGContextStrokePath(ctx);
    }
    
    [super drawRect:rect];
}

-(void)handleLabelTap
{
    if ([delegate respondsToSelector:@selector(handleUnderlinedLabelTap:)])
    {
        [delegate handleUnderlinedLabelTap:self];
    }
}

@end
