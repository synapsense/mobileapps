//
//  SystemHealthViewController.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//
#import "SystemHealthViewController.h"
#import "MMComponentStatus.h"
#import "MMSDK.h"
#import "Constants.h"
#import "MMTitleLabel.h"
#import "MMSubtitleLabel.h"

#define ROW_HEIGHT 60
#define SEPARATOR_HEIGHT 1

@interface SystemHealthViewController(private)
- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;
@end

@implementation SystemHealthViewController

@synthesize systemStatus,
            lastUpdateTime,
            temporaryContainer;



- (void)updateScreenInfo
{
    [super updateScreenInfo];
    if (errorAlert)
    {
        [errorAlert dismissWithClickedButtonIndex:errorAlert.cancelButtonIndex animated:NO];
        errorAlert = nil;
    }
    if (_reloading == NO && self.viewDidAppear == NO)
    {
        if(temporaryContainer != nil)
        {
            [detailedInfo dismissWithClickedButtonIndex:0 animated:YES];
        }
    }

    MMSDK *sdk = [MMSDK sharedInstance];
    __block int taskIdGetSystemStatus = 0;
    taskIdGetSystemStatus = [sdk getSystemStatusWithSuccess:^(NSArray *componentStatusInfo)
     {
         [self taskFinished:taskIdGetSystemStatus];
         if(temporaryContainer != nil)
         {
             detailedInfo = [[[UIAlertView alloc] init] autorelease];
             [detailedInfo addButtonWithTitle:NSLocalizedString(@"okButtonTitle", @"")];
             [detailedInfo setTitle:temporaryContainer.title];
             [detailedInfo setMessage:temporaryContainer.message];
             [detailedInfo setDelegate:self];
             temporaryContainer = detailedInfo;
             [detailedInfo show];
         }
         
         self.lastUpdateTime = [NSDate date];
         self.viewDidAppear = YES;
         [self doneLoadingTableViewData];
         self.systemStatus = componentStatusInfo;
         [atableView reloadData];
         [self updateHasFinished];
         
     } failure:^(NSError *error) {
         [self taskFinished:taskIdGetSystemStatus];
         [self updateHasFinished];
         if(temporaryContainer != nil)
         {
             detailedInfo = [[[UIAlertView alloc] init] autorelease];
             [detailedInfo addButtonWithTitle:NSLocalizedString(@"okButtonTitle", @"")];
             [detailedInfo setTitle:temporaryContainer.title];
             [detailedInfo setMessage:temporaryContainer.message];
             [detailedInfo setDelegate:self];
             temporaryContainer = detailedInfo;
             [detailedInfo show];
         }
         if (_reloading == YES)
         {
             [self doneLoadingTableViewData];
         }
         if (!errorAlert)
         {
             errorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"updatingError", @"") message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self cancelButtonTitle:NSLocalizedString(@"okButtonTitle", @"") otherButtonTitles: nil];
             [errorAlert show];
             [errorAlert release];
         }
         NSLog(@"%@", error);
     }];
    [self addTaskId:taskIdGetSystemStatus];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)loadView
{
    [super loadView];
    self.view = [[[MMView alloc] initWithFrame:[UIScreen mainScreen].bounds] autorelease];
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    atableView = [[MMTableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    [atableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    atableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [atableView setDelegate:self];
    [atableView setDataSource:self];
    [self.view addSubview:atableView];
    if (_refreshHeaderView == nil) {
		
		EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - atableView.bounds.size.height, self.view.frame.size.width, atableView.bounds.size.height)];
		view.delegate = self;
        [view setDateFormat:[AppDelegate sharedInstance].userPreferences.dateFormat];
		[atableView addSubview:view];
		_refreshHeaderView = view;
		[view release];
        [self doneLoadingTableViewData];
	}
    [atableView release];
}

- (void) viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self willAnimateRotationToInterfaceOrientation:[self interfaceOrientation] duration:0];
    [self updateScreenInfo];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [atableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ROW_HEIGHT;
}

-(NSString*)setStringState:(NSIndexPath*)indexPath componentString:(enum MMComponent)component
{
    NSString* stateString = nil;
    for (MMComponentStatus* componentStatus in  [self systemStatus])
    {

        if ([componentStatus component] == component)
        {
            switch ([componentStatus status])
            {
                case RUNNING:
                {
                    stateString = NSLocalizedString(@"stateRunning", @"");
                }
                break;
                case STOPPED:
                {
                    stateString = NSLocalizedString(@"stateStopped", @"");
                }
                break;
                case PARTIAL:
                {
                    stateString = NSLocalizedString(@"statePartial", @"");
                }
                break;
                case UNKNOWN:
                {
                    stateString = NSLocalizedString(@"stateUnknown", @"");
                }   
                break;
            }
        }
    }
    if (stateString == nil)
    {
        stateString = NSLocalizedString(@"stateUnknown", @"");
    }

    return stateString;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
    UILabel *_separator = [[UILabel alloc] initWithFrame:CGRectMake(HORIZONTAL_INDENT, ROW_HEIGHT - SEPARATOR_HEIGHT, cell.frame.size.width - 2*HORIZONTAL_INDENT, SEPARATOR_HEIGHT)];
    [_separator setBackgroundColor:[UIColor darkGrayColor]];
    _separator.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [cell addSubview:_separator];
    [_separator release];

    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor :[UIColor clearColor]];
//    cell.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    int right_indend_x = 50;
    int cell_height = ROW_HEIGHT;
    UITableViewCellAccessoryType accessoryType;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        accessoryType =  UITableViewCellAccessoryDetailButton;
        [cell setTintColor:[[[ThemeManager sharedInstance] currentTheme]subtitleLabelColor]];
    } else {
        accessoryType =  UITableViewCellAccessoryDetailDisclosureButton;
    }

    switch ([indexPath section]) {
        case 0:
        {
            //NSLog(@"cell.frame.size.width = %f",cell.frame.size.width);
            [cell setUserInteractionEnabled:NO];
            MMTitleLabel* titleLabel = [[MMTitleLabel alloc]init];
            [titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
            [titleLabel setText:NSLocalizedString(@"host", @"")];
            [titleLabel setFrame:CGRectMake(HORIZONTAL_INDENT,
                                            cell_height / 2 - titleLabel.font.lineHeight / 2,
                                            (self.view.frame.size.width - HORIZONTAL_INDENT - right_indend_x)*0.7,
                                            cell_height)];
            [titleLabel sizeToFit];
            
            MMSubtitleLabel* serverLabel = [[MMSubtitleLabel alloc]
                                    initWithFrame:CGRectMake(HORIZONTAL_INDENT + titleLabel.frame.size.width,
                                                             0,
                                                             (self.view.frame.size.width - HORIZONTAL_INDENT - right_indend_x)*0.3,
                                                             cell_height)];
            
            NSString *tmpString = [[MMSDK sharedInstance] serviceBaseURL]; 
            NSArray *elementsDividedByTwoSpot = [tmpString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":"]];
            NSString *ipAddressOnly = [[elementsDividedByTwoSpot objectAtIndex:1] substringFromIndex:2];             
            serverLabel.text = ipAddressOnly;
            
            [serverLabel setTextAlignment:UITextAlignmentRight];
            [serverLabel setFont:[UIFont systemFontOfSize:16]];
            [serverLabel sizeToFit];
            [serverLabel setBackgroundColor:[UIColor clearColor]];
            [serverLabel setFrame:CGRectMake(self.view.frame.size.width - right_indend_x - serverLabel.frame.size.width,
                                             cell_height / 2 - titleLabel.font.lineHeight / 2,
                                             serverLabel.frame.size.width, serverLabel.font.lineHeight)];
            
            [cell addSubview:titleLabel];
            [cell addSubview:serverLabel];
            
            [titleLabel release];
            [serverLabel release];
        }
            break;
        case 1:
        {
            MMTitleLabel* titleLabel = [[MMTitleLabel alloc]init];
            [titleLabel setText:NSLocalizedString(@"deviceManager", @"")];
            [titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
            [titleLabel setFrame:CGRectMake(HORIZONTAL_INDENT, cell_height / 2 - titleLabel.font.lineHeight / 2, 0, 0)];
            [titleLabel sizeToFit];
            
            MMSubtitleLabel* stateLabel = [[MMSubtitleLabel alloc]init];
            [stateLabel setText:[self setStringState:indexPath componentString:DEVICE_MANAGER]];
            if ([stateLabel.text isEqualToString: NSLocalizedString(@"stateStopped", @"")] || [stateLabel.text isEqualToString: NSLocalizedString(@"statePartial", @"")])
            {
                [cell setAccessoryType:accessoryType];
            }
            [stateLabel setFont:[UIFont systemFontOfSize:16]];
            [stateLabel sizeToFit];
            [stateLabel setFrame:CGRectMake(self.view.frame.size.width - right_indend_x - stateLabel.frame.size.width,
                                             cell_height / 2 - titleLabel.font.lineHeight / 2,
                                             stateLabel.frame.size.width, stateLabel.font.lineHeight)];
            
            [cell addSubview:stateLabel];
            [cell addSubview:titleLabel];
            
            [titleLabel release];
            [stateLabel release];
        }
            break;
        case 2:
        {
            MMTitleLabel* titleLabel = [[MMTitleLabel alloc]init];
            [titleLabel setText:NSLocalizedString(@"imagingServer", @"")];
            [titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
            [titleLabel setFrame:CGRectMake(HORIZONTAL_INDENT, cell_height / 2 - titleLabel.font.lineHeight / 2, 0, 0)];
            [titleLabel sizeToFit];
            
            MMSubtitleLabel* stateLabel = [[MMSubtitleLabel alloc]init];
            [stateLabel setText:[self setStringState:indexPath componentString:IMAGING_SERVER]];
            if ([stateLabel.text isEqualToString: NSLocalizedString(@"stateStopped", @"")] || [stateLabel.text isEqualToString: NSLocalizedString(@"statePartial", @"")])
            {
                [cell setAccessoryType:accessoryType];
            }
            [stateLabel setFont:[UIFont systemFontOfSize:16]];
            [stateLabel sizeToFit];
            [stateLabel setFrame:CGRectMake(self.view.frame.size.width - right_indend_x - stateLabel.frame.size.width,
                                            cell_height / 2 - titleLabel.font.lineHeight / 2,
                                            stateLabel.frame.size.width, stateLabel.font.lineHeight)];
            
            [cell addSubview:stateLabel];
            [cell addSubview:titleLabel];
            
            [titleLabel release];
            [stateLabel release];
        }
            break;
        case 3:
        {
            MMTitleLabel* titleLabel = [[MMTitleLabel alloc]init];
            [titleLabel setText:NSLocalizedString(@"environmentServer", @"")];
            [titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
            [titleLabel setFrame:CGRectMake(HORIZONTAL_INDENT, cell_height / 2 - titleLabel.font.lineHeight / 2, 0, 0)];
            [titleLabel sizeToFit];
            
            MMSubtitleLabel* stateLabel = [[MMSubtitleLabel alloc]init];
            [stateLabel setText:[self setStringState:indexPath componentString:ENVIRONMENTAL_SERVER]];
            if ([stateLabel.text isEqualToString: NSLocalizedString(@"stateStopped", @"")] || [stateLabel.text isEqualToString: NSLocalizedString(@"statePartial", @"")])
            {
                [cell setAccessoryType:accessoryType];
            }
            [stateLabel setFont:[UIFont systemFontOfSize:16]];
            [stateLabel sizeToFit];
            [stateLabel setFrame:CGRectMake(self.view.frame.size.width - right_indend_x - stateLabel.frame.size.width,
                                            cell_height / 2 - titleLabel.font.lineHeight / 2,
                                            stateLabel.frame.size.width, stateLabel.font.lineHeight)];
            
            [cell addSubview:stateLabel];
            [cell addSubview:titleLabel];
            
            [titleLabel release];
            [stateLabel release];
        }
            break;
        case 4:
        {
            if ([self.systemStatus count] >= 4)
            {
                NSLog(@"%d",systemStatus.count);
                MMTitleLabel* titleLabel = [[MMTitleLabel alloc]init];
                [titleLabel setText:NSLocalizedString(@"activeControl", @"")];
                [titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
                [titleLabel setFrame:CGRectMake(HORIZONTAL_INDENT, cell_height / 2 - titleLabel.font.lineHeight / 2, 0, 0)];
                [titleLabel sizeToFit];
                
                MMSubtitleLabel* stateLabel = [[MMSubtitleLabel alloc]init];
                [stateLabel setText:[self setStringState:indexPath componentString:ACTIVE_CONTROL]];
                if ([stateLabel.text isEqualToString: NSLocalizedString(@"stateStopped", @"")] || [stateLabel.text isEqualToString: NSLocalizedString(@"statePartial", @"")])
                {
                    [cell setAccessoryType:accessoryType];
                }
                [stateLabel setFont:[UIFont systemFontOfSize:16]];
                [stateLabel sizeToFit];
                [stateLabel setFrame:CGRectMake(self.view.frame.size.width - right_indend_x - stateLabel.frame.size.width,
                                                cell_height / 2 - titleLabel.font.lineHeight / 2,
                                                stateLabel.frame.size.width, stateLabel.font.lineHeight)];
                
                [cell addSubview:stateLabel];
                [cell addSubview:titleLabel];
                
                [titleLabel release];
                [stateLabel release];
            }
        }
            break;
    }
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{   
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *titleString = nil;
    for (id subview in cell.subviews) {
        if ([subview isKindOfClass:[UILabel class]])
        {
            titleString = ((UILabel*)subview).text;
            continue;
        }
    }
    int componentIndex = [indexPath section] - 1;
    detailedInfo = [[[UIAlertView alloc] init] autorelease];
    [detailedInfo addButtonWithTitle:NSLocalizedString(@"okButtonTitle", @"")];
    [detailedInfo setTitle:titleString];
    [detailedInfo setMessage:[(MMComponentStatus*)[systemStatus objectAtIndex:componentIndex] message]];
    [detailedInfo setDelegate:self];
    temporaryContainer = detailedInfo;
    [detailedInfo show];
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
	_reloading = YES;
    [self.view setUserInteractionEnabled:NO];
}

- (void)doneLoadingTableViewData{
	_reloading = NO;
    [self.view setUserInteractionEnabled:YES];
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:atableView];
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{	
	
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
	
	[self reloadTableViewDataSource];
    [self updateScreenInfo];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	
	return _reloading;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	
	return self.lastUpdateTime;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(errorAlert)
    {
        errorAlert = nil;
    }
    temporaryContainer = nil;
}

- (void)cancelCurrentConnections
{
    if (_reloading == YES)
    {
        [self doneLoadingTableViewData];
    }
    [super cancelCurrentConnections];
}

- (void)dealloc
{
    self.lastUpdateTime = nil;
    if (systemStatus)
    {
        [systemStatus release];
    }
    [super dealloc];
}
@end

