//
//  MMBadgeView.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMBadgeView.h"
#import "Constants.h"

#define DRAWLINE_WIDTH 2
#define BADGE_RADIUS   20
#define FONT_SIZE      12

@implementation MMBadgeView

@synthesize badgeText;
@synthesize badgeTextColor;
@synthesize badgeFrameColor;
@synthesize badgeCornerRoundness;

- (id)initWithInteger:(int)number
{
	self = [super initWithFrame:CGRectMake(0, 0, BADGE_RADIUS, BADGE_RADIUS)];
	if(self!=nil)
	{
        self.badgeTextColor = [UIColor whiteColor];
        self.badgeFrameColor = [UIColor whiteColor];
        self.badgeCornerRoundness = 0.40;
        [self setBadgeNumber:number];
        self.opaque = NO;
	}
	return self;
}

// Draws Method
- (void)drawRect:(CGRect)rect
{
	if(self.badgeText != nil && [self.badgeText length]>0)
	{
		CGContextRef context = UIGraphicsGetCurrentContext();
		CGContextSetShouldAntialias(context, YES);
        
        CGFloat radius = CGRectGetMaxY(rect)*self.badgeCornerRoundness;
        CGFloat puffer = CGRectGetMaxY(rect)*0.10;
        
        CGFloat maxX = CGRectGetMaxX(rect) - puffer;
        CGFloat maxY = CGRectGetMaxY(rect) - puffer;
        CGFloat minX = CGRectGetMinX(rect) + puffer;
        CGFloat minY = CGRectGetMinY(rect) + puffer;
        //create reusable path
        CGMutablePathRef path = CGPathCreateMutable();
        CGPathAddArc(path, &CGAffineTransformIdentity, maxX-radius, minY+radius, radius, M_PI+(M_PI/2), M_PI/2, 0);
        CGPathAddArc(path, &CGAffineTransformIdentity, minX+radius, maxY-radius, radius, M_PI/2, M_PI+M_PI/2, 0);
        CGPathCloseSubpath(path);
        
		CGLayerRef contentLayer = CGLayerCreateWithContext(context, rect.size, NULL);
		CGContextRef contentLayerContext = CGLayerGetContext(contentLayer);
        //append shadow
        CGContextBeginPath(contentLayerContext);
        CGContextAddPath(contentLayerContext, path);
        CGContextClosePath(contentLayerContext);
        //CGContextSetShadowWithColor(contentLayerContext, CGSizeMake(2,2), 3, [[UIColor blackColor] CGColor]);
        CGContextFillPath(contentLayerContext);
        
        //append gradient background
        CGContextBeginPath(contentLayerContext);
        CGContextAddPath(contentLayerContext, path);
        CGContextClosePath(contentLayerContext);
        CGContextClip(contentLayerContext);
        
        size_t num_locations = 2;
        CGFloat locations[2] = { 0.0, 1.0 };
        CGFloat components[8] = BADGE_COLOR_COMPONENTS;
        
        CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
        CGGradientRef gradient = CGGradientCreateWithColorComponents(colorspace, components, locations, num_locations);
        CGPoint startPoint, endPoint;
        startPoint.x = radius + DRAWLINE_WIDTH;
        startPoint.y = DRAWLINE_WIDTH;
        endPoint.x = radius + DRAWLINE_WIDTH;
        endPoint.y = radius * 2 + DRAWLINE_WIDTH;
        CGContextDrawLinearGradient (contentLayerContext, gradient, startPoint, endPoint,  kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
        CGGradientRelease(gradient);
        CGColorSpaceRelease(colorspace);
		CGContextDrawLayerInRect(context, rect, contentLayer);
		CGLayerRelease(contentLayer);
        
        //append badge frame
        CGContextSetLineWidth(context, DRAWLINE_WIDTH);
        CGContextSetStrokeColorWithColor(context, [self.badgeFrameColor CGColor]);
        CGContextBeginPath(context);
        CGContextAddPath(context, path);
        CGPathRelease(path);
        CGContextClosePath(context);
        CGContextStrokePath(context);
        
        //append badge text
        [badgeTextColor set];
        UIFont *textFont = [UIFont boldSystemFontOfSize:FONT_SIZE];
        CGSize textSize = [self.badgeText sizeWithFont:textFont];
        [self.badgeText drawAtPoint:CGPointMake((rect.size.width/2-textSize.width/2), (rect.size.height/2-textSize.height/2)) withFont:textFont];
	}
}

- (void)dealloc
{
	[badgeText release];
	[badgeTextColor release];
	[badgeFrameColor release];
    [super dealloc];
}

- (void)setBadgeNumber:(NSInteger)number
{	
	NSString *str = nil;
	str = [NSString stringWithFormat:@"%d", number];
	if (number != 0)
	{
		CGSize retValue = CGSizeMake(BADGE_RADIUS, BADGE_RADIUS);
		CGFloat rectWidth, rectHeight;
		CGSize stringSize = [str sizeWithFont:[UIFont boldSystemFontOfSize:FONT_SIZE]];
		CGFloat flexSpace;
		if ([str length]>=2) 
		{
			flexSpace = [str length]*1;
			rectWidth = BADGE_RADIUS / 5 * 3 + (stringSize.width + flexSpace); rectHeight = BADGE_RADIUS;
			retValue = CGSizeMake(rectWidth, rectHeight);
		}
		self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, retValue.width, retValue.height);
		self.badgeText = str;
	}
    else
	{
		CGSize retValue = CGSizeZero;
		self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, retValue.width, retValue.height);
		self.badgeText = nil;
	}
    
	[self setNeedsDisplay];
}

@end