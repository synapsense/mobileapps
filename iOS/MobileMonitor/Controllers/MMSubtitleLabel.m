//
//  MMSubtitleLabel.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMSubtitleLabel.h"

@implementation MMSubtitleLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [[ThemeManager sharedInstance] registerObserver:self];
        [self adjustUI:[ThemeManager sharedInstance].currentTheme];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (id)init
{
    return [self initWithFrame:CGRectZero];
}

- (void)dealloc
{
    [[ThemeManager sharedInstance]unregisterObserver:self];
    [super dealloc];
}

- (void)adjustUI:(MMDefaultTheme *)currentTheme
{
    self.textColor = currentTheme.subtitleLabelColor;
}
@end
