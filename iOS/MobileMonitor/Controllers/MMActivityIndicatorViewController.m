//
//  MMActivityIndicatorViewController.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMActivityIndicatorViewController.h"

@implementation MMActivityIndicatorViewController

- (void)loadView
{
    _activityIndicatorView = [[MMProgressPopupView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    _activityIndicatorView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    _activityIndicatorView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.view = _activityIndicatorView;
    [_activityIndicatorView release];
}

- (void)viewWillAppear:(BOOL)animated
{
    [_activityIndicatorView.indicatorView startAnimating];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [_activityIndicatorView.indicatorView stopAnimating];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end
