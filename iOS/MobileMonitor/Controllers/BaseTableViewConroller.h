//
//  BaseTableViewConroller.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface BaseTableViewConroller : UITableViewController


@property (nonatomic,retain)NSDate *timeStamp;

- (void)updateScreenInfo;     //change to real

@end
