//
//  OptionsSettingsViewController.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Constants.h"
#import "MMTableView.h"

@interface OptionsSettingsViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSArray             *optionsTableDataArray; 
    UITableViewCell     *currentCell;
    MMTableView         *atableView;
}
@property (nonatomic, retain) NSString *settingTitle;

- (id)initWithSettingString:(NSString *)settingString;

@end
