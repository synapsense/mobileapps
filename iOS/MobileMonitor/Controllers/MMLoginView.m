//
//  MMLoginView.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMLoginView.h"
#import <QuartzCore/QuartzCore.h>

#define TEXT_FIELD_INDENT 10

@interface MMLoginView()

- (void)handleBackgroundButtonTap;

@end

@implementation MMLoginView

@synthesize contentView;
@synthesize serverName;
@synthesize userName;
@synthesize password;
@synthesize loginButton;
@synthesize isLandscapeOrientation;
@synthesize logoImage;
@synthesize poweredByImage;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.autoresizesSubviews = YES;
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        UIButton *backgroundButton = [[UIButton alloc] initWithFrame:frame];
        backgroundButton.backgroundColor = [UIColor clearColor];
        [backgroundButton addTarget:self action:@selector(handleBackgroundButtonTap) forControlEvents:UIControlEventTouchDown];
        backgroundButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:backgroundButton];
        [backgroundButton release];
        
        logoImage = [[UIImageView alloc]initWithImage:[ThemeManager sharedInstance].currentTheme.logoImage];
        [logoImage setBackgroundColor:[UIColor clearColor]];
        [logoImage setFrame:CGRectMake(57, 10, 200, 110)];
        logoImage.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:logoImage];
        
        contentView = [[UIView alloc]initWithFrame:CGRectMake(0,
                                                              logoImage.frame.size.height + logoImage.frame.origin.y,
                                                              self.frame.size.width,
                                                              frame.size.height - logoImage.frame.size.height - 20 - HEIGHT_OF_FIELD - 10 * VERTICAL_INDENT)];
        contentView.backgroundColor = [UIColor clearColor];
        //on 4-inch screen this view will be higher
        contentView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
        [self addSubview:contentView];
        
        UIButton *backgroundContentButton = [[UIButton alloc] initWithFrame:contentView.bounds];
        backgroundContentButton.backgroundColor = [UIColor clearColor];
        [backgroundContentButton addTarget:self action:@selector(handleBackgroundButtonTap) forControlEvents:UIControlEventTouchDown];
        backgroundContentButton.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [contentView addSubview:backgroundContentButton];
        [backgroundContentButton release];
        
        serverNameTitle = [[MMTitleLabel alloc]init];
        [serverNameTitle setBackgroundColor:[UIColor clearColor]];
        [serverNameTitle setText:NSLocalizedString(@"serverNameWithColon", @"")];
        [serverNameTitle setFont:[UIFont systemFontOfSize:14]];
        [serverNameTitle setFrame:CGRectMake(HORIZONTAL_INDENT + TEXT_FIELD_INDENT, 0, 0, 0)];
        [serverNameTitle sizeToFit];
        serverNameTitle.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
        [contentView addSubview:serverNameTitle];
        [serverNameTitle release];
        
        serverName = [[MMTextField alloc]
                           initWithFrame:CGRectMake(HORIZONTAL_INDENT,
                                                    serverNameTitle.frame.origin.y + serverNameTitle.frame.size.height + VERTICAL_INDENT,
                                                    frame.size.width - 2 * HORIZONTAL_INDENT,
                                                    HEIGHT_OF_FIELD)];
        [serverName setText:[[AppDelegate sharedInstance].applicationPreferences stringForKey:SERVER_URL]];
        serverName.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
        [serverName setReturnKeyType:UIReturnKeyNext];
        
        serverName.borderStyle = UITextBorderStyleBezel;
        [serverName setAutocorrectionType:UITextAutocorrectionTypeNo];
        [serverName setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        serverName.horizontalPadding = TEXT_FIELD_INDENT;
        [serverName setTag:1];
        [contentView addSubview:serverName];

        userNameTitle = [[MMTitleLabel alloc]init];
        [userNameTitle setBackgroundColor:[UIColor clearColor]];
        userNameTitle.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        [userNameTitle setText:NSLocalizedString(@"userNameWithColon", @"")];
        [userNameTitle setFont:[UIFont systemFontOfSize:14]];
        [userNameTitle setFrame:CGRectMake(HORIZONTAL_INDENT + TEXT_FIELD_INDENT,
                                           serverName.frame.origin.y + serverName.frame.size.height + VERTICAL_INDENT,
                                           0,
                                           0)];
        [userNameTitle sizeToFit];
        [contentView addSubview:userNameTitle];
        [userNameTitle release];
        
        
        userName = [[MMTextField alloc]
                         initWithFrame:CGRectMake(HORIZONTAL_INDENT,
                                                  userNameTitle.frame.origin.y + userNameTitle.frame.size.height + VERTICAL_INDENT,
                                                  frame.size.width - 2 * HORIZONTAL_INDENT,
                                                  HEIGHT_OF_FIELD)];
        userName.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
        
        userName.borderStyle = UITextBorderStyleBezel;
        [userName setReturnKeyType:UIReturnKeyNext];
        [userName setText:[[[AppDelegate sharedInstance] applicationPreferences] stringForKey:@"Previous user"]];
        [userName setAutocorrectionType:UITextAutocorrectionTypeNo];
        [userName setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        userName.horizontalPadding = TEXT_FIELD_INDENT;
        [userName setTag:2];
        [contentView addSubview:userName];
        
        
        passwordTitle = [[MMTitleLabel alloc]init];
        [passwordTitle setBackgroundColor:[UIColor clearColor]];
        passwordTitle.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        [passwordTitle setText:NSLocalizedString(@"passwordWithcolon", @"")];
        [passwordTitle setFont:[UIFont systemFontOfSize:14]];
        [passwordTitle setFrame:CGRectMake(HORIZONTAL_INDENT + TEXT_FIELD_INDENT,
                                           userName.frame.origin.y + userName.frame.size.height + VERTICAL_INDENT,
                                           0,
                                           0)];
        [passwordTitle sizeToFit];
        [contentView addSubview:passwordTitle];
        [passwordTitle release];
        
        
        password = [[MMTextField alloc]
                         initWithFrame:CGRectMake(HORIZONTAL_INDENT,
                                                  passwordTitle.frame.origin.y + passwordTitle.frame.size.height + VERTICAL_INDENT,
                                                  frame.size.width - 2 * HORIZONTAL_INDENT,
                                                  HEIGHT_OF_FIELD)];
        password.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
        password.borderStyle = UITextBorderStyleBezel;
        [password setReturnKeyType:UIReturnKeyGo];
        [password setAutocorrectionType:UITextAutocorrectionTypeNo];
        [password setSecureTextEntry:YES];
        [password setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        password.horizontalPadding = TEXT_FIELD_INDENT;
        [password setTag:3];
        [contentView addSubview:password];
        
        
        self.loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [loginButton setFrame:CGRectMake(HORIZONTAL_INDENT,
                                         contentView.frame.origin.y + contentView.frame.size.height + VERTICAL_INDENT * 4,
                                         frame.size.width - 2 * HORIZONTAL_INDENT,
                                         HEIGHT_OF_FIELD)];
        [loginButton setBackgroundImage:[UIImage imageNamed:@"blueButton1pxl.png"] forState:UIControlStateNormal];
//        loginButton.layer.cornerRadius = 8.0f;
//        loginButton.layer.masksToBounds = YES;
        loginButton.layer.shadowOpacity = 3.0f;
        loginButton.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
        loginButton.layer.shadowColor = [UIColor blackColor].CGColor;

        [loginButton.titleLabel setShadowOffset:CGSizeMake(0.0, -0.75)];
        [loginButton setTitleShadowColor:[UIColor blackColor] forState:UIControlStateNormal];
        loginButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        [loginButton setTitle:NSLocalizedString(@"loginButton", @"") forState:UIControlStateNormal];
        [self addSubview:loginButton];
        
        
        poweredByImage = [[UIImageView alloc]
                          initWithImage:[ThemeManager sharedInstance].currentTheme.poweredByImage];
        [poweredByImage setCenter:CGPointMake(self.frame.size.width / 2,
                                              self.frame.size.height - poweredByImage.frame.size.height/2 - VERTICAL_INDENT)];
        [poweredByImage setBounds:CGRectMake(0, 0, loginButton.frame.size.width, 20)];
        poweredByImage.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:poweredByImage];
    }
    return self;
}

- (void)handleBackgroundButtonTap
{
    if ([self.delegate respondsToSelector:@selector(handleBackgroundTap)])
    {
        [delegate handleBackgroundTap];
    }
}

- (void)adjustUI:(MMDefaultTheme *)currentTheme
{
    [super adjustUI:currentTheme];
    poweredByImage.image = currentTheme.poweredByImage;
    logoImage.image = currentTheme.logoImage;
}

- (void)layoutSubviews
{
    BOOL isKeyboardVisible = serverName.isFirstResponder || userName.isFirstResponder || password.isFirstResponder;
    isLandscapeOrientation = UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]);
    [self.logoImage setHidden:isKeyboardVisible];
    [self.loginButton setHidden:isLandscapeOrientation];
    [self.poweredByImage setHidden:isKeyboardVisible];
    
    CGPoint offset = CGPointZero;
    
    if(isLandscapeOrientation == YES)
    {
        if (isKeyboardVisible)
        {
            if(userName.isFirstResponder || password.isFirstResponder)
            {
                offset = CGPointMake(self.contentView.frame.size.width / 2, self.contentView.frame.size.height / 4 - 42);
            }
            else
            {
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDuration:0.25];
                [contentView setFrame:CGRectMake(0,
                                                 15,
                                                 contentView.frame.size.width,
                                                 contentView.frame.size.height)];
                [UIView commitAnimations];
            }
        }
        else
        {
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.25];
            [contentView setFrame:CGRectMake(0,
                                             15,
                                             contentView.frame.size.width,
                                             contentView.frame.size.height)];
            [logoImage setHidden:YES];            
            [poweredByImage setCenter:CGPointMake(self.frame.size.width / 2,
                                                  self.frame.size.height - poweredByImage.frame.size.height / 2)];
            [UIView commitAnimations];
        }
    }
    else // isPortraitOrientation == YES
    {
        if(!isKeyboardVisible)
        {
            static float animationDuration = 0.0f;
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:animationDuration];
            [self.contentView setFrame:CGRectMake(0,
                                               logoImage.frame.size.height + logoImage.frame.origin.y,
                                             contentView.frame.size.width,
                                             contentView.frame.size.height)];
            [self.poweredByImage setCenter:CGPointMake(self.frame.size.width / 2,
                                                  self.frame.size.height - poweredByImage.frame.size.height/2 - VERTICAL_INDENT)];
            [UIView commitAnimations];
            animationDuration = 0.25f;
        }
        else//(self.isKeyboardVisible == YES)
        {
            offset = CGPointMake(160, self.frame.size.height / 2 - self.contentView.frame.size.height / 2 - VERTICAL_INDENT * 2);
        }
    }
    if (offset.x != 0 || offset.y != 0)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.25];
        [self.contentView setCenter:offset];
        [UIView commitAnimations];
    }
}

- (void)dealloc
{
    self.logoImage = nil;
    self.contentView = nil;
    self.serverName = nil;
    self.userName = nil;
    self.password = nil;
    self.loginButton = nil;
    self.poweredByImage = nil;
    [super dealloc];
}

- (void)showTextFieldForEditing:(MMTextField*)showingTextField
{
    //keyboard is shown
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    if (isLandscapeOrientation && showingTextField != serverName)
    {
        [self.contentView setCenter:CGPointMake(self.contentView.frame.size.width / 2, self.contentView.frame.size.height / 4 - 42)];
    }
    [UIView commitAnimations];
}

@end
