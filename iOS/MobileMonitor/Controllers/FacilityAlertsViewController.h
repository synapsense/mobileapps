//
//  FacilityAlertsViewController.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertsDetailsViewController.h"
#import "SortViewController.h"
#import "BaseViewController.h"
#import "FilterViewController.h"
#import "MMDataCenterInfo.h"
#import "EGORefreshTableHeaderView.h"
#import "FacilityAlertCell.h"
#import "Constants.h"
#import "MMTableView.h"

@class AlertsDetailsViewController;

@interface FacilityAlertsViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, UIGestureRecognizerDelegate, EGORefreshTableHeaderDelegate>
{
    NSString *dcGuid;
    AlertsDetailsViewController *alertsDetailsViewController;
    SortViewController* sortViewController;
    FilterViewController* filterViewController;
    MMTableView* sectionTableView;
    UILongPressGestureRecognizer* longPressRecognizer;
    NSMutableArray* alertArray;
    NSInteger numberAlertsAll;
    NSInteger currentNumberAlerts;
    NSInteger oldNumberAlerts;
    MMAlertStatus statusFilter;
    MMAlertPriority priorityFilter;
    MMAlertSort sortAlerts;
    MMDataCenterInfo* curentDataCenter;
    EGORefreshTableHeaderView *_refreshHeaderView;
	BOOL _reloading;
    NSInteger indexPathCell;
    BOOL dismissAll;
    BOOL doPaging;
    UIAlertView *errorAlert;
    
    int currentIndexForDetailedInfo;
}

@property(retain, nonatomic) MMTableView* sectionTableView;
@property(nonatomic, retain) NSMutableArray* alertArray;
@property(nonatomic, retain) FilterViewController* filterViewController;
@property (nonatomic,retain) NSDate *lastUpdateTime;

- (void)loadSections;
- (MMView*)loadHeader;
- (void)editing;

- (id)initWithDataCenter:(MMDataCenterInfo*)dataCenter guid:(NSString*)guid;
- (void)loadSortAlerts;

- (void)getNextAlert;
- (void)getPreviousAlert;


@end
