//
//  FacilityAlertCell.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "FacilityAlertCell.h"
#import "Constants.h"

//#define ALERT_STATUS_WIDTH 95

@implementation FacilityAlertCell

@synthesize alertObject;
@synthesize alertName;
@synthesize alertDate;
@synthesize icon;

- (void)dealloc
{
    [alertObject release];
    [alertName release];
    [alertDate release];
    [icon release];
    [super dealloc];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
    }
    return  self;
}

-(id)initWithFrameAlert:(CGRect)frame Status:(MMAlertStatus)status
{
    self = [super initWithFrame:frame];
    if (self) {
        float portraitWidth = [UIScreen mainScreen].bounds.size.width;
        if (status == MMAlertStatusOpened) 
        {
            alertObject = [[MMSubtitleLabel alloc] initWithFrame:CGRectMake(HORIZONTAL_INDENT, frame.size.height - 25, portraitWidth - 2 * HORIZONTAL_INDENT - WIDTH_TIMESTAMP + 20, SIZE_ICON - 3)];
            alertObject.lineBreakMode = UILineBreakModeTailTruncation;
            alertObject.font = [UIFont boldSystemFontOfSize:11.0f];
            alertObject.textAlignment = UITextAlignmentLeft;
            alertObject.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            alertObject.contentMode = UIViewContentModeLeft;
            [self.contentView addSubview:alertObject];
            
            alertName = [[MMTitleLabel alloc] initWithFrame:CGRectMake(alertObject.frame.origin.x, alertObject.frame.origin.y - 25,
                                                                       alertObject.frame.size.width, alertObject.frame.size.height + 5)];
            
            alertDate = [[MMSubtitleLabel alloc] initWithFrame:CGRectMake(portraitWidth - HORIZONTAL_INDENT - WIDTH_TIMESTAMP + 20, SIZE_ICON + 6, WIDTH_TIMESTAMP - 20, HEIGHT_TIMESTAMP * 3)];
            alertDate.lineBreakMode = UILineBreakModeTailTruncation;
            alertDate.font = [UIFont boldSystemFontOfSize:7.0f];
            alertDate.textAlignment = UITextAlignmentCenter;
            alertDate.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
            alertDate.numberOfLines = 2;
            [self.contentView addSubview:alertDate];
            
            icon = [[UIImageView alloc]initWithFrame:CGRectMake(alertDate.frame.origin.x + alertDate.frame.size.width / 2 - SIZE_ICON / 2, 6, SIZE_ICON, SIZE_ICON)];
            icon.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
            icon.backgroundColor = [UIColor clearColor];
            [self.contentView addSubview:icon];
        }
        else
        {
            alertObject = [[MMSubtitleLabel alloc] initWithFrame:CGRectMake(HORIZONTAL_INDENT, frame.size.height - 25, portraitWidth - 2 * HORIZONTAL_INDENT, SIZE_ICON - 3)];
            alertObject.lineBreakMode = UILineBreakModeTailTruncation;
            alertObject.font = [UIFont boldSystemFontOfSize:11.0f];
            alertObject.textAlignment = UITextAlignmentLeft;
            alertObject.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            alertObject.contentMode = UIViewContentModeLeft;
            [self.contentView addSubview:alertObject];
            
        }
        alertName = [[MMTitleLabel alloc] initWithFrame:CGRectMake(alertObject.frame.origin.x, alertObject.frame.origin.y - 25,
                                                                   alertObject.frame.size.width, alertObject.frame.size.height + 5)];
        alertName.lineBreakMode = UILineBreakModeTailTruncation;
        alertName.font = [UIFont boldSystemFontOfSize:13.0f];
        alertName.textAlignment = UITextAlignmentLeft;
        alertName.contentMode = UIViewContentModeLeft;
        alertName.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self.contentView addSubview:alertName];

        self.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:YES animated:YES];
}

//- (void)layoutSubviews
//{
//    [super layoutSubviews];
//    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) 
//    {
//        [alertName setFrame:CGRectMake(alertName.frame.origin.x, alertName.frame.origin.y, self.frame.size.width - 2*HORIZONTAL_INDENT - SIZE_ICON, alertName.frame.size.height)];
//    }
//    else
//    {
//        [alertName setFrame:CGRectMake(alertObject.frame.origin.x, alertObject.frame.origin.y - 25,
//                                       alertObject.frame.size.width - ALERT_STATUS_WIDTH, alertObject.frame.size.height + 5)];
//    }
//    float alertStatusX = alertName.frame.size.width + HORIZONTAL_INDENT;
//    if ([alertName.text sizeWithFont:[UIFont boldSystemFontOfSize:13.0f]].width < alertName.frame.size.width)
//    {
//        alertStatusX = [alertName.text sizeWithFont:[UIFont boldSystemFontOfSize:13.0f]].width + HORIZONTAL_INDENT + 10;
//    }
//    [alertStatus setFrame:CGRectMake(alertStatusX, alertName.frame.origin.y, ALERT_STATUS_WIDTH, alertName.frame.size.height)];
//}

@end