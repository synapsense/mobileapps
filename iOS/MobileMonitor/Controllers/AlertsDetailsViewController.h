//
//  AlertsDetailsAndResolutionViewController.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "MMDataCenterInfo.h"
#import "FacilityAlertsViewController.h"
#import "MMTableView.h"

@class MMAlertInfo;
@class FacilityAlertsViewController;

@interface AlertsDetailsViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>

{
    NSMutableString *acceptedAlertMessage;
    int prevTag;
    FacilityAlertsViewController* parent;
}

- (id)initWithAlertInfo:(NSMutableArray *)alertInformation;
- (void)loadSections;
- (void)handleSwipeRightDirection;
- (void)handleSwipeLeftDirection;
+ (NSString *)toStringAlertPriority:(int)valueOfProirity;
+ (NSString *)toStringAlertStatus:(int)valueOfStatus;
-(id)initWithAlertInfo:(NSMutableArray *)alertInformation;
-(void)loadSections;
-(void)showPopup:(NSInteger)tag;
+(NSString *)toStringAlertPriority:(int)valueOfProirity;
+(NSString *)toStringAlertStatus:(int)valueOfStatus;

@property(nonatomic, retain) MMDataCenterInfo *currentDataCenter;
@property(nonatomic, retain) NSArray *arrayObject;
@property(nonatomic, retain) MMTableView *sectionTableView;
@property(nonatomic, retain) MMAlertInfo *alertInfo;
@property(nonatomic, assign) FacilityAlertsViewController* parent;

@end
