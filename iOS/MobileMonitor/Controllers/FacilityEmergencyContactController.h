//
//  FacilityPhoneAPIsController.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "BaseViewController.h"
#import "MMDataCenterInfo.h"
#import "MMEmergencyContact.h"
#import "MMTableView.h"
#import "MMUnderlinedLabel.h"

@interface FacilityEmergencyContactController : BaseViewController<UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate, MMUnderlinedLabelDelegate>
{
    MMTableView *sectionTableView;
    NSArray *titleTableDataArray;
    UIAlertView *errorAlert;
    NSString* dcGuid;
    MMDataCenterInfo *curentDataCenter;
}

- (id)initWithDataCenter:(MMDataCenterInfo*)dataCenter guid:(NSString*)guid;
- (void)loadSections;
- (void)createEmailLetter;

@property(nonatomic, retain) MMEmergencyContact *emergencyContact;

@end
