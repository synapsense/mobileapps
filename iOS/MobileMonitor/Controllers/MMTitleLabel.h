//
//  MMTitleLabel.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ThemeManager.h"

@interface MMTitleLabel : UILabel<ThemeListenerControl>

@end
