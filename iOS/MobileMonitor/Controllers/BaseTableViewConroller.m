//
//  BaseTableViewConroller.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "BaseTableViewConroller.h"


@implementation BaseTableViewConroller

#define DERIVED_TIMEOUT 30.0 //Change accordings to user preferences

@synthesize timeStamp;

#pragma mark - View lifecycle

- (void)updateScreenInfo
{
    [self setTimeStamp:[NSDate date]];
    NSLog(@"Data was succesfully updated");
    //updating dataObject using API is going to be here
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

- (void)dealloc
{
    self.timeStamp = nil;
    [super dealloc];
}


@end
