//
//  FilterViewController.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewController.h"
#import "MMAlertInfo.h"
#import "MMliveImagingType.h"
#import "MMTableView.h"

@protocol LiveImagingTypeDelegate <NSObject>

@property (nonatomic, retain) MMliveImagingType* liveImagingType;

@end

@interface FilterViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>
{
        MMTableView* sectionTableView;
        NSDictionary* filterTableDataDictionary;
        MMAlertStatus statusFilter;
        MMAlertPriority priorityFilter;
        NSString* filterTitle;
        NSArray* livelImagingArray;
        UITableViewCell* currentCell;
}

@property (nonatomic, assign)id<LiveImagingTypeDelegate> liveImagingTypeDelegate;

- (id)initWithFilterString:(NSString*)filterString;
- (id)initWithFilterString:(NSString*)filterString FilterArray:(NSArray*)filterArray;
@end
