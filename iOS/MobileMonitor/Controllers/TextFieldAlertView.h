//
//  TextFieldAlertView.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFieldAlertView : UIAlertView
{
    UITextField *textField;
    NSString *_message;
}

- (void)setKeyboardType:(UIKeyboardType)type;

@end
