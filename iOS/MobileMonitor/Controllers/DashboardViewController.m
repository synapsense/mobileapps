//
//  DashboardViewController.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "DashboardViewController.h"
#import "MMSDK.h"
#import "MMTitleLabel.h"
#import "MMSubtitleLabel.h"
#import "MMTableViewCell.h"

#define PORTRAIT_WIDTH          640
#define PORTRAIT_HEIGHT         960
#define LANDSCAPE_WIDTH_CELL    960
#define LANDSCAPE_HIEGHT_CELL   50
#define PORTRAIT_HEIGHT_CELL    60
#define IMAGE_SIZE              37
#define SEPARATOR_HEIGHT        1

@interface DashboardViewController(private)
- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;
@end

@implementation DashboardViewController

@synthesize values, 
            lastUpdateTime;

- (id)initWithData:(NSMutableArray *)dataValues
{
    self = [super init];
    if (self) {
        self.values = dataValues;
    }
    return self;
}

- (void) updateScreenInfo
{
    [super updateScreenInfo];
    if (errorAlert)
    {
        [errorAlert dismissWithClickedButtonIndex:errorAlert.cancelButtonIndex animated:NO];
        errorAlert = nil;
    }
    __block int taskIdGetDataCenters = 0;
    __block int taskIdGetNumWarnings = 0;
    __block int taskIdGetNumActAlerts = 0;
    MMSDK *sdk = [MMSDK sharedInstance];
    taskIdGetDataCenters = [sdk getDataCentersWithSuccess:^(NSArray *dataCentersInfo) 
    {
        [self taskFinished:taskIdGetDataCenters];
        taskIdGetNumWarnings = [sdk getNumWSNWarningsWithSuccess:^(int count) 
        {
            [self taskFinished:taskIdGetNumWarnings];
            int numDC = [dataCentersInfo count];
            int totalRacks = 0;
            int totalCoolingUnits = 0;
            double totalArea = 0;
            double minPue = 999.999;
            double maxPue = -999.999;
            NSString *areaUnits = nil;
            
            for (int i = 0; i < numDC; i++)
            {
                MMDataCenterInfo *dcInfo = [dataCentersInfo objectAtIndex:i];
                totalRacks += dcInfo.totalRacksNumber;
                totalCoolingUnits += dcInfo.totalCracsNumber;
                totalArea += dcInfo.area;
                if (!areaUnits)
                {
                    areaUnits = dcInfo.areaUnits;
                }
                
                if(dcInfo.pue.value > 0 && dcInfo.pue.value < minPue )
                {
                    minPue = dcInfo.pue.value;
                }
                
                if( dcInfo.pue.value > maxPue )
                {
                    maxPue = dcInfo.pue.value;
                }
            }
            taskIdGetNumActAlerts = [sdk getNumActiveAlerts:nil withPeriod:[NSNull null] withPriority:[NSNull null] withStatus:[NSNull null]success:^(int totalAlertsCount) 
            {
                [self taskFinished:taskIdGetNumActAlerts];
                 self.values = [NSMutableArray arrayWithObjects:[NSNumber numberWithInt:numDC],[NSNumber numberWithInt:totalAlertsCount],[NSNumber numberWithInt:count],[NSNumber numberWithInt:totalRacks],[NSNumber numberWithInt:totalCoolingUnits],[NSNumber numberWithFloat:minPue],[NSNumber numberWithFloat:maxPue], [NSArray arrayWithObjects:[NSNumber numberWithFloat:totalArea], areaUnits, nil], nil];
                self.lastUpdateTime = [NSDate date];
                self.viewDidAppear = YES;
                [self doneLoadingTableViewData];
                [atableView reloadData];
                [self updateHasFinished];
            } failure:^(NSError *error) 
            {
                [self taskFinished:taskIdGetNumActAlerts];
                [self updateHasFinished];
                if (_reloading == YES)
                {
                    [self doneLoadingTableViewData];
                }
                if (!errorAlert)
                {
                    errorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"updatingError", @"") message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self cancelButtonTitle:NSLocalizedString(@"okButtonTitle", @"") otherButtonTitles: nil];
                    [errorAlert show];
                    [errorAlert release];
                }
                NSLog(@"%@",error.localizedDescription);
            }];
            [self addTaskId:taskIdGetNumActAlerts];
        } failure:^(NSError *error)
        {
            [self taskFinished:taskIdGetNumWarnings];
            [self updateHasFinished];
            if (_reloading == YES)
            {
                [self doneLoadingTableViewData];
            }
            if (!errorAlert)
            {
                errorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"updatingError", @"") message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self cancelButtonTitle:NSLocalizedString(@"okButtonTitle", @"") otherButtonTitles: nil];
                [errorAlert show];
                [errorAlert release];
            }
            NSLog(@"%@",error.localizedDescription);
        }];
        [self addTaskId:taskIdGetNumWarnings];
    } failure:^(NSError *error) 
    {
        [self taskFinished:taskIdGetDataCenters];
        [self updateHasFinished];
        if (_reloading == YES)
        {
            [self doneLoadingTableViewData];
        }
        if (!errorAlert)
        {
            errorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"updatingError", @"") message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self cancelButtonTitle:NSLocalizedString(@"okButtonTitle", @"") otherButtonTitles: nil];
            [errorAlert show];
            [errorAlert release];
        }
        NSLog(@"%@",error.localizedDescription);
    }];
    [self addTaskId:taskIdGetDataCenters];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


-(void)loadView
{
    self.view = [[[MMView alloc] initWithFrame:CGRectMake(0, 0, PORTRAIT_WIDTH, PORTRAIT_HEIGHT)] autorelease];
    atableView = [[[MMTableView alloc]initWithFrame:self.view.frame style:UITableViewStylePlain]autorelease];
    [atableView setDataSource:self];
    [atableView setDelegate:self];
    [atableView setSeparatorStyle: UITableViewCellSeparatorStyleNone];
    [atableView setScrollEnabled:YES];
    [atableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [self.view addSubview:atableView];
    self.lastUpdateTime = [NSDate date];
    if (_refreshHeaderView == nil) {
		
		EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - atableView.bounds.size.height, self.view.frame.size.width, atableView.bounds.size.height)];
		view.delegate = self;
        [view setDateFormat:[AppDelegate sharedInstance].userPreferences.dateFormat];
		[atableView addSubview:view];
		_refreshHeaderView = view;
		[view release];
        [self doneLoadingTableViewData];
	}
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateScreenInfo];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [atableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([AppDelegate sharedInstance].uiPrivileges.showPUE)
    {
        return 8;
    }
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseIdentifier = [NSString stringWithFormat:@"section %d",indexPath.section];
    MMTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (!cell)
    {
        cell = [[[MMTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier] autorelease];
        //    [cell setBounds:CGRectMake(0, 0, cell.bounds.size.width, self.view.frame.size.height / 8)];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        //draw separator
        UILabel *_separator = [[UILabel alloc] initWithFrame:CGRectMake(HORIZONTAL_INDENT, PORTRAIT_HEIGHT_CELL - SEPARATOR_HEIGHT, cell.frame.size.width - 2*HORIZONTAL_INDENT, SEPARATOR_HEIGHT)];
        [_separator setBackgroundColor:[UIColor darkGrayColor]];
        _separator.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [cell addSubview:_separator];
        [_separator release];
    }
        switch ([indexPath section])
        {
            case 0:
            {
                cell.textLabel.text = NSLocalizedString(@"totalDataCentersColon", @"");
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",values ? [values objectAtIndex:[indexPath section]] : @""];
            }
                break;
            case 1:
            {
                cell.textLabel.text = NSLocalizedString(@"totalActiveAlertsColon", @"");
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",values ? [values objectAtIndex:[indexPath section]] : @""];
            }
                break;
            case 2:
            {
                cell.textLabel.text = NSLocalizedString(@"totalWSNWarningsColon", @"");
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",values ? [values objectAtIndex:[indexPath section]] : @""];
            }
                break;
            case 3:
            {
                cell.textLabel.text = NSLocalizedString(@"totalRacksColon", @"");
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",values ? [values objectAtIndex:[indexPath section]] : @""];
            }
                break;
            case 4:
            {
                cell.textLabel.text = NSLocalizedString(@"totalCoolingUnitsColon", @"");
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",values ? [values objectAtIndex:[indexPath section]] : @""];
            }
                break;
            case 5:
            {
                if ([AppDelegate sharedInstance].uiPrivileges.showPUE)
                {
                    cell.textLabel.text = NSLocalizedString(@"minPUEColon", @"");
                    cell.detailTextLabel.text = values ? [[AppDelegate sharedInstance].userPreferences formatDouble:[[values objectAtIndex:[indexPath section]]doubleValue]] : @"";
                    break;
                }
            }
            case 6:
            {
                if ([AppDelegate sharedInstance].uiPrivileges.showPUE)
                {
                    cell.textLabel.text = NSLocalizedString(@"maxPUEColon", @"");
                    cell.detailTextLabel.text = values ? [[AppDelegate sharedInstance].userPreferences formatDouble:[[values objectAtIndex:[indexPath section]]doubleValue]] : @"";
                    break;
                }
            }
            case 7:
            {
                int currentPath = 7;
                cell.textLabel.text = NSLocalizedString(@"totalArea", @"");
                NSMutableString *valueString = [NSMutableString stringWithFormat:@"%d", [[[values objectAtIndex:currentPath] objectAtIndex:0]intValue]];
                BOOL hasUnits = [((NSString*)[[values objectAtIndex:currentPath] objectAtIndex:1]) compare:@""] != NSOrderedSame;
                if (hasUnits)
                {
                    [valueString appendFormat:@" %@",[[values objectAtIndex:currentPath] objectAtIndex:1]];
                }
                cell.detailTextLabel.text = valueString;
            }
                break;
        }
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return PORTRAIT_HEIGHT_CELL;
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
	_reloading = YES;
    [self.view setUserInteractionEnabled:NO];
}

- (void)doneLoadingTableViewData{
	_reloading = NO;
    [self.view setUserInteractionEnabled:YES];
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:atableView];
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{	
	
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
	
	[self reloadTableViewDataSource];
    [self updateScreenInfo];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	
	return _reloading;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	
	return lastUpdateTime;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (errorAlert)
    {
        errorAlert = nil;
    }
}

- (void)cancelCurrentConnections
{
    if (_reloading == YES)
    {
        [self doneLoadingTableViewData];
    }
    [super cancelCurrentConnections];
}

- (void)dealloc
{
    if (values)
    {
        [values release];        
    }
    _refreshHeaderView = nil;
    self.lastUpdateTime = nil;
    [super dealloc];
}

@end

