//
//  FacilityAlertsViewController.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "FacilityAlertsViewController.h"
#import "MMSDK.h"
#import "DownloadingResultsCell.h"
#import "AppUtility.h"
#import "Constants.h"
#import "MMTitleLabel.h"

@interface FacilityAlertsViewController(private)
- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;
- (void)pagingCancelClicked;
@end

@implementation FacilityAlertsViewController

@synthesize sectionTableView;
@synthesize alertArray;
@synthesize filterViewController;
@synthesize lastUpdateTime;

#define SECTION_TAG             1
//#define HEIGHT_HEADER           40
#define HEIGHT_SEPARETER_LINE   2
#define WIDTH_LABEL             260
#define IDENTATION              10
#define TIMESTAMP_X             255
#define DISMISS_ALL             0
#define DISMISS_ALERT           1
#define PAGING_COUNT            20
#define HEADER_ICON_SIZE        25
#define SEPARATOR_HEIGHT        1

#pragma mark - View lifecycle

- (void)updateScreenInfo
{
    [super updateScreenInfo];
    if (errorAlert)
    {
        [errorAlert dismissWithClickedButtonIndex:errorAlert.cancelButtonIndex animated:NO];
        errorAlert = nil;
    }
    int requestAlertsCount = currentNumberAlerts + (doPaging == YES ? PAGING_COUNT : 0);
    MMSDK *sdk = [MMSDK sharedInstance];
    __block int taskIdGetActiveAlerts = 0;
    __block int taskIdGetNumActALerts = 0;
    taskIdGetActiveAlerts = [sdk getActiveAlerts:dcGuid withPeriod:[NSNull null] withPriority:[NSNumber numberWithInt:[AppDelegate sharedInstance].alertPriorityFilter] withStatus:[NSNumber numberWithInt: [AppDelegate sharedInstance].alertStatusFilter] withStart:[NSNull null] withCount:[NSNumber numberWithInt:requestAlertsCount] success:^(NSArray *activeAlerts)
     {
         [self taskFinished:taskIdGetActiveAlerts];
         taskIdGetNumActALerts = [sdk getNumActiveAlerts:dcGuid withPeriod:[NSNull null] withPriority:[NSNull null] withStatus:[NSNumber numberWithInt:3] success:^(int count) 
         {
             [self taskFinished:taskIdGetNumActALerts];
             if (doPaging == YES)
             {
                 oldNumberAlerts = currentNumberAlerts;
                 doPaging = NO;
             }
             if ([self.alertArray count] != 0)
                 [self.alertArray removeAllObjects];
             
             self.alertArray = [NSMutableArray arrayWithArray:activeAlerts];
             currentNumberAlerts = [activeAlerts count];
             numberAlertsAll = count;
             [self loadSortAlerts];
             UIView *header = [self loadHeader];
             [self.view addSubview:header];
             self.lastUpdateTime = [NSDate date];
             self.viewDidAppear = YES;
             [sectionTableView reloadData];
             [self doneLoadingTableViewData];
             [self updateHasFinished];
         } failure:^(NSError *error) 
         {
             [self taskFinished:taskIdGetNumActALerts];
             if (_reloading == YES)
             {
                 [self doneLoadingTableViewData];
             }
             [self updateHasFinished];
             if (!errorAlert)
             {
                 errorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"updatingError", @"") message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self cancelButtonTitle:NSLocalizedString(@"okButtonTitle", @"") otherButtonTitles: nil];
                 [errorAlert show];
                 [errorAlert release];
             }
             NSLog(@"%@", error.localizedDescription);
         } ];
         [self addTaskId:taskIdGetNumActALerts];
         
     } failure:^(NSError *error) 
    {
        [self taskFinished:taskIdGetActiveAlerts];
         [self updateHasFinished];
         if (_reloading == YES)
         {
             [self doneLoadingTableViewData];
         }
         if (!errorAlert)
         {
             errorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"updatingError", @"") message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self cancelButtonTitle:NSLocalizedString(@"okButtonTitle", @"") otherButtonTitles: nil];
             [errorAlert show];
             [errorAlert release];
         }
         NSLog(@"%@", error.localizedDescription);
     }];
    [self addTaskId:taskIdGetActiveAlerts];
}

- (id)initWithDataCenter:(MMDataCenterInfo*)dataCenter guid:(NSString*)guid
{
    self = [super init];
    if (self)
    {
        dcGuid = guid;
        curentDataCenter = dataCenter;
        sortViewController = [[SortViewController alloc] init];
        filterViewController = [[FilterViewController alloc] initWithFilterString:NSLocalizedString(@"filter", @"")];
        doPaging = NO;
        [self loadSections];
    }
    return self;
}
- (void)loadView
{
    self.view = [[[MMView alloc] initWithFrame:[[UIScreen mainScreen] bounds] viewLevel:ViewLevelSecond] autorelease];
    longPressRecognizer = [[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress)] autorelease];
    [self.view addGestureRecognizer:longPressRecognizer];
    
    [self.view addSubview:[self loadHeaderWithTitle:[curentDataCenter name] titleWidth:self.view.frame.size.width viewLevel:ViewLevelSecond]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    oldNumberAlerts = -1;
    currentNumberAlerts = PAGING_COUNT;
    [self updateScreenInfo];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)dealloc
{
    [alertsDetailsViewController release];
    [sortViewController release];
    [filterViewController release];
    self.alertArray = nil;
    _refreshHeaderView = nil;
    self.lastUpdateTime = nil;

    [super dealloc];
}

- (void)loadSections
{
	sectionTableView = [[[MMTableView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x,
                                                                    2 * HEIGHT_HEADER,
                                                                    self.view.frame.size.width,
                                                                    self.view.frame.size.height - 1.5 * HEIGHT_CELL)
                                                                    style:UITableViewStylePlain]autorelease];
	sectionTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    sectionTableView.showsVerticalScrollIndicator = YES;
    sectionTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    sectionTableView.exclusiveTouch = YES;
    sectionTableView.scrollEnabled = YES;

	[self.view addSubview:sectionTableView];

    sectionTableView.delegate = self;
    sectionTableView.dataSource = self;
    self.lastUpdateTime = [NSDate date];
    if (_refreshHeaderView == nil)
    {
        EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - sectionTableView.bounds.size.height, self.view.frame.size.width, sectionTableView.bounds.size.height)];
        view.delegate = self;
        [view setDateFormat:[AppDelegate sharedInstance].userPreferences.dateFormat];
        [sectionTableView addSubview:view];
        _refreshHeaderView = view;
        [view release];
	}
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    int numberOfRows = [alertArray count];
    if (doPaging == YES)
    {
        numberOfRows++;
    }
    return numberOfRows;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *)indexPath
{
    if (doPaging && indexPath.row == alertArray.count)
    {
        return 40;
    }
    return HEIGHT_CELL;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    if (indexPath.row < alertArray.count) 
    {
        CGRect rect = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, tableView.frame.size.width, HEIGHT_CELL);
        MMAlertInfo *alertInfo = [alertArray objectAtIndex:indexPath.row];
        
        FacilityAlertCell *cell = [[[FacilityAlertCell alloc]initWithFrameAlert:rect Status:alertInfo.alertStatus] autorelease];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:[UIColor clearColor]];
        cell.alertObject.text = alertInfo.objectName != nil ? alertInfo.objectName : @"";
        cell.alertName.text = alertInfo.name;
        
        NSDateFormatter* dateFormater = [[NSDateFormatter alloc] init];
        [dateFormater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString* formatedString = [dateFormater stringFromDate:alertInfo.alertTimestamp];
        [dateFormater release];
        
        cell.alertDate.text = formatedString;
        switch (alertInfo.alertPriority)
        {
            case MMAlertPriorityInformational:[cell.icon setImage:[UIImage imageNamed:@"Alert-Blue.png"]];
                break;
            case MMAlertPriorityMinor:[cell.icon  setImage:[UIImage imageNamed:@"Alert-Yellow.png"]];
                break;
            case MMAlertPriorityMajor:[cell.icon  setImage:[UIImage imageNamed:@"Alert-Orange.png"]];
                break;
            case MMAlertPriorityCritical:[cell.icon setImage:[UIImage imageNamed:@"Alert-red.png"]];
        }
        
//        switch (alertInfo.alertStatus) 
//        {
//            case MMAlertStatusOpened: [cell.alertStatus setText:NSLocalizedString(@"statusOpened", @"")];
//                break;
//            case MMAlertStatusAcknowledged:[cell.alertStatus setText:NSLocalizedString(@"statusAcknowledged", @"")];
//                break;
//            case MMAlertStatusDismissed:[cell.alertStatus setText:NSLocalizedString(@"statusDismissed", @"")];
//                break;
//            case MMAlertStatusResolved:[cell.alertStatus setText:NSLocalizedString(@"statusResolved", @"")];;
//        }
        UILabel *_separator = [[UILabel alloc] initWithFrame:CGRectMake(HORIZONTAL_INDENT, HEIGHT_CELL - SEPARATOR_HEIGHT, cell.frame.size.width - 2*HORIZONTAL_INDENT, SEPARATOR_HEIGHT)];
        [_separator setBackgroundColor:[UIColor darkGrayColor]];
        _separator.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [cell addSubview:_separator];
        [_separator release];
        return cell;
    }
    else
    {
        DownloadingResultsCell* cell = (DownloadingResultsCell*)[tableView dequeueReusableCellWithIdentifier:@"UISingleSearchResultListDownloadingResultCell"];
        if (cell == nil)
        {
            cell = [[[DownloadingResultsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UISingleSearchResultListDownloadingResultCell"] autorelease];
        }
        [cell setTopMessage:NSLocalizedString(@"Getting more results", @"")];
        [cell setBottomMessage:nil];
        [cell showSpinner:YES];
        [cell showCancelButton:YES target:self action:@selector(pagingCancelClicked)];
        return cell;
    }
}

-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)showAlertView:(BOOL)noComent
{
    if (noComent)
    {
        UIAlertView* alert = [[[UIAlertView alloc] initWithTitle:nil
                                                                message:NSLocalizedString(@"noComment", @"") 
                                                                delegate:self 
                                                                cancelButtonTitle:nil
                                                                otherButtonTitles:NSLocalizedString(@"okButtonTitle", @""), nil] autorelease];
        [alert setTag:2];
        [alert show];
    }
    else
    {
        UIAlertView* alert = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"dismissButtonTitle", @"") 
                                                                message:NSLocalizedString(@"alertViewMessage", @"") 
                                                               delegate:self 
                                                      cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                                      otherButtonTitles:NSLocalizedString(@"okButtonTitle", @""), nil] autorelease];
        [alert setTag:4];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        [alert show];
    }
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        MMAlertInfo *alertInfo = [alertArray objectAtIndex:indexPath.row];
        if (alertInfo.alertStatus != MMAlertStatusResolved && alertInfo.alertStatus != MMAlertStatusDismissed)
        {
            [sectionTableView beginUpdates];
            [self showAlertView:NO];
            indexPathCell = indexPath.row;
            [sectionTableView endUpdates];
        }
       else
        [sectionTableView reloadData];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (errorAlert)
    {
        errorAlert = nil;
    }
    if (alertView.tag == 4 && buttonIndex == 1) 
    {
        if ([[alertView textFieldAtIndex:0].text length] != 0)
        {
            // OK button pressed
            if (dismissAll == NO)
            {
                MMAlertInfo *alertInfo = [alertArray objectAtIndex:indexPathCell];
                MMSDK *sdk = [MMSDK sharedInstance];
                [sdk changeAlertState:alertInfo.alertId withState:MMAlertStatusDismissed comment:[[alertView textFieldAtIndex:0]text] success:^(BOOL isChanged)
                {
                    if (isChanged)
                    {
                        alertInfo.alertStatus = MMAlertStatusDismissed;
                    }
                }failure:^(NSError *error) {
                    NSLog(@"%@", error);
                }];
            }
            else
            {
                for (int i = 0; i< [alertArray count]; i++)
                {
                    MMAlertInfo *alertInfo = [alertArray objectAtIndex:i];
                    if (alertInfo.alertStatus != MMAlertStatusDismissed && alertInfo.alertStatus != MMAlertStatusResolved)
                    {
                        MMSDK *sdk = [MMSDK sharedInstance];
                        [sdk changeAlertState:alertInfo.alertId withState:MMAlertStatusDismissed comment:[[alertView textFieldAtIndex:0]text] success:^(BOOL isChanged)
                        {
                            if (isChanged)
                            {
                                alertInfo.alertStatus = MMAlertStatusDismissed;
                            }
                        } failure:^(NSError *error) {
                            NSLog(@"%@", error);
                        }];
                    }
                }
                dismissAll = NO;
            }
        }
        else
        {
        [self showAlertView:YES];
        }
    } 
    if (alertView.tag == 4 && buttonIndex == 0)
    {
        [sectionTableView reloadData];
    }
    if (alertView.tag == 2 && buttonIndex == 0)
    {
        [self showAlertView:NO];
    }
    if (alertView.tag == 6)
    {
    [self.navigationController pushViewController: filterViewController animated:YES];
    }
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < [alertArray count]) 
	{
        MMAlertInfo *alertInfo = [alertArray objectAtIndex:indexPath.row];
        if (alertInfo.alertStatus)
        {
            return UITableViewCellEditingStyleDelete;
        }
	}
    return UITableViewCellEditingStyleNone;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (alertsDetailsViewController == nil)
    {
        //TODO: initWithAlertData here
        alertsDetailsViewController = [[AlertsDetailsViewController alloc] init];
    }
    MMAlertInfo *alertInformation = [alertArray objectAtIndex:indexPath.row];
    
    currentIndexForDetailedInfo = indexPath.row;
    
    alertsDetailsViewController.alertInfo = alertInformation;
    alertsDetailsViewController.parent = self;
    [alertsDetailsViewController willAnimateRotationToInterfaceOrientation:self.interfaceOrientation duration:0];
    [self.navigationController pushViewController:alertsDetailsViewController animated:YES];
}

- (NSString*) tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MMAlertInfo *alertInfo = [alertArray objectAtIndex:indexPath.row];
    if (alertInfo.alertStatus == MMAlertStatusDismissed)
    {
        return NSLocalizedString(@"Dismissed", @"");
    }
    if (alertInfo.alertStatus == MMAlertStatusResolved)
    {
        return NSLocalizedString(@"Resolved", @"");
    }
    else
        return NSLocalizedString(@"Dismiss", @"");
}

-(void) handleLongPress
{
    if (longPressRecognizer.state == UIGestureRecognizerStateBegan )
    {
        UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self 
                                                            cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                                            destructiveButtonTitle:nil
                                                            otherButtonTitles:NSLocalizedString(@"Dismiss all", @""),
                                                            NSLocalizedString(@"dismissButtonTitle", @""), nil];
    
        actionSheet.actionSheetStyle = UIActionSheetStyleAutomatic;
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        [actionSheet release];
    }
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == DISMISS_ALL)
    {
        [self showAlertView:NO];
        dismissAll = YES;
    }
    if (buttonIndex == DISMISS_ALERT)
    {
        [self editing];
    }
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return  YES;
}

-(MMView*)loadHeader
{
    MMView *headerView = [[[MMView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x,
                                                                   HEIGHT_HEADER ,
                                                                   sectionTableView.frame.size.width , HEIGHT_HEADER) viewLevel:ViewLevelSecond] autorelease];
    headerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;

    MMTitleLabel *labelFilter = [[[MMTitleLabel alloc] initWithFrame:CGRectMake(HORIZONTAL_INDENT,
                                                                    self.view.frame.origin.y,
                                                                    headerView.frame.size.width - 2 * HORIZONTAL_INDENT - HEADER_ICON_SIZE * 2 - 5, HEIGHT_CELL - 15)]autorelease];
    labelFilter.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f];
    labelFilter.textAlignment = UITextAlignmentLeft;
    NSString* string = [NSString stringWithFormat:NSLocalizedString(@"displaying", @""), [alertArray count],numberAlertsAll];
    labelFilter.text = string;
    labelFilter.autoresizingMask = UIViewAutoresizingFlexibleWidth;

    CustomButtonWithIncreasedHitArea * filterButton = [[[CustomButtonWithIncreasedHitArea alloc] 
                                                     initWithFrame:CGRectMake(headerView.frame.size.width - HEADER_ICON_SIZE*2 - HORIZONTAL_INDENT - 5,
                                                                              4, 
                                                                              HEADER_ICON_SIZE, 
                                                                              HEADER_ICON_SIZE)]
                                                    autorelease];
//    UIImage *buttonBackgroundImage = [UIImage imageNamed:@"button_image_gray.png"];
//    [filterButton setBackgroundImage:[[UIImage imageWithCGImage:buttonBackgroundImage.CGImage scale:2.0 orientation:UIImageOrientationUp] stretchableImageWithLeftCapWidth:buttonBackgroundImage.size.width/4 topCapHeight:0] forState:UIControlStateNormal];
    UIImage* filterButtonImage = [UIImage imageNamed:@"Filter.png"];
    [filterButton setBackgroundImage:filterButtonImage forState:UIControlStateNormal];
//    [filterButton setImage:[UIImage imageWithCGImage:filterButtonImage.CGImage scale:2.0 orientation:UIImageOrientationUp] forState:UIControlStateNormal];
    
    [filterButton addTarget:self action:@selector(filterButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [filterButton setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin];
    
    CustomButtonWithIncreasedHitArea* sortButton = [[[CustomButtonWithIncreasedHitArea alloc] 
                                                  initWithFrame:CGRectMake(headerView.frame.size.width - HEADER_ICON_SIZE - HORIZONTAL_INDENT, 
                                                                           4 , 
                                                                           HEADER_ICON_SIZE, 
                                                                           HEADER_ICON_SIZE)]
                                                 autorelease];
//    [sortButton setBackgroundImage:[[UIImage imageWithCGImage:buttonBackgroundImage.CGImage scale:2.0 orientation:UIImageOrientationUp] stretchableImageWithLeftCapWidth:buttonBackgroundImage.size.width/4 topCapHeight:0] forState:UIControlStateNormal];
    UIImage* sortButtonImage = [UIImage imageNamed:@"SortAZ.png"];
    [sortButton setBackgroundImage:sortButtonImage forState:UIControlStateNormal];
//    [sortButton setImage:[UIImage imageWithCGImage:sortButtonImage.CGImage scale:2.0 orientation:UIImageOrientationUp] forState:UIControlStateNormal];
    [sortButton addTarget:self action:@selector(sortButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [sortButton setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin];

    UILabel *SeparaterLine = [[[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.origin.x,
                                                                       HEIGHT_HEADER - HEIGHT_SEPARETER_LINE,
                                                                       headerView.frame.size.width, HEIGHT_SEPARETER_LINE)]autorelease];
    SeparaterLine.backgroundColor = [UIColor darkGrayColor];
    SeparaterLine.autoresizingMask = UIViewAutoresizingFlexibleWidth;

    [headerView addSubview:labelFilter];
    [headerView addSubview:SeparaterLine];
    [headerView addSubview:filterButton];
    [headerView addSubview:sortButton];

    return headerView;
}

-(void)editing
{
    [self.sectionTableView setEditing:!self.sectionTableView.editing animated:YES];
}

-(void)sortButtonPressed
{
    [self.navigationController pushViewController:sortViewController animated:YES];
}

-(void)filterButtonPressed
{
    [self.navigationController pushViewController: filterViewController animated:YES];
}

- (NSArray*)filteredPriority: (MMAlertPriority)filter
{
    NSMutableArray* filterArray = [[[NSMutableArray alloc] init] autorelease];
    if (priorityFilter & filter)
    {
        for (int i = 0; i < [alertArray count]; i++)
        {
            MMAlertInfo *alertInfo = [alertArray objectAtIndex:i];
            if (alertInfo.alertPriority == filter)
            {
                [filterArray addObject:alertInfo];
            }
        }
        return filterArray;
    }
    else
        return nil;
}

- (NSArray*)filteredStatus: (MMAlertStatus)filter
{
    NSMutableArray* filterArray = [[[NSMutableArray alloc] init] autorelease];
    if (statusFilter & filter)
    {
        for (int i = 0; i < [alertArray count]; i++)
        {
            MMAlertInfo *alertInfo = [alertArray objectAtIndex:i];
            if (alertInfo.alertStatus == filter)
            {
                [filterArray addObject:alertInfo];
            }
        }
        return filterArray;
    }
    else
        return nil;
}

-(void)loadSortAlerts
{
    NSArray* sortingAlertsArray;
    NSSortDescriptor* sorter;
    sortAlerts = [AppDelegate sharedInstance].alertSorting;

    switch (sortAlerts)
    {
        case MMAlertSortNone:
        {
            break;
        }
        case MMAlertAscendingSortObjectName:
        {
            sorter = [[[NSSortDescriptor alloc] initWithKey:@"objectName" ascending:YES selector:@selector(compare:)]autorelease];
            NSArray* sortDescriptors = [NSArray arrayWithObject:sorter];
            sortingAlertsArray = [self.alertArray sortedArrayUsingDescriptors:sortDescriptors];
            break;
        }
        case MMAlertAscendingSortPriority:
        {
            sorter = [[[NSSortDescriptor alloc] initWithKey:@"alertPriority" ascending:YES selector:@selector(compare:)]autorelease];
            NSArray* sortDescriptors = [NSArray arrayWithObject:sorter];
            sortingAlertsArray = [self.alertArray sortedArrayUsingDescriptors:sortDescriptors];
            break;
        }
        case MMAlertAscendingSortStatus:
        {
            sorter = [[[NSSortDescriptor alloc] initWithKey:@"alertStatus" ascending:YES selector:@selector(compare:)]autorelease];
            NSArray* sortDescriptors = [NSArray arrayWithObject:sorter];
            sortingAlertsArray = [self.alertArray sortedArrayUsingDescriptors:sortDescriptors];
            break;
        }
        case MMAlertAscendingSortTimestamp:
        {
            sorter = [[[NSSortDescriptor alloc] initWithKey:@"alertTimestamp" ascending:YES selector:@selector(compare:)]autorelease];
            NSArray* sortDescriptors = [NSArray arrayWithObject:sorter];
            sortingAlertsArray = [self.alertArray sortedArrayUsingDescriptors:sortDescriptors];
            break;
        }
        case MMAlertDescendingSortObjectName:
        {
            sorter = [[[NSSortDescriptor alloc] initWithKey:@"objectName" ascending:NO selector:@selector(compare:)]autorelease];
            NSArray* sortDescriptors = [NSArray arrayWithObject:sorter];
            sortingAlertsArray = [self.alertArray sortedArrayUsingDescriptors:sortDescriptors];
            break;
        }
        case MMAlertDescendingSortPriority:
        {
            sorter = [[[NSSortDescriptor alloc] initWithKey:@"alertPriority" ascending:NO selector:@selector(compare:)]autorelease];
            NSArray* sortDescriptors = [NSArray arrayWithObject:sorter];
            sortingAlertsArray = [self.alertArray sortedArrayUsingDescriptors:sortDescriptors];
            break;
        }
        case MMAlertDescendingSortStatus:
        {
            sorter = [[[NSSortDescriptor alloc] initWithKey:@"alertStatus" ascending:NO selector:@selector(compare:)]autorelease];
            NSArray* sortDescriptors = [NSArray arrayWithObject:sorter];
            sortingAlertsArray = [self.alertArray sortedArrayUsingDescriptors:sortDescriptors];
            break;
        }
        case MMAlertDescendingSortTimestamp:
        {
            sorter = [[[NSSortDescriptor alloc] initWithKey:@"alertTimestamp" ascending:NO selector:@selector(compare:)]autorelease];
            NSArray* sortDescriptors = [NSArray arrayWithObject:sorter];
            sortingAlertsArray = [self.alertArray sortedArrayUsingDescriptors:sortDescriptors];
            break;
        }
    }
    if (sortAlerts)
    {
        [alertArray removeAllObjects];
        [alertArray addObjectsFromArray:sortingAlertsArray];
    }
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
	_reloading = YES;
    [self.view setUserInteractionEnabled:NO];
}

- (void)doneLoadingTableViewData{
	_reloading = NO;
    [self.view setUserInteractionEnabled:YES];
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:sectionTableView];
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{	
	
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    if (sectionTableView.contentOffset.y >= (sectionTableView.contentSize.height - sectionTableView.bounds.size.height) && doPaging == NO && oldNumberAlerts != currentNumberAlerts && currentNumberAlerts < numberAlertsAll && currentNumberAlerts >= PAGING_COUNT)
    {
        doPaging = YES;
        //Append additional cell with downloading process info
        NSArray* array = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:[sectionTableView numberOfRowsInSection:0] inSection:0]];
        [sectionTableView beginUpdates];
        [sectionTableView insertRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationNone];
        [sectionTableView endUpdates];
        [self updateScreenInfo];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
	
	[self reloadTableViewDataSource];
    [self updateScreenInfo];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	
	return _reloading;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	
	return self.lastUpdateTime;
}

//"next" means to bot of screen
- (void)getNextAlert
{
    if (alertsDetailsViewController == nil)
    {
        alertsDetailsViewController = [[AlertsDetailsViewController alloc] init];
    }
    
    int numberOfAlerts = [alertArray count];
     
    if(currentIndexForDetailedInfo < numberOfAlerts - 1)
    {
        ++currentIndexForDetailedInfo;
        //creating of new VC
        MMAlertInfo *alertInformation = [alertArray objectAtIndex:currentIndexForDetailedInfo];
        alertsDetailsViewController.alertInfo = alertInformation;
        alertsDetailsViewController.parent = self;
        [alertsDetailsViewController.sectionTableView reloadData];

        //creating of transition
        CATransition *betweenDetailed = [CATransition animation];
        betweenDetailed.duration = 0.3;
        betweenDetailed.timingFunction = [CAMediaTimingFunction
                                          functionWithName:kCAMediaTimingFunctionEaseIn];
        betweenDetailed.type = kCATransitionPush;
        betweenDetailed.subtype = kCATransitionFromRight;
        
        [[alertsDetailsViewController.view layer] addAnimation:betweenDetailed
                                                        forKey:kCATransitionReveal];
        
        [self.navigationController popViewControllerAnimated:NO];
        [self.navigationController pushViewController:alertsDetailsViewController                                          animated:NO];
    }
}

//"previous" means to top of screen
- (void)getPreviousAlert
{
    if (alertsDetailsViewController == nil)
    {
        alertsDetailsViewController = [[AlertsDetailsViewController alloc] init];
    }
    
    if(currentIndexForDetailedInfo != 0)
    {
        --currentIndexForDetailedInfo;
        
        if(currentIndexForDetailedInfo >= 0)
        {
            //creating of new VC
            MMAlertInfo *alertInformation = [alertArray objectAtIndex:currentIndexForDetailedInfo];
            alertsDetailsViewController.alertInfo = alertInformation;
            alertsDetailsViewController.parent = self;
            [alertsDetailsViewController.sectionTableView reloadData];
            
            //creating of transition
            CATransition *betweenDetailed = [CATransition animation];
            betweenDetailed.duration = 0.3;
            betweenDetailed.timingFunction = [CAMediaTimingFunction
                                              functionWithName:kCAMediaTimingFunctionEaseIn];
            betweenDetailed.type = kCATransitionPush;
            betweenDetailed.subtype = kCATransitionFromLeft;
            
            [[alertsDetailsViewController.view layer] addAnimation:betweenDetailed
                                                            forKey:kCATransitionReveal];
            
            [self.navigationController popViewControllerAnimated:NO];
            [self.navigationController pushViewController:alertsDetailsViewController                                          animated:NO];
        }
    }
}

-(void)AlertCancelButtonClicked
{
    [self cancelCurrentConnections];
}

- (void)pagingCancelClicked
{
    doPaging = NO;
    NSArray* array = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:alertArray.count inSection:0]];
    [sectionTableView beginUpdates];
    [sectionTableView deleteRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationNone];
    [sectionTableView endUpdates];
    [self AlertCancelButtonClicked];
}

- (void)cancelCurrentConnections
{
    if (_reloading == YES)
    {
        [self doneLoadingTableViewData];
    }
    [super cancelCurrentConnections];
}

@end