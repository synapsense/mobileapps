//
//  FilterViewController.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "FilterViewController.h"
#import "FacilityAlertsViewController.h"
#import "MMTableViewCell.h"

@interface FilterViewController (privateMethods)

- (NSArray*) curentFilter: (NSInteger)index;
- (void)backPressed;

@end

@implementation FilterViewController

@synthesize liveImagingTypeDelegate;

#define PRIORITY_CRITICAL            0
#define PRIORITY_MAJOR               1
#define PRIORITY_MINOR               2
#define PRIORITY_INFORMATIONAL       3
#define STATUS_OPENED                0
#define STATUS_ACKNOWLEDGED          1

- (void)updateScreenInfo
{
    //do nothing
}

- (void) dealloc
{
    [filterTableDataDictionary release];
    
    [super dealloc];
}

- (id)initWithFilterString:(NSString*)filterString FilterArray:(NSArray*)filterArray
{
    if (self = [super init])
    {
        filterTitle = [[NSString alloc] initWithString:filterString];
        if ([filterTitle isEqualToString:NSLocalizedString(@"filter", @"")])
        {
            NSArray* priority = [NSArray arrayWithObjects:NSLocalizedString(@"critical", @""), NSLocalizedString(@"major", @""), 
                                 NSLocalizedString(@"minor", @""), NSLocalizedString(@"informational", @""), nil];
            NSArray* status = [NSArray arrayWithObjects:NSLocalizedString(@"opened", @""), NSLocalizedString(@"acknowledged", @""), nil];
            filterTableDataDictionary = [[NSDictionary dictionaryWithObjectsAndKeys:priority, NSLocalizedString(@"priorityAscending", @""), status, NSLocalizedString(@"statusAscending", @""), nil] retain];
            [self.view addSubview:[self loadHeaderWithTitle:NSLocalizedString(@"filter", @"") titleWidth:self.view.frame.size.width viewLevel:ViewLevelSecond]];
            
            
           // UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Back", @"") style:UIBarButtonItemStylePlain target:self action:@selector(backPressed)];
           // self.navigationItem.leftBarButtonItem = back;
           //[back release];
        }  
        else if([filterTitle isEqualToString:NSLocalizedString(@"livelImaging", @"")] && filterArray)
        {
            livelImagingArray = filterArray;
            [self.view addSubview:[self loadHeaderWithTitle:[NSString stringWithFormat:@"%@%@",NSLocalizedString(@"livelImaging", @""),@"\u2122"] titleWidth:self.view.frame.size.width  viewLevel:ViewLevelSecond]];
        } 
        
        sectionTableView = [[[MMTableView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 35,
                                                                          self.view.frame.size.width, self.view.frame.size.height - 35) 
                                                         style:UITableViewStyleGrouped] autorelease];
        [sectionTableView setDelegate:self];
        [sectionTableView setDataSource:self];
        [sectionTableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self.view addSubview:sectionTableView];
    }
    return self;
}

- (void)backPressed
{
    if ([[AppDelegate sharedInstance] alertStatusFilter] == MMAlertStatusNone || [[AppDelegate sharedInstance] alertPriorityFilter] == MMAlertPriorityNone)
    {
        UIAlertView* alert = [[[UIAlertView alloc] initWithTitle:nil
                                                         message:NSLocalizedString(@"message", @"") 
                                                        delegate:self 
                                               cancelButtonTitle:nil
                                               otherButtonTitles:NSLocalizedString(@"okButtonTitle", @""), nil] autorelease];
        [alert show];
    }
    else
    {
        [[AppDelegate sharedInstance].applicationPreferences setInteger:statusFilter forKey:STATUS_FILTER];
        [[AppDelegate sharedInstance].applicationPreferences setInteger:priorityFilter forKey:PRIORITY_FILTER];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (id)initWithFilterString:(NSString*)filterString
{
    return [self initWithFilterString:filterString FilterArray:nil];
}

- (void) loadView
{
    self.view = [[[MMView alloc] initWithFrame:CGRectMake(0, 0, 640, 960) viewLevel:ViewLevelSecond]autorelease];
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.navigationItem.title = NSLocalizedString(@"applicationTitle", @"");
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [sectionTableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([filterTitle isEqualToString:NSLocalizedString(@"filter", @"")])
        return [filterTableDataDictionary count];
    else 
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    if ([filterTitle isEqualToString:NSLocalizedString(@"filter", @"")])
    {
        NSArray* curentData = [self curentFilter:section];
        return [curentData count];
    }
    else
    {
        return [livelImagingArray count] + 1;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *)indexPat
{
    if ([filterTitle isEqualToString:NSLocalizedString(@"filter", @"")])
        return 35;
    else
        return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    MMTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"FilterCell"];
    if (cell == nil)
    {
        cell = [[[MMTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilterCell"]autorelease];
        [cell setTableviewStyle:UITableViewStyleGrouped];
    }
    if ([filterTitle isEqualToString:NSLocalizedString(@"filter", @"")])
    {
        NSArray* curentData = [self curentFilter:indexPath.section];
        cell.textLabel.text = [curentData objectAtIndex:indexPath.row];
        cell.textLabel.numberOfLines = 2;
        cell.textLabel.font = [UIFont boldSystemFontOfSize:15.0f];
    
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
            cell.accessoryType = UITableViewCellAccessoryNone;
    
        priorityFilter = [AppDelegate sharedInstance].alertPriorityFilter;
        statusFilter = [AppDelegate sharedInstance].alertStatusFilter;

        switch (indexPath.section)
            {
                case 0:
                {
                    switch (indexPath.row)
                    {
                        case PRIORITY_CRITICAL:
                        {
                            if (priorityFilter & MMAlertPriorityCritical)
                                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                            break;
                        }
                        case PRIORITY_MAJOR:
                        {
                            if (priorityFilter & MMAlertPriorityMajor)
                                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                            break;
                        }
                        case PRIORITY_MINOR:
                        {
                            if (priorityFilter & MMAlertPriorityMinor)
                                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                            break;
                        }
                        case PRIORITY_INFORMATIONAL:
                        {
                            if (priorityFilter & MMAlertPriorityInformational)
                                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                            break;
                        }
                    }
                    break;
                }
                case 1:
                {
                    switch (indexPath.row)
                    {
                        case STATUS_OPENED:
                        {
                            if (statusFilter & MMAlertStatusOpened)
                                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                            break;
                        }
                        case STATUS_ACKNOWLEDGED:
                        {
                            if (statusFilter & MMAlertStatusAcknowledged)
                                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                            break;
                        }
                    }
                    break;
                }
            }
        }
        else
        {
            cell.textLabel.font = [UIFont boldSystemFontOfSize:15.0f];
            if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
                cell.accessoryType = UITableViewCellAccessoryNone;
            if (indexPath.row == 0) {
                cell.textLabel.text = NSLocalizedString(@"Off", @"");
                if (![liveImagingTypeDelegate liveImagingType]) {
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
            }
            else
            {
                MMliveImagingType *liveImagingType = [livelImagingArray objectAtIndex:(indexPath.row - 1)];
                cell.textLabel.text = liveImagingType.name;
                if ([liveImagingTypeDelegate liveImagingType] && [liveImagingType.name compare:[liveImagingTypeDelegate liveImagingType].name] == NSOrderedSame)
                {
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
            }
        }
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([filterTitle isEqualToString:NSLocalizedString(@"filter", @"")])
    {
        return 35;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ([filterTitle isEqualToString:NSLocalizedString(@"filter", @"")])
    {
        UIView *view = [[[UIView alloc] init] autorelease];
        view.backgroundColor = [UIColor clearColor];
        MMTitleLabel *headerText = [[MMTitleLabel alloc] initWithFrame:CGRectMake(20, 10, 0, 0)];
        [headerText setFont:[UIFont boldSystemFontOfSize:17]];
        headerText.text = [[filterTableDataDictionary allKeys] objectAtIndex:section];
        [headerText sizeToFit];
        [view addSubview:headerText];
        [headerText release];
        return view;
    }
    return nil;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if ([filterTitle isEqualToString:NSLocalizedString(@"filter", @"")])
    {
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
            cell.accessoryType = UITableViewCellAccessoryNone;
        else
            cell.accessoryType = UITableViewCellAccessoryCheckmark;

        if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
        {
            switch (indexPath.section)
            {
                case 0:
                {
                    switch (indexPath.row)
                    {
                        case PRIORITY_CRITICAL:
                        {
                            priorityFilter |= MMAlertPriorityCritical;
                            break;
                        }
                        case PRIORITY_MAJOR:
                        {
                            priorityFilter |= MMAlertPriorityMajor;
                            break;
                        }
                        case PRIORITY_MINOR:
                        {
                            priorityFilter |= MMAlertPriorityMinor;
                            break;
                        }
                        case PRIORITY_INFORMATIONAL:
                        {
                            priorityFilter |= MMAlertPriorityInformational;
                            break;
                        }
                    }
                    break;
                }
                case 1:
                {
                    switch (indexPath.row)
                    {
                        case STATUS_OPENED:
                        {
                            statusFilter |= MMAlertStatusOpened;
                            break;
                        }
                        case STATUS_ACKNOWLEDGED:
                        {
                            statusFilter |= MMAlertStatusAcknowledged;
                            break;
                        }
                    }
                    break;
                }
            }
        }
        if (cell.accessoryType == UITableViewCellAccessoryNone)
        {
            switch (indexPath.section)
            {
                case 0:
                    switch (indexPath.row)
                    {
                        case PRIORITY_CRITICAL:
                        {
                            priorityFilter ^= MMAlertPriorityCritical;
                            break;
                        }
                        case PRIORITY_MAJOR:
                        {
                            priorityFilter ^= MMAlertPriorityMajor;
                            break;
                        }
                        case PRIORITY_MINOR:
                        {
                            priorityFilter ^= MMAlertPriorityMinor;
                            break;
                        }
                        case PRIORITY_INFORMATIONAL:
                        {
                            priorityFilter ^= MMAlertPriorityInformational;
                            break;
                        }
                    }
                    break;
                case 1:
                    switch (indexPath.row)
                    {
                        case STATUS_OPENED:
                        {
                            statusFilter ^= MMAlertStatusOpened;
                            break;
                        }
                        case STATUS_ACKNOWLEDGED:
                        {
                            statusFilter ^= MMAlertStatusAcknowledged;
                            break;
                        }
                    }
                    break;
            }
        }
        [AppDelegate sharedInstance].alertPriorityFilter = priorityFilter;
        [AppDelegate sharedInstance].alertStatusFilter = statusFilter;
    }
        else
        {
            UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
            if (newCell.accessoryType == UITableViewCellAccessoryNone) 
            {
                currentCell.accessoryType = UITableViewCellAccessoryNone;
                newCell.accessoryType = UITableViewCellAccessoryCheckmark;
                currentCell = newCell;
            }
            if (indexPath.row == 0 && liveImagingTypeDelegate)
            {
                [liveImagingTypeDelegate setLiveImagingType:nil];
            }
            else
            {
                [liveImagingTypeDelegate setLiveImagingType:[livelImagingArray objectAtIndex:(indexPath.row - 1)]];
            }
            [[[AppDelegate sharedInstance] navigaionController] popViewControllerAnimated:YES];
        }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];   
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (NSArray*) curentFilter: (NSInteger)index
{
    NSArray* keys = [filterTableDataDictionary allKeys];
    NSString* curentKey = [keys objectAtIndex:index];
    NSArray* curentFilter = [filterTableDataDictionary objectForKey:curentKey];
    return curentFilter;
}

@end
