//
//  UIPIEChartView.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FacilityDashbordViewController;

typedef enum
{
    PUETYPE_FULL,
    PUETYPE_LITE
}PUEType;

@interface UIPIEChartView : UIView
{
    FacilityDashbordViewController* DashbordController;
    CGFloat badgeCornerRoundness;
    NSMutableArray* array;
}

@property (nonatomic, readwrite) CGFloat badgeCornerRoundness;
@property (nonatomic, retain) NSMutableArray* array;
@property PUEType puetype;

- (id)initWithInteger:(int)number;

@end
