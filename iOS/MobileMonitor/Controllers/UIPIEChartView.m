//
//  UIPIEChartView.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "UIPIEChartView.h"

#define RED_COLOR_COMPONENTS {211.0f/255, 37.0f/255, 41.0f/255, 1, 211.0f/255, 37.0f/255, 41.0f/255, 1}  //0xD32529
#define BLUE_COLOR_COMPONENTS {31.0f/255, 51.0f/255, 104.0f/255, 1, 31.0f/255, 51.0f/255, 104.0f/255, 1}  //0x1F3668
#define GREEN_COLOR_COMPONENTS {0, 121.0f/255, 62.0f/255, 1, 0, 121.0f/255, 62.0f/255, 1}  //0x00793E
#define YELLOW_COLOR_COMPONENTS {240.0f/255, 153.0f/255, 2.0f/255, 1, 240.0f/255, 153.0f/255, 2.0f/255, 1}  //0xF099022
#define GRAY_COLOR_COMPONENTS {149.0f/255, 149.0f/255, 1, 1, 149.0f/255, 149.0f/255, 1, 1}  //0x9595FF
#define SECTION_WHITE_COLOR {1, 1, 1, 1, 1, 1, 1, 1}
#define DRAWLINE_WIDTH 2

@interface UIPIEChartView()

- (CGPoint)convertAffinePointWithRadius:(CGFloat)radius withAngle:(CGFloat)angle;
- (CGPoint)convertAffinePointWithRadius:(CGFloat)radius withAngle:(CGFloat)angle withOffset:(CGPoint)origin;

@end

@implementation UIPIEChartView

@synthesize badgeCornerRoundness;
@synthesize array;
@synthesize puetype;

- (id)initWithInteger:(int)number
{
	self = [super initWithFrame:CGRectMake(0, 0, 150, 150)];
	if(self!=nil)
	{
        self.backgroundColor = [UIColor clearColor];
        self.badgeCornerRoundness = 0.40;
	}
	return self;
}

// Draws Method
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetShouldAntialias(context, YES);
//    CGContextSetShadowWithColor(context, CGSizeMake(2, 2), 3.0, [UIColor colorWithRed:51.0f/255.0f green:153.0f/255.0f blue:204.f/255.0f alpha:1.0f].CGColor);
    CGFloat radius = CGRectGetMaxY(rect)*self.badgeCornerRoundness;
    CGFloat puffer = CGRectGetMaxY(rect)*0.10;
    
    CGFloat maxX = CGRectGetMaxX(rect) - puffer;
    CGFloat minY = CGRectGetMinY(rect) + puffer;
    CGFloat beta = 0;

    CGPoint startPoint, endPoint;
    startPoint.x = maxX -DRAWLINE_WIDTH;
    startPoint.y = minY -DRAWLINE_WIDTH;
    endPoint.x = radius;
    endPoint.y = radius;
    CGFloat components[][8] = {RED_COLOR_COMPONENTS, BLUE_COLOR_COMPONENTS, YELLOW_COLOR_COMPONENTS, GREEN_COLOR_COMPONENTS};
    CGFloat components2[][8] = {RED_COLOR_COMPONENTS, GRAY_COLOR_COMPONENTS};
    for (int i = 0 ; i < [self.array count] ;  i++)
    {
        double component = [[self.array objectAtIndex:i] doubleValue];
        //create reusable path
        CGMutablePathRef path = CGPathCreateMutable();
        CGPoint origin = CGPointMake(maxX-radius, minY+radius);
        CGFloat alfa = beta;
        beta += component * 360;
        CGPoint point =[self convertAffinePointWithRadius:radius withAngle:-alfa withOffset:origin];
        CGPathAddArc(path, &CGAffineTransformIdentity, (maxX-radius), (minY+radius), radius, -alfa / 180 * M_PI, -beta / 180 * M_PI, true);
        CGPathAddLineToPoint(path, &CGAffineTransformIdentity, origin.x, origin.y);
        CGPathAddLineToPoint(path, &CGAffineTransformIdentity, point.x, point.y);
        CGPathCloseSubpath(path);

        CGLayerRef contentLayer = CGLayerCreateWithContext(context, rect.size, NULL);
        CGContextRef contentLayerContext = CGLayerGetContext(contentLayer);
        //append gradient background
        CGContextBeginPath(contentLayerContext);
        CGContextAddPath(contentLayerContext, path);
        CGContextClosePath(contentLayerContext);
        CGContextClip(contentLayerContext);
        
        CGGradientRef gradient = nil;
        size_t num_locations = 2;
        CGFloat locations[2] = { 0.0, 1.0 };
        CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
        if (self.puetype == PUETYPE_FULL) 
        {
            gradient = CGGradientCreateWithColorComponents(colorspace, components[i], locations, num_locations);
        }
        else
        {
            gradient = CGGradientCreateWithColorComponents(colorspace, components2[i], locations, num_locations);
        } 
        CGContextDrawRadialGradient(contentLayerContext, gradient, origin, radius, origin, 0, kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);

//        CGContextSetLineWidth(contentLayerContext, 4.0f);
//        CGContextSetStrokeColorWithColor(contentLayerContext, [UIColor whiteColor].CGColor);
//        CGContextMoveToPoint(contentLayerContext, point2.x, point2.y);
//        CGContextAddLineToPoint(contentLayerContext, origin.x, origin.y);
//        CGContextStrokePath(contentLayerContext);
//
//        CGContextSetLineWidth(contentLayerContext, 3.0f);
//        CGContextAddArc(contentLayerContext, origin.x, origin.y, radius, alfa, beta, NO);
//        CGContextStrokePath(contentLayerContext);

        CGGradientRelease(gradient);
        CGContextDrawLayerInRect(context, rect, contentLayer);

        //----------
        //draw external shadow
        if (beta - alfa < 20)
        {
            CGGradientRef gradientShadow = nil;
            CGColorSpaceRef colorspace2 = CGColorSpaceCreateDeviceRGB();
            num_locations = 3;
            CGFloat locations[] = {0.0, 0.9, 1.0};
            CGFloat shadowComponents[] = {0, 0, 0, 0.5, 0, 0, 0, 0.5, 0, 0, 0, 0};
            gradientShadow = CGGradientCreateWithColorComponents(colorspace2, shadowComponents, locations, num_locations);
            CGContextDrawRadialGradient(contentLayerContext, gradientShadow, origin, radius, origin, radius - 2, kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
            
            CGGradientRelease(gradientShadow);
            CGContextDrawLayerInRect(context, rect, contentLayer);
        }
        else
        {
            CGFloat gamma = 0;
            if (beta > alfa + 180)
            {
                gamma = beta - alfa - 180;
            }
            CGMutablePathRef path = CGPathCreateMutable();
            CGMutablePathRef path1 = CGPathCreateMutable();
            CGPoint point1[4] = 
            {
                [self convertAffinePointWithRadius:radius withAngle:-alfa withOffset:origin],
                origin,
                [self convertAffinePointWithRadius:radius withAngle:-(beta - gamma) withOffset:origin],
                origin
                
            };
            CGPathAddLines(path1, &CGAffineTransformIdentity, point1, 4);
            if (gamma > 0) 
            {
                CGMutablePathRef path2 = CGPathCreateMutable();
                CGMutablePathRef path3 = CGPathCreateMutable();
                CGPoint point2[4] = 
                {
                    [self convertAffinePointWithRadius:radius withAngle:-(beta - gamma) withOffset:origin],
                    origin,
                    [self convertAffinePointWithRadius:radius withAngle:-beta withOffset:origin],
                    origin
                };
                CGPathAddLines(path2, &CGAffineTransformIdentity, point2, 4);
                CGPathAddArc(path3, &CGAffineTransformIdentity, (maxX-radius), (minY+radius), radius, -(beta - gamma) / 180 * M_PI, -beta / 180 * M_PI, true);
                CGPathAddPath(path2, &CGAffineTransformIdentity, path3);
                CGPathAddPath(path1, &CGAffineTransformIdentity, path2);
                CGPathRelease(path2);
                CGPathRelease(path3);
            }
            CGPathAddArc(path, &CGAffineTransformIdentity, (maxX-radius), (minY+radius), radius, -alfa / 180 * M_PI, -(beta - gamma) / 180 * M_PI, true);
            CGPathAddPath(path, &CGAffineTransformIdentity, path1);
            CGPathCloseSubpath(path);
            
            CGLayerRef contentLayer = CGLayerCreateWithContext(context, rect.size, NULL);
            CGContextRef contentLayerContext = CGLayerGetContext(contentLayer);
            CGContextBeginPath(contentLayerContext);
            CGContextAddPath(contentLayerContext, path);
            CGContextClosePath(contentLayerContext);
            CGContextClip(contentLayerContext);
            
            CGGradientRef gradient = nil;
            int  num_locations = 2;
            CGFloat locations2[2] = { 0.0, 1.0};
            CGColorSpaceRef colorspace2 = CGColorSpaceCreateDeviceRGB();
            CGFloat shadowComponents[] = {0, 0, 0, 0.5, 0, 0, 0, 0.0};
            gradient = CGGradientCreateWithColorComponents(colorspace2, shadowComponents, locations2, num_locations);
            CGContextDrawRadialGradient(contentLayerContext, gradient, origin, radius, origin, radius - 5, kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
            
            CGGradientRelease(gradient);
            CGContextDrawLayerInRect(context, rect, contentLayer);
            CGLayerRelease(contentLayer);        
            CGPathRelease(path);
            CGPathRelease(path1);
        }
        //----------

        CGLayerRelease(contentLayer);
        CGPathRelease(path);
        
        //draw rays shadows
        //------------------
        if (beta - alfa > 8)
        {
            CGMutablePathRef shadowPath = CGPathCreateMutable();
            CGPoint pointShadow[3] = 
            {
                [self convertAffinePointWithRadius:radius withAngle:(-alfa) withOffset:origin],
                origin,
                [self convertAffinePointWithRadius:radius withAngle:-(alfa + 5) withOffset:origin],
            };
            CGPathAddLines(shadowPath, &CGAffineTransformIdentity, pointShadow, 3);
            CGPathCloseSubpath(shadowPath);
            CGLayerRef contentLayer = CGLayerCreateWithContext(context, rect.size, NULL);
            CGContextRef contentLayerContext = CGLayerGetContext(contentLayer);
            CGContextBeginPath(contentLayerContext);
            CGContextAddPath(contentLayerContext, shadowPath);
            CGContextClosePath(contentLayerContext);
            CGContextClip(contentLayerContext);
            int num_locations = 2;
            CGFloat locations3[2] = { 0.0, 1};
            CGColorSpaceRef colorspace3 = CGColorSpaceCreateDeviceRGB();
            CGFloat shadowComponents3[] = {0.0, 0.0, 0.0, 0.2, 0.0, 0.0, 0.0, 0.5};
            CGGradientRef gradient2 = CGGradientCreateWithColorComponents(colorspace3, shadowComponents3, locations3, num_locations);
            CGContextDrawRadialGradient(contentLayerContext, gradient2, origin, radius, origin, 0, kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
            CGGradientRelease(gradient2);
            CGContextDrawLayerInRect(context, rect, contentLayer);
            CGLayerRelease(contentLayer);        
            CGPathRelease(shadowPath);
        }
        if (beta - alfa > 15)
        {
            CGMutablePathRef shadowPath = CGPathCreateMutable();
            CGPoint pointShadow[3] = 
            {
                [self convertAffinePointWithRadius:radius withAngle:(-beta) withOffset:origin],
                origin,
                [self convertAffinePointWithRadius:radius withAngle:-(beta - 5) withOffset:origin],
            };
            CGPathAddLines(shadowPath, &CGAffineTransformIdentity, pointShadow, 3);
            CGPathCloseSubpath(shadowPath);
            CGLayerRef contentLayer = CGLayerCreateWithContext(context, rect.size, NULL);
            CGContextRef contentLayerContext = CGLayerGetContext(contentLayer);
            CGContextBeginPath(contentLayerContext);
            CGContextAddPath(contentLayerContext, shadowPath);
            CGContextClosePath(contentLayerContext);
            CGContextClip(contentLayerContext);
            int num_locations = 2;
            CGFloat locations3[2] = { 0.0, 1};
            CGColorSpaceRef colorspace3 = CGColorSpaceCreateDeviceRGB();
            CGFloat shadowComponents3[] = {0.0, 0.0, 0.0, 0.2, 0.0, 0.0, 0.0, 0.5};
            CGGradientRef gradient2 = CGGradientCreateWithColorComponents(colorspace3, shadowComponents3, locations3, num_locations);
            CGContextDrawRadialGradient(contentLayerContext, gradient2, origin, radius, origin, 0, kCGGradientDrawsBeforeStartLocation | kCGGradientDrawsAfterEndLocation);
            CGGradientRelease(gradient2);
            CGContextDrawLayerInRect(context, rect, contentLayer);
            CGLayerRelease(contentLayer);        
            CGPathRelease(shadowPath);
        }
        //------------------
    }
}

- (void)dealloc
{
    self.array = nil;
    [super dealloc];
}

- (CGPoint)convertAffinePointWithRadius:(CGFloat)radius withAngle:(CGFloat)angle
{
    CGPoint rval = CGPointZero;
    rval.x = radius * cos(angle / 180 * M_PI);
    rval.y = radius * sin(angle / 180 * M_PI);
    return rval;
}

- (CGPoint)convertAffinePointWithRadius:(CGFloat)radius withAngle:(CGFloat)angle withOffset:(CGPoint)origin
{
    CGPoint rval = [self convertAffinePointWithRadius:radius withAngle:angle];
    rval.x += origin.x;
    rval.y += origin.y;
    return rval;
}

@end
