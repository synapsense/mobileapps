//
//  FacilitiesViewCell.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "FacilitiesViewCell.h"

@implementation FacilitiesViewCell

@synthesize delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        numberOfFacilitiesInCell = 0;
    }
    return self;
}

-(void)handleFacilityTap:(MMDataCenterInfo *)facility
{
    [delegate handleFacilityTap:facility]; 
}

-(void) setFacility:(MMDataCenterInfo *)facility withCurrentOrientation:(UIInterfaceOrientation)currentOrientation withNumOfActiveAlerts: (int) numOfActiveAlerts
{
    CGRect rect = CGRectZero;
    numberOfFacilitiesInCell++;
    int divider;
    if (UIInterfaceOrientationIsPortrait(currentOrientation))
    {
        divider = 3;
    }
    else
    {
        divider = 4;
    }
    rect = CGRectMake(self.frame.size.width / divider * (numberOfFacilitiesInCell - 1), 0, self.frame.size.width / divider, self.frame.size.height);
    FacilitiesViewCellElement *facilityCellElement = [[FacilitiesViewCellElement alloc]initWithFrame:rect andFacility:facility andNumOfActiveAlerts:numOfActiveAlerts];
    [facilityCellElement setDelegate:self];
    [self addSubview:facilityCellElement];
    [facilityCellElement release];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
