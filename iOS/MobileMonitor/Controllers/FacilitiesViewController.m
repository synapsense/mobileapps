//
//  FacilitiesViewController.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "FacilitiesViewController.h"
#import "AppDelegate.h"
#import "FacilityDashboardViewController.h"
#import "FacilityAlertsViewController.h"
#import "FloorPlanViewController.h"
#import "FacilitiesViewCell.h"
#import "FacilityEmergencyContactController.h"
#import "MMSDK.h"
#import "MMDataCenterInfo.h"
#import "MMFacilitiesViewCell.h"

#define ROW_HEIGHT 60

@interface FacilitiesViewController(private)
- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;
- (void)handleFacilityTap:(MMDataCenterInfo *)facility;
- (void)sortDataCentersByName;
@end

@implementation FacilitiesViewController

@synthesize dataCenters;
@synthesize lastUpdateTime;
@synthesize mapPresentationForFacilitiesVC;
@synthesize mapView;
@synthesize aTableView;

- (void)updateScreenInfo
{
    [super updateScreenInfo];
    if (errorAlert)
    {
        [errorAlert dismissWithClickedButtonIndex:errorAlert.cancelButtonIndex animated:NO];
        errorAlert = nil;
    }
    MMSDK *sdk = [MMSDK sharedInstance];
    __block int taskIdGetDataCenters = -1;
    taskIdGetDataCenters = [sdk getDataCentersWithSuccess:^(NSArray *dataCentersInfo)
     {
         [self taskFinished:taskIdGetDataCenters];
         self.dataCenters = dataCentersInfo;
         [self sortDataCentersByName];
         self.lastUpdateTime = [NSDate date];
         self.viewDidAppear = YES;
         [self doneLoadingTableViewData];
         //first launch, we must load map
         if(self.mapPresentationForFacilitiesVC == YES && self.mapView == nil && self.aTableView == nil)
         {
//             [self updateFacilitiesTab:YES];
             [self loadMapWithFacilities];
         }
         //first launch, we must load table
         else if(self.mapPresentationForFacilitiesVC == NO && self.mapView == nil && self.aTableView == nil)
         {
//             [self updateFacilitiesTab:NO];
             [self loadTableWithFacilities];   
         }
         
         //map was loaded, now we must load table
         else if(self.mapPresentationForFacilitiesVC == NO && self.mapView != nil && self.aTableView == nil)
         {
             self.mapView.delegate = nil;
             [self.mapView removeFromSuperview];
             self.mapView = nil;
             [self loadTableWithFacilities];
         }
         
         //table was loaded, now we must load map
         else if(self.mapPresentationForFacilitiesVC == YES && self.mapView == nil && self.aTableView != nil)
         {
             [self.aTableView removeFromSuperview];
             self.aTableView = nil;
             _refreshHeaderView = nil;
             [self loadMapWithFacilities];
         }
         else
         {
             [aTableView reloadData];
         }
         [self updateHasFinished];
     }
                           failure:^(NSError *error)
     {
         [self taskFinished:taskIdGetDataCenters];
         [self updateHasFinished];
         if (_reloading == YES)
         {
             [self doneLoadingTableViewData];
         }
         if (!errorAlert)
         {
             errorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"updatingError", @"") message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self cancelButtonTitle:NSLocalizedString(@"okButtonTitle", @"") otherButtonTitles: nil];
             [errorAlert show];
             [errorAlert release];
         }
     }];
    [self addTaskId:taskIdGetDataCenters];
}

- (void)sortDataCentersByName
{
    self.dataCenters = [self.dataCenters sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSString *first = ((MMDataCenterInfo*)obj1).name;
        NSString *second = ((MMDataCenterInfo*)obj2).name;
        return [first compare:second];
    }];
}

- (id)initWithDataCenters:(NSArray*)dcs
{
    self = [super init];
    if (self)
    {
        self.dataCenters = dcs;
    }

    return self;
}

#pragma mark - View lifecycle

- (void)loadView
{
    self.view = [[[MMView alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    [self.view setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    self.tabBarController.navigationItem.title = NSLocalizedString(@"applicationTitle", @"");
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"backButtonTitle", @"") style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.tabBarController.navigationItem.backBarButtonItem = backButton;
    [backButton release];
    UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [settingsButton setBackgroundImage:[UIImage imageNamed:@"Settings-Blue.png"] forState:UIControlStateNormal];
    [settingsButton addTarget:self action:@selector(settingsButtonWasPressed) forControlEvents:UIControlEventTouchUpInside];
    [settingsButton setBounds:CGRectMake(0, 0, SETTINGS_BUTTON_SIZE, SETTINGS_BUTTON_SIZE)];
    UIBarButtonItem *settingsBarButton = [[UIBarButtonItem alloc]initWithCustomView:settingsButton];
    self.tabBarController.navigationItem.rightBarButtonItem = settingsBarButton;
    [settingsBarButton release];
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(CustomMKAnnotationView *)view
{
    self.mapView.delegate = self;
    MMDataCenterInfo * facility = [[[MMDataCenterInfo alloc] init] autorelease];
    
    for (NSObject<MKAnnotation> *annotation in [self.mapView selectedAnnotations]) 
    {
        [self.mapView deselectAnnotation:(id <MKAnnotation>)annotation animated:NO];
    }
    
    //to find current DC in array
    for(int i = 0; i < self.dataCenters.count ; ++i)
    {
        facility = [self.dataCenters objectAtIndex: i];
        if([facility.guid isEqualToString:view.annotation.title])
        {
            break;
        }
    }
    [self handleFacilityTap: facility];
}


- (CustomMKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation 
{   
    static NSString* AnnotationIdentifier = @"AnnotationIdentifier";
    CustomMKAnnotationView *annotationView = (CustomMKAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationIdentifier];
    
    if(annotationView)
    {
        return annotationView;
    }
    else
    {
        CustomMKAnnotationView *annotationView = [[[CustomMKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier]autorelease];
        annotationView.canShowCallout = NO;
        MMDataCenterInfo *dc = [self.dataCenters objectAtIndex:((MapAnnotation*)annotation).tag];
        if (dc.numberOfActiveAlerts > 0)
        {
            annotationView.image = [UIImage imageNamed:[NSString stringWithFormat:@"FacilitiesIconAlerts.png"]];            
        }
        else if (dc.status.value == 0)
        {
            annotationView.image = [UIImage imageNamed:[NSString stringWithFormat:@"FacilitiesIconWarnings.png"]];
        }
        else
        {
            annotationView.image = [UIImage imageNamed:[NSString stringWithFormat:@"FacilitiesIcon.png"]];
        }
        annotationView.draggable = NO;
        return annotationView;
    }
    return nil;
}

/*- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}*/

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.mapPresentationForFacilitiesVC = [[[AppDelegate sharedInstance] applicationPreferences] boolForKey:MAP_PRESENTATION];
    _mapRegion = [AppDelegate sharedInstance].mapCoordinateRegion;
    [self updateScreenInfo];
    [self willAnimateRotationToInterfaceOrientation:[self interfaceOrientation] duration:0];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (self.mapPresentationForFacilitiesVC)
    {
        [AppDelegate sharedInstance].mapCoordinateRegion = self.mapView.region;
        [[AppDelegate sharedInstance].applicationPreferences setDouble:_mapRegion.center.latitude forKey:MAP_VIEW_REGION_LATITUDE];
        [[AppDelegate sharedInstance].applicationPreferences setDouble:_mapRegion.center.longitude forKey:MAP_VIEW__REGION_LONGITUDE];
        [[AppDelegate sharedInstance].applicationPreferences setDouble:_mapRegion.span.latitudeDelta forKey:MAP_VIEW__REGION_DELTA_LATITUDE];
        [[AppDelegate sharedInstance].applicationPreferences setDouble:_mapRegion.span.longitudeDelta forKey:MAP_VIEW__REGION_DELTA_LONGITUDE];
    }
}

- (void)loadMapWithFacilities
{
    MKMapView *map = [[MKMapView alloc] initWithFrame:CGRectMake(0,
                                                                 0,
                                                                 VIEW_WIDTH,
                                                                 VIEW_HEIGHT)];
    self.mapView = map;
    [map release];
    
    [self.view addSubview:mapView];
    self.mapView.delegate = self;
    int annotationsCount = 0;
    for(int i = 0; i < dataCenters.count; ++i)
    {
        MMDataCenterInfo * dc = [self.dataCenters objectAtIndex:i];
        if (dc.latitude != 0 && dc.longitude != 0)
        {
            MapAnnotation *annotation = [MapAnnotation new];
            annotation.tag = i;
            annotation.title = dc.guid;
            annotation.subtitle = @"annotation example";
            annotation.coordinate = CLLocationCoordinate2DMake(dc.latitude, dc.longitude);
            
            [mapView addAnnotation:annotation];
            [annotation release];
            annotationsCount++;
        }
    }
    if (_mapRegion.span.latitudeDelta == MAP_VIEW_REGION_UNKNOWN && annotationsCount > 0)
    {
        float totalLongitude = 0;
        float totalLatitude = 0;
        float minLongitude;
        float maxLongitude;
        float minLatitude;
        float maxLatitude;
        
        for(int i = 0; i < dataCenters.count; ++i)
        {
            MMDataCenterInfo * dc = [self.dataCenters objectAtIndex:i];
            if (dc.latitude != 0 && dc.longitude != 0)
            {
                if(i == 0)
                {
                    //to initialize parameters
                    minLongitude = dc.longitude;
                    maxLongitude = dc.longitude;
                    minLatitude = dc.latitude;
                    maxLatitude = dc.latitude;
                }
                else
                {
                    if(dc.longitude < minLongitude)
                        minLongitude = dc.longitude;
                    if(dc.longitude > maxLongitude)
                        maxLongitude = dc.longitude;
                    if(dc.latitude < minLatitude)
                        minLatitude = dc.latitude;
                    if(dc.latitude > maxLatitude)
                        maxLatitude = dc.latitude;
                }
                
                totalLatitude += dc.latitude;
                totalLongitude += dc.longitude;
            }
        }
        
        float averageLongitude = totalLongitude/(annotationsCount);
        float averageLatitude = totalLatitude/(annotationsCount);
        
        MKCoordinateSpan span;
        if(annotationsCount == 1)
        {
            span.latitudeDelta = 40.0;
            span.longitudeDelta = 40.0;
        }
        else
        {
            //to show all DCs inside one screen
            span.latitudeDelta = maxLatitude - minLatitude;
            span.longitudeDelta = maxLongitude - minLongitude;
        }
        _mapRegion.center = CLLocationCoordinate2DMake(averageLatitude, averageLongitude);
        _mapRegion.span = span;
    }
    else if (_mapRegion.span.latitudeDelta == MAP_VIEW_REGION_UNKNOWN && annotationsCount == 0)
    {
        MKCoordinateSpan span = {.latitudeDelta = 180, .longitudeDelta = 360};
        _mapRegion.center = CLLocationCoordinate2DMake(39.1, -98.88);
        _mapRegion.span = span;

    }
    [self.mapView setRegion:_mapRegion animated:NO];
    
}

- (void)loadTableWithFacilities
{
    [aTableView release];
    aTableView = [[MMTableView alloc]initWithFrame:self.view.frame style:UITableViewStylePlain];
    [aTableView setDataSource:self];
    [aTableView setDelegate:self];
    [aTableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [aTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:aTableView];
    
    self.lastUpdateTime = [NSDate date];
    if (_refreshHeaderView == nil) {
        
        EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - aTableView.bounds.size.height, self.view.frame.size.width, aTableView.bounds.size.height)];
        view.delegate = self;
        [view setDateFormat:[AppDelegate sharedInstance].userPreferences.dateFormat];
        [aTableView addSubview:view];
        _refreshHeaderView = view;
        [view release];
        [self doneLoadingTableViewData];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    if(self.mapPresentationForFacilitiesVC == YES)
    {
        [self.mapView setFrame:CGRectMake(self.view.frame.origin.x,
                                          self.view.frame.origin.y,
                                          VIEW_WIDTH,
                                          VIEW_HEIGHT)];
    }
    else if(self.mapPresentationForFacilitiesVC == NO)
    {
        [aTableView reloadData];
    }
    
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ROW_HEIGHT;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return dataCenters.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseIdentifier = [NSString stringWithFormat:@"cell %d",[indexPath indexAtPosition:0]];
    MMFacilitiesViewCell *cell = (MMFacilitiesViewCell*)[tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (!cell)
    {
        cell = [[[MMFacilitiesViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier]autorelease];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    MMDataCenterInfo * dc = [dataCenters objectAtIndex:[indexPath indexAtPosition:0]];
    [cell setDCName:dc.name];
    [cell setZoneNumber:dc.totalZoneNumber];
    [cell setNumAlerts:dc.numberOfActiveAlerts];
    if (dc.status.value == 0)
    {
        [cell setNumWarnings:0];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self handleFacilityTap:[dataCenters objectAtIndex:[indexPath indexAtPosition:0]]];
}

-(void)handleFacilityTap:(MMDataCenterInfo *)facility
{
    FacilityDashboardViewController *facilityDashboardViewController = [[FacilityDashboardViewController alloc] initWithDataCenter:facility];
    UIImage *dashboardTabImageSelected = [UIImage imageWithCGImage:[[UIImage imageNamed:@"dsashboardTab-Blue.png"] CGImage] scale:1.5 orientation:UIImageOrientationUp];
    UIImage *dashboardTabImageUnselected = [UIImage imageWithCGImage:[[UIImage imageNamed:@"dsashboardTab-Gray.png"] CGImage] scale:1.5 orientation:UIImageOrientationUp];
    UITabBarItem *facilityDashboardTab = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"detailScreenTabBarFacilityItemTitle", @"") image:nil tag:0];
    [facilityDashboardTab setFinishedSelectedImage:dashboardTabImageSelected withFinishedUnselectedImage:dashboardTabImageUnselected];
    [facilityDashboardViewController setTabBarItem:facilityDashboardTab];
    [facilityDashboardTab release];

    FacilityAlertsViewController *facilityAlertsViewController = [[FacilityAlertsViewController alloc] initWithDataCenter:facility guid:facility.guid];
    UIImage *alertsTabImageSelected = [UIImage imageWithCGImage:[[UIImage imageNamed:@"Alert-Tab-Blue.png"] CGImage] scale:1.5 orientation:UIImageOrientationUp];
    UIImage *alertsTabImageUnselected = [UIImage imageWithCGImage:[[UIImage imageNamed:@"Alert-Tab-Gray.png"] CGImage] scale:1.5 orientation:UIImageOrientationUp];
    UITabBarItem *facilityAlertsTab = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"detailScreenTabBarAlertsItemTitle", @"") image:nil tag:0];
    [facilityAlertsTab setFinishedSelectedImage:alertsTabImageSelected withFinishedUnselectedImage:alertsTabImageUnselected];
    [facilityAlertsViewController setTabBarItem:facilityAlertsTab];
    [facilityAlertsTab release];

    FloorPlanViewController *floorPlanViewController = [[FloorPlanViewController alloc] initWithDataCenter:facility];
    UIImage *floorplanTabImageSelected = [UIImage imageWithCGImage:[[UIImage imageNamed:@"floorPlan-Blue.png"] CGImage] scale:1.5 orientation:UIImageOrientationUp];
    UIImage *floorplanTabImageUnselected = [UIImage imageWithCGImage:[[UIImage imageNamed:@"floorPlan-Gray.png"] CGImage] scale:1.5 orientation:UIImageOrientationUp];
    UITabBarItem *floorPlanTab = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"detailScreenTabBarFloorPlaneItemTitle", @"") image:nil tag:0];
    [floorPlanTab setFinishedSelectedImage:floorplanTabImageSelected withFinishedUnselectedImage:floorplanTabImageUnselected];
    [floorPlanViewController setTabBarItem:floorPlanTab];
    [floorPlanTab release];
    
    FacilityEmergencyContactController *facilityEmergencyContactController = [[FacilityEmergencyContactController alloc] initWithDataCenter:facility guid:facility.guid];
    UIImage *emergencyTabImageSelected = [UIImage imageWithCGImage:[[UIImage imageNamed:@"EmergencyContact-Blue.png"] CGImage] scale:1.5 orientation:UIImageOrientationUp];
    UIImage *emergencyTabImageUnselected = [UIImage imageWithCGImage:[[UIImage imageNamed:@"EmergencyContact-Gray.png"] CGImage] scale:1.5 orientation:UIImageOrientationUp];
    UITabBarItem *facilityPhoneTab = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"detailScreenTabBarPhoneItemTitle", @"") image:nil tag:0];
    [facilityPhoneTab setFinishedSelectedImage:emergencyTabImageSelected withFinishedUnselectedImage:emergencyTabImageUnselected];    [facilityEmergencyContactController setTabBarItem:facilityPhoneTab];
    [facilityPhoneTab release];
    
    NSArray *detailsViewControllersArray = [[NSArray alloc] initWithObjects:facilityDashboardViewController, facilityAlertsViewController, floorPlanViewController, facilityEmergencyContactController, nil];
    [facilityAlertsViewController release];
    [facilityDashboardViewController release];
    [floorPlanViewController release];
    [facilityEmergencyContactController release];
    
    UITabBarController *detailsTabBarController = [[UITabBarController alloc] init];
    [detailsTabBarController setViewControllers:detailsViewControllersArray];
    [detailsViewControllersArray release];
    if ([detailsTabBarController.tabBar respondsToSelector:@selector(setBarTintColor:)]) {
        [[detailsTabBarController tabBar] setBarTintColor:[UIColor darkGrayColor]];
        [[detailsTabBarController tabBar] setTintColor:[UIColor whiteColor]];
        [[detailsTabBarController tabBar] setTranslucent:NO];
    } else {
        detailsTabBarController.view.backgroundColor = [[[ThemeManager sharedInstance] currentTheme] backgroundColor];
    }
    [self.navigationController pushViewController:detailsTabBarController animated:YES];
    [detailsTabBarController release];
}


#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
	_reloading = YES;
    [self.view setUserInteractionEnabled:NO];
}

- (void)doneLoadingTableViewData{
	_reloading = NO;
    [self.view setUserInteractionEnabled:YES];
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:aTableView];
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{	
	
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
	
	[self reloadTableViewDataSource];
    [self updateScreenInfo];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	
	return _reloading;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	
	return self.lastUpdateTime;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (errorAlert)
    {
        errorAlert = nil;
    }
}

- (void)settingsButtonWasPressed
{
    SettingsViewController *settingsViewController = [[SettingsViewController alloc]init];
    [settingsViewController setSettingsType:SETTINGS_TYPE_LITE];
    [settingsViewController setDelegate:self];
    [self.navigationController pushViewController:settingsViewController animated:YES];
    [settingsViewController release];
}

//- (void)updateFacilitiesTab:(BOOL)showMap
//{
//    if (showMap)
//    {
//        UIImage *facilityTabImageSelected = [UIImage imageWithCGImage:[[UIImage imageNamed:@"Map-Blue.png"] CGImage] scale:1.5 orientation:UIImageOrientationUp];
//        UIImage *facilityTabImageUnselected = [UIImage imageWithCGImage:[[UIImage imageNamed:@"Map-Gray.png"] CGImage] scale:1.5 orientation:UIImageOrientationUp];
//        UITabBarItem *facilitiesViewTab = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"mainScreenTabBarFacilitiesItemTitle", @"") image:facilityTabImageSelected tag:0];
//        if ([[UIDevice currentDevice].systemVersion floatValue] >= 5.0)
//        {
//            [facilitiesViewTab setFinishedSelectedImage:facilityTabImageSelected withFinishedUnselectedImage:facilityTabImageUnselected];
//        }
//        [self setTabBarItem:facilitiesViewTab];
//        [facilitiesViewTab release];
//    }
//    else
//    {
//        UIImage *facilityTabImageSelected = [UIImage imageWithCGImage:[[UIImage imageNamed:@"facilitiesTab-Blue.png"] CGImage] scale:1.5 orientation:UIImageOrientationUp];
//        UIImage *facilityTabImageUnselected = [UIImage imageWithCGImage:[[UIImage imageNamed:@"facilitiesTab-Gray.png"] CGImage] scale:1.5 orientation:UIImageOrientationUp];
//        UITabBarItem *facilitiesViewTab = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"mainScreenTabBarFacilitiesItemTitle", @"") image:facilityTabImageSelected tag:0];
//        if ([[UIDevice currentDevice].systemVersion floatValue] >= 5.0)
//        {
//            [facilitiesViewTab setFinishedSelectedImage:facilityTabImageSelected withFinishedUnselectedImage:facilityTabImageUnselected];
//        }
//        [self setTabBarItem:facilitiesViewTab];
//        [facilitiesViewTab release];
//    }
//}

- (void)cancelCurrentConnections
{
    if (_reloading == YES)
    {
        [self doneLoadingTableViewData];
    }
    [super cancelCurrentConnections];
}

- (void)dealloc
{
    self.dataCenters = nil;
    _refreshHeaderView = nil;
    self.lastUpdateTime = nil;
    self.mapView.delegate = nil;
    self.mapView = nil;
    self.aTableView = nil;
    [super dealloc];
}

@end


@implementation MapAnnotation

@synthesize coordinate,title,subtitle,tag;

- (void)dealloc {
    self.title = nil;
    self.subtitle = nil;
    [super dealloc];
}
@end


@implementation CustomMKAnnotationView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

@end