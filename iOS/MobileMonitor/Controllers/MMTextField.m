//
//  MMTextField.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMTextField.h"
#import <QuartzCore/QuartzCore.h>

@implementation MMTextField

@synthesize horizontalPadding;
@synthesize vericalPadding;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [[ThemeManager sharedInstance] registerObserver:self];
        self.layer.borderWidth = 1.0f;
        [self adjustUI:[ThemeManager sharedInstance].currentTheme];
        //[self setDelegate:self];
    }
    return self;
}

- (void)dealloc
{
    [[ThemeManager sharedInstance]unregisterObserver:self];
    [super dealloc];
}

- (void)adjustUI:(MMDefaultTheme *)currentTheme
{
    self.textColor = currentTheme.textFieldTextColor;
    self.layer.borderColor = currentTheme.textFieldBorderColor.CGColor;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.layer.borderWidth = self.isEditing ? 3.0f : 1.0f;
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, horizontalPadding, vericalPadding);
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, horizontalPadding, vericalPadding);
}

@end
