//
//  ViewController.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController (PrivateMethods)

- (void)login;
- (void)handleSingleTap;

@end

@implementation LoginViewController

@synthesize loginView;

#define TRANSITION_DURATION     0.5
//n102569.merann.ru
//dendrite.merann.ru

#pragma mark - View lifecycle

- (void)dealloc
{
    self.loginView = nil;
    //[_settingsViewController release];
    [super dealloc];
}

- (void)loadView
{
    [super loadView];    
    self.navigationItem.title = NSLocalizedString(@"appTitle", @"");
    UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [settingsButton setBackgroundImage:[UIImage imageNamed:@"Settings-Blue.png"] forState:UIControlStateNormal];
    [settingsButton addTarget:self action:@selector(settingsButtonWasPressed) forControlEvents:UIControlEventTouchUpInside];
    [settingsButton setBounds:CGRectMake(0, 0, SETTINGS_BUTTON_SIZE, SETTINGS_BUTTON_SIZE)];
    UIBarButtonItem *settingsBarButton = [[UIBarButtonItem alloc]initWithCustomView:settingsButton];
    self.navigationItem.rightBarButtonItem = settingsBarButton;
    [settingsBarButton release];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"backButtonTitle", @"") style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton* _buttonWithBigArea = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    _buttonWithBigArea.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [_buttonWithBigArea addTarget:self action:@selector(handleSingleTap) forControlEvents:UIControlEventTouchUpInside];
    [_buttonWithBigArea setBackgroundColor:[UIColor clearColor]];
    
    _buttonWithBigArea.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [self.view addSubview:_buttonWithBigArea];
    [_buttonWithBigArea release];
    
    self.loginView = [[[MMLoginView alloc] initWithFrame: CGRectMake(0, 0, 320, 420)] autorelease];
    self.view = self.loginView;
    self.loginView.delegate = self;
    self.loginView.autoresizesSubviews = YES;
    self.loginView.serverName.delegate = self;
    self.loginView.serverName.borderStyle = UITextBorderStyleBezel;
    [self.loginView.serverName addTarget:self action:@selector(returnKeyPressed:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    self.loginView.userName.delegate = self;
    [self.loginView.userName addTarget:self action:@selector(returnKeyPressed:) forControlEvents:UIControlEventEditingDidEndOnExit];

    self.loginView.password.delegate = self;
    [self.loginView.password addTarget:self action:@selector(returnKeyPressed:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [self.loginView.loginButton addTarget:self action:@selector(loginButtonPressed) forControlEvents:UIControlEventTouchUpInside];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide) name:UIKeyboardWillHideNotification object:nil];
    
    self.loginView.frame = [[UIScreen mainScreen] bounds];
    
    int orientation = [self interfaceOrientation];
    if (UIInterfaceOrientationIsLandscape(orientation))
    {
        self.loginView.isLandscapeOrientation = YES;
    }
    else
    {
        self.loginView.isLandscapeOrientation = NO;
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    self.loginView = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.loginView.password.text = @"";
}

- (void)settingsButtonWasPressed
{
    _settingsViewController = [[SettingsViewController alloc]init];
    [self.navigationController pushViewController:_settingsViewController animated:YES];
    [_settingsViewController release];
}

- (void)returnKeyPressed:(UITextField*)sender
{
    switch (sender.tag) {
        case 1:
            [self.loginView.userName becomeFirstResponder];
            break;
        case 2:
            [self.loginView.password becomeFirstResponder];
            break;
        case 3:
            [self performSelector:@selector(loginButtonPressed)];
            break;
    }
}
-(void)handleBackgroundTap
{
    [self.view endEditing:YES];
}

- (void)keyboardWillShow
{
//    self.loginView.isKeyboardVisible = YES;
    [self.view setNeedsLayout];
}

- (void)keyboardWillHide
{
//    self.loginView.isKeyboardVisible = NO;
    [self.view setNeedsLayout];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.loginView showTextFieldForEditing:textField];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation))
    {
        self.loginView.isLandscapeOrientation = YES;
    }
    else if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation))
    {
        self.loginView.isLandscapeOrientation = NO;
    }
    [self.view setNeedsLayout];
}

-(void)AlertCancelButtonClicked
{
    MMSDK *sdk = [MMSDK sharedInstance];
    [sdk cancelRequests:[NSArray arrayWithObject:[NSNumber numberWithInt:loginRequestId]]];
}

- (void)loginButtonPressed
{
    if (self.loginView.serverName.text.length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Please provide URL.", @"") message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"") otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    else if (self.loginView.password.text.length == 0 || self.loginView.userName.text.length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Please provide user name and password.", @"") message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"") otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    else
    {
        MMSDK *sdk = [MMSDK sharedInstance];
        if ([sdk isDeviceRegistredOnService:self.loginView.serverName.text])
        {
            [self login];
        }
        else
        {
            registerAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"deviceRegistration", @"The device should be registred") message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"") otherButtonTitles:NSLocalizedString(@"okButtonTitle", @""), nil];
            [registerAlertView setDelegate:self];
            [registerAlertView show];
            [registerAlertView release];
        }
    }
}

- (void)login
{
    BOOL useHTTPS = [[[AppDelegate sharedInstance] applicationPreferences] boolForKey:USE_HTTPS];
    [MMSDK setConnectionIsSecure:useHTTPS];
    NSInteger port = useHTTPS ? [AppDelegate sharedInstance].securedPort : [AppDelegate sharedInstance].unsecuredPort;
    [MMSDK setPort:port];
    [[[AppDelegate sharedInstance] applicationPreferences] setObject:self.loginView.userName.text forKey:@"Previous user"];
    
    TransferringAlertView *transferringAlert = [[TransferringAlertView alloc]init];
    [transferringAlert setTitle:NSLocalizedString(@"Authentication in progress...", @"")];
    [transferringAlert showAlertView];
    [transferringAlert setDelegate:self];
    
    MMSDK *sdk = [MMSDK sharedInstance];
    loginRequestId =[sdk login:self.loginView.userName.text withPassword:self.loginView.password.text  withServiceURL:self.loginView.serverName.text success:^(MMUserInfo *userInfo, MMUserPreferences *userPrefs)
     {
         [AppDelegate sharedInstance].userPreferences = userPrefs;
         [[AppDelegate sharedInstance].applicationPreferences setObject:self.loginView.serverName.text forKey:SERVER_URL];
         [sdk getUIPrivileges:^(MMUIPrivileges privileges) 
          {
              [transferringAlert hideAlertView];
              [transferringAlert release];
              NSLog(@"\n server name: %@ \n login: %@ \n password: %@",self.loginView.serverName.text,self.loginView.userName.text,self.loginView.password.text);
              //save port to preferences
              if (useHTTPS) 
              {
                  [[AppDelegate sharedInstance].applicationPreferences setInteger:port forKey:SECURED_PORT];
              }
              else
              {
                  [[AppDelegate sharedInstance].applicationPreferences setInteger:port forKey:UNSECURED_PORT];
              }
              [[AppDelegate sharedInstance] setUiPrivileges:privileges];
              [MMSDK setUIPrivileges:privileges];
              FacilitiesViewController *facilitiesViewController = [[FacilitiesViewController alloc] initWithDataCenters:nil];
              UIImage *facilityTabImageSelected = [UIImage imageWithCGImage:[[UIImage imageNamed:@"facilitiesTab-Blue.png"] CGImage] scale:1.5 orientation:UIImageOrientationUp];
              UIImage *facilityTabImageUnselected = [UIImage imageWithCGImage:[[UIImage imageNamed:@"facilitiesTab-Gray.png"] CGImage] scale:1.5 orientation:UIImageOrientationUp];
              UITabBarItem *facilitiesViewTab = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"mainScreenTabBarFacilitiesItemTitle", @"") image:nil tag:0];
              [facilitiesViewTab setFinishedSelectedImage:facilityTabImageSelected withFinishedUnselectedImage:facilityTabImageUnselected];
              [facilitiesViewController setTabBarItem:facilitiesViewTab];
              [facilitiesViewTab release];
              [facilitiesViewController setWantsFullScreenLayout:YES];

              DashboardViewController* dashboardViewController = [[DashboardViewController alloc] initWithData:nil];
              UIImage *dashboardTabImageSelected = [UIImage imageWithCGImage:[[UIImage imageNamed:@"dsashboardTab-Blue.png"] CGImage] scale:1.5 orientation:UIImageOrientationUp];
              UIImage *dashboardTabImageUnselected = [UIImage imageWithCGImage:[[UIImage imageNamed:@"dsashboardTab-Gray.png"] CGImage] scale:1.5 orientation:UIImageOrientationUp];
              UITabBarItem *dashboardTab = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"mainScreenTabBarDashboardItemTitle", @"") image:nil tag:0];
              [dashboardTab setFinishedSelectedImage:dashboardTabImageSelected withFinishedUnselectedImage:dashboardTabImageUnselected];
              
              [dashboardViewController setTabBarItem:dashboardTab];
              [dashboardTab release];

              NSMutableArray *arrayWithViewControllers = [[NSMutableArray alloc]initWithObjects:facilitiesViewController, dashboardViewController, nil];
              [facilitiesViewController release];
              [dashboardViewController release];
              if ([AppDelegate sharedInstance].uiPrivileges.showSystemHealth)
              {
                  SystemHealthViewController *systemHealthViewController = [[SystemHealthViewController alloc] init];
                  UIImage *systemTabImageSelected = [UIImage imageWithCGImage:[[UIImage imageNamed:@"systermHealth-Blue.png"] CGImage] scale:1.5 orientation:UIImageOrientationUp];
                  UIImage *systemTabImageUnselected = [UIImage imageWithCGImage:[[UIImage imageNamed:@"systermHealth-Gray.png"] CGImage] scale:1.5 orientation:UIImageOrientationUp];
                  UITabBarItem *systemHealthTab = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"mainScreenTabBarSystemHealthItemTitle", @"") image:nil tag:0];
                  [systemHealthTab setFinishedSelectedImage:systemTabImageSelected withFinishedUnselectedImage:systemTabImageUnselected];
                  [systemHealthViewController setTabBarItem:systemHealthTab];
                  [systemHealthTab release];
                  [arrayWithViewControllers addObject:systemHealthViewController];
                  [systemHealthViewController release];
              }
              UITabBarController *tabBarController = [[UITabBarController alloc] init];
              
              [tabBarController setViewControllers:arrayWithViewControllers];
              tabBarController.navigationItem.hidesBackButton = YES;
              [tabBarController setSelectedIndex:0];
              [arrayWithViewControllers release];
              if ([tabBarController.tabBar respondsToSelector:@selector(setBarTintColor:)]) {
                  [[tabBarController tabBar] setBarTintColor:[UIColor darkGrayColor]];
                  [[tabBarController tabBar] setTintColor:[UIColor whiteColor]];
                  [[tabBarController tabBar] setTranslucent:NO];
              } else {
                  tabBarController.view.backgroundColor = [ThemeManager sharedInstance].currentTheme.backgroundColor;
              }
              [self.navigationController pushViewController:tabBarController animated:YES];
              [tabBarController release];
          }];
     }
       failure:^(NSError *error)
     {
         //NSLog(@"%@", error);
         UIAlertView *alerView = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"loginError", @"") message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:nil cancelButtonTitle:NSLocalizedString(@"okButtonTitle", @"") otherButtonTitles: nil];
         [transferringAlert hideAlertView];
         [transferringAlert release];
         [alerView show];
         [alerView release];
     }];
}
#pragma mark -
#pragma mark UIAlertViewDelegate

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == registerAlertView)
    {
        //ok button case
        if (buttonIndex == 1)
        {
            [self login];
        }
    }
}

@end
