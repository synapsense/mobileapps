//
//  MMTableViewCell.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"

@implementation MMTableViewCell

@synthesize tableviewStyle;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [[ThemeManager sharedInstance] registerObserver:self];
        [self adjustUI:[ThemeManager sharedInstance].currentTheme];
        self.detailTextLabel.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)dealloc
{
    [[ThemeManager sharedInstance]unregisterObserver:self];
    [super dealloc];
}

- (void)adjustUI:(MMDefaultTheme *)currentTheme
{
    self.textLabel.textColor = currentTheme.titleLabelColor;
    self.detailTextLabel.textColor = currentTheme.subtitleLabelColor;
    [self setBackgroundColor: currentTheme.tableViewCellBackgroundColor];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if (tableviewStyle != UITableViewStyleGrouped)
    {
        [self.textLabel setFrame:CGRectMake(HORIZONTAL_INDENT, self.textLabel.frame.origin.y, self.textLabel.frame.size.width, self.textLabel.frame.size.height)];
        [self.detailTextLabel setFrame:CGRectMake(HORIZONTAL_INDENT, self.detailTextLabel.frame.origin.y, self.detailTextLabel.frame.size.width, self.detailTextLabel.frame.size.height)];
    }
}

@end
