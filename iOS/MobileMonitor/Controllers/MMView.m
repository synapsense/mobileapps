//
//  MMView.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMView.h"

@implementation MMView

@synthesize viewLevel;

- (id)initWithFrame:(CGRect)frame
{

    return [self initWithFrame:frame viewLevel:ViewLevelFirst];
}

- (id)initWithFrame:(CGRect)frame viewLevel:(ViewLevel)level
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        self.viewLevel = level;
        [[ThemeManager sharedInstance] registerObserver:self];
        [self adjustUI:[ThemeManager sharedInstance].currentTheme];
    }
    return self;
}

- (void)dealloc
{
    [[ThemeManager sharedInstance]unregisterObserver:self];
    [super dealloc];
}

- (void)adjustUI:(MMDefaultTheme *)currentTheme
{
    if (self.viewLevel == ViewLevelSecond) 
    {
        self.backgroundColor = currentTheme.backgroundColorSecondLevel;
    }
    else
    {
        self.backgroundColor = currentTheme.backgroundColor;
    }
}

@end
