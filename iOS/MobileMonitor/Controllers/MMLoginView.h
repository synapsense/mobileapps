//
//  MMLoginView.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppUtility.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "MMView.h"
#import "MMTextField.h"
#import "MMTitleLabel.h"

#define VERTICAL_INDENT      5
#define HEIGHT_OF_FIELD     40

static NSString *const SELECTED_CHECKBOX = @"\u2611";
static NSString *const UNSELECTED_CHECKBOX = @"\u2610";

@protocol MMLoginViewDelegate <NSObject>

- (void)handleBackgroundTap;

@end

@interface MMLoginView : MMView
{
    UIImageView* logoImage;

    UIView* contentView;

    MMTitleLabel* serverNameTitle;
    MMTextField* serverName;

    MMTitleLabel* userNameTitle;
    MMTextField* userName;

    MMTitleLabel* passwordTitle;
    MMTextField* password;

    UIButton* loginButton;

    UIImageView* poweredByImage;
}

@property (nonatomic,retain) UIImageView* logoImage;
@property (nonatomic,retain) UIView* contentView;
@property (nonatomic,retain) MMTextField* serverName;
@property (nonatomic,retain) MMTextField* userName;
@property (nonatomic,retain) MMTextField* password;
@property (nonatomic,retain) UIButton* loginButton;
@property (nonatomic,assign) BOOL isLandscapeOrientation;
@property (nonatomic,retain) UIImageView* poweredByImage;
@property (nonatomic,assign) id<MMLoginViewDelegate>delegate;

- (void)showTextFieldForEditing:(UITextField*)showingTextField;

@end
