//
//  RequestListener.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestListener : NSObject
{
    NSMutableArray* _taskIds;
}

@property (nonatomic, retain)NSMutableArray* taskIds;

- (void)taskFinished:(int)taskId;

@end

@protocol RequestListenerDelegate <NSObject>

- (void)taskFinished:(int)taskId;
- (void)addTaskId:(int)taskId;

@end