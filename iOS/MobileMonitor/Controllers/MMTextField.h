//
//  MMTextField.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ThemeManager.h"

@interface MMTextField : UITextField<ThemeListenerControl, UITextFieldDelegate>

@property (nonatomic)float horizontalPadding;
@property (nonatomic)float vericalPadding;

@end
