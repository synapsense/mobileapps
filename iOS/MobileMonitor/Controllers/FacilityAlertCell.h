//
//  FacilityAlertCell.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMTitleLabel.h"
#import "MMSubtitleLabel.h"
#import "MMAlertInfo.h"

#define HEIGHT_CELL             50
#define HEIGHT_LABEL            20
#define WIDTH_TIMESTAMP         60
#define HEIGHT_TIMESTAMP        6
#define SIZE_ICON               25

@interface FacilityAlertCell : UITableViewCell
{
    MMSubtitleLabel* alertObject;
    MMTitleLabel* alertName;
    MMSubtitleLabel* alertDate;
    UIImageView* icon;
}

@property(nonatomic, retain) UILabel* alertObject;
@property(nonatomic, retain) UILabel* alertName;
@property(nonatomic, retain) UILabel* alertDate;
@property(nonatomic, retain) UIImageView* icon;

-(id)initWithFrameAlert:(CGRect)frame Status:(MMAlertStatus)status;

@end
