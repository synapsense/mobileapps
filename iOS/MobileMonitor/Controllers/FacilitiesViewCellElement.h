//
//  FacilitiesViewCellElement.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MMDataCenterInfo;

@protocol FacilitiesViewCellElementDelegate <NSObject>
-(void)handleFacilityTap:(MMDataCenterInfo*) dataCenterInfo;
@end

@interface FacilitiesViewCellElement : UIView <UIGestureRecognizerDelegate>
{
    MMDataCenterInfo *dataCenterInfo;
}

@property (nonatomic, assign)id<FacilitiesViewCellElementDelegate> delegate;
@property (nonatomic, assign) MMDataCenterInfo* dataCenterInfo;

- (id)initWithFrame:(CGRect)frame
        andFacility:(MMDataCenterInfo*)dataCenterInfo andNumOfActiveAlerts:(int)numOfActiveAlerts;

@end

