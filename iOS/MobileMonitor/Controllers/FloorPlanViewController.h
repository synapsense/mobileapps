//
//  FloorPlanViewController.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FloorPlanObjectDetailsViewController.h"
#import "FloorPlanFilterViewController.h"
#import "BaseViewController.h"
#import "MMDataCenterInfo.h"
#import "MMFloorPlanInfo.h"
#import "FilterViewController.h"
#import "MMliveImagingType.h"

@interface FloorPlanViewController : BaseViewController <UIGestureRecognizerDelegate, UIScrollViewDelegate, LiveImagingTypeDelegate>
{
    FloorPlanObjectDetailsViewController *floorPlanObjectDetailsViewController;
    FilterViewController* filterViewController;
    CGSize floorplanSize;
    UIImage *_schemeImage;
}

@property (nonatomic, retain) FloorPlanObjectDetailsViewController *floorPlanObjectDetailsViewController;
@property (nonatomic, retain) FloorPlanFilterViewController *floorPlanFilterViewController;
@property (nonatomic, retain) UIScrollView *mainview;
@property (nonatomic, retain) UIView *mapview;
@property (nonatomic, retain) UIView *labelview;
@property (nonatomic, retain) MMDataCenterInfo *currentDataCenter;
@property (nonatomic, retain) MMFloorPlanInfo *floorplaneinfo;
@property (nonatomic, retain) UIImageView *bitmap;
@property (nonatomic, retain) NSMutableArray *buttons;
@property (nonatomic, retain) MMliveImagingType *liveImagingType;
@property (nonatomic, retain) NSArray *liveImagingTypes;
@property (nonatomic) float zoomscale;
@property (nonatomic) float scaleFactor;
@property (nonatomic) CGPoint offsetBeforeUpdate;
@property (nonatomic) MMFloorPlanFilter filterMask;
@property (nonatomic, retain) UIImage *floorPlanImage;
@property (nonatomic, retain) NSDate *lastUpdateSchemeDate;

- (id)initWithDataCenter:(MMDataCenterInfo*)dataCenter;
- (void) setContentElementsWithType: (NSString*)type;
- (void)fillData;

@end
