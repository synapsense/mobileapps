//
//  FacilitiesViewCell.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FacilitiesViewCellElement.h"

@protocol FacilitiesViewCellDelegate <NSObject>
-(void)handleFacilityTap:(MMDataCenterInfo *) facility;
@end

@interface FacilitiesViewCell : UITableViewCell <FacilitiesViewCellElementDelegate>
{
    int numberOfFacilitiesInCell;
}

@property (nonatomic,assign)id<FacilitiesViewCellDelegate> delegate;

-(void) setFacility:(MMDataCenterInfo *)facility withCurrentOrientation:(UIInterfaceOrientation)currentOrientation withNumOfActiveAlerts: (int) numOfActiveAlerts;

@end