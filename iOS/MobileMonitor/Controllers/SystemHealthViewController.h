//
//  SystemHealthViewController.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "EGORefreshTableHeaderView.h"
#import "MMTableView.h"

@interface SystemHealthViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, EGORefreshTableHeaderDelegate, UIAlertViewDelegate>
{
    MMTableView *atableView;
    UIAlertView *errorAlert;
    EGORefreshTableHeaderView *_refreshHeaderView;
	BOOL _reloading;
    UIAlertView *detailedInfo;
}
@property(nonatomic,retain)NSArray *systemStatus;
@property (nonatomic,retain) NSDate *lastUpdateTime;
@property(nonatomic,copy)UIAlertView *temporaryContainer;

@end
