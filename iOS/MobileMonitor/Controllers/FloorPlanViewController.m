//
//  FloorPlanViewController.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "FloorPlanViewController.h"
#import "TransferringAlertView.h"
#import "AppDelegate.h"
#import "MMSDK.h"
#import "AppUtility.h"
#import "Constants.h"

#define RACK_HEIGHT_IN_INCHES 60

@interface FloorPlanViewController()

- (float)calculateZoomScaleWithoutOverlapping;
- (void)updateMinimumZoomScale;

@end

@implementation FloorPlanViewController

@synthesize floorPlanObjectDetailsViewController;
@synthesize mainview;
@synthesize mapview;
@synthesize labelview;
@synthesize currentDataCenter;
@synthesize liveImagingType;
@synthesize liveImagingTypes;

@synthesize floorplaneinfo;
@synthesize bitmap;
@synthesize buttons;
@synthesize zoomscale;
@synthesize scaleFactor;

@synthesize offsetBeforeUpdate;
@synthesize filterMask;
@synthesize floorPlanFilterViewController;
@synthesize floorPlanImage;
@synthesize lastUpdateSchemeDate;

enum FloorPlanElementStatus
{
    elementNotResponding = 0,
    elementOK,
    elementDisabled
};

- (void)updateScreenInfo
{
    [super updateScreenInfo];
    [self fillData];
}

- (id)initWithDataCenter:(MMDataCenterInfo*)dataCenter
{
    self = [super init];
    self.lastUpdateSchemeDate = [NSDate dateWithTimeIntervalSince1970:10];
    currentDataCenter = dataCenter;
    buttons = [[NSMutableArray alloc] init];
    MMSDK *sdk = [MMSDK sharedInstance];
    [sdk getLiveImagingTypesWithSuccess:^(NSArray *array) {
        self.liveImagingTypes = array;
    } failure:^(NSError *error) {
        
    }];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, VIEW_HEIGHT)] autorelease];
    self.view.backgroundColor = [UIColor whiteColor];
    self.tabBarController.navigationItem.title = NSLocalizedString(@"applicationTitle", @"");
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    labelview = [self loadHeaderWithTitle:[currentDataCenter name] titleWidth:self.view.frame.size.width / 3 * 2 viewLevel:ViewLevelSecond];
    
    UIImage *FilterImage = [UIImage imageNamed:@"Filter.png"];
    
    CustomButtonWithIncreasedHitArea *FilterButton = [[CustomButtonWithIncreasedHitArea alloc] 
                                                       initWithFrame:CGRectMake(labelview.frame.size.width - REAL_HIT_AREA_FOR_BUTTONS + 10,
                                                                                5,
                                                                                25,
                                                                                25)] 
                                                      ;
    [FilterButton addTarget:self action:@selector(filterButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [FilterButton setBackgroundImage:FilterImage forState:UIControlStateNormal];
//    UIImage *buttonBackgroundImage = [UIImage imageNamed:@"button_image_gray.png"];
//    [FilterButton setBackgroundImage:[[UIImage imageWithCGImage:buttonBackgroundImage.CGImage scale:2.0 orientation:UIImageOrientationUp] stretchableImageWithLeftCapWidth:buttonBackgroundImage.size.width/4 topCapHeight:0] forState:UIControlStateNormal];
//    [FilterButton setImage:[UIImage imageWithCGImage:FilterImage.CGImage scale:2.0 orientation:UIImageOrientationUp] forState:UIControlStateNormal];
    [FilterButton setAutoresizingMask: UIViewAutoresizingFlexibleLeftMargin];
    [FilterButton setCenter:CGPointMake(FilterButton.center.x, labelview.frame.size.height/2 - 1)];
    
    CustomButtonWithIncreasedHitArea* liveImagingButton = [[CustomButtonWithIncreasedHitArea alloc] 
                                                            initWithFrame:CGRectMake(labelview.frame.size.width - REAL_HIT_AREA_FOR_BUTTONS*2 + 20,
                                                                                     0,
                                                                                     27, 
                                                                                     27)]
                                                           ;
    [liveImagingButton addTarget:self action:@selector(liveImagingButtonPresed) forControlEvents:UIControlEventTouchUpInside];
    [liveImagingButton setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin];
    [liveImagingButton setBackgroundImage:[UIImage imageNamed:@"LiveImaging.png"] forState:UIControlStateNormal];
//    [liveImagingButton sizeToFit];

    [labelview addSubview:FilterButton];
    [labelview addSubview:liveImagingButton];
    [FilterButton release];
    [liveImagingButton release];
    [labelview sizeToFit];
    [labelview setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [self.view addSubview:labelview];
    
    //zoom+
    UITapGestureRecognizer* doubleTapOneFinger = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTapOneFinger:)];
    doubleTapOneFinger.numberOfTapsRequired = 2;
    doubleTapOneFinger.numberOfTouchesRequired = 1;
    doubleTapOneFinger.delegate = self;
    [self.view addGestureRecognizer:doubleTapOneFinger];
    [doubleTapOneFinger release];
    
    //zoom-
    UITapGestureRecognizer* doubleTapTwoFingers = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTapTwoFingers:)];
    doubleTapTwoFingers.numberOfTapsRequired = 2;
    doubleTapTwoFingers.numberOfTouchesRequired = 2;
    doubleTapTwoFingers.delegate = self;
    [self.view addGestureRecognizer:doubleTapTwoFingers];
    [doubleTapTwoFingers release];
}

- (void)handleDoubleTapOneFinger:(UIGestureRecognizer *)sender
{
    float newZoom = zoomscale * 1.25;
    [self.mainview setZoomScale:newZoom animated:YES];
}

- (void)handleDoubleTapTwoFingers:(UIGestureRecognizer *)sender
{
    float newZoom = zoomscale * 0.8; // 1/1.25 = 0.8
    [self.mainview setZoomScale:newZoom animated:YES];
}


- (void)dealloc
{
    self.bitmap = nil;
    [_schemeImage release];
    self.floorPlanFilterViewController = nil;
    self.mapview = nil;
    self.buttons = nil;
    self.floorplaneinfo = nil;
    self.liveImagingType = nil;
    self.liveImagingTypes = nil;
    self.floorPlanImage = nil;
    self.lastUpdateSchemeDate = nil;
    if (filterViewController)
    {
        [filterViewController release];
    }
    [super dealloc];
}

- (void)CRACButtonWasPressed:(UIButton*) sender
{
    FloorPlanElement *pressedCRAC = [floorplaneinfo.CRAC objectAtIndex:sender.tag];
    floorPlanObjectDetailsViewController = [[FloorPlanObjectDetailsViewController alloc] initWithObjectID:pressedCRAC.element_id name:pressedCRAC.name andType:@"CRAC"];
    [self.navigationController pushViewController:floorPlanObjectDetailsViewController animated:YES];
    [floorPlanObjectDetailsViewController release];
}

- (void)RACKButtonWasPressed:(UIButton*) sender
{
    FloorPlanElement *pressedRACK = [floorplaneinfo.RACK objectAtIndex:sender.tag];
    floorPlanObjectDetailsViewController = [[FloorPlanObjectDetailsViewController alloc] initWithObjectID:pressedRACK.element_id name:pressedRACK.name andType:@"RACK"];
    [self.navigationController pushViewController:floorPlanObjectDetailsViewController animated:YES];
    [floorPlanObjectDetailsViewController release];
}

- (void)PressureButtonWasPressed:(UIButton*) sender
{
    FloorPlanElement *pressedPressure = [floorplaneinfo.pressure objectAtIndex:sender.tag];
    floorPlanObjectDetailsViewController = [[FloorPlanObjectDetailsViewController alloc] initWithObjectID:pressedPressure.element_id name:pressedPressure.name andType:@"PRESSURE"];
    [self.navigationController pushViewController:floorPlanObjectDetailsViewController animated:YES];
    [floorPlanObjectDetailsViewController release];
}

- (void)ion_deltaButtonWasPressed:(UIButton*) sender
{
    FloorPlanElement *pressedEnergy = [floorplaneinfo.ion_delta objectAtIndex:sender.tag];
    floorPlanObjectDetailsViewController = [[FloorPlanObjectDetailsViewController alloc] initWithObjectID:pressedEnergy.element_id name: pressedEnergy.name andType:@"ION_DELTA"];
    [self.navigationController pushViewController:floorPlanObjectDetailsViewController animated:YES];
    [floorPlanObjectDetailsViewController release];
}

- (void)ion_wyeButtonWasPressed:(UIButton*) sender
{
    FloorPlanElement *pressedEnergy = [floorplaneinfo.ion_wye objectAtIndex:sender.tag];
    floorPlanObjectDetailsViewController = [[FloorPlanObjectDetailsViewController alloc] initWithObjectID:pressedEnergy.element_id name:pressedEnergy.name andType:@"ION_WYE"];
    [self.navigationController pushViewController:floorPlanObjectDetailsViewController animated:YES];
    [floorPlanObjectDetailsViewController release];
}

- (void)ion_wirelessButtonWasPressed:(UIButton*) sender
{
    FloorPlanElement *pressedEnergy = [floorplaneinfo.ion_wireless objectAtIndex:sender.tag];
    floorPlanObjectDetailsViewController = [[FloorPlanObjectDetailsViewController alloc] initWithObjectID:pressedEnergy.element_id name:pressedEnergy.name andType:@"ION_WIRELESS"];
    [self.navigationController pushViewController:floorPlanObjectDetailsViewController animated:YES];
    [floorPlanObjectDetailsViewController release];
}

- (void)DoorButtonWasPressed:(UIButton*) sender
{
    FloorPlanElement *pressedDoor = [floorplaneinfo.doors objectAtIndex:sender.tag];
    floorPlanObjectDetailsViewController = [[FloorPlanObjectDetailsViewController alloc] initWithObjectID:pressedDoor.element_id name:pressedDoor.name andType:@"DOOR"];
    [self.navigationController pushViewController:floorPlanObjectDetailsViewController animated:YES];
    [floorPlanObjectDetailsViewController release];
}

- (void)genericTempButtonWasPressed:(UIButton*) sender
{
    FloorPlanElement *pressed = [floorplaneinfo.generic_temp objectAtIndex:sender.tag];
    floorPlanObjectDetailsViewController = [[FloorPlanObjectDetailsViewController alloc] initWithObjectID:pressed.element_id name:pressed.name andType:@"GENERICTEMPERATURE"];
    [self.navigationController pushViewController:floorPlanObjectDetailsViewController animated:YES];
    [floorPlanObjectDetailsViewController release];
}

- (void)verticalTempButtonWasPressed:(UIButton*) sender
{
    FloorPlanElement *pressed = [floorplaneinfo.vert_temp objectAtIndex:sender.tag];
    floorPlanObjectDetailsViewController = [[FloorPlanObjectDetailsViewController alloc] initWithObjectID:pressed.element_id name:pressed.name andType:@"VERTICALTEMPERATURE"];
    [self.navigationController pushViewController:floorPlanObjectDetailsViewController animated:YES];
    [floorPlanObjectDetailsViewController release];
}

- (void)equipmentStatusButtonWasPressed:(UIButton*) sender
{
    FloorPlanElement *pressed = [floorplaneinfo.equipment_status objectAtIndex:sender.tag];
    floorPlanObjectDetailsViewController = [[FloorPlanObjectDetailsViewController alloc] initWithObjectID:pressed.element_id name:pressed.name andType:@"EQUIPMENTSTATUS"];
    [self.navigationController pushViewController:floorPlanObjectDetailsViewController animated:YES];
    [floorPlanObjectDetailsViewController release];
}

- (void)leakButtonWasPressed:(UIButton*) sender
{
    FloorPlanElement *pressed = [floorplaneinfo.leak objectAtIndex:sender.tag];
    floorPlanObjectDetailsViewController = [[FloorPlanObjectDetailsViewController alloc] initWithObjectID:pressed.element_id name:pressed.name andType:@"LEAK"];
    [self.navigationController pushViewController:floorPlanObjectDetailsViewController animated:YES];
    [floorPlanObjectDetailsViewController release];
}

- (void)setContentElementsWithType:(NSString *)type
{
    if([type compare:@"RACK"] == NSOrderedSame)
    {
        for(int i = 0; i < [floorplaneinfo.RACK count]; i++)
        {
            FloorPlanElement * currentElement = [floorplaneinfo.RACK objectAtIndex:i];
            
            CustomButtonWithIncreasedHitArea *floorPlanObjectDetailTempButton = [[CustomButtonWithIncreasedHitArea alloc] initWithFrame:CGRectMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor, 0, 0)];
            
            floorPlanObjectDetailTempButton.zoom = zoomscale;
            [floorPlanObjectDetailTempButton addTarget:self action:@selector(RACKButtonWasPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            [floorPlanObjectDetailTempButton setTag:i];
            NSString *iconName = @"RACK-";
            if(currentElement.status == elementDisabled)
            {
                iconName = [iconName stringByAppendingString:@"gray"];
            }
            else if(currentElement.num_alerts != 0)
            {
                iconName = [iconName stringByAppendingString:@"red"];
            }
            else if(currentElement.status == elementNotResponding)
            {
                iconName = [iconName stringByAppendingString:@"yellow"];
            }
            else if(currentElement.status == elementOK)
            {
                iconName = [iconName stringByAppendingString:@"green"];
            }
            else
            {
                NSLog(@"!!!!! BAD RACK !!!!!!");
            }
            
            if(currentElement.door != 0)
            {
                iconName = [iconName stringByAppendingString:@"_open"];
            }
            iconName = [iconName stringByAppendingString:@".png"];
            UIImage *rackImage = [UIImage imageNamed:iconName];
            [floorPlanObjectDetailTempButton setImage:rackImage forState:UIControlStateNormal];
            [floorPlanObjectDetailTempButton setCenter:CGPointMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor)];
            [floorPlanObjectDetailTempButton setBounds:CGRectMake(0.0f, 0.0f, rackImage.size.width / zoomscale, rackImage.size.height / zoomscale)];
            [floorPlanObjectDetailTempButton setContentVerticalAlignment:UIControlContentVerticalAlignmentFill];
            [floorPlanObjectDetailTempButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentFill];
            [mapview addSubview:floorPlanObjectDetailTempButton];
            [buttons addObject:floorPlanObjectDetailTempButton];
            [floorPlanObjectDetailTempButton release];
        }
    }    else if([type compare:@"CRAC"] == NSOrderedSame)
    {
        for(int i = 0; i < [floorplaneinfo.CRAC count]; i++)
        {
            FloorPlanElement * currentElement = [floorplaneinfo.CRAC objectAtIndex:i];
            
            CustomButtonWithIncreasedHitArea *floorPlanObjectDetailTempButton = [[CustomButtonWithIncreasedHitArea alloc] initWithFrame:CGRectMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor, 0, 0)];
            
            floorPlanObjectDetailTempButton.zoom = zoomscale;
            [floorPlanObjectDetailTempButton addTarget:self action:@selector(CRACButtonWasPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            [floorPlanObjectDetailTempButton setTag:i];
            //[floorPlanObjectDetailTempButton setFrame:CGRectMake(currentElement.x, currentElement.y, 0, 0)];
            NSString *iconName = @"CRAC-";
            if(currentElement.status == elementDisabled)
            {
                iconName = [iconName stringByAppendingString:@"gray"];
            }
            else if(currentElement.num_alerts != 0)
            {
                iconName = [iconName stringByAppendingString:@"red"];
            }
            else if(currentElement.status == elementNotResponding)
            {
                iconName = [iconName stringByAppendingString:@"yellow"];
            }
            else if(currentElement.status == elementOK)
            {
                iconName = [iconName stringByAppendingString:@"green"];
            }
            else
            {
                NSLog(@"!!!!! BAD CRAC !!!!!!");
            }
            iconName = [iconName stringByAppendingString:@".png"];
            UIImage *cracImage = [UIImage imageNamed:iconName];
            [floorPlanObjectDetailTempButton setImage:cracImage forState:UIControlStateNormal];
            [floorPlanObjectDetailTempButton sizeToFit];
            [floorPlanObjectDetailTempButton setCenter:CGPointMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor)];
            [floorPlanObjectDetailTempButton setBounds:CGRectMake(0.0f, 0.0f, cracImage.size.width / zoomscale, cracImage.size.height / zoomscale)];
            [floorPlanObjectDetailTempButton setContentVerticalAlignment:UIControlContentVerticalAlignmentFill];
            [floorPlanObjectDetailTempButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentFill];
            [mapview addSubview:floorPlanObjectDetailTempButton];
            [buttons addObject:floorPlanObjectDetailTempButton];
            [floorPlanObjectDetailTempButton release];
        }
    }
    else if([type compare:@"PRESSURE"] == NSOrderedSame)
    {
        for(int i = 0; i < [floorplaneinfo.pressure count]; i++)
        {
            FloorPlanElement * currentElement = [floorplaneinfo.pressure objectAtIndex:i];
            
            CustomButtonWithIncreasedHitArea *floorPlanObjectDetailTempButton = [[CustomButtonWithIncreasedHitArea alloc] initWithFrame:CGRectMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor, 0, 0)];
            
            floorPlanObjectDetailTempButton.zoom = zoomscale;
            [floorPlanObjectDetailTempButton addTarget:self action:@selector(PressureButtonWasPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            [floorPlanObjectDetailTempButton setTag:i];
            NSString *iconName = @"Pressure-";
            if(currentElement.status == elementDisabled)
            {
                iconName = [iconName stringByAppendingString:@"gray"];
            }
            else if(currentElement.num_alerts != 0)
            {
                iconName = [iconName stringByAppendingString:@"red"];
            }
            else if(currentElement.status == elementNotResponding)
            {
                iconName = [iconName stringByAppendingString:@"yellow"];
            }
            else if(currentElement.status == elementOK)
            {
                iconName = [iconName stringByAppendingString:@"green"];
            }
            else
            {
                NSLog(@"!!!!! BAD PRESSURE !!!!!!");
            }
            iconName = [iconName stringByAppendingString:@".png"];
            UIImage *pressureImage = [UIImage imageNamed:iconName];
            [floorPlanObjectDetailTempButton setImage:pressureImage forState:UIControlStateNormal];
            [floorPlanObjectDetailTempButton setCenter:CGPointMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor)];
            [floorPlanObjectDetailTempButton setBounds:CGRectMake(0.0f, 0.0f, pressureImage.size.width / zoomscale, pressureImage.size.height / zoomscale)];
            [floorPlanObjectDetailTempButton setContentVerticalAlignment:UIControlContentVerticalAlignmentFill];
            [floorPlanObjectDetailTempButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentFill];
            [mapview addSubview:floorPlanObjectDetailTempButton];
            [buttons addObject:floorPlanObjectDetailTempButton];
            [floorPlanObjectDetailTempButton release];
        }
    }
    else if([type compare:@"ION_WYE"] == NSOrderedSame)
    {
        for(int i = 0; i < [floorplaneinfo.ion_wye count]; i++)
        {
            FloorPlanElement * currentElement = [floorplaneinfo.ion_wye objectAtIndex:i];
            
            CustomButtonWithIncreasedHitArea *floorPlanObjectDetailTempButton = [[CustomButtonWithIncreasedHitArea alloc] initWithFrame:CGRectMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor, 0, 0)];
            
            floorPlanObjectDetailTempButton.zoom = zoomscale;
            [floorPlanObjectDetailTempButton addTarget:self action:@selector(ion_wyeButtonWasPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            [floorPlanObjectDetailTempButton setTag:i];
            NSString *iconName = @"Energy-";
            if(currentElement.status == elementDisabled)
            {
                iconName = [iconName stringByAppendingString:@"gray"];
            }
            else if(currentElement.num_alerts != 0)
            {
                iconName = [iconName stringByAppendingString:@"red"];
            }
            else if(currentElement.status == elementNotResponding)
            {
                iconName = [iconName stringByAppendingString:@"yellow"];
            }
            else if(currentElement.status == elementOK)
            {
                iconName = [iconName stringByAppendingString:@"green"];
            }
            else
            {
                NSLog(@"!!!!! BAD ION_WYE !!!!!!");
            }
            iconName = [iconName stringByAppendingString:@".png"];
            UIImage *ionWyeImage = [UIImage imageNamed:iconName];
            [floorPlanObjectDetailTempButton setImage:ionWyeImage forState:UIControlStateNormal];
            [floorPlanObjectDetailTempButton sizeToFit];
            [floorPlanObjectDetailTempButton setCenter:CGPointMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor)];
            [floorPlanObjectDetailTempButton setBounds:CGRectMake(0.0f, 0.0f, ionWyeImage.size.width / zoomscale, ionWyeImage.size.height / zoomscale)];
            [floorPlanObjectDetailTempButton setContentVerticalAlignment:UIControlContentVerticalAlignmentFill];
            [floorPlanObjectDetailTempButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentFill];
            [mapview addSubview:floorPlanObjectDetailTempButton];
            [buttons addObject:floorPlanObjectDetailTempButton];
            [floorPlanObjectDetailTempButton release];
        }
    }
    else if([type compare:@"ION_DELTA"] == NSOrderedSame)
    {
        for(int i = 0; i < [floorplaneinfo.ion_delta count]; i++)
        {
            FloorPlanElement * currentElement = [floorplaneinfo.ion_delta objectAtIndex:i];
            
            CustomButtonWithIncreasedHitArea *floorPlanObjectDetailTempButton = [[CustomButtonWithIncreasedHitArea alloc] initWithFrame:CGRectMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor, 0, 0)];
            
            floorPlanObjectDetailTempButton.zoom = zoomscale;
            [floorPlanObjectDetailTempButton addTarget:self action:@selector(ion_deltaButtonWasPressed:) forControlEvents:UIControlEventTouchUpInside];
            [floorPlanObjectDetailTempButton setTag:i];
            NSString *iconName = @"Energy-";
            if(currentElement.status == elementDisabled)
            {
                iconName = [iconName stringByAppendingString:@"gray"];
            }
            else if(currentElement.num_alerts != 0)
            {
                iconName = [iconName stringByAppendingString:@"red"];
            }
            else if(currentElement.status == elementNotResponding)
            {
                iconName = [iconName stringByAppendingString:@"yellow"];
            }
            else if(currentElement.status == elementOK)
            {
                iconName = [iconName stringByAppendingString:@"green"];
            }
            else
            {
                NSLog(@"!!!!! BAD ION_DELTA !!!!!!");
            }
            iconName = [iconName stringByAppendingString:@".png"];
            UIImage *ionDeltaImage = [UIImage imageNamed:iconName];
            [floorPlanObjectDetailTempButton setImage:ionDeltaImage forState:UIControlStateNormal];
            [floorPlanObjectDetailTempButton sizeToFit];
            [floorPlanObjectDetailTempButton setCenter:CGPointMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor)];
            [floorPlanObjectDetailTempButton setBounds:CGRectMake(0.0f, 0.0f, ionDeltaImage.size.width / zoomscale, ionDeltaImage.size.height / zoomscale)];
            [floorPlanObjectDetailTempButton setContentVerticalAlignment:UIControlContentVerticalAlignmentFill];
            [floorPlanObjectDetailTempButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentFill];
            [mapview addSubview:floorPlanObjectDetailTempButton];
            [buttons addObject:floorPlanObjectDetailTempButton];
            [floorPlanObjectDetailTempButton release];
        }
    }
    else if([type compare:@"ION_WIRELESS"] == NSOrderedSame)
    {
        for(int i = 0; i < [floorplaneinfo.ion_wireless count]; i++)
        {
            FloorPlanElement * currentElement = [floorplaneinfo.ion_wireless objectAtIndex:i];
            
            CustomButtonWithIncreasedHitArea *floorPlanObjectDetailTempButton = [[CustomButtonWithIncreasedHitArea alloc] initWithFrame:CGRectMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor, 0, 0)];
            
            floorPlanObjectDetailTempButton.zoom = zoomscale;
            [floorPlanObjectDetailTempButton addTarget:self action:@selector(ion_wirelessButtonWasPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            [floorPlanObjectDetailTempButton setTag:i];
            NSString *iconName = @"Energy-";
            if(currentElement.status == elementDisabled)
            {
                iconName = [iconName stringByAppendingString:@"gray"];
            }
            else if(currentElement.num_alerts != 0)
            {
                iconName = [iconName stringByAppendingString:@"red"];
            }
            else if(currentElement.status == elementNotResponding)
            {
                iconName = [iconName stringByAppendingString:@"yellow"];
            }
            else if(currentElement.status == elementOK)
            {
                iconName = [iconName stringByAppendingString:@"green"];
            }
            else
            {
                NSLog(@"!!!!! BAD ION_WIRELESS !!!!!!");
            }
            iconName = [iconName stringByAppendingString:@".png"];
            UIImage *ionWirelessImage = [UIImage imageNamed:iconName];
            [floorPlanObjectDetailTempButton setImage:ionWirelessImage forState:UIControlStateNormal];
            [floorPlanObjectDetailTempButton sizeToFit];
            [floorPlanObjectDetailTempButton setCenter:CGPointMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor)];
            [floorPlanObjectDetailTempButton setBounds:CGRectMake(0.0f, 0.0f, ionWirelessImage.size.width / zoomscale, ionWirelessImage.size.height / zoomscale)];
            [floorPlanObjectDetailTempButton setContentVerticalAlignment:UIControlContentVerticalAlignmentFill];
            [floorPlanObjectDetailTempButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentFill];
            [mapview addSubview:floorPlanObjectDetailTempButton];
            [buttons addObject:floorPlanObjectDetailTempButton];
            [floorPlanObjectDetailTempButton release];
        }
    }
    else if([type compare:@"GENERIC_TEMP"] == NSOrderedSame)
    {
        for(int i = 0; i < [floorplaneinfo.generic_temp count]; i++)
        {
            FloorPlanElement * currentElement = [floorplaneinfo.generic_temp objectAtIndex:i];
            
            CustomButtonWithIncreasedHitArea *floorPlanObjectDetailTempButton = [[CustomButtonWithIncreasedHitArea alloc] initWithFrame:CGRectMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor, 0, 0)];
            
            floorPlanObjectDetailTempButton.zoom = zoomscale;
            [floorPlanObjectDetailTempButton addTarget:self action:@selector(genericTempButtonWasPressed:) forControlEvents:UIControlEventTouchUpInside];
            [floorPlanObjectDetailTempButton setTag:i];
            NSString *iconName = @"GenericTemp-";
            if(currentElement.status == elementDisabled)
            {
                iconName = [iconName stringByAppendingString:@"gray"];
            }
            else if(currentElement.num_alerts != 0)
            {
                iconName = [iconName stringByAppendingString:@"red"];
            }
            else if(currentElement.status == elementNotResponding)
            {
                iconName = [iconName stringByAppendingString:@"yellow"];
            }
            else if(currentElement.status == elementOK)
            {
                iconName = [iconName stringByAppendingString:@"green"];
            }
            else
            {
                NSLog(@"!!!!! BAD GENERIC_TEMP !!!!!!");
            }
            iconName = [iconName stringByAppendingString:@".png"];
            UIImage *genericTempImage = [UIImage imageNamed:iconName];
            [floorPlanObjectDetailTempButton setImage:genericTempImage forState:UIControlStateNormal];
            [floorPlanObjectDetailTempButton sizeToFit];
            [floorPlanObjectDetailTempButton setCenter:CGPointMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor)];
            [floorPlanObjectDetailTempButton setBounds:CGRectMake(0.0f, 0.0f, genericTempImage.size.width / zoomscale, genericTempImage.size.height / zoomscale)];
            [floorPlanObjectDetailTempButton setContentVerticalAlignment:UIControlContentVerticalAlignmentFill];
            [floorPlanObjectDetailTempButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentFill];
            [mapview addSubview:floorPlanObjectDetailTempButton];
            [buttons addObject:floorPlanObjectDetailTempButton];
            [floorPlanObjectDetailTempButton release];
        }
    }
    else if([type compare:@"VERTICAL_TEMP"] == NSOrderedSame)
    {
        for(int i = 0; i < [floorplaneinfo.vert_temp count]; i++)
        {
            FloorPlanElement * currentElement = [floorplaneinfo.vert_temp objectAtIndex:i];
            
            CustomButtonWithIncreasedHitArea *floorPlanObjectDetailTempButton = [[CustomButtonWithIncreasedHitArea alloc] initWithFrame:CGRectMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor, 0, 0)];
            
            floorPlanObjectDetailTempButton.zoom = zoomscale;
            [floorPlanObjectDetailTempButton addTarget:self action:@selector(verticalTempButtonWasPressed:) forControlEvents:UIControlEventTouchUpInside];
            [floorPlanObjectDetailTempButton setTag:i];
            NSString *iconName = @"VerticalTemp-";
            if(currentElement.status == elementDisabled)
            {
                iconName = [iconName stringByAppendingString:@"gray"];
            }
            else if(currentElement.num_alerts != 0)
            {
                iconName = [iconName stringByAppendingString:@"red"];
            }
            else if(currentElement.status == elementNotResponding)
            {
                iconName = [iconName stringByAppendingString:@"yellow"];
            }
            else if(currentElement.status == elementOK)
            {
                iconName = [iconName stringByAppendingString:@"green"];
            }
            else
            {
                NSLog(@"!!!!! BAD VERTICAL_TEMP !!!!!!");
            }
            iconName = [iconName stringByAppendingString:@".png"];
            UIImage *verticalTempImage = [UIImage imageNamed:iconName];
            [floorPlanObjectDetailTempButton setImage:verticalTempImage forState:UIControlStateNormal];
            [floorPlanObjectDetailTempButton sizeToFit];
            [floorPlanObjectDetailTempButton setCenter:CGPointMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor)];
            [floorPlanObjectDetailTempButton setBounds:CGRectMake(0.0f, 0.0f, verticalTempImage.size.width / zoomscale, verticalTempImage.size.height / zoomscale)];
            [floorPlanObjectDetailTempButton setContentVerticalAlignment:UIControlContentVerticalAlignmentFill];
            [floorPlanObjectDetailTempButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentFill];
            [mapview addSubview:floorPlanObjectDetailTempButton];
            [buttons addObject:floorPlanObjectDetailTempButton];
            [floorPlanObjectDetailTempButton release];
        }
    }
    else if([type compare:@"EQUIPMENT_STATUS"] == NSOrderedSame)
    {
        for(int i = 0; i < [floorplaneinfo.equipment_status count]; i++)
        {
            FloorPlanElement * currentElement = [floorplaneinfo.equipment_status objectAtIndex:i];
            
            CustomButtonWithIncreasedHitArea *floorPlanObjectDetailTempButton = [[CustomButtonWithIncreasedHitArea alloc] initWithFrame:CGRectMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor, 0, 0)];
            
            floorPlanObjectDetailTempButton.zoom = zoomscale;
            [floorPlanObjectDetailTempButton addTarget:self action:@selector(equipmentStatusButtonWasPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            [floorPlanObjectDetailTempButton setTag:i];
            
            NSString *iconName = @"EquipmentStatus-";
            if(currentElement.status == elementDisabled)
            {
                iconName = [iconName stringByAppendingString:@"gray"];
            }
            else if(currentElement.num_alerts != 0)
            {
                iconName = [iconName stringByAppendingString:@"red"];
            }
            else if(currentElement.status == elementNotResponding)
            {
                iconName = [iconName stringByAppendingString:@"yellow"];
            }
            else if(currentElement.status == elementOK)
            {
                iconName = [iconName stringByAppendingString:@"green"];
            }
            else
            {
                NSLog(@"!!!!! BAD EQUIPMENT_STATUS !!!!!!");
            }
            iconName = [iconName stringByAppendingString:@".png"];
            UIImage *equipmetStatusImage = [UIImage imageNamed:iconName];
            [floorPlanObjectDetailTempButton setImage:equipmetStatusImage forState:UIControlStateNormal];
            [floorPlanObjectDetailTempButton sizeToFit];
            [floorPlanObjectDetailTempButton setCenter:CGPointMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor)];
            [floorPlanObjectDetailTempButton setBounds:CGRectMake(0.0f, 0.0f, equipmetStatusImage.size.width / zoomscale, equipmetStatusImage.size.height / zoomscale)];
            [floorPlanObjectDetailTempButton setContentVerticalAlignment:UIControlContentVerticalAlignmentFill];
            [floorPlanObjectDetailTempButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentFill];
            [mapview addSubview:floorPlanObjectDetailTempButton];
            [buttons addObject:floorPlanObjectDetailTempButton];
            [floorPlanObjectDetailTempButton release];
        }
    }
    else if([type compare:@"LEAK"] == NSOrderedSame)
    {
        for(int i = 0; i < [floorplaneinfo.leak count]; i++)
        {
            FloorPlanElement * currentElement = [floorplaneinfo.leak objectAtIndex:i];
            
            CustomButtonWithIncreasedHitArea *floorPlanObjectDetailTempButton = [[CustomButtonWithIncreasedHitArea alloc] initWithFrame:CGRectMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor, 0, 0)];
            
            floorPlanObjectDetailTempButton.zoom = zoomscale;
            [floorPlanObjectDetailTempButton addTarget:self action:@selector(leakButtonWasPressed:) forControlEvents:UIControlEventTouchUpInside];
            [floorPlanObjectDetailTempButton setTag:i];
            NSString *iconName = @"Leak-";
            if(currentElement.status == elementDisabled)
            {
                iconName = [iconName stringByAppendingString:@"gray"];
            }
            else if(currentElement.num_alerts != 0)
            {
                iconName = [iconName stringByAppendingString:@"red"];
            }
            else if(currentElement.status == elementNotResponding)
            {
                iconName = [iconName stringByAppendingString:@"yellow"];
            }
            else if(currentElement.status == elementOK)
            {
                iconName = [iconName stringByAppendingString:@"green"];
            }
            else
            {
                NSLog(@"!!!!! BAD LEAK !!!!!!");
            }
            iconName = [iconName stringByAppendingString:@".png"];
            UIImage *leakImage = [UIImage imageNamed:iconName];
            [floorPlanObjectDetailTempButton setImage:leakImage forState:UIControlStateNormal];
            [floorPlanObjectDetailTempButton sizeToFit];
            [floorPlanObjectDetailTempButton setCenter:CGPointMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor)];
            [floorPlanObjectDetailTempButton setBounds:CGRectMake(0.0f, 0.0f, leakImage.size.width / zoomscale, leakImage.size.height / zoomscale)];
            [floorPlanObjectDetailTempButton setContentVerticalAlignment:UIControlContentVerticalAlignmentFill];
            [floorPlanObjectDetailTempButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentFill];
            [mapview addSubview:floorPlanObjectDetailTempButton];
            [buttons addObject:floorPlanObjectDetailTempButton];
            [floorPlanObjectDetailTempButton release];
        }
    }
    else if([type compare:@"DOOR"] == NSOrderedSame)
    {
        for(int i = 0; i < [floorplaneinfo.doors count]; i++)
        {
            FloorPlanElement * currentElement = [floorplaneinfo.doors objectAtIndex:i];
            
            CustomButtonWithIncreasedHitArea *floorPlanObjectDetailTempButton = [[CustomButtonWithIncreasedHitArea alloc] initWithFrame:CGRectMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor, 0, 0)];
            
            floorPlanObjectDetailTempButton.zoom = zoomscale;
            [floorPlanObjectDetailTempButton addTarget:self action:@selector(DoorButtonWasPressed:) forControlEvents:UIControlEventTouchUpInside];
            [floorPlanObjectDetailTempButton setTag:i];
            NSString *iconName = @"Door-";
            if(currentElement.status == elementDisabled)
            {
                iconName = [iconName stringByAppendingString:@"gray"];
            }
            else if(currentElement.num_alerts != 0)
            {
                iconName = [iconName stringByAppendingString:@"red"];
            }
            else if(currentElement.status == elementNotResponding)
            {
                iconName = [iconName stringByAppendingString:@"yellow"];
            }
            else if(currentElement.status == elementOK)
            {
                iconName = [iconName stringByAppendingString:@"green"];
            }
            else
            {
                NSLog(@"!!!!! BAD DOOR !!!!!!");
            }
            
            if(currentElement.door != 0)
            {
                iconName = [iconName stringByAppendingString:@"_open"];
            }
            else
            {
                iconName = [iconName stringByAppendingString:@"_close"];
            }
            iconName = [iconName stringByAppendingString:@".png"];
            UIImage *doorImage = [UIImage imageNamed:iconName];
            [floorPlanObjectDetailTempButton setImage:doorImage forState:UIControlStateNormal];
            [floorPlanObjectDetailTempButton sizeToFit];
            [floorPlanObjectDetailTempButton setCenter:CGPointMake(currentElement.x / scaleFactor, currentElement.y / scaleFactor)];
            [floorPlanObjectDetailTempButton setBounds:CGRectMake(0.0f, 0.0f, doorImage.size.width / zoomscale, doorImage.size.height / zoomscale)];
            [floorPlanObjectDetailTempButton setContentVerticalAlignment:UIControlContentVerticalAlignmentFill];
            [floorPlanObjectDetailTempButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentFill];
            [mapview addSubview:floorPlanObjectDetailTempButton];
            [buttons addObject:floorPlanObjectDetailTempButton];
            [floorPlanObjectDetailTempButton release];
        }
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.mapview;
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
    for(int i = 0; i < [buttons count]; i++)
    {
        UIButton *tmp = [buttons objectAtIndex:i];
        CGRect tmpframe = CGRectMake(tmp.frame.origin.x, tmp.frame.origin.y, tmp.frame.size.width * zoomscale, tmp.frame.size.height * zoomscale);
        CGPoint center = tmp.center;

        tmpframe.size.height /= scale;
        tmpframe.size.width /= scale;
        
        [tmp setFrame:tmpframe];        
        [tmp setContentVerticalAlignment:UIControlContentVerticalAlignmentFill];
        [tmp setContentHorizontalAlignment:UIControlContentHorizontalAlignmentFill];
        [tmp setCenter:center];
    }
    zoomscale = scale;
}

-(void)filterButtonPressed
{
    self.floorPlanFilterViewController = [[FloorPlanFilterViewController alloc] init];
    [self.navigationController pushViewController:self.floorPlanFilterViewController animated:YES];
    [self.floorPlanFilterViewController release];
}

-(void)liveImagingButtonPresed
{
    if (self.liveImagingTypes)
    {
        if (filterViewController == nil)
        {
            filterViewController = [[FilterViewController alloc] initWithFilterString:NSLocalizedString(@"livelImaging", @"") FilterArray:liveImagingTypes];
            [filterViewController setLiveImagingTypeDelegate:self];
        }
        [self.navigationController pushViewController:filterViewController animated:YES];
    }
    else
    {
        //to-do
    }
}

- (void)fillData
{
    MMSDK *sdk = [MMSDK sharedInstance];
    if (self.liveImagingType)
    {
        [sdk setLiveImagingSwitch:LiveImagingTypeOn];
    }
    else
    {
        [sdk setLiveImagingSwitch:LiveImagingTypeOff];
    }
    __block int taskIdGetFloorplanData = 0;
    __block int taskIdGetFloorplan = 0;
    __block int taskIdGetFloorplanScheme = 0;
    __block int taskIdGetFloorplanSize = 0;
    taskIdGetFloorplanData = [sdk getFloorPlanData:self.currentDataCenter.guid filterMask:[AppDelegate sharedInstance].floorPlanFilter success:^(MMFloorPlanInfo *info)
     {
         [self taskFinished:taskIdGetFloorplanData];
         taskIdGetFloorplan = [sdk getFloorPlan:self.currentDataCenter.guid DataClassID:self.liveImagingType.dataClass Layer:self.liveImagingType.layer success:^(UIImage *floorplanLiveImage) 
         {
             [self taskFinished:taskIdGetFloorplan];
             taskIdGetFloorplanScheme = [sdk getFloorplanSchemeImage:self.currentDataCenter.guid lastUpdateDate:lastUpdateSchemeDate success:^(UIImage *schemeImage) 
             {
                 [self taskFinished:taskIdGetFloorplanScheme];
                 taskIdGetFloorplanSize = [sdk getFloorPlanSize:self.currentDataCenter.guid success:^(CGSize dcSize) 
                 {
                     [self taskFinished:taskIdGetFloorplanSize];
                     [self.view setUserInteractionEnabled:NO];
                     float floorplanWidth = dcSize.width;
                     self.viewDidAppear = YES;
                     self.floorplaneinfo = info;
                     if (schemeImage)
                     {
                         [_schemeImage release];
                         _schemeImage = [[UIImage alloc] initWithCGImage:[schemeImage CGImage]];
                         self.floorPlanImage = schemeImage;
                         self.lastUpdateSchemeDate = [NSDate date];
                     }
                     else if (floorplanLiveImage)
                     {
                         self.floorPlanImage = floorplanLiveImage;
                     }
                     else
                     {
                         self.floorPlanImage = _schemeImage;
                     }
                     if (floorplanSize.width == 0 || floorplanSize.height == 0) 
                     {
                         floorplanSize.width = self.floorPlanImage.size.width;
                         floorplanSize.height = self.floorPlanImage.size.height;
                     }
                     if(self.mainview)
                     {
                         offsetBeforeUpdate = [self.mainview contentOffset];
                         [self.mainview removeFromSuperview];
                     }
                     if (!self.scaleFactor)
                     {
                         self.scaleFactor = floorplanWidth / floorplanSize.width;
                     }
                     mainview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, [labelview frame].size.height, self.view.frame.size.width, self.view.frame.size.height - [labelview frame].size.height)];
                     mainview.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
                     mainview.bounces = NO;
                     [mapview release];
                     mapview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, floorplanSize.width, floorplanSize.height)];
                     [mainview addSubview:mapview];
                     [mainview setContentSize:CGSizeMake(mapview.frame.size.width, mapview.frame.size.height)];
                     [mainview setScrollEnabled:TRUE];
                     [mainview setMultipleTouchEnabled:TRUE];
                     [mainview setContentOffset:CGPointMake(0, 0)];
                     
                     mainview.delegate = self;
                     [self.mainview setMaximumZoomScale:2.0];
                     [self updateMinimumZoomScale];
                     if(!self.zoomscale)
                     {
                         //first time we show floorplan
                         self.zoomscale = [self calculateZoomScaleWithoutOverlapping];
                         [self.mainview setZoomScale:self.zoomscale animated:NO];
//                         CGRect showingRect = CGRectMake(0, 0, mainview.frame.size.width,  mainview.frame.size.height);
//                         showingRect.origin.x -= self.mainview.frame.size.width / 2;
//                         showingRect.origin.y -= self.mainview.frame.size.height / 2;
//                         [self.mainview scrollRectToVisible:showingRect animated:NO];
                     }
                     else
                     {
                         [self.mainview setZoomScale:self.zoomscale animated:NO];
                         [self.mainview setContentOffset:offsetBeforeUpdate animated:NO];
                     }
                     UIImageView *imageView = [[UIImageView alloc] initWithImage:self.floorPlanImage];
                     self.bitmap = imageView;
                     [self.bitmap setFrame:CGRectMake(0, 0, floorplanSize.width, floorplanSize.height)];
                     [self.mapview addSubview:self.bitmap];
                     [imageView release];
                     
                     filterMask = [AppDelegate sharedInstance].floorPlanFilter;
                     [self setContentElementsWithType:@"RACK"];
                     [self setContentElementsWithType:@"CRAC"];
                     [self setContentElementsWithType:@"PRESSURE"];
                     [self setContentElementsWithType:@"ION_WYE"];
                     [self setContentElementsWithType:@"ION_DELTA"];
                     [self setContentElementsWithType:@"ION_WIRELESS"];
                     [self setContentElementsWithType:@"DOOR"];
                     [self setContentElementsWithType:@"EQUIPMENT_STATUS"];
                     [self setContentElementsWithType:@"VERTICAL_TEMP"];
                     [self setContentElementsWithType:@"GENERIC_TEMP"];
                     [self setContentElementsWithType:@"LEAK"];
                     
                     [self.view addSubview:mainview];
                     [mainview release];
                     [self.view reloadInputViews];
                     [self.view setUserInteractionEnabled:YES];
                     [self updateHasFinished];
                 }failure:^(NSError *error) 
                 {
                     [self taskFinished:taskIdGetFloorplanSize];
                 } ];
                 [self addTaskId:taskIdGetFloorplanSize];
             } failure:^(NSError *error) 
             {
                 [self taskFinished:taskIdGetFloorplanScheme];
                 [self updateHasFinished];
                 NSLog(@"%@", error);
             }];
             [self addTaskId:taskIdGetFloorplanScheme];
         } failure:^(NSError *error) 
         {
             [self taskFinished:taskIdGetFloorplan];
             [self updateHasFinished];
             NSLog(@"%@", error);
         }];
         [self addTaskId:taskIdGetFloorplan];
     } failure:^(NSError *error) 
    {
        [self taskFinished:taskIdGetFloorplanData];
         [self updateHasFinished];
         NSLog(@"%@", error);
     }];
    [self addTaskId:taskIdGetFloorplanData];
}

- (float)calculateZoomScaleWithoutOverlapping
{
    UIImage *rackImage = [UIImage imageNamed:@"RACK-red.png"];
    float rackHeightOnMinimumZoomScale = rackImage.size. height / self.mainview.minimumZoomScale;
    float rackHeightToAvoidOverlapping = RACK_HEIGHT_IN_INCHES / self.scaleFactor;
    return rackHeightToAvoidOverlapping / rackHeightOnMinimumZoomScale;
}

- (void)updateMinimumZoomScale
{
    float scale = 0;
    scale = MIN(self.mainview.frame.size.width/floorplanSize.width, self.mainview.frame.size.height/floorplanSize.height);
    [self.mainview setMinimumZoomScale:(scale)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateScreenInfo];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateMinimumZoomScale];
}

@end