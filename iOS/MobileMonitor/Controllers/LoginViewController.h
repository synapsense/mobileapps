//
//  ViewController.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransferringAlertView.h"
#import "FacilitiesViewController.h"
#import "DashboardViewController.h"
#import "SystemHealthViewController.h"
#import "MMSDK.h"
#import "AppDelegate.h"
#import "AppUtility.h"
#import "MMLoginView.h"
#import "SettingsViewController.h"

@interface LoginViewController:UIViewController <MMLoginViewDelegate,TransferringAlertViewDelegate, UITextFieldDelegate, UIAlertViewDelegate>
{
    UIAlertView* registerAlertView;
    int warningsCount;
    int loginRequestId;
    SettingsViewController* _settingsViewController;
}

@property (nonatomic,retain) MMLoginView* loginView;

@end