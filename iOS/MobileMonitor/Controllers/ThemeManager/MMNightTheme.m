//
//  MMNightTheme.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMNightTheme.h"

#define NIGHT_MODE_BACKGROUND_COLOR [UIColor colorWithRed:41.0f/255.0f green:41.0f/255.0f blue:41.0f/255.0f alpha:1.0f]
#define NIGHT_MODE_BACKGROUND_COLOR_SECOND_LEVEL [UIColor colorWithRed:61.0f/255.0f green:61.0f/255.0f blue:61.0f/255.0f alpha:1.0f]
//#define TABLE_GROUPED_BACKGROUND_COLOR [UIColor colorWithRed:77.0f/255.0f green:77.0f/255.0f blue:77.0f/255.0f alpha:1.0f]  //0x4d4d4d

@implementation MMNightTheme

- (id)init
{
    if (self = [super init])
    {
        [_titleLabelColor release];
        _titleLabelColor = [[UIColor whiteColor] retain];
        
        [_backgroundColor release];
        _backgroundColor = [NIGHT_MODE_BACKGROUND_COLOR retain];

        [_backgroundColorSecondLevel release];
        _backgroundColorSecondLevel = [NIGHT_MODE_BACKGROUND_COLOR_SECOND_LEVEL retain];

        [_tableViewGroupedBackgroundColor release];
        _tableViewGroupedBackgroundColor = [NIGHT_MODE_BACKGROUND_COLOR_SECOND_LEVEL retain];
        
        [_tableViewCellBackgroundColor release];
        _tableViewCellBackgroundColor = [NIGHT_MODE_BACKGROUND_COLOR retain];
        
        [_poweredByImage release];
        _poweredByImage = [[UIImage imageNamed:@"SynapSense-White-Logo.png"] retain];

        [_logoImage release];
        _logoImage = [[UIImage imageNamed:@"Customer-logo.png"] retain];
        
        [_egoRefreshArrowImage release];
        _egoRefreshArrowImage = [[UIImage imageNamed:@"whiteArrow.png"] retain];

        [_textFieldBorderColor release];
        _textFieldBorderColor = [SUBTITLE_BLUE_COLOR retain];

        [_textFieldTextColor release];
        _textFieldTextColor = [[UIColor whiteColor] retain];
        
        [_separatorTableViewColor release];
        _separatorTableViewColor = [[UIColor darkGrayColor] retain];
        
        [_underlinedLabelColor release];
        _underlinedLabelColor = [SUBTITLE_BLUE_COLOR retain];
        
        _activityViewIndicatorStyle = UIActivityIndicatorViewStyleWhite;
    }
    return self;
}

@end
