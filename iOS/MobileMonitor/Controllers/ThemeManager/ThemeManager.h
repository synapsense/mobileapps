//
//  ThemeManager.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMDefaultTheme.h"
#import "MMNightTheme.h"

typedef enum
{
    UIThemeDefault = 1,
    UIThemeNightMode = 2
}UITheme;

@protocol ThemeListenerControl <NSObject>

- (void)adjustUI:(MMDefaultTheme*)currentTheme;

@end

@interface ThemeManager : NSObject
{
    MMDefaultTheme*  _currentTheme;
    MMDefaultTheme*  _defaultTheme;
    MMNightTheme*    _nightTheme;
    NSMutableArray*  _observers;
}

+ (ThemeManager*)sharedInstance;

- (void)setUITheme:(UITheme)theme;
- (MMDefaultTheme*)currentTheme;
- (void)registerObserver:(id<ThemeListenerControl>)observer;
- (void)unregisterObserver:(id<ThemeListenerControl>)observer;

@end
