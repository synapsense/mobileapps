//
//  ThemeManager.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "ThemeManager.h"
#import "AppDelegate.h"

static ThemeManager* _sharedInstance;

@interface ThemeManager()

- (void)broadcastThemeChanged;

@end

@implementation ThemeManager

+ (ThemeManager*)sharedInstance
{
    if (!_sharedInstance)
    {
        _sharedInstance = [[ThemeManager alloc] init];
    }
    return _sharedInstance;
}

- (id)init
{
    if (self = [super init])
    {
        _nightTheme = [[MMNightTheme alloc] init];
        _defaultTheme = [[MMDefaultTheme alloc] init];
        _observers = [[NSMutableArray alloc] init];
        [self setUITheme:[[AppDelegate sharedInstance].applicationPreferences integerForKey:UI_THEME]];
    }
    return self;
}

- (void)dealloc
{
    [_observers release];
    [_defaultTheme release];
    [_nightTheme release];
    [super dealloc];
}

- (void)registerObserver:(id<ThemeListenerControl>)observer
{
    [_observers addObject:observer];
    [observer release];
}

- (void)unregisterObserver:(id<ThemeListenerControl>)observer
{
    [observer retain];
    [_observers removeObject:observer];
}

- (void)setUITheme:(UITheme)theme
{
    switch (theme)
    {
        case UIThemeNightMode:
        {
            _currentTheme = _nightTheme;
        }
            break;
            
        default:
        {
            _currentTheme = _defaultTheme;
        }
            break;
    }
    [self broadcastThemeChanged];
}

- (void)broadcastThemeChanged
{
    for (id<ThemeListenerControl> observer in _observers) 
    {
        [observer adjustUI:_currentTheme];
    }
}

- (MMDefaultTheme*)currentTheme
{
    return _currentTheme;
}

@end
