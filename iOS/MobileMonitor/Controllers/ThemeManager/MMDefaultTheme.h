//
//  MMDefaultTheme.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SUBTITLE_BLUE_COLOR [UIColor colorWithRed:51.0f/255.0f green:153.0f/255.0f blue:204.f/255.0f alpha:1.0f]

@interface MMDefaultTheme : NSObject
{
    UIColor* _backgroundColor;
    UIColor* _backgroundColorSecondLevel;
    UIColor* _tableViewGroupedBackgroundColor;
    UIColor* _tableViewCellBackgroundColor;
    UIColor* _titleLabelColor;
    UIColor* _subtitleLabelColor;
    UIColor* _textFieldBorderColor;
    UIColor* _textFieldTextColor;
    UIColor* _underlinedLabelColor;
    UIColor* _separatorTableViewColor;
    UIImage* _poweredByImage;
    UIImage* _logoImage;
    UIImage* _egoRefreshArrowImage;
    UIActivityIndicatorViewStyle _activityViewIndicatorStyle;
}

- (UIColor*)backgroundColor;
- (UIColor*)backgroundColorSecondLevel;
- (UIColor*)tableViewGroupedBackgroundColor;
- (UIColor*)tableViewCellBackgroundColor;
- (UIColor*)titleLabelColor;
- (UIColor*)subtitleLabelColor;
- (UIColor*)textFieldBorderColor;
- (UIColor*)textFieldTextColor;
- (UIColor*)underlinedLabelColor;
- (UIColor*)separatorTableViewColor;
- (UIImage*)poweredByImage;
- (UIImage*)logoImage;
- (UIImage*)egoRefreshArrowImage;
- (UIActivityIndicatorViewStyle)activityViewIndicatorStyle;

@end
