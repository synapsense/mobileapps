//
//  MMDefaultTheme.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMDefaultTheme.h"

@implementation MMDefaultTheme

- (id)init
{
    if (self = [super init])
    {
        _titleLabelColor = [[UIColor alloc] initWithRed:0.2f green:0.2f blue:0.2f alpha:1.0f];
        _tableViewGroupedBackgroundColor = [[UIColor groupTableViewBackgroundColor] retain];
        _tableViewCellBackgroundColor = [[UIColor whiteColor] retain];
        _subtitleLabelColor = [SUBTITLE_BLUE_COLOR retain];
        _backgroundColor = [[UIColor whiteColor] retain];
        _backgroundColorSecondLevel = [[UIColor whiteColor] retain];
        _textFieldBorderColor = [[UIColor darkGrayColor] retain];
        _textFieldTextColor = [SUBTITLE_BLUE_COLOR retain];
        _underlinedLabelColor = [SUBTITLE_BLUE_COLOR retain];
        _separatorTableViewColor = [[UIColor lightGrayColor] retain];
        _poweredByImage = [[UIImage imageNamed:@"SynapSense-Logo.png"] retain];
        _logoImage = [[UIImage imageNamed:@"Customer-logo2.png"] retain];
        _egoRefreshArrowImage = [[UIImage imageNamed:@"blueArrow.png"] retain];
        _activityViewIndicatorStyle = UIActivityIndicatorViewStyleGray;
    }
    return self;
}

- (void)dealloc
{
    [_titleLabelColor release];
    [_subtitleLabelColor release];
    [_backgroundColor release];
    [_backgroundColorSecondLevel release];
    [_tableViewGroupedBackgroundColor release];
    [_tableViewCellBackgroundColor release];
    [_textFieldBorderColor release];
    [_textFieldTextColor release];
    [_underlinedLabelColor release];
    [_separatorTableViewColor release];
    [_poweredByImage release];
    [_logoImage release];
    [super dealloc];
}

- (UIColor*)backgroundColor
{
    return _backgroundColor;
}

- (UIColor*)backgroundColorSecondLevel
{
    return _backgroundColorSecondLevel;
}

- (UIColor*)tableViewGroupedBackgroundColor
{
    return _tableViewGroupedBackgroundColor;
}

- (UIColor*)tableViewCellBackgroundColor
{
    return _tableViewCellBackgroundColor;
}

- (UIColor*)titleLabelColor
{
    return _titleLabelColor;
}

- (UIColor*)subtitleLabelColor
{
    return _subtitleLabelColor;
}

- (UIColor*)textFieldBorderColor
{
    return _textFieldBorderColor;
}

- (UIColor*)textFieldTextColor
{
    return _textFieldTextColor;
}

- (UIColor*)underlinedLabelColor
{
    return _underlinedLabelColor;
}

- (UIColor*)separatorTableViewColor
{
    return _separatorTableViewColor;
}

- (UIImage*)poweredByImage
{
    return _poweredByImage;
}

- (UIImage*)logoImage
{
    return _logoImage;
}

- (UIImage*)egoRefreshArrowImage
{
    return _egoRefreshArrowImage;
}

- (UIActivityIndicatorViewStyle)activityViewIndicatorStyle
{
    return _activityViewIndicatorStyle;
}

@end
