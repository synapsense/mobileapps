//
//  BaseViewController.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "RequestListener.h"
#import "MMView.h"

#define HEIGHT_HEADER           35

@interface BaseViewController : UIViewController<RequestListenerDelegate>
{
    RequestListener* _requestListener;
}

@property (nonatomic, retain) NSDate *timeStamp;
@property BOOL viewDidAppear;

- (void)updateScreenInfo;
- (void)cancelCurrentConnections;
- (MMView *) loadHeaderWithTitle:(NSString *)currentScreenTitle;
- (MMView *) loadHeaderWithTitle:(NSString *)currentScreenTitle titleWidth:(float)titleWidth;
- (MMView *) loadHeaderWithTitle:(NSString *)currentScreenTitle titleWidth:(float)titleWidth viewLevel:(ViewLevel)level;
- (void)updateHasFinished;
@end
