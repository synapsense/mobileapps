//
//  SettingsViewController.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//
#import "SettingsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "MMAboutView.h"
#import "ThemeManager.h"
#import "MMTableViewCell.h"

static bool portEdited = NO;

@interface SettingsViewController()

- (void)mapPresentationWasSwitched:(id)sender;
- (void)popUpOptionWasSwitched:(id)sender;
- (void)secureConnectionSwitched:(id)sender;
- (void)uiThemeSwithed:(id)sender;

@end

@implementation SettingsViewController

#define CURRENT_SETTING_FONT_SIZE   10
#define HEIGHT_OF_A_CELL            60

#define REQUEST_FREQUENCY_30_SECONDS 30
#define REQUEST_FREQUENCY_1_MINUTE   60
#define REQUEST_FREQUENCY_2_MINUTES  180
#define REQUEST_FREQUENCY_5_MINUTES  300
#define REQUEST_FREQUENCY_10_MINUTES 600

#define REQUEST_TIME_PERIOD_LAST_HOUR     1
#define REQUEST_TIME_PERIOD_LAST_12_HOURS 12
#define REQUEST_TIME_PERIOD_LAST_DAY      24
#define REQUEST_TIME_PERIOD_LAST_WEEK     168
#define REQUEST_TIME_PERIOD_LAST_30_DAYS  720
#define REQUEST_TIME_PERIOD_ALL_TIME      999

#define VERTICAL_INDENT_FOR_TOGGLE        17.0
#define RIGHT_INDENT_FOR_TOGGLE           25.0
#define TOGGLE_WIDTH                      50.0
#define TOGGLE_HEIGTH                     20.0

@synthesize settingsType;
@synthesize delegate;

#pragma mark - View lifecycle

- (id)init
{
    if (self = [super init])
    {
        tableView = [[MMTableView alloc]
                      initWithFrame:CGRectMake
                      (self.view.frame.origin.x,
                       self.view.frame.origin.y + HEIGHT_HEADER,
                       self.view.frame.size.width, 
                       self.view.frame.size.height - HEIGHT_HEADER) 
                      style:UITableViewStyleGrouped];
        [tableView setDelegate:self];
        [tableView setDataSource:self];
        [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self.view addSubview:tableView];
        [tableView release];
        settingsTableDataArray = [[NSArray alloc] 
                                  initWithObjects:
                                  NSLocalizedString(@"tableViewSettingRequestFrequency", @""),
                                  NSLocalizedString(@"tableViewSettingPresentationOfFacilities", @""),
                                  NSLocalizedString(@"Night mode", @""),
                                  NSLocalizedString(@"useSecureConnection", @""),
                                  NSLocalizedString(@"port", @""),
                                  NSLocalizedString(@"about", @""),
                                  nil];
        [self.view addSubview:[self loadHeaderWithTitle:NSLocalizedString(@"preferencesTitle", @"")]];
        self.navigationItem.title = NSLocalizedString(@"applicationTitle", @"");
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"backButtonTitle", @"") style:UIBarButtonItemStyleBordered target:nil action:nil];
        self.navigationItem.backBarButtonItem = backButton;
        [backButton release];
    }
    return self;
}

- (void)updateScreenInfo
{
    //do nothing
}

- (void)loadView
{
    self.view = [[[MMView alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self willAnimateRotationToInterfaceOrientation:[self interfaceOrientation] duration:0];
    [tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    if (facilitiesPresentationChanged)
//    {
//        if ([delegate respondsToSelector:@selector(updateFacilitiesTab:)])
//        {
//            [delegate updateFacilitiesTab:_mapPresentationForFacilities.on];
//        }
//    }
}

- (void)dealloc
{    
    [settingsTableDataArray release];
    [super dealloc];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)_tableView numberOfRowsInSection:(NSInteger)section 
{
    if (settingsType == SETTINGS_TYPE_LITE)
    {
        return 4;
    }
    return [settingsTableDataArray count];	
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    float horizontalIndent = 0;  
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell %d",indexPath.row];
    MMTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[MMTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        [cell setTableviewStyle:UITableViewStyleGrouped];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    switch (indexPath.row) {
        case 0:
        {
            switch ([[[AppDelegate sharedInstance] applicationPreferences] integerForKey:REQUEST_FREQUENCY]) 
            {
                case REQUEST_FREQUENCY_30_SECONDS:
                        cell.detailTextLabel.text = NSLocalizedString(@"requestFrequencyOption_1", @"");
                        break;
                case REQUEST_FREQUENCY_1_MINUTE:
                        cell.detailTextLabel.text = NSLocalizedString(@"requestFrequencyOption_2", @"");
                        break;
                case REQUEST_FREQUENCY_2_MINUTES:
                        cell.detailTextLabel.text = NSLocalizedString(@"requestFrequencyOption_3", @"");                        
                    break;
                case REQUEST_FREQUENCY_5_MINUTES:
                        cell.detailTextLabel.text = NSLocalizedString(@"requestFrequencyOption_4", @"");                        
                    break;
                case REQUEST_FREQUENCY_10_MINUTES:
                        cell.detailTextLabel.text = NSLocalizedString(@"requestFrequencyOption_5", @"");                           break;
                default:
                        break;
            }
        }
            break;
//        
//        not needed option now
//            
//        case 1:
//        {
//            switch ([[[AppDelegate sharedInstance] applicationPreferences] integerForKey:REQUEST_TIME_PERIOD]) 
//            {
//                case REQUEST_TIME_PERIOD_LAST_HOUR:
//                    cell.detailTextLabel.text = NSLocalizedString(@"requestTimePeriodOption_1", @"");
//                    break;
//                case REQUEST_TIME_PERIOD_LAST_12_HOURS:
//                    cell.detailTextLabel.text = NSLocalizedString(@"requestTimePeriodOption_2", @"");
//                    break;
//                case REQUEST_TIME_PERIOD_LAST_DAY:
//                    cell.detailTextLabel.text = NSLocalizedString(@"requestTimePeriodOption_3", @"");
//                    break;
//                case REQUEST_TIME_PERIOD_LAST_WEEK:
//                    cell.detailTextLabel.text = NSLocalizedString(@"requestTimePeriodOption_4", @"");
//                    break;
//                case REQUEST_TIME_PERIOD_LAST_30_DAYS:
//                    cell.detailTextLabel.text = NSLocalizedString(@"requestTimePeriodOption_5", @"");
//                    break;
//                case REQUEST_TIME_PERIOD_ALL_TIME:
//                    cell.detailTextLabel.text = NSLocalizedString(@"requestTimePeriodOption_6", @"");
//                    break;
//                default:
//                    break;
//            }
//        }
//            break;
            
        case 1:
        {
            //presentation of facilities
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];            
            if(_mapPresentationForFacilities == nil)
            {
                _mapPresentationForFacilities = [[UISwitch alloc] init];
                [_mapPresentationForFacilities setFrame:CGRectMake(
                                                                  cell.frame.size.width - _mapPresentationForFacilities.frame.size.width - RIGHT_INDENT_FOR_TOGGLE, 
                                                                  self.view.frame.origin.y+VERTICAL_INDENT_FOR_TOGGLE, 
                                                                  _mapPresentationForFacilities.frame.size.width, 
                                                                  _mapPresentationForFacilities.frame.size.height
                                                                  )];
                [_mapPresentationForFacilities addTarget:self 
                                                      action:@selector(mapPresentationWasSwitched:)
                                            forControlEvents:UIControlEventValueChanged];
                _mapPresentationForFacilities.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
                _mapPresentationForFacilities.on = [[[AppDelegate sharedInstance] applicationPreferences] boolForKey:MAP_PRESENTATION];
                [cell addSubview:_mapPresentationForFacilities];
                [_mapPresentationForFacilities release];
            }
        }
            break;
        case 2:
        {
            //ui theme
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];            
            if (!nightTheme) 
            {
                nightTheme = [[UISwitch alloc] initWithFrame:CGRectMake(self.view.frame.origin.x+horizontalIndent,self.view.frame.origin.y+VERTICAL_INDENT_FOR_TOGGLE, TOGGLE_WIDTH, TOGGLE_HEIGTH)];
                [nightTheme setFrame:CGRectMake(
                                                cell.frame.size.width - nightTheme.frame.size.width - RIGHT_INDENT_FOR_TOGGLE, 
                                                self.view.frame.origin.y+VERTICAL_INDENT_FOR_TOGGLE, 
                                                nightTheme.frame.size.width, 
                                                nightTheme.frame.size.height
                                                )];
                [nightTheme addTarget:self action:@selector(uiThemeSwithed:) forControlEvents:UIControlEventValueChanged];
                nightTheme.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
                nightTheme.on = [[[AppDelegate sharedInstance] applicationPreferences] integerForKey:UI_THEME] == UIThemeNightMode;
                [cell addSubview:nightTheme];
                [nightTheme release];
            }
        }
            break;
        case 3:
        {
            if (settingsType != SETTINGS_TYPE_LITE)
            {
                //Use secure connection
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                if (!useSecure) 
                {
                    useSecure = [[UISwitch alloc] initWithFrame:CGRectMake(self.view.frame.origin.x+horizontalIndent,self.view.frame.origin.y+VERTICAL_INDENT_FOR_TOGGLE, TOGGLE_WIDTH, TOGGLE_HEIGTH)];
                    [useSecure setFrame:CGRectMake(
                                                   cell.frame.size.width - useSecure.frame.size.width - RIGHT_INDENT_FOR_TOGGLE, 
                                                   self.view.frame.origin.y+VERTICAL_INDENT_FOR_TOGGLE, 
                                                   useSecure.frame.size.width, 
                                                   useSecure.frame.size.height
                                                   )];
                    [useSecure addTarget:self action:@selector(secureConnectionSwitched:) forControlEvents:UIControlEventValueChanged];
                    useSecure.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
                    useSecure.on = [[[AppDelegate sharedInstance] applicationPreferences] boolForKey:USE_HTTPS];
                    [cell addSubview:useSecure];
                    [useSecure release];
                } 
            }
            
        }
            break;

        case 4:
        {
            if (settingsType != SETTINGS_TYPE_LITE)
            {
                //Port
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                if (!portLabel) 
                {
                    portLabel = [[MMUnderlinedLabel alloc] init];
                    [portLabel setDelegate:self];
                    [portLabel setShowUnderline:YES];
                    portLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
                    NSUInteger port = useSecure.on ? [AppDelegate sharedInstance].securedPort : [AppDelegate sharedInstance].unsecuredPort;
                    portLabel.text = [NSString stringWithFormat:@"%d",port];
                    [portLabel setFont:[UIFont systemFontOfSize:18]];
                    [portLabel setTextAlignment:UITextAlignmentRight];
                    [portLabel setFrame:CGRectMake(cell.frame.size.width - 100 - RIGHT_INDENT_FOR_TOGGLE, HEIGHT_OF_A_CELL / 2 - portLabel.font.lineHeight / 2, 100, portLabel.font.lineHeight)];
                    [cell addSubview:portLabel];
                    [portLabel release];
                }
            }
            break;
        }
        default:
                break;
    }
    if (settingsType == SETTINGS_TYPE_LITE && indexPath.row == 3)
    {
        cell.textLabel.text = [settingsTableDataArray objectAtIndex:([settingsTableDataArray count] - 1)];
    }
    else
    {
        cell.textLabel.text = [settingsTableDataArray objectAtIndex:indexPath.row];
    }
    return cell;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)_tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{   
    return HEIGHT_OF_A_CELL;
}

- (void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(indexPath.row == 0)
    {
        OptionsSettingsViewController *optionsSettingsViewController = [[OptionsSettingsViewController alloc] initWithSettingString:[settingsTableDataArray objectAtIndex:indexPath.row]];
        [[self navigationController] pushViewController:optionsSettingsViewController animated:YES];
        [optionsSettingsViewController release];
    }
    if(indexPath.row == 5 || (settingsType == SETTINGS_TYPE_LITE && indexPath.row == 3))
    {
        //creating of transition
        CATransition *aboutTransition = [CATransition animation];
        aboutTransition.duration = 0.5;
        aboutTransition.timingFunction = [CAMediaTimingFunction
                                          functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        aboutTransition.type = kCATransitionReveal;
        aboutTransition.delegate = self;
        
        _aboutMessage = [[MMAboutView alloc] initWithFrame:self.view.frame];
        
        [[_aboutMessage layer] addAnimation:aboutTransition
                                         forKey:kCATransitionReveal];
        
        [self.view addSubview:_aboutMessage];
        [_aboutMessage release];
//        _aboutMessage.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        _buttonWithBigArea = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
        _buttonWithBigArea.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [_buttonWithBigArea addTarget:self action:@selector(touchToRemoveAboutMessage) forControlEvents:UIControlEventTouchUpInside];
        [_buttonWithBigArea setBackgroundColor:[UIColor clearColor]];
        
        _buttonWithBigArea.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        [self.view addSubview:_buttonWithBigArea];
        [_buttonWithBigArea release];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)popUpOptionWasSwitched:(id)sender
{
    BOOL switcher = _popUpNotificationOptionOnOff.on;
    if(switcher == YES)
    {
        //TODO: appropriate actions for turned on "Popup notification" option
    }
    else if(switcher == NO)
    {
        //TODO: appropriate actions for turned off "Popup notification" option
    }
} 

- (void)mapPresentationWasSwitched:(id)sender
{
    [[[AppDelegate sharedInstance] applicationPreferences] setBool:_mapPresentationForFacilities.on forKey:MAP_PRESENTATION];
    facilitiesPresentationChanged = YES;
}

- (void)uiThemeSwithed:(id)sender
{
    UITheme themeType = nightTheme.on ? UIThemeNightMode : UIThemeDefault;
    [[ThemeManager sharedInstance] setUITheme:themeType];
    [[AppDelegate sharedInstance].applicationPreferences setInteger:themeType forKey:UI_THEME];
    [tableView reloadData];
}

- (void)secureConnectionSwitched:(id)sender
{
    UISwitch *secureSwitch = (UISwitch*)sender;
    [[[AppDelegate sharedInstance] applicationPreferences] setBool:secureSwitch.on forKey:USE_HTTPS];
    if (!portEdited) 
    {
        NSUInteger port = secureSwitch.on ? [AppDelegate sharedInstance].securedPort : [AppDelegate sharedInstance].unsecuredPort;
        portLabel.text = [NSString stringWithFormat:@"%d",port];
    }
    else
    {
        if (secureSwitch.on)
        {
            [AppDelegate sharedInstance].securedPort = [portLabel.text intValue];
        }
        else
        {
            [AppDelegate sharedInstance].unsecuredPort = [portLabel.text intValue];
        }
    }
}

- (void)handleUnderlinedLabelTap:(MMUnderlinedLabel*)label
{
    UIAlertView *inputPortPopup = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"inputPort", @"")
                                                                            message:nil
                                                                                   delegate:self
                                                                          cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                                                          otherButtonTitles: NSLocalizedString(@"okButtonTitle", @""), nil];
    inputPortPopup.alertViewStyle = UIAlertViewStylePlainTextInput;
    [[inputPortPopup textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
    [inputPortPopup show];
    [inputPortPopup release];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != alertView.cancelButtonIndex)
    {
        if (![[alertView textFieldAtIndex:0].text isEqualToString:@""]) 
        {
            [portLabel setText:[alertView textFieldAtIndex:0].text];
            if (useSecure.on) 
            {
                [AppDelegate sharedInstance].securedPort = [[alertView textFieldAtIndex:0].text intValue];
            }
            else
            {
                [AppDelegate sharedInstance].unsecuredPort = [[alertView textFieldAtIndex:0].text intValue];
            }
            [portLabel setNeedsLayout];
        }
        portEdited = YES;
    }
}

- (void)touchToRemoveAboutMessage
{
    [_aboutMessage removeFromSuperview];
    [_buttonWithBigArea removeFromSuperview];
}

@end