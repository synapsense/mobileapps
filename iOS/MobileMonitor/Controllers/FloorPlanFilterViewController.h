//
//  FloorPlanFilterViewController.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewController.h"
#import "MMFloorPlanInfo.h"
#import "MMTableView.h"

@interface FloorPlanFilterViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) MMTableView *table;
@property (nonatomic, retain) NSArray *types;
@property (nonatomic) MMFloorPlanFilter filterMask;

@end
