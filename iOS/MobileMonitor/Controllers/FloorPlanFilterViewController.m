//
//  FloorPlanFilterViewController.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "FloorPlanFilterViewController.h"
#import "AppDelegate.h"
#import "MMFloorPlanInfo.h"
#import "MMTableViewCell.h"

@implementation FloorPlanFilterViewController

@synthesize table;
@synthesize types;
@synthesize filterMask;

- (void)updateScreenInfo
{
    //do nothing
}

-(void) viewDidLoad
{
    [super viewDidLoad];
    self.view = [[[MMView alloc] initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, VIEW_HEIGHT) viewLevel:ViewLevelSecond] autorelease];
    self.navigationItem.title = NSLocalizedString(@"applicationTitle", @"");
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    table = [[[MMTableView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 35, self.view.frame.size.width, self.view.frame.size.height - 35) style:UITableViewStyleGrouped] autorelease];
    table.delegate = self;
    table.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    filterMask = [AppDelegate sharedInstance].floorPlanFilter;
    types = [[NSArray alloc] initWithObjects:NSLocalizedString(@"rack", @""), NSLocalizedString(@"crack", @""), NSLocalizedString(@"door", @""), NSLocalizedString(@"pressure", @""), NSLocalizedString(@"POWER_METER", @""), NSLocalizedString(@"genericTemp", @""), NSLocalizedString(@"verticalTemp", @""), NSLocalizedString(@"equipmentStatus", @""), NSLocalizedString(@"leak", @""), nil];
    table.delegate = self;
    table.dataSource = self;
    [self.view addSubview:[self loadHeaderWithTitle:NSLocalizedString(@"filterFloorPlane", @"") titleWidth:self.view.frame.size.width viewLevel:ViewLevelSecond]];
    [self.view addSubview:table];
}

- (void)dealloc
{
    self.types = nil;
    [super dealloc];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[AppDelegate sharedInstance].applicationPreferences setInteger:filterMask forKey:FLOORPLAN_FILTER];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [types count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MMTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"FilterCell"];
    if (cell == nil)
    {
        cell = [[[MMTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FilterCell"]autorelease];
        [cell setTableviewStyle:UITableViewStyleGrouped];
    }
    
    [cell.textLabel setText:[types objectAtIndex:indexPath.row]];
    [cell setTag:(1 << indexPath.row)];
    if(filterMask & cell.tag)
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
        cell.accessoryType = UITableViewCellAccessoryNone;
    else
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
    {
        filterMask |= [tableView cellForRowAtIndexPath:indexPath].tag;
    }
    else if (cell.accessoryType == UITableViewCellAccessoryNone)
    {        
        filterMask ^= [tableView cellForRowAtIndexPath:indexPath].tag;
    }
    [AppDelegate sharedInstance].floorPlanFilter = filterMask;
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    [super shouldAutorotateToInterfaceOrientation:toInterfaceOrientation];
    return YES;
}

@end
