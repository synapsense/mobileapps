//
//  SortViewController.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "SortViewController.h"
#import "FacilityAlertsViewController.h"
#import "MMTableViewCell.h"

#define OBGECT_SORT     0
#define PRIORITY_SORT   1
#define STATUS_SORT     2
#define TIMESTAMP_SORT  3

@implementation SortViewController

@synthesize sortTableDataArray;
@synthesize sortingAlerts;

- (void)updateScreenInfo
{
    //do nothing
}

-(void)dealloc
{
    [sortTableDataDictionary release];

    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self != nil)
    {
        NSArray* ascending = [NSArray arrayWithObjects:NSLocalizedString(@"objectAscending", @""), NSLocalizedString(@"priorityAscending", @""),
                                                        NSLocalizedString(@"statusAscending", @""),  NSLocalizedString(@"timestampAscending", @""), nil];

        NSArray* descending = [NSArray arrayWithObjects:NSLocalizedString(@"objectAscending", @""),NSLocalizedString(@"priorityAscending", @""),
                                                        NSLocalizedString(@"statusAscending", @""),NSLocalizedString(@"timestampAscending", @""), nil];
        
        sortTableDataDictionary = [[NSDictionary dictionaryWithObjectsAndKeys:ascending, NSLocalizedString(@"ascendingOrder", @""), descending, NSLocalizedString(@"descendingOrder", @""), nil] retain];
        
         sectionTableView = [[[MMTableView alloc]initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 30,
                                                                    self.view.frame.size.width, self.view.frame.size.height - 30) 
                                                                    style:UITableViewStyleGrouped] autorelease];
        [sectionTableView setDelegate:self];
        [sectionTableView setDataSource:self];
        [sectionTableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self.view addSubview:sectionTableView];

        [self.view addSubview:[self loadHeaderWithTitle:NSLocalizedString(@"sort", @"") titleWidth:self.view.frame.size.width  viewLevel:ViewLevelSecond]];
    }
    return self;
}

- (void) loadView
{
    self.view = [[[MMView alloc] initWithFrame:CGRectMake(0, 0, 640, 960) viewLevel:ViewLevelSecond]autorelease];
    self.navigationItem.title = NSLocalizedString(@"applicationTitle", @"");
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(NSArray*)curentSort: (NSInteger)index
{
    NSArray* keys = [sortTableDataDictionary allKeys];
    NSString* curentKey = [keys objectAtIndex:index];
    NSArray* curentSort = [sortTableDataDictionary objectForKey:curentKey];
    return curentSort;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [sortTableDataDictionary count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    NSArray* curentSort = [self curentSort:section];
    return [curentSort count];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *)indexPat
{
    return 35;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[[UIView alloc] init] autorelease];
    view.backgroundColor = [UIColor clearColor];
    MMTitleLabel *headerText = [[MMTitleLabel alloc] initWithFrame:CGRectMake(20, 10, 0, 0)];
    [headerText setFont:[UIFont boldSystemFontOfSize:17]];
    headerText.text = [[sortTableDataDictionary allKeys]objectAtIndex:section];
    [headerText sizeToFit];
    [view addSubview:headerText];
    [headerText release];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    MMTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"SortCell"];
    if (cell == nil)
    {
        cell = [[[MMTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SortCell"]autorelease];
        [cell setTableviewStyle:UITableViewStyleGrouped];
    }
    NSArray* curentSort = [self curentSort:indexPath.section];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
        cell.accessoryType = UITableViewCellAccessoryNone;

    cell.textLabel.text = [curentSort objectAtIndex:indexPath.row];
    cell.textLabel.numberOfLines = 2;
    cell.textLabel.font = [UIFont boldSystemFontOfSize:15.0f];
    
    sortingAlerts = [AppDelegate sharedInstance].alertSorting;
    switch (indexPath.section)
    {
        case 0:
        {
            switch (indexPath.row)
            {
                case OBGECT_SORT:
                {
                    if (sortingAlerts == MMAlertAscendingSortObjectName)
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    break;
                }
                case PRIORITY_SORT:
                {
                    if (sortingAlerts == MMAlertAscendingSortPriority)
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    break;
                }
                case STATUS_SORT:
                {
                    if (sortingAlerts == MMAlertAscendingSortStatus)
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    break;
                }
                case TIMESTAMP_SORT:
                {
                    if (sortingAlerts == MMAlertAscendingSortTimestamp)
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    break;
                }
            }
            break;
        }
        case 1:
        {
            switch (indexPath.row)
            {
                case OBGECT_SORT:
                {
                    if (sortingAlerts == MMAlertDescendingSortObjectName)
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    break;
                }
                case PRIORITY_SORT:
                {
                    if (sortingAlerts == MMAlertDescendingSortPriority)
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    break;
                }
                case STATUS_SORT:
                {
                    if (sortingAlerts == MMAlertDescendingSortStatus)
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    break;
                }
                case TIMESTAMP_SORT:
                {
                    if (sortingAlerts == MMAlertDescendingSortTimestamp)
                        cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    break;
                }
            }
            break;
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
    if (newCell.accessoryType == UITableViewCellAccessoryNone) 
    {
        currentCell.accessoryType = UITableViewCellAccessoryNone;
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        currentCell = newCell;
    }
    switch (indexPath.section)
    {
        case 0:
        {
            switch (indexPath.row)
            {
                case OBGECT_SORT:
                {
                    sortingAlerts = MMAlertAscendingSortObjectName;
                    break;
                }
                case PRIORITY_SORT:
                {
                    sortingAlerts = MMAlertAscendingSortPriority;
                    break;
                }
                case STATUS_SORT:
                {
                    sortingAlerts = MMAlertAscendingSortStatus;
                    break;
                }
                case TIMESTAMP_SORT:
                {
                    sortingAlerts = MMAlertAscendingSortTimestamp;
                    break;
                }
            }
            break;
        }
        case 1:
        {
            switch (indexPath.row)
            {
                case OBGECT_SORT:
                {
                    sortingAlerts = MMAlertDescendingSortObjectName;
                    break;
                }
                case PRIORITY_SORT:
                {
                    sortingAlerts = MMAlertDescendingSortPriority;
                    break;
                }
                case STATUS_SORT:
                {
                    sortingAlerts = MMAlertDescendingSortStatus;
                    break;
                }
                case TIMESTAMP_SORT:
                {
                    sortingAlerts = MMAlertDescendingSortTimestamp;
                    break;
                }
            }
            break;
        }
    }
    [sectionTableView reloadData];
    [AppDelegate sharedInstance].alertSorting = sortingAlerts;
    [[AppDelegate sharedInstance].applicationPreferences setInteger:sortingAlerts forKey:ALERT_SORTING_CONDITION];
    [[[AppDelegate sharedInstance] navigaionController] popViewControllerAnimated:YES];
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [sectionTableView reloadData];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end
