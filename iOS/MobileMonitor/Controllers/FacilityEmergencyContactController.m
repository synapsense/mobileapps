//
//  FacilityPhoneAPIsController.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "FacilityEmergencyContactController.h"
#import "MMSDK.h"
#import "MMTitleLabel.h"
#import "MMSubtitleLabel.h"

#define HEIGHT_CELL             50
#define SEPARATOR_HEIGHT        1

@implementation FacilityEmergencyContactController

@synthesize emergencyContact;

- (void)updateScreenInfo
{
    [super updateScreenInfo];
    if (errorAlert)
    {
        [errorAlert dismissWithClickedButtonIndex:errorAlert.cancelButtonIndex animated:NO];
        errorAlert = nil;
    }
    
    MMSDK *sdk = [MMSDK sharedInstance];
    __block int taskIdGetEmergemcyContact = 0;
    taskIdGetEmergemcyContact = [sdk getEmergencyContact:dcGuid success:^(MMEmergencyContact* emergency)
    {
        [self taskFinished:taskIdGetEmergemcyContact];
        self.emergencyContact = emergency;
        self.viewDidAppear = YES;
        [sectionTableView reloadData];
        [self updateHasFinished];
    }
    failure:^(NSError *error)
    {
        [self taskFinished:taskIdGetEmergemcyContact];
        [self updateHasFinished];
        if (!errorAlert)
        {
            errorAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"updatingError", @"") message:[NSString stringWithFormat:@""] delegate:self cancelButtonTitle:NSLocalizedString(@"okButtonTitle", @"") otherButtonTitles: nil];
            [errorAlert show];
            [errorAlert release];
        }
    }];
    [self addTaskId:taskIdGetEmergemcyContact];
}

- (id)initWithDataCenter:(MMDataCenterInfo*)dataCenter guid:(NSString*)guid
{
    self = [super init];
    if (self)
    {
        curentDataCenter = dataCenter;
        dcGuid = guid;
        titleTableDataArray = [[NSArray alloc] initWithObjects:NSLocalizedString(@"name", @""), NSLocalizedString(@"title", @""),
                                                              NSLocalizedString(@"officePhone", @""), NSLocalizedString(@"mobilePhone", @""),
                                                               NSLocalizedString(@"email", @""), nil];
    }
    return self;
}

- (void)loadView
{
    self.view = [[[MMView alloc] initWithFrame:CGRectMake(0, 0, 640, 960) viewLevel:ViewLevelSecond] autorelease];
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:[self loadHeaderWithTitle:[curentDataCenter name] titleWidth:self.view.frame.size.width viewLevel:ViewLevelSecond]];
    [self loadSections];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateScreenInfo];
}

- (void)dealloc
{
    curentDataCenter = nil;
    dcGuid = nil;
    self.emergencyContact = nil;
    [sectionTableView release];
    [titleTableDataArray release];
    
    [super dealloc];
}

-(void)loadSections
{
    sectionTableView = [[MMTableView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 35,
                                                                    self.view.frame.size.width, self.view.frame.size.height - 35) style:UITableViewStylePlain];
    sectionTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [sectionTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    sectionTableView.delegate = self;
    sectionTableView.dataSource = self;
    sectionTableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:sectionTableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
   return [titleTableDataArray count]; 
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return HEIGHT_CELL;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyIdentifier"] autorelease];
    float portraitWidth = [UIScreen mainScreen].bounds.size.width;
    MMTitleLabel* titleLabel = [[MMTitleLabel alloc] initWithFrame:CGRectMake(HORIZONTAL_INDENT, self.view.frame.origin.y, portraitWidth / 2 - HORIZONTAL_INDENT, HEIGHT_CELL)];
    titleLabel.text = [titleTableDataArray objectAtIndex:indexPath.row];
    titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    titleLabel.font = [UIFont boldSystemFontOfSize:15];
    titleLabel.textAlignment = UITextAlignmentLeft;

    UILabel *_separator = [[UILabel alloc] initWithFrame:CGRectMake(HORIZONTAL_INDENT, HEIGHT_CELL - SEPARATOR_HEIGHT, cell.frame.size.width - 2*HORIZONTAL_INDENT, SEPARATOR_HEIGHT)];
    [_separator setBackgroundColor:[UIColor darkGrayColor]];
    _separator.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [cell addSubview:_separator];
    [_separator release];

    switch (indexPath.row)
    {
        case 0:
        {
            MMSubtitleLabel *label = [[MMSubtitleLabel alloc]initWithFrame:CGRectMake(portraitWidth/2, titleLabel.frame.origin.y, portraitWidth/2 - HORIZONTAL_INDENT, HEIGHT_CELL)];
            if ([self.emergencyContact.emergencyName length] == 0)
                label.text = NSLocalizedString(@"N/A", @"");
            else
                label.text = self.emergencyContact.emergencyName;
            label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin;
            label.font = [UIFont boldSystemFontOfSize:14];
            label.textAlignment = UITextAlignmentRight;
            [cell.contentView addSubview:label];
            [label release];
            break;
        }
        case 1:
        {
            MMSubtitleLabel *label = [[MMSubtitleLabel alloc]initWithFrame:CGRectMake(portraitWidth/2, titleLabel.frame.origin.y, portraitWidth/2 - HORIZONTAL_INDENT, HEIGHT_CELL)];
            if ([self.emergencyContact.emergencyTitle length] == 0)
                label.text = NSLocalizedString(@"N/A", @"");
            else
                label.text = self.emergencyContact.emergencyTitle;
            label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin;
            label.font = [UIFont boldSystemFontOfSize:14];
            label.textAlignment = UITextAlignmentRight;
            [cell.contentView addSubview:label];
            [label release];
            break;
        }
        case 2:
        {
            MMUnderlinedLabel* phoneLabel = [[MMUnderlinedLabel alloc]initWithFrame:CGRectMake(portraitWidth/2, titleLabel.frame.origin.y, portraitWidth/2 - HORIZONTAL_INDENT, HEIGHT_CELL)];
            if ([self.emergencyContact.emergencyOfficePhone length] == 0)
                phoneLabel.text = NSLocalizedString(@"N/A", @"");
            else
            {
                phoneLabel.showUnderline = YES;
                phoneLabel.text = self.emergencyContact.emergencyOfficePhone;
            }
            phoneLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin;
            phoneLabel.font = [UIFont boldSystemFontOfSize:14];
            phoneLabel.textAlignment = UITextAlignmentRight;
            phoneLabel.tag = 2;
            phoneLabel.delegate = self;
            [cell.contentView addSubview:phoneLabel];
            [phoneLabel release];
            break;
        }
        case 3:
        {
            MMUnderlinedLabel* phoneLabel = [[MMUnderlinedLabel alloc]initWithFrame:CGRectMake(portraitWidth/2, titleLabel.frame.origin.y, portraitWidth/2 - HORIZONTAL_INDENT, HEIGHT_CELL)];
            if ([self.emergencyContact.emergencyMobilePhone length] ==0)
                phoneLabel.text = NSLocalizedString(@"N/A", @"");
            else
            {
                phoneLabel.showUnderline = YES;
                phoneLabel.text = self.emergencyContact.emergencyMobilePhone;
            }
            phoneLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin;
            phoneLabel.font = [UIFont boldSystemFontOfSize:14];
            phoneLabel.textAlignment = UITextAlignmentRight;
            phoneLabel.tag = 3;
            phoneLabel.delegate = self;
            [cell.contentView addSubview:phoneLabel];
            [phoneLabel release];
            break;
        }
        case 4:
        {
            MMUnderlinedLabel* phoneLabel = [[MMUnderlinedLabel alloc]initWithFrame:CGRectMake(portraitWidth/2, titleLabel.frame.origin.y, portraitWidth/2 - HORIZONTAL_INDENT, HEIGHT_CELL)];
            if ([self.emergencyContact.emergencyEmail length] == 0)
                phoneLabel.text = NSLocalizedString(@"N/A", @"");
            else
            {
                phoneLabel.showUnderline = YES;
                phoneLabel.text = self.emergencyContact.emergencyEmail;
            }
            phoneLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin;
            phoneLabel.font = [UIFont boldSystemFontOfSize:14];
            phoneLabel.textAlignment = UITextAlignmentRight;
            phoneLabel.tag = 4;
            phoneLabel.delegate = self;
            [cell.contentView addSubview:phoneLabel];
            [phoneLabel release];
            break;
        }
    }
    [cell.contentView addSubview:titleLabel];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];

    [titleLabel release];
    return cell;
}

- (void)handleUnderlinedLabelTap:(MMUnderlinedLabel *)underlinedLabel
{
    switch (underlinedLabel.tag)
    {
        case 2:
        {
            if (self.emergencyContact.emergencyOfficePhone)
            {
                NSString *phoneNumber = [NSString stringWithUTF8String:[self.emergencyContact.emergencyOfficePhone UTF8String]];
                phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString: @"(" withString: @""];
                phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString: @")" withString: @""];
                phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString: @" " withString: @""];
                phoneNumber = [NSString stringWithFormat:@"telprompt://%@", phoneNumber];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
            }
            break;
        }
        case 3:
        {
            if (self.emergencyContact.emergencyMobilePhone)
            {
                NSString *phoneNumber = [NSString stringWithUTF8String:[self.emergencyContact.emergencyMobilePhone UTF8String]];
                phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString: @"(" withString: @""];
                phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString: @")" withString: @""];
                phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString: @" " withString: @""];
                phoneNumber = [NSString stringWithFormat:@"telprompt://%@", phoneNumber];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
            }
            break;
        }
        case 4:
        {
            if (self.emergencyContact.emergencyEmail)
            {
                [self createEmailLetter];
            }
            break;
        }
    }
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [sectionTableView reloadData];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return  YES;
}

- (void)createEmailLetter
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self;        
        [picker setSubject:@""];
        NSArray* toRecipients = [NSArray arrayWithObject:self.emergencyContact.emergencyEmail];
        [picker setToRecipients:toRecipients];
        NSString *emailBody = @"";
        [picker setMessageBody:emailBody isHTML:NO];
        picker.navigationBar.tintColor = [UIColor darkGrayColor];
        
        [self presentModalViewController:picker animated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Client Error" message:@"Please setup your email account first." delegate:self cancelButtonTitle:NSLocalizedString(@"okButtonTitle", @"") otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSString *message = nil;
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            message = NSLocalizedString(@"canceled", @"");
            break;
        case MFMailComposeResultSaved:
            message = NSLocalizedString(@"saved", @"");
            break;
        case MFMailComposeResultSent:
            message = NSLocalizedString(@"sent", @"");
            break;
        case MFMailComposeResultFailed:
        {
            if ([error.domain isEqualToString:MFMailComposeErrorDomain])
            {
                switch (error.code)
                {
                    case MFMailComposeErrorCodeSendFailed:
                        message = NSLocalizedString(@"notSent", @"");
                        break;
                    case MFMailComposeErrorCodeSaveFailed:
                        message = NSLocalizedString(@"notSaved", @"");
                        break;
                }
            }
            else
            {
                message = NSLocalizedString(@"notSentError", @"");
            }
        }
        default:
            message = NSLocalizedString(@"notSentError", @"");
    }

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"sending", @"") message:message delegate:self cancelButtonTitle:NSLocalizedString(@"okButtonTitle", @"") otherButtonTitles:nil];
    [alert show];
    [alert release];

    [self dismissModalViewControllerAnimated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (errorAlert)
    {
        errorAlert = nil;
    }
}

@end