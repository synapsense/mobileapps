//
//  FacilityDashboardViewController.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIPIEChartView.h"
#import "BaseViewController.h"
#import "MMDataCenterInfo.h"
#import "EGORefreshTableHeaderView.h"
#import "MMTableView.h"

@interface FacilityDashboardViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource ,UIAlertViewDelegate, EGORefreshTableHeaderDelegate>
{
    UIPIEChartView* PIEChartView;
    NSMutableArray* array;
    IBOutlet MMTableView* sectionTableView;
    NSArray *headsets;
    MMDataCenterInfo *currentDataCenter;
    int activeAlertsCount;
    EGORefreshTableHeaderView *_refreshHeaderView;
	BOOL _reloading;
    UIAlertView* errorAlert;
}

-(NSMutableArray*)solutionInPercent;
- (void)loadSections;

@property(nonatomic, retain) NSMutableArray* array;
@property(nonatomic, retain) IBOutlet MMTableView* sectionTableView;
@property(nonatomic, retain) NSArray *arrayObject;
@property(nonatomic, retain) MMDataCenterInfo *currentDataCenter;
@property (nonatomic,retain) NSDate *lastUpdateTime;

- (id)initWithDataCenter:(MMDataCenterInfo*)dataCenter;

@end
