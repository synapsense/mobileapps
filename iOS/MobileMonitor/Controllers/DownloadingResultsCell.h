//
//  DownloadingResultsCell.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DownloadingResultsCell : UITableViewCell
{
    UILabel*                  topMessageLabel;
    UILabel*                  bottomMessageLabel;
    UIActivityIndicatorView*  spinner;
    UIButton*                 cancelButton;
    UIView*                   topSeparator;
    UIView*                   bottomSeparator;
}

- (void)setTopMessage:(NSString*)text;
- (void)setBottomMessage:(NSString*)text;
- (void)showSpinner:(BOOL)show;
- (void)showCancelButton:(BOOL)show target:(id)target action:(SEL)action;

@end
