//
//  MMUnderlinedLabel.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ThemeManager.h"

@class MMUnderlinedLabel;

@protocol MMUnderlinedLabelDelegate <NSObject>
-(void)handleUnderlinedLabelTap:(MMUnderlinedLabel*)underlinedLabel;
@end

@interface MMUnderlinedLabel : UILabel<UIGestureRecognizerDelegate, ThemeListenerControl>

@property (nonatomic, assign) BOOL showUnderline;
@property (nonatomic, assign) id<MMUnderlinedLabelDelegate> delegate;

@end
