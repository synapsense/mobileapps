//
//  MMAboutView.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

#define FONT_SIZE 11

static NSString *const COPY_RIGHT = @"\u24B8";
static NSString *const TRADE_MARK = @"\u2122";

@interface MMAboutView : UIView

@property (nonatomic,retain) UIView* contentView;

@end
