//
//  MMAboutView.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMAboutView.h"
#import "Constants.h"
#import <QuartzCore/QuartzCore.h>

#define INDENT_X 10

@implementation MMAboutView

@synthesize contentView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
//        self.autoresizesSubviews = YES;
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        float portraitWidth = [UIScreen mainScreen].bounds.size.width;
        UIView* containerForMessage = [[UIView alloc]
                                initWithFrame:CGRectMake(HORIZONTAL_INDENT,
                                                         (self.frame.size.height - 160)/2,
                                                         portraitWidth - 2 * HORIZONTAL_INDENT,
                                                         160)];
        self.contentView = containerForMessage;
        self.contentView.center = self.center;
        containerForMessage.backgroundColor = [UIColor grayColor];
        [containerForMessage release];
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [self addSubview:contentView];
//        self.contentView.layer.borderColor = [UIColor whiteColor].CGColor;
//        self.contentView.layer.borderWidth = 1.0f;
        [self.contentView.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
        [self.contentView.layer setShadowColor:[UIColor whiteColor].CGColor];
        [self.contentView.layer setShadowRadius:5];
        [self.contentView.layer setShadowOpacity:3];
        
        
        
        UILabel* labelAbout = [[UILabel alloc] initWithFrame:CGRectMake(0,
                                                                        0,
                                                                        containerForMessage.frame.size.width,
                                                                        40)];
        labelAbout.text = @"   About";
        labelAbout.textAlignment = UITextAlignmentLeft;
        labelAbout.textColor = [UIColor whiteColor];
        labelAbout.backgroundColor = [UIColor blackColor];
        [self.contentView addSubview:labelAbout];
        [labelAbout release];
        
        UIImageView* logoImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo.png"]];
        logoImage.backgroundColor = [UIColor clearColor];
        [logoImage setFrame:CGRectMake(INDENT_X,
                                       labelAbout.frame.origin.y + labelAbout.frame.size.height,
                                       containerForMessage.frame.size.width - 2*INDENT_X,
                                       logoImage.frame.size.height)];
        [self.contentView addSubview:logoImage];
        [logoImage release];
        
        UILabel* labelAppVersion = [[UILabel alloc] initWithFrame:CGRectMake(INDENT_X,
                                                                             logoImage.frame.origin.y + logoImage.frame.size.height,
                                                                             containerForMessage.frame.size.width- 2*INDENT_X,
                                                                             30)];
        float appVersion = 1.0;
        labelAppVersion.text = [NSString stringWithFormat: @"Application Version: %.1f", appVersion];
        labelAppVersion.textAlignment = UITextAlignmentCenter;
        labelAppVersion.backgroundColor = [UIColor clearColor];
        labelAppVersion.textColor = [UIColor blackColor];
        [labelAppVersion setFont:[UIFont systemFontOfSize:FONT_SIZE]];
        [self.contentView addSubview:labelAppVersion];
        [labelAppVersion release];
        
        UILabel* labelCopyright = [[UILabel alloc] initWithFrame:CGRectMake(INDENT_X,
                                                                             labelAppVersion.frame.origin.y + labelAppVersion.frame.size.height,
                                                                             containerForMessage.frame.size.width- 2*INDENT_X,
                                                                             30)];
        labelCopyright.text = [NSString stringWithFormat:@" Copyright%@ 2012 SynapSense%@ Corporation.", COPY_RIGHT, TRADE_MARK];
        labelCopyright.textAlignment = UITextAlignmentLeft;
        labelCopyright.backgroundColor = [UIColor clearColor];
        labelCopyright.textColor = [UIColor blackColor];
        [labelCopyright setFont:[UIFont systemFontOfSize:FONT_SIZE]];
        [self.contentView addSubview:labelCopyright];
        [labelCopyright release];
        
        UILabel* labelAllRights = [[UILabel alloc] initWithFrame:CGRectMake(INDENT_X,
                                                                            labelCopyright.frame.origin.y + labelCopyright.frame.size.height,
                                                                            containerForMessage.frame.size.width- 2*INDENT_X,
                                                                            30)];
        labelAllRights.text = @" All rights reserved.";
        labelAllRights.textAlignment = UITextAlignmentLeft;
        labelAllRights.backgroundColor = [UIColor clearColor];
        labelAllRights.textColor = [UIColor blackColor];
        [labelAllRights setFont:[UIFont systemFontOfSize:FONT_SIZE]];
        [self.contentView addSubview:labelAllRights];
        [labelAllRights release];
    }
    return self;
}

- (void) dealloc
{
    self.contentView = nil;
    [super dealloc];
}

@end
