//
//  TransferringAlertView.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "TransferringAlertView.h"

@implementation TransferringAlertView

@synthesize delegate;

- (id)initWithFrame:(CGRect)frame withCancelButton:(BOOL)showCancelButton
{
    self = [super initWithFrame:frame];
    if (self) {
        alertView = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"transferringAlertViewTitle", @"") message:@"\n\n" delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"") otherButtonTitles: nil];
        [alertView setFrame:frame];
        //alertView = [[UIAlertView alloc] initWithFrame:frame];
        [alertView setTitle:NSLocalizedString(@"transferringAlertViewTitle", @"")];
        [alertView setMessage:@"\n\n"];
        [alertView setCancelButtonIndex:0];
        if (showCancelButton == YES)
        {
            [alertView addButtonWithTitle:NSLocalizedString(@"Cancel",@"")];
        }
        [alertView setDelegate:self];
        activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [alertView addSubview:activityIndicator];
        [activityIndicator setCenter:CGPointMake(140, 65)];
        [activityIndicator release];
    }
    return self;
}

- (id)init
{
    return [self initWithFrame:CGRectZero withCancelButton:NO];
}

- (void)showAlertView
{
    [alertView show];
    [activityIndicator startAnimating];
}

- (void)hideAlertView
{
    [alertView dismissWithClickedButtonIndex:0 animated:YES];
    [activityIndicator stopAnimating];
}

- (void)setTitle:(NSString*)titleText
{
    [alertView setTitle:titleText];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([delegate respondsToSelector:@selector(AlertCancelButtonClicked)])
    {
        [delegate AlertCancelButtonClicked];
    }
}

- (void)dealloc
{
    [alertView release];
    [super dealloc];
}

@end
