//
//  FacilityDashboardViewController.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "FacilityDashboardViewController.h"
#import "MMSDK.h"
#import "Constants.h"
#import "AppUtility.h"
#import "MMTitleLabel.h"
#import "MMSubtitleLabel.h"
#import "MMView.h"
#import "MMTableViewCell.h"

#define SECTION_TAG             1
#define PUE_COMPONENT_HEIGHT    150
#define PUE_COMPONENT_Y         215
#define SIZE_ICON               30
#define HEIGHT_HEADER           35
#define IDENTATION              20
#define ROW_HEIGHT              50
#define SEPARATOR_HEIGHT        1

@interface FacilityDashboardViewController(private)
- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;
- (void)updatePIEChartView;
@end

@implementation FacilityDashboardViewController

@synthesize array;
@synthesize sectionTableView;
@synthesize arrayObject;
@synthesize currentDataCenter;
@synthesize lastUpdateTime;

- (void)updateScreenInfo
{
    [super updateScreenInfo];
    if (errorAlert)
    {
        [errorAlert dismissWithClickedButtonIndex:errorAlert.cancelButtonIndex animated:NO];
        errorAlert = nil;
    }
    
    MMSDK *sdk = [MMSDK sharedInstance];
    __block int taskIdGetDashboardInfo = 0;
    __block int taskIdGetNumActAlerts = 0;
    taskIdGetDashboardInfo = [sdk getDataCenterDashboardInfo:self.currentDataCenter.guid success:^(MMDataCenterInfo *dcInfo)
     {
         [self taskFinished:taskIdGetDashboardInfo];
         taskIdGetNumActAlerts = [sdk getNumActiveAlerts:dcInfo.guid withPeriod:[NSNull null] withPriority:[NSNull null] withStatus:[NSNull null] success:^(int totalAlertsCount)
          {
              [self taskFinished:taskIdGetNumActAlerts];
              activeAlertsCount = totalAlertsCount;
              self.currentDataCenter = dcInfo;
              self.lastUpdateTime = [NSDate date];
              [self updatePIEChartView];
              if (self.sectionTableView == nil)
              {
                [self loadSections];
              }
              else
              {
                [self.sectionTableView reloadData];
              }
              self.viewDidAppear = YES;
              [self doneLoadingTableViewData];
              [self updateHasFinished];
          }
          failure:^(NSError *error)
          {
              [self taskFinished:taskIdGetNumActAlerts];
              [self updateHasFinished];
              if (_reloading == YES)
              {
                  [self doneLoadingTableViewData];
              }
              if (!errorAlert)
              {
                  errorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"updatingError", @"") message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self cancelButtonTitle:NSLocalizedString(@"okButtonTitle", @"") otherButtonTitles: nil];
                  [errorAlert show];
                  [errorAlert release];
              }
              NSLog(@"%@", error.localizedDescription);
          }
          ];
         [self addTaskId:taskIdGetNumActAlerts];
     }
     failure:^(NSError *error)
     {
         [self taskFinished:taskIdGetDashboardInfo];
         [self updateHasFinished];
         if (_reloading == YES)
         {
             [self doneLoadingTableViewData];
         }
         if (!errorAlert)
         {
             errorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"updatingError", @"") message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self cancelButtonTitle:NSLocalizedString(@"okButtonTitle", @"") otherButtonTitles: nil];
             [errorAlert show];
             [errorAlert release];
         }
         NSLog(@"%@", error.localizedDescription);
     }];
    [self addTaskId:taskIdGetDashboardInfo];
}

- (void)updatePIEChartView
{
    [PIEChartView release];
    PIEChartView = [[UIPIEChartView alloc] initWithInteger:0];
    NSNumber* it = [NSNumber numberWithDouble:self.currentDataCenter.itPower.value];
    NSNumber* cooling = [NSNumber numberWithDouble:self.currentDataCenter.coolingPower.value];
    NSNumber* build = [NSNumber numberWithDouble:self.currentDataCenter.powerLoss.value];
    NSNumber* light = [NSNumber numberWithDouble:self.currentDataCenter.lightingPower.value];
    NSNumber* infrastructure = [NSNumber numberWithDouble:self.currentDataCenter.infrastrucurePower.value];
    if ([self.currentDataCenter.pueType isEqualToString:@"full"])
    {
        [PIEChartView setPuetype:PUETYPE_FULL];
        self.array = [NSMutableArray arrayWithObjects:it, cooling, build, light, nil];
    }
    else if ([self.currentDataCenter.pueType isEqualToString:@"lite"])
    {
        [PIEChartView setPuetype:PUETYPE_LITE];
        self.array = [NSMutableArray arrayWithObjects:it, infrastructure, nil];
    }
    PIEChartView.array = [self solutionInPercent];
    PIEChartView.userInteractionEnabled = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view = [[[MMView alloc] initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, VIEW_HEIGHT) viewLevel:ViewLevelSecond]autorelease];
    self.tabBarController.navigationItem.title = NSLocalizedString(@"applicationTitle", @"");
    UIBarButtonItem *backButton = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"backButtonTitle", @"") style:UIBarButtonItemStyleBordered target:nil action:nil] autorelease];
    self.tabBarController.navigationItem.backBarButtonItem = backButton;
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:[self loadHeaderWithTitle:[currentDataCenter name] titleWidth:self.view.frame.size.width viewLevel:ViewLevelSecond]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.sectionTableView = nil;
    self.arrayObject = nil;
    self.currentDataCenter = nil;
    self.lastUpdateTime = nil;
    _refreshHeaderView = nil;    
}

- (void) dealloc
{
    [PIEChartView release];
    self.array = nil;
    self.lastUpdateTime = nil;
    _refreshHeaderView = nil;
    self.currentDataCenter = nil;
    self.arrayObject = nil;
    self.sectionTableView = nil;
    [super dealloc];
}

- (id)initWithDataCenter:(MMDataCenterInfo*)dataCenter
{
    self = [super init];
    if (self)
    {
        self.currentDataCenter = dataCenter;
    }
    return self;
}


- (void)loadSections
{   
    sectionTableView = [[MMTableView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, HEIGHT_HEADER,
                                                                    self.view.frame.size.width,
                                                                    self.view.frame.size.height)
                                                                    style:UITableViewStylePlain];
	sectionTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    sectionTableView.showsVerticalScrollIndicator = YES;
    sectionTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [sectionTableView setScrollEnabled:YES];

	[self.view addSubview:sectionTableView];

    sectionTableView.delegate = self;
    sectionTableView.dataSource = self;
    self.lastUpdateTime = [NSDate date];
    if (_refreshHeaderView == nil)
    {
        EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - sectionTableView.bounds.size.height, self.view.frame.size.width, sectionTableView.bounds.size.height)];
        view.delegate = self;
        [view setDateFormat:[AppDelegate sharedInstance].userPreferences.dateFormat];
        
        [sectionTableView addSubview:view];
        [view release];
        
        _refreshHeaderView = view;
        
	}

     NSMutableArray *arraySection = [[NSMutableArray alloc] init];

    [arraySection addObject:NSLocalizedString(@"activeAlerts", @"")];
    [arraySection addObject:NSLocalizedString(@"racks", @"")];
    [arraySection addObject:NSLocalizedString(@"coolingUnits", @"")];
    [arraySection addObject:NSLocalizedString(@"estimatedEnergyUsage", @"")];
    [arraySection addObject:NSLocalizedString(@"PUE", @"")];

	 self.arrayObject = arraySection;
     [arraySection release];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    if ([AppDelegate sharedInstance].uiPrivileges.showPUE)
    {
        return [self.arrayObject count] + 1;
    }
    return 3;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *)indexPat
{
    if (indexPat.row == 5)
        return 200;
    else
        return ROW_HEIGHT;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [sectionTableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	MMTableViewCell *cell = [[[MMTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"] autorelease];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setBackgroundColor:[UIColor clearColor]];
    //TO-DO: refactor
    NSNumber* activeAlerts = [NSNumber numberWithInt:activeAlertsCount]; 
    NSNumber* racks = [NSNumber numberWithInt:self.currentDataCenter.totalRacksNumber];
    NSNumber* coolingUnits = [NSNumber numberWithInt:self.currentDataCenter.totalCracsNumber];
    NSNumber* pue = [NSNumber numberWithDouble:self.currentDataCenter.pue.value];
    NSNumber* estimatedEnergyUsage = [NSNumber numberWithDouble:self.currentDataCenter.estimatedEnergyUsage.value];
    
    NSMutableArray* arrayFacility = [NSMutableArray arrayWithObjects:activeAlerts, racks, coolingUnits, estimatedEnergyUsage, pue, nil];
    switch (indexPath.row)
    {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        {
            cell.textLabel.text = [self.arrayObject objectAtIndex:indexPath.row];
            if(indexPath.row == 3)
            {
                double estimatedEnergyValue = [[arrayFacility objectAtIndex:indexPath.row] doubleValue];
                NSString *units = nil;
                if (estimatedEnergyValue == 0)
                {
                    units = @"";
                }
                else
                {
                    //if value > 10000 then kWh -> MWh
                    if (estimatedEnergyValue > 10000)
                    {
                        estimatedEnergyValue /= 10000;
                        units = NSLocalizedString(@"MWh", @"");
                    }
                    else
                    {
                        units = NSLocalizedString(@"kWh", @"");
                    } 
                }
                NSString *value =  estimatedEnergyValue != 0 ? [[AppDelegate sharedInstance].userPreferences formatDouble:estimatedEnergyValue] : @"N/A";
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@%@%@",value, units != @"" ? @" " : @"", units];
            }
            else if (indexPath.row == 4)
            {
                NSString *value = [[arrayFacility objectAtIndex:indexPath.row] doubleValue] != 0 ? [[AppDelegate sharedInstance].userPreferences formatDouble:[[arrayFacility objectAtIndex:indexPath.row] doubleValue]] : @"N/A";
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",value];
            }
            else
            {
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%d",[[arrayFacility objectAtIndex:indexPath.row] intValue]];
            }
            UILabel *_separator = [[UILabel alloc] initWithFrame:CGRectMake(HORIZONTAL_INDENT, ROW_HEIGHT - SEPARATOR_HEIGHT, cell.frame.size.width - 2*HORIZONTAL_INDENT, SEPARATOR_HEIGHT)];
            [_separator setBackgroundColor:[UIColor darkGrayColor]];
            _separator.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            [cell addSubview:_separator];
            [_separator release];
        }
            break;
        case 5:
        {
            if (!PIEChartView.array)
            {
                break;
            }
            UIView* view = [[UIView alloc] init];
            view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y,
                                    self.view.frame.size.width , PUE_COMPONENT_HEIGHT);
            view.backgroundColor = [UIColor clearColor];
            [view addSubview:PIEChartView];
            NSArray *units = nil;
            NSArray *itemTitles = nil;
            NSArray *colors = nil;
            if ([self.currentDataCenter.pueType isEqualToString:@"full"])
            {
                units = [NSArray arrayWithObjects: [AppUtility safeString:self.currentDataCenter.itPower.unit], [AppUtility safeString:self.currentDataCenter.coolingPower.unit], [AppUtility safeString:self.currentDataCenter.powerLoss.unit], [AppUtility safeString:self.currentDataCenter.lightingPower.unit], nil];
                itemTitles = [NSArray arrayWithObjects:NSLocalizedString(@"itWithColon", @""), NSLocalizedString(@"coolingWithColon", @""), NSLocalizedString(@"buildingAndITPowerLossWithColon", @""), NSLocalizedString(@"lightingAndMiscLoadsWithColon", @""), nil];
                colors = [NSArray arrayWithObjects:PIECHART_RED_COLOR, PIECHART_BLUE_COLOR, PIECHART_YELLOW_COLOR, PIECHART_GREEN_COLOR, nil];
            }
            else if ([self.currentDataCenter.pueType isEqualToString:@"lite"])
            {
                units = [NSArray arrayWithObjects: [AppUtility safeString:self.currentDataCenter.itPower.unit], [AppUtility safeString:self.currentDataCenter.infrastrucurePower.unit], nil];
                itemTitles = [NSArray arrayWithObjects:NSLocalizedString(@"itWithColon", @""), NSLocalizedString(@"infrastructurePower", @""), nil];
                colors = [NSArray arrayWithObjects:PIECHART_RED_COLOR, PIECHART_GRAY_COLOR, nil];
            }
            int labelY = 10;
            for (int i = 0; i < array.count; i++)
            {
                MMTitleLabel *labelItem = [[MMTitleLabel alloc]initWithFrame:CGRectMake(155, labelY, [UIScreen mainScreen].bounds.size.width - 155 - HORIZONTAL_INDENT, 30)];
                labelItem.backgroundColor = [UIColor clearColor];
                NSString *valueOfItem = [[array objectAtIndex:i] doubleValue] != 0 ? [NSString stringWithFormat:@"%d",(int)roundf([[array objectAtIndex:i] doubleValue])] : @"N/A";
                int percent = (int)roundf([[PIEChartView.array objectAtIndex:i] doubleValue] * 100);
                NSString *itemString = [NSString stringWithFormat:@"%@ %@ %@ (%d %%)", [itemTitles objectAtIndex:i], valueOfItem, [units objectAtIndex:i], percent];
                labelItem.text = itemString;
                labelItem.font = [UIFont boldSystemFontOfSize:10];
                labelItem.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
                labelItem.numberOfLines = 2;
                
                UIView *viewItem = [[UIView alloc]initWithFrame:CGRectMake(labelItem.frame.origin.x - 15, labelItem.frame.origin.y + 10, 10, 10)];
                viewItem.backgroundColor = [colors objectAtIndex:i];
                viewItem.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
                [view addSubview:labelItem];
                [view addSubview:viewItem];
                labelY += labelItem.frame.size.height;
                [labelItem release];
                [viewItem release];
            }
            [view setNeedsLayout];
            [cell.contentView addSubview:view];
            [view release];
        }
            break;
    }
    return cell;
}

-(NSMutableArray*)solutionInPercent
{
    NSMutableArray* arrayPercent = [NSMutableArray array];
    double divider = 0;
    for (int i = 0; i < array.count; i++)
    {
        divider += [[array objectAtIndex:i] doubleValue];
    }
    if (divider == 0)
    {
        return nil;
    }
    for (int i = 0; i < array.count; i++)
    {
        double elementValue = [[array objectAtIndex:i] doubleValue];
        [arrayPercent addObject:[NSNumber numberWithFloat:fabs(elementValue/divider)]];
    }
    return arrayPercent;
}



- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return  YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateScreenInfo];
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
	_reloading = YES;
}

- (void)doneLoadingTableViewData{
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:sectionTableView];
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{	
	
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
	
	[self reloadTableViewDataSource];
    [self updateScreenInfo];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	
	return _reloading;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	
	return self.lastUpdateTime;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (errorAlert)
    {
        errorAlert = nil;
    }
}

- (void)cancelCurrentConnections
{
    if (_reloading == YES)
    {
        [self doneLoadingTableViewData];
    }
    [super cancelCurrentConnections];
}

@end
