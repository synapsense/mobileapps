//
//  TransferringAlertView.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TransferringAlertViewDelegate <NSObject>

- (void)AlertCancelButtonClicked;

@end

@interface TransferringAlertView : UIView <UIAlertViewDelegate>
{
    UIAlertView *alertView;
    UIActivityIndicatorView *activityIndicator;
}

@property(nonatomic,assign)id<TransferringAlertViewDelegate>delegate;

-(void)showAlertView;
-(void)hideAlertView;
- (void)setTitle:(NSString*)titleText;

@end
