//
//  MMProgressPopupView.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMProgressPopupView : UIView

@property (nonatomic, retain) UIActivityIndicatorView* indicatorView;

@end
