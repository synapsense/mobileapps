//
//  FacilitiesViewController.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FacilitiesViewCell.h"
#import "BaseViewController.h"
#import "EGORefreshTableHeaderView.h"
#import <MapKit/MapKit.h>
#import "Constants.h"
#import "SettingsViewController.h"
#import "MMTableView.h"

@class SettingsViewController;

@interface FacilitiesViewController : BaseViewController<UITableViewDelegate, UITableViewDataSource, EGORefreshTableHeaderDelegate, UIAlertViewDelegate, MKMapViewDelegate, SettingsViewControllerDelegate>
{
    MMTableView *aTableView;
    UIAlertView *errorAlert;
    EGORefreshTableHeaderView *_refreshHeaderView;
	BOOL _reloading;
    MKMapView* mapView;
    MKCoordinateRegion _mapRegion;
}

- (id)initWithDataCenters:(NSArray*)dataCenters;
- (void)loadTableWithFacilities;
- (void)loadMapWithFacilities;

@property (nonatomic, retain) NSArray* dataCenters;
@property (nonatomic,retain) NSDate *lastUpdateTime;
@property (nonatomic, assign) BOOL mapPresentationForFacilitiesVC;
@property (nonatomic, retain) MKMapView* mapView;
@property (nonatomic, retain) UITableView *aTableView;

@end

@interface MapAnnotation : NSObject <MKAnnotation>
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic) NSUInteger tag;

@end


@interface CustomMKAnnotationView : MKAnnotationView

@end

