//
//  SettingsViewController.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "OptionsSettingsViewController.h"
#import "BaseViewController.h"
#import "Constants.h"
#import "MMUnderlinedLabel.h"
#import "MMTableView.h"

typedef enum
{
    SETTINGS_TYPE_FULL,
    SETTINGS_TYPE_LITE
}SettingsType;

@protocol SettingsViewControllerDelegate <NSObject>

//- (void)updateFacilitiesTab:(BOOL)showMap;

@end

@interface SettingsViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, MMUnderlinedLabelDelegate, UIAlertViewDelegate>
{
    NSArray                 *settingsTableDataArray;
    MMTableView             *tableView;
    UISwitch                *_popUpNotificationOptionOnOff;
    UISwitch                *_mapPresentationForFacilities;
    UISwitch                *useSecure;
    UISwitch                *nightTheme;
    MMUnderlinedLabel       *portLabel;
    BOOL                    facilitiesPresentationChanged;
    UIView*                 _aboutMessage;
    UIButton*               _buttonWithBigArea;
}

@property (nonatomic) SettingsType settingsType;
@property (nonatomic, assign) id<SettingsViewControllerDelegate> delegate;

@end


