//
//  TextFieldAlertView.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "TextFieldAlertView.h"

@implementation TextFieldAlertView


- (id)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...
{
    if (self = [super initWithTitle:title message:message delegate:delegate cancelButtonTitle:cancelButtonTitle otherButtonTitles:otherButtonTitles, nil])
    {
        if (!message)
        {
            self.message = @"\n\n";
        }
        else
        {
            self.message = [self.message stringByAppendingFormat:@"\n\n\n"];
        }
        _message = message;
        textField = [[UITextField alloc]init];
        [textField setFont:[UIFont systemFontOfSize:18]];
        [textField setTextColor:[UIColor blackColor]];
        [textField becomeFirstResponder];
        [textField setBackgroundColor:[UIColor whiteColor]];
        [textField setBorderStyle:UITextBorderStyleLine];
        [textField setKeyboardAppearance:UIKeyboardAppearanceAlert];
        [textField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        [self addSubview:textField];
    }
    return self;
}

- (UITextField*)textFieldAtIndex:(NSInteger)textFieldIndex
{
    return textField;
}

- (void)setKeyboardType:(UIKeyboardType)type
{
    [textField setKeyboardType:type];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation))
    {
        if (!_message)
        {
            [textField setFrame:CGRectMake(20, 53, 240, 30)];
        }
        else
        {
            [textField setFrame:CGRectMake(20, 80, 240, 30)];
        }
    }
    else
    {
        if (!_message)
        {
            [textField setFrame:CGRectMake(20, 40, 240, 30)];
        }
        else
        {
            [textField setFrame:CGRectMake(20, 65, 240, 30)];;
        }
    }
}

- (void)dealloc
{
    [textField release];
    [super dealloc];
}

@end
