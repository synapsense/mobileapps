//
//  MMTableView.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMTableView.h"
#import "MMNightTheme.h"
#import <QuartzCore/QuartzCore.h>

@implementation MMTableView

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:style];
    if (self) 
    {
        [[ThemeManager sharedInstance] registerObserver:self];
        [self adjustUI:[ThemeManager sharedInstance].currentTheme];
        if (style == UITableViewStyleGrouped)
        {
            self.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;            
        }
    }
    return self;
}

- (id)init
{
    return [self initWithFrame:CGRectZero style:UITableViewStylePlain];
}

- (void)dealloc
{
    [[ThemeManager sharedInstance]unregisterObserver:self];
    [super dealloc];
}

- (void)adjustUI:(MMDefaultTheme *)currentTheme
{
    if (self.style == UITableViewStyleGrouped)
    {
        self.backgroundColor = currentTheme.tableViewGroupedBackgroundColor;
        if ([currentTheme isKindOfClass:[MMNightTheme class]])
        {
            [self.backgroundView setHidden:YES];
        }
        else
        {
            [self.backgroundView setHidden:NO];
        }

        self.separatorColor = currentTheme.separatorTableViewColor;
    }
    else
    {
        self.backgroundColor = [UIColor clearColor];;
    }
}

@end