//
//  DownloadingResultsCell.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "DownloadingResultsCell.h"
#import "AppUtility.h"

#define DOWNLOADING_RESULT_CELL_BUTTON_HEIGHT   30.0
#define DOWNLOADING_RESULT_CELL_BUTTON_WIDTH    80.0
#define DOWNLOADING_RESULT_CELL_BOUNDS   10.0
#define DOWNLOADING_RESULT_CELL_MARGIN  15.0
#define DOWNLOADING_RESULT_CANCEL_BUTTON_SIZE   40
#define DOWNLOADING_RESULT_CANCEL_BUTTON_INSET   7.5

@interface DownloadingResultsCell()

@property (nonatomic, retain) UILabel* topMessageLabel;
@property (nonatomic, retain) UILabel* bottomMessageLabel;
@property (nonatomic, retain) UIActivityIndicatorView* spinner;
@property (nonatomic, retain) UIButton* cancelButton;
@property (nonatomic, retain) UIView* topSeparator;
@property (nonatomic, retain) UIView* bottomSeparator;

@end


@implementation DownloadingResultsCell

@synthesize topMessageLabel;
@synthesize bottomMessageLabel;
@synthesize spinner;
@synthesize cancelButton;
@synthesize topSeparator;
@synthesize bottomSeparator;

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        self.clipsToBounds = YES;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        topMessageLabel = [[UILabel alloc] init];
        topMessageLabel.font = [UIFont systemFontOfSize:14.0];
        topMessageLabel.textColor = [UIColor colorWithRed:87.0/255.0 green:108.0/255.0 blue:137.0/255.0 alpha:1.0];
        topMessageLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:topMessageLabel];
        
        CGRect frame = self.bounds;
        frame.size.height = 0.75;
        bottomSeparator = [[UIView alloc] initWithFrame:frame];
        bottomSeparator.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        frame.origin.y = 0.75;
        topSeparator = [[UIView alloc] initWithFrame:frame];
        topSeparator.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        [self addSubview:topSeparator];
        [self addSubview:bottomSeparator];
    }
    return self;
}

- (void) dealloc
{
    self.topMessageLabel = nil;
    self.bottomMessageLabel = nil;
    self.spinner = nil;
    self.cancelButton = nil;
    self.topSeparator = nil;
    self.bottomSeparator = nil;
    [super dealloc];
}

- (void) setTopMessage:(NSString*)text
{
    topMessageLabel.text = text;
}

- (void) setBottomMessage:(NSString*)text
{
    if (text)
    {
        //lazy initialization
        if (!bottomMessageLabel)
        {
            bottomMessageLabel = [[UILabel alloc] init];
            bottomMessageLabel.font = [UIFont systemFontOfSize:14.0];
            bottomMessageLabel.textColor = [UIColor whiteColor];
            bottomMessageLabel.backgroundColor = [UIColor clearColor];
            [self addSubview:bottomMessageLabel];
        }
        bottomMessageLabel.text = text;
    }
    else
    {
        [bottomMessageLabel removeFromSuperview];
        self.bottomMessageLabel = nil;
    }
}

- (void) showSpinner:(BOOL)show
{
    if ((spinner && show) || (!spinner && !show))
        return;
    
    if (show)
    {
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinner startAnimating];
        [self addSubview:spinner];
    }
    else
    {
        [spinner removeFromSuperview];
        self.spinner = nil;
    }
}

- (void) showCancelButton:(BOOL)show target:(id)target action:(SEL)action
{
    if ((cancelButton && show) || (!cancelButton && !show))
        return;
    
    if (show)
    {
        //Create cancel button
        self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage* image = [UIImage imageNamed:@"bar_cancel_btn.png"];
        [cancelButton setImage:image forState:UIControlStateNormal];
        [cancelButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
        [cancelButton setImageEdgeInsets:UIEdgeInsetsMake(DOWNLOADING_RESULT_CANCEL_BUTTON_INSET, DOWNLOADING_RESULT_CANCEL_BUTTON_INSET, DOWNLOADING_RESULT_CANCEL_BUTTON_INSET, DOWNLOADING_RESULT_CANCEL_BUTTON_INSET)];
        cancelButton.frame = CGRectMake(0, 0, DOWNLOADING_RESULT_CANCEL_BUTTON_SIZE, DOWNLOADING_RESULT_CANCEL_BUTTON_SIZE);
        [self addSubview:cancelButton];
    }
    else
    {
        [cancelButton removeFromSuperview];
        self.cancelButton = nil;
    }
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    self.backgroundColor = [UIColor colorWithRed:226.0/255.0 green:231.0/255.0 blue:237.0/255.0 alpha:1.0];
    
    CGSize topTextSize = [topMessageLabel.text sizeWithFont:topMessageLabel.font];
    
    if (spinner)
    {
        spinner.frame = CGRectMake((self.bounds.size.width - topTextSize.width - DOWNLOADING_RESULT_CELL_BOUNDS - spinner.frame.size.width) / 2,
                                   (self.bounds.size.height - spinner.frame.size.height) / 2,
                                   spinner.frame.size.width, spinner.frame.size.height);
        
        topMessageLabel.frame = CGRectMake(spinner.frame.origin.x + spinner.frame.size.width + DOWNLOADING_RESULT_CELL_BOUNDS,
                                           (self.bounds.size.height - topTextSize.height) / 2,
                                           topTextSize.width, topTextSize.height);
        if (cancelButton)
        {
            cancelButton.center = CGPointMake(self.bounds.size.width - DOWNLOADING_RESULT_CANCEL_BUTTON_SIZE + 2 * DOWNLOADING_RESULT_CANCEL_BUTTON_INSET, self.bounds.size.height / 2);
            
            CGFloat xDiff = topMessageLabel.frame.origin.x + topMessageLabel.frame.size.width - DOWNLOADING_RESULT_CELL_BOUNDS - cancelButton.frame.origin.x;
            if (xDiff > 0.0)
            {
                CGFloat spinnerXoffset = spinner.frame.origin.x - DOWNLOADING_RESULT_CELL_BOUNDS - xDiff;
                if (spinnerXoffset >= DOWNLOADING_RESULT_CELL_BOUNDS)
                {
                    CGRect newFrame = spinner.frame;
                    newFrame.origin.x = spinnerXoffset;
                    spinner.frame = newFrame;
                    
                    newFrame = topMessageLabel.frame;
                    newFrame.origin.x = topMessageLabel.frame.origin.x - DOWNLOADING_RESULT_CELL_BOUNDS - xDiff;
                    topMessageLabel.frame = newFrame;
                }
                else
                {
                    CGRect newFrame = spinner.frame;
                    newFrame.origin.x = DOWNLOADING_RESULT_CELL_BOUNDS;
                    spinner.frame = newFrame;
                    
                    newFrame = topMessageLabel.frame;
                    newFrame.origin.x = spinner.frame.origin.x + spinner.frame.size.width + DOWNLOADING_RESULT_CELL_BOUNDS;
                    newFrame.size.width = cancelButton.frame.origin.x - newFrame.origin.x - DOWNLOADING_RESULT_CELL_BOUNDS;
                    topMessageLabel.frame = newFrame;
                }
            }
        }
    }
    else
    {
        topMessageLabel.frame = CGRectMake((self.bounds.size.width - topTextSize.width) / 2,
                                           (self.bounds.size.height - topTextSize.height) / 2,
                                           topTextSize.width, topTextSize.height);
    }
}

@end
