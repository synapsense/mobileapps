//
//  MMProgressPopupView.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMProgressPopupView.h"

@implementation MMProgressPopupView

@synthesize indicatorView;

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        self.indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        self.indicatorView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin |UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [self.indicatorView release];
        [self addSubview:self.indicatorView];
        [self.indicatorView setCenter:self.center];
        [self.indicatorView startAnimating];
    }
    return self;
}

- (void)dealloc
{
    self.indicatorView = nil;
    [super dealloc];
}

@end
