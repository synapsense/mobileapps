//
//  MMTextView.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMTextView.h"
#import <QuartzCore/QuartzCore.h>

@implementation MMTextView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [[ThemeManager sharedInstance] registerObserver:self];
        self.layer.borderWidth = 1.0f;
        [self adjustUI:[ThemeManager sharedInstance].currentTheme];
        //[self setDelegate:self];
    }
    return self;
}

- (void)dealloc
{
    [[ThemeManager sharedInstance]unregisterObserver:self];
    [super dealloc];
}

- (void)adjustUI:(MMDefaultTheme *)currentTheme
{
    self.textColor = currentTheme.textFieldTextColor;
    self.layer.borderColor = currentTheme.textFieldBorderColor.CGColor;
}

@end
