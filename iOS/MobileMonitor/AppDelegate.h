//
//  AppDelegate.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MMAlertInfo.h"
#import "Constants.h"
#import "MMFloorPlanInfo.h"
#import "MMUserPreferences.h"
#import "MMSDK.h"
#import "MMActivityIndicatorViewController.h"
#import "ThemeManager.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    UIWindow* _popupWindow;
    UIViewController* _loginViewController;
    NSTimer *timerDataUpdater;
    MKCoordinateRegion _mapCoordinateRegion;
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navigaionController;

@property (nonatomic, retain) NSTimer *timerDataUpdater;

@property (nonatomic, retain) NSUserDefaults *applicationPreferences;

@property (nonatomic, assign) MMAlertStatus alertStatusFilter;

@property (nonatomic, assign) MMAlertPriority alertPriorityFilter;

@property (nonatomic, assign) MMAlertSort alertSorting;

@property (nonatomic, assign) MMFloorPlanFilter floorPlanFilter;

@property (nonatomic, retain) MMUserPreferences *userPreferences;

@property (nonatomic) MKCoordinateRegion mapCoordinateRegion;

@property (nonatomic) NSUInteger securedPort;
@property (nonatomic) NSUInteger unsecuredPort;

@property MMUIPrivileges uiPrivileges;
           
+ (AppDelegate *)sharedInstance;

- (void)startTimerUpdate;
- (void)stopTimerUpdate;

- (void)showUpdatingPopup;
- (void)hideUpdatingPopup;

-(void)checkViews:(NSArray *)subviews;


@end
