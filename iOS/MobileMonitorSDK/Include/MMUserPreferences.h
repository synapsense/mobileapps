//
//  MMUserPreferences.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMUserPreferences : NSObject

@property(nonatomic,retain) NSString* decimalSeparator;
@property(nonatomic,retain) NSString* dateFormat;
@property(nonatomic,retain) NSString* savedLanguage;
@property(nonatomic,retain) NSString* unitsSystem;
@property int decimalFormat;

- (NSString*)formatDouble:(double)number;
- (NSString*)dateFormat;

@end
