//
//  MMSDK.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMUserInfo.h"
#import "MMDataCenterInfo.h"
#import "MMComponentStatus.h"
#import "MMFloorPlanInfo.h"
#import "MMFloorPlanElement.h"
#import "MMAlertInfo.h"
#import "MMUserPreferences.h"
#import "MMEmergencyContact.h"
#import "DSJSONRPC.h"
#import "MMFloorPlanInfo.h"

#define BINARY_UNIT_SIZE 3
#define BASE64_UNIT_SIZE 4

enum MMRequestInfoFlags
{
    MMRINone            = 0,
    MMRITimestamp       = 1 << 0,
    MMRIUnits           = 1 << 1,
    MMRIHistorical      = 1 << 2,
    MMRILink            = 1 << 3,
    MMRIMappedName      = 1 << 4,
    MMRIMappedValue     = 1 << 5,
    MMRIFilerHistAndTO  = 1 << 6     //show only historical or TO (link to object).
};


//typedef struct
//{
//    NSInteger loginRequestId;
//    NSInteger getNumWSNWarningsRequestId;
//    NSInteger getActiveAlertsId;
//    NSInteger getNumActiveAlertsId;
//    NSInteger getDataCentersId;
//    NSInteger getSystemStatusId;
//    NSInteger getDataCenterDashboardInfoId;
//    NSInteger getFloorPlanInfoId;
//    NSInteger getFloorPlanObjectInfoId;
//    NSInteger getEmergencyContactId;
//    NSInteger getFloorPlanData;
//    NSInteger getFloorPlan;
//    NSInteger getUIPrivileges;
//}ids;

typedef  struct {
    BOOL showSystemHealth;
    BOOL showPUE;
}MMUIPrivileges;

typedef enum
{    
    LiveImagingTypeOff,
    LiveImagingTypeOn

}LiveImagingTypeSwitch;

//typedef enum
//{
//    LoginRequest,
//    GetNumWSNWarnings,
//    GetActiveAlerts,
//    GetNumActiveAlerts,
//    GetDataCenters,
//    GetSystemStatus,
//    GetDataCenterDashboardInfo,
//    GetFloorPlanInfo,
//    GetFloorPlanData,
//    GetFloorPlan,
//    GetEmergencyContact,
//    GetFloorPlanObjectInfo,
//    GetUIPrivileges
//}RequestTypes;

@interface MMSDK : NSObject
{
    NSString* _serviceBaseURL;
    MMUserInfo* _userInfo;
    NSString* _csrfNonce;   /* Contains token for protection atainst CSRF attacks */
//    ids connectionIds;

    DSJSONRPC* currentRequestManager;
    NSInteger   currentRequestId;
    
    DSJSONRPC* jsonRPCUserManager;
    DSJSONRPC* jsonRPCObjectManager;
    DSJSONRPC* jsonRPCAlertManager;
    DSJSONRPC* jsonRPCStatusManager;
    DSJSONRPC* jsonRPCSystemManager;
    DSJSONRPC* jsonRPCConfigManager;
    LiveImagingTypeSwitch liveImagingTypeSwitch;
}
+ (MMSDK*)sharedInstance;

+ (void)setConnectionIsSecure: (BOOL)isSecure;

+ (void)setUIPrivileges: (MMUIPrivileges)uiPriveleges;

+ (void)setPort: (int)port;

- (NSString*)serviceBaseURL;

- (void)cancelRequests:(NSMutableArray*)requestIds;

- (int)getNumWSNWarningsWithSuccess:(void(^)(int count))success 
                  failure:(void(^)(NSError *error))failure;

- (int)getActiveAlerts:(id)objId withPeriod:(id)period withPriority:(id)priority withStatus:(id)status withStart:(id)start withCount:(id)count success:(void(^)(NSArray *activeAlerts))success 
                failure:(void(^)(NSError *error))failure;

- (int)changeAlertState:(int)alertId withState:(MMAlertStatus)state comment:(NSString*)comment success:(void(^)(BOOL isChanged))success 
                failure:(void(^)(NSError *error))failure;

- (int)getNumActiveAlerts:(NSString*)objId withPeriod:(id)period withPriority:(id)priority withStatus:(id)status success:(void(^)(int count))success 
                   failure:(void(^)(NSError *error))failure;

- (BOOL)isDeviceRegistredOnService:(NSString*)rawServiceURL;

- (int)login:(NSString*)login withPassword:(NSString*)password withServiceURL:(NSString*)serviceURL success:(void(^)(MMUserInfo *userInfo, MMUserPreferences *userPreferences))success
                 failure:(void(^)(NSError *error))failure;

- (int)logoutWithSuccess:(void(^)(BOOL isLogout))success 
      failure:(void(^)(NSError *error))failure;

- (int)getDataCentersWithSuccess:(void(^)(NSArray *dataCentersInfo))success 
                          failure:(void(^)(NSError *error))failure;

- (int)getSystemStatusWithSuccess:(void(^)(NSArray *componentStatusInfo))success 
                          failure:(void(^)(NSError *error))failure;

- (int)getDataCenterDashboardInfo:(NSString*)datacenterId success:(void(^)(MMDataCenterInfo *info))success 
failure:(void(^)(NSError *error))failure;

- (int)getFloorPlanInfo:(NSString*)datacenterId withImage:(Boolean)isImageNeeded success:(void(^)(MMFloorPlanInfo *))success  
                 failure:(void(^)(NSError *error))failure;

- (int)getFloorPlanObjectInfo:(NSString*)objectId withType:(NSString*)type success:(void(^)(NSMutableArray *, NSString*))success
                       failure:(void(^)(NSError *error))failure;

- (int)getLiveImagingTypesWithSuccess:(void(^)(NSArray *))success
                       failure:(void(^)(NSError *error))failure;

- (int)getFloorPlan:(NSString*)datacenterId DataClassID:(int)dataClassId Layer:(int)layer success:(void(^)(UIImage *))success
                               failure:(void(^)(NSError *error))failure;

- (int)decodeBase64:(NSString* )input p_output:(char *)decodedData;

- (void)initManagers;

- (void)setLiveImagingSwitch:(LiveImagingTypeSwitch)state;

+ (NSString *)getUUID;

- (int)getEmergencyContact: (NSString*)datacenterId  success:(void(^)(MMEmergencyContact *))success
                           failure:(void(^)(NSError *error))failure;

- (int)getFloorPlanData:(NSString*)datacenterId filterMask:(MMFloorPlanFilter)filterMask success:(void(^)(MMFloorPlanInfo *))success  
                 failure:(void(^)(NSError *error))failure;

- (int)getFloorplanSchemeImage:(NSString*)dataCenterId lastUpdateDate:(NSDate*)date success:(void(^)(UIImage *))success
                        failure:(void(^)(NSError *error))failure;

- (int)getFloorPlanSize:(NSString*)datacenterId success:(void(^)(CGSize))success  
                 failure:(void(^)(NSError *error))failure;

- (int)getUIPrivileges:(void(^)(MMUIPrivileges))success;
@end
