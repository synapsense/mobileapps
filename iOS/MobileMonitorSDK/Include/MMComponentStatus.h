//
//  MMComponentStatus.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//
#import <Foundation/Foundation.h>

enum MMComponent
{
    DEVICE_MANAGER,
    IMAGING_SERVER,
    ENVIRONMENTAL_SERVER,
    ACTIVE_CONTROL
};

enum MMStatus
{
    UNKNOWN,
    STOPPED,
    RUNNING,
    PARTIAL
};
@interface MMComponentStatus : NSObject

@property enum MMComponent component;
@property enum MMStatus status;
@property (nonatomic,retain) NSString* message;

@end
