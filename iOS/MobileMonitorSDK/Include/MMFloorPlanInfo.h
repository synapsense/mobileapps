//
//  MMFloorPlanInfo.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum{
    MMFloorPlan_NO              = 0,
    MMFloorPlan_RACK            = (1 << 0),
    MMFloorPlan_CRAC            = (1 << 1),
    MMFloorPlan_DOOR            = (1 << 2),
    MMFloorPlan_PRESSURE        = (1 << 3),
    MMFloorPlan_POWER_METER     = (1 << 4),
    MMFloorPlan_GENERIC_TEMP    = (1 << 5),
    MMFloorPlan_VERTICAL_TEMP   = (1 << 6),
    MMFloorPlan_EQUIPMENT       = (1 << 7),
    MMFloorPlan_LEAK            = (1 << 8)
} MMFloorPlanFilter;

@interface FloorPlanElement : NSObject

@property (nonatomic, retain) NSString* element_id;
@property (nonatomic, retain) NSString* name;
@property (nonatomic) int status;
@property (nonatomic) int num_alerts;
@property (nonatomic) double x;
@property (nonatomic) double y;
@property (nonatomic) int door;

@end

@interface MMFloorPlanInfo : NSObject

@property (nonatomic) double width;
@property (nonatomic) double height;
@property (nonatomic, retain) NSMutableArray *RACK;
@property (nonatomic, retain) NSMutableArray *CRAC;
@property (nonatomic, retain) NSMutableArray *pressure;
@property (nonatomic, retain) NSMutableArray *ion_wye;
@property (nonatomic, retain) NSMutableArray *ion_delta;
@property (nonatomic, retain) NSMutableArray *doors;
@property (nonatomic, retain) NSMutableArray *ion_wireless;
@property (nonatomic, retain) NSMutableArray *vert_temp;
@property (nonatomic, retain) NSMutableArray *generic_temp;
@property (nonatomic, retain) NSMutableArray *leak;
@property (nonatomic, retain) NSMutableArray *equipment_status;
@property (nonatomic, retain) UIImage *image;

-(id)init;
-(void)dealloc;

@end
