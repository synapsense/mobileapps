//
//  MMliveImagingType.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMliveImagingType : NSObject

@property (nonatomic, retain) NSString* name;
@property int dataClass;
@property int layer;

@end
