//
//  MMFloorPlanElement.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FPObjectDetails : NSObject

@property (nonatomic, retain) NSString *valueName;
@property (nonatomic, retain) NSString *lastValue;
@property (nonatomic, retain) NSString *valueUnits;
@property (nonatomic, assign) int dataClass;

@end
