//
//  UserInfo.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    MMLS_Ok = 0,
    MMLS_Failed = 1,
    MMLS_DeviceNotAllowed = 2,
    MMLS_DeviceRegistrationRequested = 3
} MMLoginStatus;

@interface MMUserInfo : NSObject

@property (nonatomic, retain) NSString* sessionId;
@property (nonatomic, retain) NSString* userName;
@property (nonatomic, assign) int userRole;
@property (nonatomic, assign) MMLoginStatus loginStatus;

@end
