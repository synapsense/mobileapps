//
//  MMDataCenterInfo.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMDCInfoValue : NSObject

@property int value;
@property (nonatomic, retain) NSString* unit;
@property (nonatomic, retain) NSDate* timestamp;

@end

@interface MMPueInfoValue : NSObject

@property double value;
@property (nonatomic, retain) NSString* unit;
@property (nonatomic, retain) NSDate* timestamp;

@end


@interface MMDataCenterInfo : NSObject

@property (nonatomic, retain) NSString* guid;
@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) MMDCInfoValue* status;
@property (nonatomic, assign) int totalZoneNumber;
@property (nonatomic, assign) int totalRacksNumber;
@property (nonatomic, assign) int totalCracsNumber;
@property (nonatomic, assign) float area;
@property (nonatomic, retain) NSString* areaUnits;
@property (nonatomic, retain) MMPueInfoValue* pue;
@property (nonatomic, retain) MMPueInfoValue* itPower;
@property (nonatomic, retain) MMPueInfoValue* coolingPower;
@property (nonatomic, retain) MMPueInfoValue* powerLoss;
@property (nonatomic, retain) MMPueInfoValue* lightingPower;
@property (nonatomic, retain) MMPueInfoValue* estimatedEnergyUsage;
@property (nonatomic, retain) MMPueInfoValue* infrastrucurePower;
@property (nonatomic, retain) NSString* pueType;
@property (nonatomic, assign) float longitude;
@property (nonatomic, assign) float latitude;
@property (nonatomic, assign) int numberOfActiveAlerts;

@end


