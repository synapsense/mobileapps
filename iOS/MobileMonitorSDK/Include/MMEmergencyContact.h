//
//  MMEmergencyContact.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMEmergencyContact : NSObject

@property (nonatomic, retain) NSString *emergencyName;
@property (nonatomic, retain) NSString *emergencyTitle;
@property (nonatomic, retain) NSString *emergencyMobilePhone;
@property (nonatomic, retain) NSString *emergencyOfficePhone;
@property (nonatomic, retain) NSString *emergencyEmail;

@end
