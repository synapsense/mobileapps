//
//  MMDataCenterInfo.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef struct {
    NSString* id;
    NSString* type;
    int count;
} MMTreeObject;

typedef enum{
    MMAlertStatusNone         = 0,
    MMAlertStatusOpened       = 1,
    MMAlertStatusAcknowledged = 2,
    MMAlertStatusDismissed    = 4,
    MMAlertStatusResolved     = 8
} MMAlertStatus;

typedef enum{
    MMAlertPriorityNone          = 0,
    MMAlertPriorityInformational = 1,
    MMAlertPriorityMinor         = 2,
    MMAlertPriorityMajor         = 4,
    MMAlertPriorityCritical      = 8
} MMAlertPriority;

typedef enum{
    MMAlertSortNone                 = 0,
    MMAlertAscendingSortObjectName  = 1,
    MMAlertAscendingSortPriority    = 2,
    MMAlertAscendingSortStatus      = 3,
    MMAlertAscendingSortTimestamp   = 4,
    MMAlertDescendingSortObjectName = 5,
    MMAlertDescendingSortPriority   = 6,
    MMAlertDescendingSortStatus     = 7,
    MMAlertDescendingSortTimestamp  = 9,
} MMAlertSort;

@interface MMAlertInfo : NSObject

@property (nonatomic, assign) int alertId;
@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* objectName;
@property (nonatomic, assign) MMTreeObject object;
@property (nonatomic, assign) int alertStatus;
@property (nonatomic, assign) int alertPriority;
@property (nonatomic, retain) NSString* lastActionUser;
@property (nonatomic, retain) NSString* location;
@property (nonatomic, retain) NSDate *lastActionTime;
@property (nonatomic, retain) NSDate *alertTimestamp;
@property (nonatomic, retain) NSString* alertDescription;
@property (nonatomic, retain) NSString* alertMessage;
@property (nonatomic, retain) NSString* alertComment;
@end


