//
//  MMUserPreferences.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMUserPreferences.h"

@implementation MMUserPreferences

@synthesize decimalFormat,
            decimalSeparator,
            dateFormat,
            unitsSystem,
            savedLanguage;

- (NSString*)formatDouble:(double)number
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc]init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:decimalFormat];
    [formatter setMinimumFractionDigits:decimalFormat];
    [formatter setDecimalSeparator:[[decimalSeparator componentsSeparatedByString:@"\""]objectAtIndex:1]];
    [formatter setGroupingSeparator:@""];
    NSString *formattedString = [formatter stringFromNumber:[NSNumber numberWithDouble:number]];
    [formatter release];
    return formattedString;
}

- (NSString*)dateFormat
{
    return [[dateFormat componentsSeparatedByString:@"\""] objectAtIndex:1];
}

- (void)dealloc
{
    self.dateFormat = nil;
    self.decimalSeparator = nil;
    self.unitsSystem = nil;
    self.savedLanguage = nil;
    [super dealloc];
}

@end