//
//  MMEmergencyContact.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMEmergencyContact.h"

@implementation MMEmergencyContact

@synthesize emergencyName,
            emergencyTitle,
            emergencyMobilePhone,
            emergencyOfficePhone,
            emergencyEmail;
            
- (void)dealloc
{
    self.emergencyName = nil;
    self.emergencyTitle = nil;
    self.emergencyMobilePhone = nil;
    self.emergencyOfficePhone = nil;
    self.emergencyEmail = nil;

    [super dealloc];
}

@end
