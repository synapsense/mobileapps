//
//  MMFloorPlanElement.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMFloorPlanElement.h"

@implementation FPObjectDetails

@synthesize valueName;
@synthesize lastValue;
@synthesize valueUnits;
@synthesize dataClass;

@end
