//
//  MMliveImagingType.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMliveImagingType.h"

@implementation MMliveImagingType

@synthesize name, dataClass, layer;

- (void)dealloc
{
    self.name = nil;
    [super dealloc];
}

@end
