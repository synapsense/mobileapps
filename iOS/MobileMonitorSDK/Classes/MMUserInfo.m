//
//  UserInfo.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMUserInfo.h"

@implementation MMUserInfo

@synthesize sessionId,
            userName,
            userRole,
            loginStatus;

- (void)dealloc
{
    self.sessionId = nil;
    self.userName = nil;
    [super dealloc];
}

@end