//
//  MMDataCenterInfo.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMDataCenterInfo.h"

@implementation MMDCInfoValue

@synthesize value,
            unit,
            timestamp;

- (void)dealloc
{
    self.unit = nil;
    self.timestamp = nil;
    [super dealloc];
}

@end

@implementation MMPueInfoValue

@synthesize value,
            unit,
            timestamp;

- (void)dealloc
{
    self.unit = nil;
    self.timestamp = nil;
    [super dealloc];
}

@end


@implementation MMDataCenterInfo

@synthesize guid,
            name,
            status,
            totalZoneNumber,
            totalRacksNumber,
            totalCracsNumber,
            area,
            pue,
            itPower,
            coolingPower,
            powerLoss,
            lightingPower,
            estimatedEnergyUsage,
            infrastrucurePower,
            pueType,
            areaUnits,
            longitude,
            latitude,
            numberOfActiveAlerts;

- (void)dealloc
{
    self.guid = nil;
    self.name = nil;
    self.status = nil;
    self.pue = nil;
    self.itPower = nil;
    self.coolingPower = nil;
    self.powerLoss = nil;
    self.lightingPower = nil;
    self.estimatedEnergyUsage = nil;
    self.infrastrucurePower = nil;
    self.pueType = nil;
    self.areaUnits = nil;
    [super dealloc];
}

@end
