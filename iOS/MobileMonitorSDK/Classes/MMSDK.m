//
//  MMSDK.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//
#import "MMSDK.h"
#import "DSJSONRPC.h"
#import "JSONKit.h"
#import "MMliveImagingType.h"
#import "STKeychain.h"

#define APP_DOMAIN      @"ru.merann.mobilemonitor.sdk"

@interface MMSDK (PrivateMethods)

- (NSString *)userManagerURL;
- (NSString *)objectManagerURL;
- (NSString *)systemManagerURL;
- (NSString *)alertManagerURL;
- (NSString *)statusManagerURL;
- (NSString *)configManagerURL;
- (NSString *)floorplanSchemeImageManagerURL:(NSString*)dcId;

- (int)getAttributeIntValue:(id)node;
- (double)getAttributeDoubleValue:(id)node;
- (NSString*)getAttributeUnits:(id)node;
- (NSDate*)getAttributeTimeStamp:(id)node;
- (BOOL)storeAppToken:(NSString*)aToken forServiceURL:(NSString*)aServiceURL;

@end

@implementation MMSDK

static BOOL isSecureConnection;
static MMSDK* __sharedInstance;
static bool isValueExisting;
static MMUIPrivileges privileges;
static int port = 8080;

- (BOOL)storeAppToken:(NSString*)aToken forServiceURL:(NSString*)aServiceURL
{
    //store secure token to the Keychain
    NSError* keychainError = nil;
    if ([STKeychain storeUsername:aServiceURL andPassword:aToken forServiceName:APP_DOMAIN updateExisting:YES error:&keychainError])
    {
        NSLog(@"New device id stored: %@", aToken);
        return YES;
    }
    else
    {
        NSLog(@"Keychaining failed!");
        assert(false);
        return NO;
    }
}
+ (MMSDK*)sharedInstance
{
    if (__sharedInstance == nil)
    {
        __sharedInstance = [[MMSDK alloc] init];
    }
    return __sharedInstance;
}

- (void)dealloc
{
    [_userInfo release];
    [_serviceBaseURL release];
    [jsonRPCUserManager release];
    [jsonRPCSystemManager release];
    [jsonRPCAlertManager release];
    [jsonRPCObjectManager release];
    [jsonRPCConfigManager release];
    [super dealloc];
}

+ (void)setConnectionIsSecure: (BOOL)isSecure
{
    isSecureConnection = isSecure;
}

+ (void)setUIPrivileges: (MMUIPrivileges)uiPriveleges
{
    privileges = uiPriveleges;
}

+ (void)setPort: (int)connectionPort
{
    port = connectionPort;
}

- (NSString*)serviceBaseURL
{
    return _serviceBaseURL;
}

- (int)getAttributeIntValue:(id)node
{
    int result = 0;
    id attributes = [node objectForKey:@"attributes"];
    isValueExisting = NO;
    if (attributes == [NSNull null])
    {
        return result;
    }

    for ( id attr in attributes)
    {
        NSString *attr_name = [attr objectForKey:@"name"];
        if ([attr_name compare:@"v"] == NSOrderedSame)
        {
            result = [[attr objectForKey:@"value"] intValue];
            isValueExisting = YES;
            break;
        }
    }
    return result; 
}

- (double)getAttributeDoubleValue:(id)node
{
    double result = 0;
    id attributes = [node objectForKey:@"attributes"];
    isValueExisting = NO;
    if (attributes == [NSNull null])
    {
        return result;
    }
    
    for (id attr in attributes)
    {
        NSString *attr_name = [attr objectForKey:@"name"];
        if ([attr_name compare:@"v"] == NSOrderedSame)
        {
            result = [[attr objectForKey:@"value"] doubleValue];
            isValueExisting = YES;
            break;
        }
    }
    return result;
}

- (NSString*)getAttributeStringValue:(id)node
{
    NSString *result = 0;
    id attributes = [node objectForKey:@"attributes"];
    isValueExisting = NO;
    if (attributes == [NSNull null])
    {
        return result;
    }

    for (id attr in attributes)
    {
        NSString *attr_name = [attr objectForKey:@"name"];
        if ([attr_name compare:@"v"] == NSOrderedSame)
        {
            result = [attr objectForKey:@"value"];
            isValueExisting = YES;
            break;
        }
    }
    return result;
}

- (NSString*)getAttributeUnits:(id)node
{
    NSString *result = 0;
    id attributes = [node objectForKey:@"attributes"];
    isValueExisting = NO;
    if (attributes == [NSNull null])
    {
        return result;
    }
    for (id attr in attributes)
    {
        NSString *attr_name = [attr objectForKey:@"name"];
        if ([attr_name compare:@"u"] == NSOrderedSame)
        {
            result = [attr objectForKey:@"value"];
            isValueExisting = YES;
            break;
        }
    }
    return result;
}

- (NSString*)getAttributeName:(id)node
{
    NSString *result = 0;
    id attributes = [node objectForKey:@"attributes"];
    isValueExisting = NO;
    if (attributes == [NSNull null])
    {
        return result;
    }

    for (id attr in attributes)
    {
        NSString *attr_name = [attr objectForKey:@"name"];
        if ([attr_name compare:@"mn"] == NSOrderedSame || [attr_name compare:@"v"] == NSOrderedSame)
        {
            result = [attr objectForKey:@"value"];
            isValueExisting = YES;
            break;
        }
    }
    return result;
}

- (NSDate*)getAttributeTimeStamp:(id)node
{
    NSDate *result = nil;
    id attributes = [node objectForKey:@"attributes"];
    isValueExisting = NO;
    if (attributes == [NSNull null])
    {
        return result;
    }
    for (id attr in attributes)
    {
        NSString *attr_name = [attr objectForKey:@"name"];
        if ([attr_name compare:@"t"] == NSOrderedSame)
        {
            result = [NSDate dateWithTimeIntervalSince1970:[[attr objectForKey:@"value"] doubleValue] / 1000];
            isValueExisting = YES;
            break;
        }
    }
    return result;
}

- (FloorPlanElement*)getFloorPlanElement:(id)node
{
    FloorPlanElement *element = [[[FloorPlanElement alloc] init] autorelease];
    id attributes = [node objectForKey:@"attributes"];
    if (attributes == [NSNull null])
    {
        return nil;
    }
    for(id attribute in attributes)
    {
        NSString *elementNodeName = [attribute objectForKey:@"name"];
        if([elementNodeName compare:@"id"] == NSOrderedSame)
        {
            element.element_id = [attribute objectForKey:@"value"];
        }
    }
    if ([node objectForKey:@"subElements"] == [NSNull null])
    {
        return nil;
    }
    for(id elements in [node objectForKey:@"subElements"])
    {
        NSString *elementNodeName = [elements objectForKey:@"name"];
        if([elementNodeName compare:@"name"] == NSOrderedSame)
        {
            element.name = [self getAttributeName:elements];
        }
        else if([elementNodeName compare:@"x"] == NSOrderedSame)
        {
            element.x = [self getAttributeDoubleValue:elements];
        }
        else if([elementNodeName compare:@"y"] == NSOrderedSame)
        {
            element.y = [self getAttributeDoubleValue:elements];
        }
        else if([elementNodeName compare:@"status"] == NSOrderedSame)
        {
            element.status = [self getAttributeIntValue:elements];
        }
        else if([elementNodeName compare:@"numAlerts"] == NSOrderedSame)
        {
            element.num_alerts = [self getAttributeIntValue:elements];
        }
        else if([elementNodeName compare:@"door"] == NSOrderedSame)
        {
            if(([elements objectForKey:@"subElements"] != [NSNull null]))
            {
                for(id links in [elements objectForKey:@"subElements"])
                {
                    elementNodeName = [links objectForKey:@"name"];
                    if([elementNodeName compare:@"lastValue"] == NSOrderedSame)
                    {
                        element.door = [self getAttributeIntValue:links];
                        break;
                    }
                }
            }
        }
    }
    return element;
}



#pragma mark -
#pragma mark API Methods


- (int)getNumWSNWarningsWithSuccess:(void(^)(int count))success
      failure:(void(^)(NSError *error))failure;
{    
    int requestId = [jsonRPCStatusManager callMethod:@"getNumWarnings" withParameters:nil onCompletion:^(NSString *methodName, NSInteger callId, id methodResult, DSJSONRPCError *methodError, NSError *internalError)
     {
         if (!internalError && methodResult)
         {
             int count = [methodResult intValue];
             success(count);
         }
         else
         {
             failure(internalError);
         }
     }];
    return requestId;
}

- (int)getActiveAlerts:(id)objId withPeriod:(id)period withPriority:(id)priority withStatus:(id)status withStart:(id)start withCount:(id)count success:(void(^)(NSArray *activeAlerts))success
                             failure:(void(^)(NSError *error))failure;
{    
    int requestId = [jsonRPCAlertManager callMethod:@"getActiveAlerts" withParameters:[NSArray arrayWithObjects:objId, period, priority, status, start, count, nil] onCompletion:^(NSString *methodName, NSInteger callId, id methodResult, DSJSONRPCError *methodError, NSError *internalError)
     {
         if (!internalError && methodResult)
         {
             //NSLog(@"%@",methodResult);
             NSMutableArray* alertsArray = [[[NSMutableArray alloc] init] autorelease];
            
             for (int i = 0; i < [methodResult count]; i++)
             {
                 id alert = [methodResult objectAtIndex:i];
                 MMAlertInfo *alertInfo = [[MMAlertInfo alloc] init];
                 
                 alertInfo.alertId = [[alert objectForKey:@"id"] intValue];
                 alertInfo.name = [alert objectForKey:@"name"];
                 
                 alertInfo.alertStatus = [[alert objectForKey:@"status"] intValue];
                 alertInfo.alertPriority = [[alert objectForKey:@"priority"] intValue];

                 if ( [alert objectForKey:@"description"] != [NSNull null] )
                 {
                     alertInfo.alertDescription = [alert objectForKey:@"description"];
                 }
                 if ( [alert objectForKey:@"message"] != [NSNull null] )
                 {
                     alertInfo.alertMessage = [alert objectForKey:@"message"];
                 }
                 if ( [alert objectForKey:@"comment"] != [NSNull null] )
                 {
                     alertInfo.alertComment = [alert objectForKey:@"comment"];
                 }
                 
                 if ( [alert objectForKey:@"lastActionTime"] != [NSNull null] )
                 {
                     alertInfo.lastActionTime = [NSDate dateWithTimeIntervalSince1970:[[alert objectForKey:@"lastActionTime"] doubleValue] / 1000];
                 }

                 if ( [alert objectForKey:@"lastActionUser"] != [NSNull null] )
                 {
                     alertInfo.lastActionUser = [alert objectForKey:@"lastActionUser"];                 
                 }

                 if ( [alert objectForKey:@"objectName"] != [NSNull null] )
                 {
                     alertInfo.objectName = [alert objectForKey:@"objectName"];                 
                 }

                 if ( [alert objectForKey:@"timestamp"] != [NSNull null] )
                 {
                     alertInfo.alertTimestamp = [NSDate dateWithTimeIntervalSince1970:[[alert objectForKey:@"timestamp"] doubleValue] / 1000];
                 }
                 if ( [alert objectForKey:@"objectLocation"] != [NSNull null] )
                 {
                     alertInfo.location = [alert objectForKey:@"objectLocation"];
                 }
                 [alertsArray addObject:alertInfo];
                 [alertInfo release];
             }
             success(alertsArray);
         }
         else
         {
             failure(internalError);
         }
     }];
    return requestId;
}

- (int)changeAlertState:(int)alertId withState:(MMAlertStatus)state comment:(NSString*)comment success:(void(^)(BOOL isChanged))success 
                 failure:(void(^)(NSError *error))failure
{
    NSString *methodName;
    switch (state) {
        case MMAlertStatusAcknowledged:
            methodName = @"acknowledge";
            break;
        case MMAlertStatusDismissed:
            methodName = @"dismiss";
            break;
        case MMAlertStatusResolved:
            methodName = @"resolve";
            break;
        default:
            methodName = @"acknowledge";
            break;
    }
    int requestId = [jsonRPCAlertManager callMethod:methodName withParameters:[NSArray arrayWithObjects:[NSNumber numberWithInt:alertId], comment, nil] onCompletion:^(NSString *methodName, NSInteger callId, id methodResult, DSJSONRPCError *methodError, NSError *internalError)
     {
         if (!internalError && methodResult)
         {
             success([methodResult boolValue]);
         }
         else
         {
             failure(internalError);
         }
     }];
    return requestId;
}

- (int)getNumActiveAlerts:(NSString*)objId withPeriod:(id)period withPriority:(id)priority withStatus:(id)status success:(void(^)(int count))success 
                             failure:(void(^)(NSError *error))failure;
{    
    id objectId = nil;
    
    if (objId == nil)
    {
        objectId = [NSNull null];
    }
    else
    {
        objectId = [NSString stringWithString:objId];
    }
    
    int requestId = [jsonRPCAlertManager callMethod:@"getNumActiveAlerts" withParameters:[NSArray arrayWithObjects:objectId, period, priority, status, nil] onCompletion:^(NSString *methodName, NSInteger callId, id methodResult, DSJSONRPCError *methodError, NSError *internalError)
     {
         if (!internalError && methodResult)
         {
             int count = [methodResult intValue];
             success(count);
         }
         else
         {
             failure(internalError);
         }
     }];
    return requestId;
}

- (BOOL)isDeviceRegistredOnService:(NSString*)rawServiceURL
{
    NSError* keychainError = nil;
    NSString* appToken = [STKeychain getPasswordForUsername:rawServiceURL andServiceName:APP_DOMAIN error:&keychainError];
    if (appToken && !keychainError)
    {
        return YES;
    }
    else
        return NO;
}


- (int)login:(NSString*)login withPassword:(NSString*)password withServiceURL:(NSString*)rawServiceURL success:(void(^)(MMUserInfo *userInfo, MMUserPreferences *userPreferences))success 
      failure:(void(^)(NSError *error))failure;
{
    [_serviceBaseURL release];
    _serviceBaseURL = [rawServiceURL retain];
    NSURL *serviceURL = [NSURL URLWithString:[self userManagerURL]];
    [jsonRPCUserManager release];
    jsonRPCUserManager = [[DSJSONRPC alloc] initWithServiceEndpoint:serviceURL];
    
    if (login == nil)
    {
        login = @"";
    }
    
    if (password == nil)
    {
        password = @"";
    }

    //generate secure token
    NSError* keychainError = nil;
    NSString* appToken = [STKeychain getPasswordForUsername:rawServiceURL andServiceName:APP_DOMAIN error:&keychainError];
    if (!appToken || keychainError)
    {
        appToken = [MMSDK getUUID];
    }

    NSString* deviceName = [[UIDevice currentDevice] name];
    
    int requestId = [jsonRPCUserManager callMethod:@"login" withParameters:[NSArray arrayWithObjects:login, password, appToken, deviceName, [NSArray arrayWithObjects:@"DecimalSeparator", @"DecimalFormat",@"DateFormat",@"SavedLanguage",@"UnitsSystem", nil], nil] onCompletion:^(NSString *methodName, NSInteger callId, id methodResult, DSJSONRPCError *methodError, NSError *internalError)
     {
         if (!internalError && methodResult)
         {
             [_userInfo release];
             _userInfo = nil;
             _userInfo = [[MMUserInfo alloc] init];
             _userInfo.sessionId = [methodResult objectForKey:@"jSessionId"];
             _userInfo.userName = [methodResult objectForKey:@"userName"];
             _userInfo.loginStatus = [[methodResult objectForKey:@"loginStatus"] intValue];
             _userInfo.userRole = [[methodResult objectForKey:@"userRole"] intValue];
             
             switch (_userInfo.loginStatus)
             {
                 case MMLS_Ok:
                 {
                     [self initManagers];
                     MMUserPreferences *userPreferences = [[[MMUserPreferences alloc]init]autorelease];
                     NSArray *preferencesArray = [methodResult objectForKey:@"userPreferences"];
                     for (NSDictionary* userPreference in preferencesArray)
                     {
                         if (![userPreference isEqual:[NSNull null]])
                         {
                             if ([[userPreference objectForKey:@"preferenceName"] isEqual:@"DecimalSeparator"])
                             {
                                 [userPreferences setDecimalSeparator:[userPreference objectForKey:@"value"]];
                             }
                             if ([[userPreference objectForKey:@"preferenceName"] isEqual:@"DecimalFormat"])
                             {
                                 [userPreferences setDecimalFormat:[[userPreference objectForKey:@"value"] intValue]];
                             }
                             if ([[userPreference objectForKey:@"preferenceName"] isEqual:@"DateFormat"])
                             {
                                 [userPreferences setDateFormat:[userPreference objectForKey:@"value"]];
                             }
                             if ([[userPreference objectForKey:@"preferenceName"] isEqual:@"SavedLanguage"])
                             {
                                 [userPreferences setSavedLanguage:[userPreference objectForKey:@"value"]];
                             }
                             if ([[userPreference objectForKey:@"preferenceName"] isEqual:@"UnitsSystem"])
                             {
                                 [userPreferences setUnitsSystem:[userPreference objectForKey:@"value"]];
                             }
                         }
                     }
                     success(_userInfo, userPreferences);
                 }
                 break;
                 case MMLS_DeviceNotAllowed:
                 {
                     failure([NSError errorWithDomain:_serviceBaseURL code:0 userInfo:[NSDictionary dictionaryWithObject:NSLocalizedString(@"MMLS_DeviceNotAllowed", @"") forKey:NSLocalizedDescriptionKey]]);
                 }
                 break;
                 case MMLS_DeviceRegistrationRequested:
                 {
                      [self storeAppToken:appToken forServiceURL:rawServiceURL];
                      failure([NSError errorWithDomain:_serviceBaseURL code:0 userInfo:[NSDictionary dictionaryWithObject:NSLocalizedString(@"MMLS_DeviceRegistrationRequested", @"") forKey:NSLocalizedDescriptionKey]]);
                 }
                 break;
                 case MMLS_Failed:
                 {
                      failure([NSError errorWithDomain:_serviceBaseURL code:0 userInfo:[NSDictionary dictionaryWithObject:NSLocalizedString(@"MMLS_Failed", @"") forKey:NSLocalizedDescriptionKey]]);
                 }
                 break;
                 default:
                 break;
             }
         }
         else if(!internalError && !methodResult)
         {
             failure([NSError errorWithDomain:_serviceBaseURL code:0 userInfo:[NSDictionary dictionaryWithObject:NSLocalizedString(@"authenticationError", @"") forKey:NSLocalizedDescriptionKey]]);
         }
         else
         {
             failure([NSError errorWithDomain:_serviceBaseURL code:0 userInfo:[NSDictionary dictionaryWithObject:NSLocalizedString(@"Communication error.", @"") forKey:NSLocalizedDescriptionKey]]);
         }
     }];
    return requestId;
}

- (int)getDataCenterDashboardInfo:(NSString*)datacenterId success:(void(^)(MMDataCenterInfo *))success  
                   failure:(void(^)(NSError *error))failure;
{
    assert(_userInfo);
    NSDictionary* zoneNumDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"ZONE", @"type",
                                       [NSNull null], @"id",
                                       [NSNull null], @"parentsRequests",
                                       [NSNull null], @"linkRequests",
                                       [NSNull null], @"childrenRequests",
                                       [NSArray arrayWithObjects:nil], @"properties",
                                       [NSNumber numberWithBool:YES], @"requestDirectChildrenOnly",
                                       [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                       [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                       nil];
    
    NSDictionary* pueDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"PUE", @"type",
                                   [NSNull null], @"id",
                                   [NSNull null], @"parentsRequests",
                                   [NSNull null], @"linkRequests",
                                   [NSNull null], @"childrenRequests",
                                   [NSArray arrayWithObjects:@"pue", @"itPower", @"coolingPower", @"powerLoss", @"lightingPower", @"estimatedAnEnergyUsageToDate", @"type", @"infrastructurePower", nil], @"properties",
                                   [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                   [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                   [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                   [NSNumber numberWithInt:2], @"additionalInfo",
                                   nil];
    
    NSDictionary* rackNumDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"RACK", @"type",
                                       [NSNull null], @"id",
                                       [NSNull null], @"parentsRequests",
                                       [NSNull null], @"linkRequests",
                                       [NSNull null], @"childrenRequests",
                                       [NSArray arrayWithObjects:nil], @"properties",
                                       [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                       [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                       [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                       nil];
    
    NSDictionary* cracNumDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"CRAC", @"type",
                                       [NSNull null], @"id",
                                       [NSNull null], @"parentsRequests",
                                       [NSNull null], @"linkRequests",
                                       [NSNull null], @"childrenRequests",
                                       [NSArray arrayWithObjects:nil], @"properties",
                                       [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                       [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                       [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                       nil];
    NSMutableArray *childrenDictionaries = [NSMutableArray arrayWithObjects:zoneNumDictionary, rackNumDictionary, cracNumDictionary, nil];
    if (privileges.showPUE)
    {
        [childrenDictionaries addObject:pueDictionary];
    }
    NSDictionary* dcDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                  @"DC", @"type",
                                  datacenterId, @"id",
                                  [NSNull null], @"parentsRequests",
                                  [NSNull null], @"linkRequests",
                                  [NSArray arrayWithObjects:@"status", nil], @"properties",
                                  childrenDictionaries, @"childrenRequests",
                                  [NSNumber numberWithBool:YES], @"requestDirectChildrenOnly",
                                  [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                  [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                  nil];

    NSArray* requestArray = [NSArray arrayWithObject:[NSArray arrayWithObject:dcDictionary]];
    
    int requestId = [jsonRPCObjectManager callMethod:@"getObjectsData" withParameters:requestArray onCompletion:^(NSString *methodName, NSInteger callId, id methodResult, DSJSONRPCError *methodError, NSError *internalError)
    {
        MMDataCenterInfo* dcInfo = [[[MMDataCenterInfo alloc] init] autorelease];
        dcInfo.guid = datacenterId;
        
        if (!internalError && methodResult)
        {
            //parsing
            id rootSubElements = [methodResult objectForKey:@"subElements"];
            for (id dataCenter in rootSubElements)
            {
                id dataCenterIdAttributes = [dataCenter objectForKey:@"attributes"];
                id dataCenterIdAttribute = [dataCenterIdAttributes firstObject];
                
                NSString* elementNodeName = [dataCenterIdAttribute objectForKey:@"name"];

                for (id dataCenterAttribute in [dataCenter objectForKey:@"subElements"])
                {
                    elementNodeName = [dataCenterAttribute objectForKey:@"name"];
                    if ([elementNodeName compare:@"racks"] == NSOrderedSame)
                    {
                        dcInfo.totalRacksNumber = [self getAttributeIntValue:dataCenterAttribute];
                    }
                    else if ([elementNodeName compare:@"cracUnits"] == NSOrderedSame)
                    {
                        dcInfo.totalCracsNumber = [self getAttributeIntValue:dataCenterAttribute];
                    }
                    else if ([elementNodeName compare:@"children"] == NSOrderedSame)
                    {
                        if (![[dataCenterAttribute objectForKey:@"subElements"] isEqual:[NSNull null]])
                        {
                            for (id chieldElement in [dataCenterAttribute objectForKey:@"subElements"])
                            {
                                elementNodeName = [chieldElement objectForKey:@"name"];
                                if ([elementNodeName compare:@"ZONE"] == NSOrderedSame)
                                {
                                    dcInfo.totalZoneNumber++;
                                }
                                else if ([elementNodeName compare:@"RACK"] == NSOrderedSame)
                                {
                                    dcInfo.totalRacksNumber++;
                                }
                                else if ([elementNodeName compare:@"CRAC"] == NSOrderedSame)
                                {
                                    dcInfo.totalCracsNumber++;
                                }
                                else if ([elementNodeName compare:@"PUE"] == NSOrderedSame)
                                {
                                    if ([chieldElement objectForKey:@"subElements"] != [NSNull null])
                                    for (id pueChieldElement in [chieldElement objectForKey:@"subElements"])
                                    {
                                        elementNodeName = [pueChieldElement objectForKey:@"name"];
                                        if ([elementNodeName compare:@"pue"] == NSOrderedSame)
                                        {
                                            MMPueInfoValue* pueInfo = [[MMPueInfoValue alloc] init];
                                            dcInfo.pue = pueInfo;
                                            [pueInfo release];
                                            dcInfo.pue.value = [self getAttributeDoubleValue:pueChieldElement];
                                            dcInfo.pue.timestamp = [self getAttributeTimeStamp:pueChieldElement];
                                            dcInfo.pue.unit = [self getAttributeUnits:pueChieldElement];
                                        }
                                        else if ([elementNodeName compare:@"itPower"] == NSOrderedSame)
                                        {
                                            MMPueInfoValue* pueInfo = [[MMPueInfoValue alloc] init];
                                            dcInfo.itPower = pueInfo;
                                            [pueInfo release];
                                            dcInfo.itPower.value = [self getAttributeDoubleValue:pueChieldElement];
                                            dcInfo.itPower.timestamp = [self getAttributeTimeStamp:pueChieldElement];
                                            dcInfo.itPower.unit = [self getAttributeUnits:pueChieldElement];
                                        }
                                        else if ([elementNodeName compare:@"coolingPower"] == NSOrderedSame)
                                        {
                                            MMPueInfoValue* pueInfo = [[MMPueInfoValue alloc] init];
                                            dcInfo.coolingPower = pueInfo;
                                            [pueInfo release];
                                            dcInfo.coolingPower.value = [self getAttributeDoubleValue:pueChieldElement];
                                            dcInfo.coolingPower.timestamp = [self getAttributeTimeStamp:pueChieldElement];
                                            dcInfo.coolingPower.unit = [self getAttributeUnits:pueChieldElement];
                                        }
                                        else if ([elementNodeName compare:@"powerLoss"] == NSOrderedSame)
                                        {
                                            MMPueInfoValue* pueInfo = [[MMPueInfoValue alloc] init];
                                            dcInfo.powerLoss = pueInfo;
                                            [pueInfo release];
                                            dcInfo.powerLoss.value = [self getAttributeDoubleValue:pueChieldElement];
                                            dcInfo.powerLoss.timestamp = [self getAttributeTimeStamp:pueChieldElement];
                                            dcInfo.powerLoss.unit = [self getAttributeUnits:pueChieldElement];
                                        }
                                        else if ([elementNodeName compare:@"lightingPower"] == NSOrderedSame)
                                        {
                                            MMPueInfoValue* pueInfo = [[MMPueInfoValue alloc] init];
                                            dcInfo.lightingPower = pueInfo;
                                            [pueInfo release];
                                            dcInfo.lightingPower.value = [self getAttributeDoubleValue:pueChieldElement];
                                            dcInfo.lightingPower.timestamp = [self getAttributeTimeStamp:pueChieldElement];
                                            dcInfo.lightingPower.unit = [[NSString alloc]initWithString:[self getAttributeUnits:pueChieldElement]];
                                            
                                        }
                                        else if ([elementNodeName compare:@"estimatedAnEnergyUsageToDate"] == NSOrderedSame)
                                        {
                                            MMPueInfoValue* pueInfo = [[MMPueInfoValue alloc] init];
                                            dcInfo.estimatedEnergyUsage = pueInfo;
                                            [pueInfo release];
                                            dcInfo.estimatedEnergyUsage.value = [self getAttributeDoubleValue:pueChieldElement];
                                            dcInfo.estimatedEnergyUsage.timestamp = [self getAttributeTimeStamp:pueChieldElement];
                                            dcInfo.estimatedEnergyUsage.unit = [self getAttributeUnits:pueChieldElement];
                                        }
                                        else if ([elementNodeName compare:@"infrastructurePower"] == NSOrderedSame)
                                        {
                                            MMPueInfoValue* pueInfo = [[MMPueInfoValue alloc] init];
                                            dcInfo.infrastrucurePower = pueInfo;
                                            [pueInfo release];
                                            dcInfo.infrastrucurePower.value = [self getAttributeDoubleValue:pueChieldElement];
                                            dcInfo.infrastrucurePower.timestamp = [self getAttributeTimeStamp:pueChieldElement];
                                            dcInfo.infrastrucurePower.unit = [self getAttributeUnits:pueChieldElement];
                                        }
                                        else if ([elementNodeName compare:@"type"] == NSOrderedSame)
                                        {
                                            id attributes = [pueChieldElement objectForKey:@"attributes"];
                                            if (attributes != [NSNull null])
                                            for (id attr in attributes)
                                            {
                                                NSString *attr_name = [attr objectForKey:@"name"];
                                                if ([attr_name compare:@"v"] == NSOrderedSame)
                                                {
                                                    dcInfo.pueType = [attr objectForKey:@"value"];
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            success(dcInfo);
        }
        else
        {
            failure(internalError);
        }
    }];
    return requestId;
}


- (int)logoutWithSuccess:(void(^)(BOOL isLogout))success 
                  failure:(void(^)(NSError *error))failure
{
    int requestId = [jsonRPCUserManager callMethod:@"logout" withParameters:nil onCompletion:^(NSString *methodName, NSInteger callId, id methodResult, DSJSONRPCError *methodError, NSError *internalError)
     {
         if (!internalError && methodResult)
         {
             BOOL isLogout = [methodResult boolValue];
             success(isLogout);
         }
         else
         {
             failure(internalError);
         }
     }];
    return requestId;
}

- (int)getDataCentersWithSuccess:(void(^)(NSArray *dataCentersInfo))success 
                          failure:(void(^)(NSError *error))failure
{
    assert(_userInfo);

    NSDictionary* zoneNumDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"ZONE", @"type",
                                       [NSNull null], @"id",
                                       [NSNull null], @"parentsRequests",
                                       [NSNull null], @"linkRequests",
                                       [NSNull null], @"childrenRequests",
                                       [NSArray arrayWithObjects:nil], @"properties",
                                       [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                       [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                       [NSNumber numberWithBool:YES], @"requestChildrenNumberOnly",
                                       nil];

    NSDictionary* rackNumDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"RACK", @"type",
                                       [NSNull null], @"id",
                                       [NSNull null], @"parentsRequests",
                                       [NSNull null], @"linkRequests",
                                       [NSNull null], @"childrenRequests",
                                       [NSArray arrayWithObjects:nil], @"properties",
                                       [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                       [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                       [NSNumber numberWithBool:YES], @"requestChildrenNumberOnly",
                                       nil];
    
    NSDictionary* cracNumDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"CRAC", @"type",
                                       [NSNull null], @"id",
                                       [NSNull null], @"parentsRequests",
                                       [NSNull null], @"linkRequests",
                                       [NSNull null], @"childrenRequests",
                                       [NSArray arrayWithObjects:nil], @"properties",
                                       [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                       [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                       [NSNumber numberWithBool:YES], @"requestChildrenNumberOnly",
                                       nil];
    
    NSDictionary* pueDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"PUE", @"type",
                                   [NSNull null], @"id",
                                   [NSNull null], @"parentsRequests",
                                   [NSNull null], @"linkRequests",
                                   [NSNull null], @"childrenRequests",
                                   [NSArray arrayWithObjects:@"pue", nil], @"properties",
                                   [NSNumber numberWithBool:YES], @"requestDirectChildrenOnly",
                                   [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                   [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                   [NSNumber numberWithInt:0], @"additionalInfo",
                                   nil];
    
    NSDictionary* dcDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                  @"DC", @"type",
                                  [NSNull null], @"id",
                                  [NSNull null], @"parentsRequests",
                                  [NSNull null], @"linkRequests",
                                  [NSArray arrayWithObjects:@"name", @"status",@"area", @"latitude", @"longitude", @"numAlerts",nil], @"properties",
                                  [NSArray arrayWithObjects:zoneNumDictionary, rackNumDictionary, cracNumDictionary, pueDictionary, nil], @"childrenRequests",
                                  [NSNumber numberWithBool:YES], @"requestDirectChildrenOnly",
                                  [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                  [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                  [NSNumber numberWithInt:2], @"additionalInfo",
                                  nil];

    NSArray* requestArray = [NSArray arrayWithObject:[NSArray arrayWithObject:dcDictionary]];

    int requestId = [jsonRPCObjectManager callMethod:@"getObjectsData" withParameters:requestArray onCompletion:^(NSString *methodName, NSInteger callId, id methodResult, DSJSONRPCError *methodError, NSError *internalError)
     {
         NSMutableArray* dataCenters = [[[NSMutableArray alloc] init] autorelease];
         if (!internalError && methodResult)
         {
             //parsing
             //NSLog(@"%@",methodResult);
             id rootSubElements = [methodResult objectForKey:@"subElements"];
             for (id dataCenter in rootSubElements)
             {
                 MMDataCenterInfo* dcInfo = [[[MMDataCenterInfo alloc] init] autorelease];
                 id dataCenterIdAttributes = [dataCenter objectForKey:@"attributes"];
                 id dataCenterIdAttribute = [dataCenterIdAttributes firstObject];
                 
                 NSString* elementNodeName = [dataCenterIdAttribute objectForKey:@"name"];
                 if ([elementNodeName compare:@"id"] == NSOrderedSame)
                 {
                     dcInfo.guid = [dataCenterIdAttribute objectForKey:@"value"];
                 }
                 
                 for (id dataCenterAttribute in [dataCenter objectForKey:@"subElements"])
                 {
                     elementNodeName = [dataCenterAttribute objectForKey:@"name"];
                     if ([elementNodeName compare:@"status"] == NSOrderedSame)
                     {
                         id alertAttributes = [dataCenterAttribute objectForKey:@"attributes"];
                         for (id alertAttribute in alertAttributes)
                         {
                             elementNodeName = [alertAttribute objectForKey:@"name"];
                             if ([elementNodeName compare:@"v"] == NSOrderedSame)
                             {
                                 MMDCInfoValue* infoValue = [[MMDCInfoValue alloc] init];
                                 dcInfo.status = infoValue;
                                 [infoValue release];
                                 dcInfo.status.value = [[alertAttribute objectForKey:@"value"] intValue];
                             }
                             else if ([elementNodeName compare:@"t"] == NSOrderedSame)
                             {
                                 MMDCInfoValue* infoValue = [[MMDCInfoValue alloc] init];
                                 dcInfo.status = infoValue;
                                 [infoValue release];
                                 dcInfo.status.timestamp = [[NSDate dateWithTimeIntervalSince1970:[[alertAttribute objectForKey:@"value"] doubleValue] / 1000] retain];
                             }
                         }
                     }
                     else if ([elementNodeName compare:@"name"] == NSOrderedSame)
                     {
                         id alertAttributes = [dataCenterAttribute objectForKey:@"attributes"];
                         for (id alertAttribute in alertAttributes)
                         {
                             elementNodeName = [alertAttribute objectForKey:@"name"];
                             if ([elementNodeName compare:@"v"] == NSOrderedSame)
                             {
                                 dcInfo.name = [alertAttribute objectForKey:@"value"];
                             }
                         }
                     }
                     else if ([elementNodeName compare:@"area"] == NSOrderedSame)
                     {
                         id alertAttributes = [dataCenterAttribute objectForKey:@"attributes"];
                         if (![alertAttributes isEqual:[NSNull null]])
                         {
                             for (id alertAttribute in alertAttributes)
                             {
                                 elementNodeName = [alertAttribute objectForKey:@"name"];
                                 if ([elementNodeName compare:@"v"] == NSOrderedSame)
                                 {
                                     dcInfo.area = [[alertAttribute objectForKey:@"value"] floatValue];
                                 }
                                 if ([elementNodeName compare:@"u"] == NSOrderedSame)
                                 {
                                    dcInfo.areaUnits = [alertAttribute objectForKey:@"value"];
                                 }
                             }
                         }
                     }
                     
                     //parsing for map (longitude)
                     else if ([elementNodeName compare:@"longitude"] == NSOrderedSame)
                     {
                         id alertAttributes = [dataCenterAttribute objectForKey:@"attributes"];
                         if (![alertAttributes isEqual:[NSNull null]])
                         {
                             for (id alertAttribute in alertAttributes)
                             {
                                 elementNodeName = [alertAttribute objectForKey:@"name"];
                                 if ([elementNodeName compare:@"v"] == NSOrderedSame)
                                 {
                                     dcInfo.longitude = [[alertAttribute objectForKey:@"value"] floatValue];
                                 }
                             }
                         }
                     }
                     //parsing for map (latitude)
                     else if ([elementNodeName compare:@"latitude"] == NSOrderedSame)
                     {
                         id alertAttributes = [dataCenterAttribute objectForKey:@"attributes"];
                         if (![alertAttributes isEqual:[NSNull null]])
                         {
                             for (id alertAttribute in alertAttributes)
                             {
                                 elementNodeName = [alertAttribute objectForKey:@"name"];
                                 if ([elementNodeName compare:@"v"] == NSOrderedSame)
                                 {
                                     dcInfo.latitude = [[alertAttribute objectForKey:@"value"] floatValue];
                                 }
                             }
                         }
                     }
                     
                     else if ([elementNodeName compare:@"numAlerts"] == NSOrderedSame)
                     {
                         id alertAttributes = [dataCenterAttribute objectForKey:@"attributes"];
                         if (![alertAttributes isEqual:[NSNull null]])
                         {
                             for (id alertAttribute in alertAttributes)
                             {
                                 elementNodeName = [alertAttribute objectForKey:@"name"];
                                 if ([elementNodeName compare:@"v"] == NSOrderedSame)
                                 {
                                     dcInfo.numberOfActiveAlerts = [[alertAttribute objectForKey:@"value"] floatValue];
                                 }
                             }
                         }
                     }
                     
                     
                     else if ([elementNodeName compare:@"children"] == NSOrderedSame)
                     {
                         NSString *typeName = nil;
                         id alertAttributes = [dataCenterAttribute objectForKey:@"attributes"];
                         for (id alertAttribute in alertAttributes)
                         {
                             elementNodeName = [alertAttribute objectForKey:@"name"];

                             if (([elementNodeName compare:@"typeName"] == NSOrderedSame))
                             {
                                 typeName = [alertAttribute objectForKey:@"value"];
                             }
                             
                             if ([typeName compare:@"PUE"] == NSOrderedSame)
                             {
                                 if (![[dataCenterAttribute objectForKey:@"subElements"] isEqual:[NSNull null]])
                                 {
                                     for (id pueChieldElement in [dataCenterAttribute objectForKey:@"subElements"])
                                     {
                                         elementNodeName = [pueChieldElement objectForKey:@"name"];
                                         for (id pueElement in [pueChieldElement objectForKey:@"subElements"])
                                         {
                                             elementNodeName = [pueElement objectForKey:@"name"];
                                             if ([elementNodeName compare:@"pue"] == NSOrderedSame)
                                             {
                                                 MMPueInfoValue* pueInfo = [[MMPueInfoValue alloc] init];
                                                 dcInfo.pue = pueInfo;
                                                 [pueInfo release];
                                                 dcInfo.pue.value = [self getAttributeDoubleValue:pueElement];
                                                 dcInfo.pue.timestamp = [self getAttributeTimeStamp:pueElement];
                                                 dcInfo.pue.unit = [self getAttributeUnits:pueElement];
                                             }
                                         }
                                     }
                                 }
                             }
                             
                             if ([elementNodeName compare:@"totalNumber"] == NSOrderedSame)
                             {
                                 if ([typeName compare:@"ZONE"] == NSOrderedSame)
                                 {
                                     dcInfo.totalZoneNumber = [[alertAttribute objectForKey:@"value"] intValue];
                                 }
                                 else if ([typeName compare:@"RACK"] == NSOrderedSame)
                                 {
                                     dcInfo.totalRacksNumber = [[alertAttribute objectForKey:@"value"] intValue];
                                 }
                                 else if ([typeName compare:@"CRAC"] == NSOrderedSame)
                                 {
                                     dcInfo.totalCracsNumber = [[alertAttribute objectForKey:@"value"] intValue];
                                 }
                             }
                         }
                     }

                 }
                 [dataCenters addObject:dcInfo];
             }
             success(dataCenters);
         }
         else
         {
             failure(internalError);
         }
     }];
    return requestId;
}

- (int)getSystemStatusWithSuccess:(void(^)(NSArray *componentStatusInfo))success 
                           failure:(void(^)(NSError *error))failure
{
    assert(_userInfo);
    int requestId = [jsonRPCSystemManager callMethod:@"getEnvironmentalServerState" withParameters:nil onCompletion:^(NSString *methodName, NSInteger callId, id methodResult, DSJSONRPCError *methodError, NSError *internalError)
     {
         //NSLog(@"%@",methodResult);
         NSMutableArray* componentsStatusArray = [[[NSMutableArray alloc] init] autorelease];
         if (!internalError && methodResult)
         {
             for (NSDictionary *dictionaryElement in methodResult)
             {
                 MMComponentStatus *componentStatus = [[MMComponentStatus alloc]init];
                 componentStatus.component = [[dictionaryElement objectForKey:@"component"] intValue];
                 if ([dictionaryElement objectForKey:@"message"] != [NSNull null]) {
                     componentStatus.message = [dictionaryElement objectForKey:@"message"];
                 }
                if ([dictionaryElement objectForKey:@"status"] != [NSNull null]) {
                     componentStatus.status = [[dictionaryElement objectForKey:@"status"] intValue];
                 }
                 [componentsStatusArray addObject:componentStatus];
                 [componentStatus release];
             }
             success(componentsStatusArray);
         }
         else
         {
             failure(internalError);
         }
     }];
    return requestId;
}

- (int)getFloorPlanObjectInfo:(NSString*)objectId withType:(NSString*)type success:(void(^)(NSMutableArray *, NSString*))success
                 failure:(void(^)(NSError *error))failure;
{
    assert(_userInfo);
    
    NSDictionary* dcmodbuspropertyDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                         @"MODBUSPROPERTY", @"type",
                                         [NSNull null], @"id",
                                         [NSNull null], @"parentsRequests",
                                         [NSArray arrayWithObjects:@"lastValue", @"dataclass", nil], @"properties",
                                         [NSArray arrayWithObjects:nil], @"childrenRequests",
                                         [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                         [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                         [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                        [NSNumber numberWithInt:
                                         MMRIHistorical |
                                         MMRILink |
                                         MMRIMappedName |
                                         MMRIMappedValue |
                                         MMRITimestamp |
                                         MMRIUnits
                                         ], @"additionalInfo",
                                         nil];
    
    NSDictionary* dcwsnsendorDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                            @"WSNSENSOR", @"type",
                                            [NSNull null], @"id",
                                            [NSNull null], @"parentsRequests",
                                            [NSArray arrayWithObjects:@"lastValue", @"dataclass", nil], @"properties",
                                            [NSArray arrayWithObjects:nil], @"childrenRequests",
                                            [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                            [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                            [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                           [NSNumber numberWithInt:
                                            MMRIHistorical |
                                            MMRILink |
                                            MMRIMappedName |
                                            MMRIMappedValue |
                                            MMRITimestamp |
                                            MMRIUnits
                                            ], @"additionalInfo",
                                            nil];
    
    NSDictionary* dcElementDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                  type, @"type",
                                  objectId, @"id",
                                  [NSArray arrayWithObjects:dcmodbuspropertyDictionary, dcwsnsendorDictionary, nil], @"linkRequests",
                                  [NSArray arrayWithObjects:
                                          /* RACK properties */
                                          @"cTop", @"cMid", @"cBot", @"chimney", @"hTop", @"hMid", @"hBot", @"ref", @"rh", @"ra", @"bpa", @"coldDp", @"cTROfChange", @"nodeTemp",
                                          /*CRAC properties*/
                                          @"supplyT", @"returnT", @"supplyRh", @"returnRh", @"supplyDp", @"returnDp", @"alr", @"coolingEfficiency", @"tonsDesign", @"tonsActual", @"cracDeltaT",
                                          /*PRESSURE properties*/
                                          @"pressure",
                                          /*ION common properties*/
                                          @"kwTotal", @"kwDemand", @"kvaDemand", @"kvarDemand", @"iaThd", @"ibThd", @"icThd",
                                          /*ION_DELTA properties*/
                                          @"current", @"pfTotal", @"voltageAB", @"voltageBC", @"voltageCA", @"vabThd", @"vbcThd", @"vcaThd",
                                          /*ION_WYE properties*/
                                          @"kwA", @"kwB", @"kwC", @"currentA", @"currentB", @"currentC", @"pfA", @"pfB", @"pfC", @"vrmsA", @"vrmsB", @"vrmsC", @"vaThd", @"vbThd", @"vcThd",
										  /*ION_WWIRELESS properties*/
                                          @"energy",
										  /*LEAK properties*/
										  @"sensor",
										  /*Vertical temp properties*/
                                          @"top", @"middle", @"bottom",
										  /*Generic temp properties*/
                                          @"sensor",
										  /*Door properties*/
                                          @"door",
                                          /* Common properties */
                                          @"name",
                                          nil], @"properties",
                                  [NSArray arrayWithObjects:nil], @"childrenRequests",
                                  [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                  [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                  [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                  [NSNumber numberWithInt:
                                   MMRIHistorical |
                                   MMRILink |
                                   MMRIMappedName |
                                   MMRIMappedValue |
                                   MMRITimestamp |
                                   MMRIUnits
                                   ], @"additionalInfo",
                                  nil];
    
    NSArray* requestArray = [NSArray arrayWithObject:[NSArray arrayWithObject:dcElementDictionary]];
    
    int requestId = [jsonRPCObjectManager callMethod:@"getObjectsData" withParameters:requestArray onCompletion:^(NSString *methodName, NSInteger callId, id methodResult, DSJSONRPCError *methodError, NSError *internalError)
    {
        if (!internalError && methodResult)
        {
            //parsing
            NSString* elementName = nil;
            id rootSubElements = [methodResult objectForKey:@"subElements"];
            NSMutableArray *fpElement = [[[NSMutableArray alloc] init] autorelease];
            for (id element in rootSubElements)
            {
                NSString *elementNodeName = [element objectForKey:@"name"];
                
                if([element objectForKey:@"attributes"] == [NSNull null])
                {
                    continue;
                }
                FPObjectDetails *fpDetails = nil;
                if([element objectForKey:@"subElements"] == [NSNull null])
                    continue;
                
                for(id elementData in [element objectForKey:@"subElements"])
                {
                    fpDetails = [[[FPObjectDetails alloc] init] autorelease];
                    Boolean isLink = false;
                    Boolean isAdding = YES;
                    elementNodeName = [elementData objectForKey:@"name"];
                    if ([elementNodeName isEqualToString:@"door"] && [elementData objectForKey:@"subElements"] == [NSNull null])
                    {
                        continue;
                    }
                    if ([elementNodeName isEqualToString:@"name"])
                    {
                        id attributes = [elementData objectForKey:@"attributes"];
                        if (attributes == [NSNull null])
                        {
                            continue;
                        }
                        for (id attr in attributes)
                        {
                            if ([[attr objectForKey:@"name"] isEqualToString:@"v"])
                            {
                                elementName = [attr objectForKey:@"value"];
                                break;
                            }
                        }
                    }
                    else
                    {
                        fpDetails.valueName = [self getAttributeName:elementData];
                        fpDetails.lastValue = [self getAttributeStringValue:elementData];
                        if(!isValueExisting)
                        {
                            fpDetails.lastValue = nil;
                        }
                        fpDetails.valueUnits = [self getAttributeUnits:elementData];
                        if(!isValueExisting && fpDetails.lastValue == nil)
                            isAdding = NO;
                        
                        id attributes = [elementData objectForKey:@"attributes"];
                        if (attributes == [NSNull null])
                        {
                            continue;
                        }
                        for (id attr in attributes)
                        {
                            NSString *attr_name = [attr objectForKey:@"name"];
                            if([attr_name compare:@"isLink"] == NSOrderedSame)
                            {
                                isLink = [[attr objectForKey:@"value"] boolValue];
                                break;
                            }
                        }
                        if(isLink && ([elementData objectForKey:@"subElements"] != [NSNull null]))
                        {
                            for(id links in [elementData objectForKey:@"subElements"])
                            {
                                elementNodeName = [links objectForKey:@"name"];
                                if([elementNodeName compare:@"lastValue"] == NSOrderedSame)
                                {
                                    isAdding = YES;
                                    fpDetails.lastValue = [self getAttributeStringValue:links];
                                    if(!isValueExisting)
                                    {
                                        fpDetails.lastValue = nil;
                                    }
                                    fpDetails.valueUnits = [self getAttributeUnits:links];
                                    if(!isValueExisting && fpDetails.lastValue == nil)
                                        isAdding = NO;
//                                    break;
                                }
                                else if([elementNodeName compare:@"dataclass"] == NSOrderedSame)
                                {
                                    isAdding = YES;
                                    fpDetails.dataClass = [self getAttributeIntValue:links];
                                    if(!isValueExisting)
                                    {
                                        fpDetails.dataClass = -1;
                                    }
                                    if(!isValueExisting && fpDetails.dataClass == -1)
                                        isAdding = NO;
                                }
                            }
                        }
                        if(isAdding)
                            [fpElement addObject:fpDetails];
                    }
                }
            }
            success(fpElement, elementName);
        }
        else
        {
            failure(internalError);
        }
    }];
    return requestId;
}

- (int)getFloorPlanInfo:(NSString*)datacenterId withImage:(Boolean)isImageNeeded success:(void(^)(MMFloorPlanInfo *))success
                           failure:(void(^)(NSError *error))failure;
{
    assert(_userInfo);
    NSDictionary* dcwsnsendorDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                           @"WSNSENSOR", @"type",
                                           [NSNull null], @"id",
                                           [NSNull null], @"parentsRequests",
                                           [NSArray arrayWithObjects:@"lastValue", nil], @"properties",
                                           [NSArray arrayWithObjects:nil], @"childrenRequests",
                                           [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                           [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                           [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                           [NSNumber numberWithInt:63], @"additionalInfo",
                                           nil];
    
    NSDictionary* doorDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @"DOOR", @"type",
                                    [NSNull null], @"id",
                                    [NSNull null], @"parentsRequests",
                                    [NSArray arrayWithObjects:dcwsnsendorDictionary, nil], @"linkRequests",
                                    [NSNull null], @"childrenRequests",
                                    [NSArray arrayWithObjects: @"name", @"x", @"y", @"status", @"numAlerts", @"door", nil], @"properties",
                                    [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                    [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                    [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                    nil];
    
    NSDictionary* genericTemperatureDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                  @"GENERICTEMPERATURE", @"type",
                                                  [NSNull null], @"id",
                                                  [NSNull null], @"parentsRequests",
                                                  [NSNull null], @"linkRequests",
                                                  [NSNull null], @"childrenRequests",
                                                  [NSArray arrayWithObjects: @"name", @"x", @"y", @"status", @"numAlerts", nil], @"properties",
                                                  [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                                  [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                                  [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                                  nil];
    
    NSDictionary* leakDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @"LEAK", @"type",
                                    [NSNull null], @"id",
                                    [NSNull null], @"parentsRequests",
                                    [NSNull null], @"linkRequests",
                                    [NSNull null], @"childrenRequests",
                                    [NSArray arrayWithObjects: @"name", @"x", @"y", @"status", @"numAlerts", nil], @"properties",
                                    [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                    [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                    [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                    nil];
    
    NSDictionary* equipmentStatusDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                               @"EQUIPMENTSTATUS", @"type",
                                               [NSNull null], @"id",
                                               [NSNull null], @"parentsRequests",
                                               [NSNull null], @"linkRequests",
                                               [NSNull null], @"childrenRequests",
                                               [NSArray arrayWithObjects: @"name", @"x", @"y", @"status", @"numAlerts", nil], @"properties",
                                               [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                               [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                               [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                               nil];
    
    NSDictionary* verticalTemperatureDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                   @"VERTICALTEMPERATURE", @"type",
                                                   [NSNull null], @"id",
                                                   [NSNull null], @"parentsRequests",
                                                   [NSNull null], @"linkRequests",
                                                   [NSNull null], @"childrenRequests",
                                                   [NSArray arrayWithObjects: @"name", @"x", @"y", @"status", @"numAlerts", nil], @"properties",
                                                   [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                                   [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                                   [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                                   nil];
    
    NSDictionary* ion_deltaDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                         @"ION_DELTA", @"type",
                                         [NSNull null], @"id",
                                         [NSNull null], @"parentsRequests",
                                         [NSNull null], @"linkRequests",
                                         [NSNull null], @"childrenRequests",
                                         [NSArray arrayWithObjects: @"name", @"x", @"y", @"status", @"numAlerts", nil], @"properties",
                                         [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                         [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                         [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                         nil];
    
    NSDictionary* ion_wyeDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"ION_WYE", @"type",
                                       [NSNull null], @"id",
                                       [NSNull null], @"parentsRequests",
                                       [NSNull null], @"linkRequests",
                                       [NSNull null], @"childrenRequests",
                                       [NSArray arrayWithObjects: @"name", @"x", @"y", @"status", @"numAlerts", nil], @"properties",
                                       [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                       [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                       [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                       nil];
    
    NSDictionary* ion_wirelessDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                            @"ION_WIRELESS", @"type",
                                            [NSNull null], @"id",
                                            [NSNull null], @"parentsRequests",
                                            [NSNull null], @"linkRequests",
                                            [NSNull null], @"childrenRequests",
                                            [NSArray arrayWithObjects: @"name", @"x", @"y", @"status", @"numAlerts", nil], @"properties",
                                            [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                            [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                            [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                            nil];
    
    NSDictionary* pressureDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                        @"PRESSURE", @"type",
                                        [NSNull null], @"id",
                                        [NSNull null], @"parentsRequests",
                                        [NSNull null], @"linkRequests",
                                        [NSNull null], @"childrenRequests",
                                        [NSArray arrayWithObjects: @"name", @"x", @"y", @"status", @"numAlerts", nil], @"properties",
                                        [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                        [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                        [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                        nil];
    
    NSDictionary* rackDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @"RACK", @"type",
                                    [NSNull null], @"id",
                                    [NSNull null], @"parentsRequests",
                                    [NSArray arrayWithObjects:dcwsnsendorDictionary, nil], @"linkRequests",
                                    [NSNull null], @"childrenRequests",
                                    [NSArray arrayWithObjects: @"name", @"x", @"y", @"status", @"numAlerts", @"door", nil], @"properties",
                                    [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                    [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                    [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                    nil];
    
    NSDictionary* cracDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @"CRAC", @"type",
                                    [NSNull null], @"id",
                                    [NSNull null], @"parentsRequests",
                                    [NSNull null], @"linkRequests",
                                    [NSNull null], @"childrenRequests",
                                    [NSArray arrayWithObjects: @"name", @"x", @"y", @"status", @"numAlerts", nil], @"properties",
                                    [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                    [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                    [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                    nil];
    NSMutableArray *dcProperty = [NSMutableArray arrayWithObjects:@"width", @"height", @"name", nil];
    if (liveImagingTypeSwitch == LiveImagingTypeOff && isImageNeeded == YES)
    {
        [dcProperty addObject:@"image"];
    }
    NSDictionary* dcDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                  @"DC", @"type",
                                  datacenterId, @"id",
                                  [NSNull null], @"parentsRequests",
                                  [NSNull null], @"linkRequests",
                                  dcProperty, @"properties",
                                  [NSArray arrayWithObjects:rackDictionary, cracDictionary, pressureDictionary, ion_wyeDictionary, ion_deltaDictionary, ion_wirelessDictionary, verticalTemperatureDictionary, equipmentStatusDictionary, leakDictionary, genericTemperatureDictionary, doorDictionary, nil], @"childrenRequests",
                                  [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                  [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                  [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                  nil];
    
    NSArray* requestArray = [NSArray arrayWithObject:[NSArray arrayWithObject:dcDictionary]];
    
    int requestId = [jsonRPCObjectManager callMethod:@"getObjectsData" withParameters:requestArray onCompletion:^(NSString *methodName, NSInteger callId, id methodResult, DSJSONRPCError *methodError, NSError *internalError)
     {
         MMFloorPlanInfo* fpInfo = [[[MMFloorPlanInfo alloc] init] autorelease];
         if (!internalError && methodResult)
         {
             //parsing
             id rootSubElements = [methodResult objectForKey:@"subElements"];
             for (id dataCenter in rootSubElements)
             {
                 if ([dataCenter objectForKey:@"subElements"] == [NSNull null])
                 {
                     continue;
                 }
                 for(id dataCenterIdAttribute in [dataCenter objectForKey:@"subElements"])
                 {
                     NSString *elementNodeName = [dataCenterIdAttribute objectForKey:@"name"];
                     if([elementNodeName compare:@"width"] == NSOrderedSame)
                     {
                         fpInfo.width = [self getAttributeDoubleValue:dataCenterIdAttribute];
                     }
                     else if([elementNodeName compare:@"height"] == NSOrderedSame)
                     {
                         fpInfo.height = [self getAttributeDoubleValue:dataCenterIdAttribute];
                     }
                     else if([elementNodeName compare:@"image"] == NSOrderedSame)
                     {
                         id attributes = [dataCenterIdAttribute objectForKey:@"attributes"];
                         if (attributes == [NSNull null])
                         {
                             continue;
                         }
                         for (id attr in attributes)
                         {
                             NSString * encodedString = [attr objectForKey:@"value"];
                             int input_length = [encodedString length];
                             int output_length = ((input_length + BASE64_UNIT_SIZE - 1) / BASE64_UNIT_SIZE) * BINARY_UNIT_SIZE;
                             char decodedData[output_length];                             
                             
                             int retVal = [self decodeBase64:encodedString p_output:decodedData];
                             if(retVal != 0)
                             {
                                 NSLog(@"Parsing:error!");
                             }
                             NSString *attr_name = [attr objectForKey:@"name"];
                             if ([attr_name compare:@"v"] == NSOrderedSame)
                             {
                                 NSData *imagedata = [[NSData alloc] initWithBytes:decodedData length:output_length];
                                 fpInfo.image = [[UIImage alloc]initWithData:imagedata];
                             }
                         }
                     }
                     else if([elementNodeName compare:@"children"] == NSOrderedSame)
                     {
                         if ([dataCenterIdAttribute objectForKey:@"subElements"] == [NSNull null])
                         {
                             continue;
                         }
                         for(id floorelement in [dataCenterIdAttribute objectForKey:@"subElements"])
                         {
                             NSString *elementNodeName = [floorelement objectForKey:@"name"];
                             if([elementNodeName compare:@"RACK"] == NSOrderedSame)
                             {
                                 [fpInfo.RACK addObject:[self getFloorPlanElement:floorelement]];
                             }
                             else if([elementNodeName compare:@"CRAC"] == NSOrderedSame)
                             {
                                 [fpInfo.CRAC addObject:[self getFloorPlanElement:floorelement]];
                             }
                             else if([elementNodeName compare:@"PRESSURE"] == NSOrderedSame)
                             {
                                 [fpInfo.pressure addObject:[self getFloorPlanElement:floorelement]];
                             }
                             else if([elementNodeName compare:@"GENERICTEMPERATURE"] == NSOrderedSame)
                             {
                                 [fpInfo.generic_temp addObject:[self getFloorPlanElement:floorelement]];
                             }
                             else if([elementNodeName compare:@"LEAK"] == NSOrderedSame)
                             {
                                 [fpInfo.leak addObject:[self getFloorPlanElement:floorelement]];
                             }
                             else if([elementNodeName compare:@"EQUIPMENTSTATUS"] == NSOrderedSame)
                             {
                                 [fpInfo.equipment_status addObject:[self getFloorPlanElement:floorelement]];
                             }
                             else if([elementNodeName compare:@"VERTICALTEMPERATURE"] == NSOrderedSame)
                             {
                                 [fpInfo.vert_temp addObject:[self getFloorPlanElement:floorelement]];
                             }
                             else if([elementNodeName compare:@"ION_DELTA"] == NSOrderedSame)
                             {
                                 [fpInfo.ion_delta addObject:[self getFloorPlanElement:floorelement]];
                             }
                             else if([elementNodeName compare:@"ION_WYE"] == NSOrderedSame)
                             {
                                 [fpInfo.ion_wye addObject:[self getFloorPlanElement:floorelement]];
                             }
                             else if([elementNodeName compare:@"ION_WIRELESS"] == NSOrderedSame)
                             {
                                 [fpInfo.ion_wireless addObject:[self getFloorPlanElement:floorelement]];
                             }
                             else if([elementNodeName compare:@"DOOR"] == NSOrderedSame)
                             {
                                 [fpInfo.doors addObject:[self getFloorPlanElement:floorelement]];
                             }
                         }
                     }
                 }
             }
             success(fpInfo);
         }
         else
         {
             failure(internalError);
         }
     }];
    return requestId;
}

- (int)getFloorPlanData:(NSString*)datacenterId filterMask:(MMFloorPlanFilter)filterMask success:(void(^)(MMFloorPlanInfo *))success  
                 failure:(void(^)(NSError *error))failure
{
    assert(_userInfo);
    NSMutableArray *florplanObjects = [NSMutableArray array];
    if(filterMask & MMFloorPlan_RACK) [florplanObjects addObject:@"RACK"];
    if(filterMask & MMFloorPlan_CRAC) [florplanObjects addObject:@"CRAC"];
    if(filterMask & MMFloorPlan_PRESSURE) [florplanObjects addObject:@"PRESSURE"];
    if(filterMask & MMFloorPlan_POWER_METER) 
    {
        [florplanObjects addObject:@"ION_WYE"];
        [florplanObjects addObject:@"ION_DELTA"];
        [florplanObjects addObject:@"ION_WIRELESS"];
    }
    if(filterMask & MMFloorPlan_DOOR) [florplanObjects addObject:@"DOOR"];
    if(filterMask & MMFloorPlan_EQUIPMENT) [florplanObjects addObject:@"EQUIPMENTSTATUS"];
    if(filterMask & MMFloorPlan_VERTICAL_TEMP) [florplanObjects addObject:@"VERTICALTEMPERATURE"];
    if(filterMask & MMFloorPlan_GENERIC_TEMP) [florplanObjects addObject:@"GENERICTEMPERATURE"];
    if(filterMask & MMFloorPlan_LEAK) [florplanObjects addObject:@"LEAK"];
    int requestId = [jsonRPCObjectManager callMethod:@"getFloorPlanData" withParameters:[NSArray arrayWithObjects:datacenterId, florplanObjects, nil] onCompletion:^(NSString *methodName, NSInteger callId, id methodResult, DSJSONRPCError *methodError, NSError *internalError)
     {
         MMFloorPlanInfo* fpInfo = [[[MMFloorPlanInfo alloc] init] autorelease];
         if (!internalError && methodResult)
         {
             for (NSArray* floorplanObject in methodResult)
             {
                 int count = [floorplanObject count];
                 FloorPlanElement *element = [[FloorPlanElement alloc]init];
                 element.element_id = [floorplanObject objectAtIndex:0];
                 element.name = [floorplanObject objectAtIndex:0];
                 element.status = [[floorplanObject objectAtIndex:1] intValue];
                 element.num_alerts = [[floorplanObject objectAtIndex:2] intValue];
                 element.x = [[floorplanObject objectAtIndex:3] intValue];
                 element.y = [[floorplanObject objectAtIndex:4] intValue];
                 if (count == 6) 
                 {
                     element.door = [[floorplanObject objectAtIndex:5] intValue];
                 }
                 if ([element.element_id rangeOfString:@"VERTICALTEMPERATURE"].location != NSNotFound) 
                 {
                     [fpInfo.vert_temp addObject:element];
                 }
                 else if ([element.element_id rangeOfString:@"DOOR"].location != NSNotFound)
                 {
                     [fpInfo.doors addObject:element];
                 }
                 else if ([element.element_id rangeOfString:@"GENERICTEMPERATURE"].location != NSNotFound)
                 {
                     [fpInfo.generic_temp addObject:element];
                 }
                 else if ([element.element_id rangeOfString:@"LEAK"].location != NSNotFound)
                 {
                     [fpInfo.leak addObject:element];
                 }
                 else if ([element.element_id rangeOfString:@"EQUIPMENTSTATUS"].location != NSNotFound)
                 {
                     [fpInfo.equipment_status addObject:element];
                 }
                 else if ([element.element_id rangeOfString:@"ION_DELTA"].location != NSNotFound)
                 {
                     [fpInfo.ion_delta addObject:element];
                 }
                 else if ([element.element_id rangeOfString:@"ION_WYE"].location != NSNotFound)
                 {
                     [fpInfo.ion_wye addObject:element];
                 }
                 else if ([element.element_id rangeOfString:@"ION_WIRELESS"].location != NSNotFound)
                 {
                     [fpInfo.ion_wireless addObject:element];
                 }
                 else if ([element.element_id rangeOfString:@"PRESSURE"].location != NSNotFound)
                 {
                     [fpInfo.pressure addObject:element];
                 }
                 else if ([element.element_id rangeOfString:@"RACK"].location != NSNotFound)
                 {
                     [fpInfo.RACK addObject:element];
                 }
                 else if ([element.element_id rangeOfString:@"CRAC"].location != NSNotFound)
                 {
                     [fpInfo.CRAC addObject:element];
                 }
                 [element release];
             }
             success(fpInfo);
         }
         else
         {
             failure(internalError);
         }
     }];
    return requestId;
}
- (int)getLiveImagingTypesWithSuccess:(void(^)(NSArray *))success
                               failure:(void(^)(NSError *error))failure
{
    int requestId = [jsonRPCObjectManager callMethod:@"getLiveImagingTypes" withParameters:nil onCompletion:^(NSString *methodName, NSInteger callId, id methodResult, DSJSONRPCError *methodError, NSError *internalError)
     {
         if (!internalError && methodResult)
         {
             NSMutableArray *array = [[NSMutableArray alloc]initWithCapacity:8];
             for (id liveImagingType in methodResult)
             {
                 MMliveImagingType *litype = [[MMliveImagingType alloc] init];
                 litype.name = [liveImagingType objectForKey:@"name"];
                 litype.dataClass = [[liveImagingType objectForKey:@"dataClass"] intValue];
                 litype.layer = [[liveImagingType objectForKey:@"layer"] intValue];
                 [array addObject:litype];
                 [litype release];
             }
             NSArray *liveImagingTypesArray = [NSArray arrayWithArray:array];
             [array release];
             success(liveImagingTypesArray);
         }
         else
         {
             failure(internalError);
         }
     }];
    return requestId;
}

- (int)getFloorPlan:(NSString*)objectId DataClassID:(int)dataClassId Layer:(int)layer success:(void(^)(UIImage *))success
             failure:(void(^)(NSError *error))failure
{
    int requestId = -1;
    if (liveImagingTypeSwitch == LiveImagingTypeOn) 
    {
        requestId = [jsonRPCObjectManager callMethod:@"getFloorplan" withParameters:[NSArray arrayWithObjects:objectId, [NSNumber numberWithInt:dataClassId], [NSNumber numberWithInt:layer], nil] onCompletion:^(NSString *methodName, NSInteger callId, id methodResult, DSJSONRPCError *methodError, NSError *internalError)
         {
             if (!internalError && methodResult)
             {
                 if (methodResult != [NSNull null])
                 {
                     NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:methodResult]];
                     success([UIImage imageWithData:data]);
                 }
                 else
                 {
                     success(nil);
                 }
             }
             else
             {
                 failure(internalError);
             }
         }];
    }
    else
    {
        success(nil);
    }
    return requestId;
}

- (int)getFloorplanSchemeImage:(NSString*)dataCenterId lastUpdateDate:(NSDate*)date  success:(void(^)(UIImage *))success
                        failure:(void(^)(NSError *error))failure
{
    int requestId = -2;
    if (liveImagingTypeSwitch == LiveImagingTypeOff)
    {
        assert(dataCenterId);
        DSJSONRPC *jsonRPCS = [[DSJSONRPC alloc] initWithServiceEndpoint:[NSURL URLWithString:[self floorplanSchemeImageManagerURL:dataCenterId]] andHTTPHeaders:[NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"JSESSIONID=%@", _userInfo.sessionId] forKey:@"Cookie"]];
        requestId = [jsonRPCS getFloorplanSchemeImageWithLastDate:date onCompletion:^(UIImage* methodResult, NSError *internalError) {
            if (!internalError && methodResult)
            {
                success(methodResult);
            }
            else if (!internalError)
            {
                success(nil);
            }
            else
            {
                failure(internalError);
            }
        } ];
        [jsonRPCS release];
    }
    else
    {
        success(nil);
    }
    return requestId;
}

- (int)getFloorPlanSize:(NSString*)datacenterId success:(void(^)(CGSize))success  
                 failure:(void(^)(NSError *error))failure
{
    assert(_userInfo);
    NSMutableArray *dcProperty = [NSMutableArray arrayWithObjects:@"width", @"height", nil];
    NSDictionary* dcDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                  @"DC", @"type",
                                  datacenterId, @"id",
                                  [NSNull null], @"parentsRequests",
                                  [NSNull null], @"linkRequests",
                                  dcProperty, @"properties",
                                  [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                  [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                  [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                  nil];
    
    NSArray* requestArray = [NSArray arrayWithObject:[NSArray arrayWithObject:dcDictionary]];
    
    int requestId = [jsonRPCObjectManager callMethod:@"getObjectsData" withParameters:requestArray onCompletion:^(NSString *methodName, NSInteger callId, id methodResult, DSJSONRPCError *methodError, NSError *internalError)
    {
        if (!internalError && methodResult)
        {
            CGSize floorplanSize = CGSizeZero;
            //parsing
            id rootSubElements = [methodResult objectForKey:@"subElements"];
            for (id dataCenter in rootSubElements)
            {
                if ([dataCenter objectForKey:@"subElements"] == [NSNull null])
                {
                    continue;
                }
                for(id dataCenterIdAttribute in [dataCenter objectForKey:@"subElements"])
                {
                    NSString *elementNodeName = [dataCenterIdAttribute objectForKey:@"name"];
                    if([elementNodeName compare:@"width"] == NSOrderedSame)
                    {
                        floorplanSize.width = [self getAttributeDoubleValue:dataCenterIdAttribute];
                    }
                    else if([elementNodeName compare:@"height"] == NSOrderedSame)
                    {
                        floorplanSize.height = [self getAttributeDoubleValue:dataCenterIdAttribute];
                    }
                }
            }
            success(floorplanSize);
        }
        else
        {
            failure(internalError);
        }
    }];
    return requestId;
}

- (NSString *)userManagerURL
{
    assert(_serviceBaseURL);
    NSString *prefix = isSecureConnection == YES ? @"https://" : @"http://";
    NSRange range = [_serviceBaseURL rangeOfString:prefix];
    if (range.length == 0)
    {
        [_serviceBaseURL release];
        _serviceBaseURL = [[NSString stringWithFormat:@"%@%@:%d",prefix, _serviceBaseURL, port] retain];
    }
    return [NSString stringWithFormat:@"%@/mobile/userManager", _serviceBaseURL];
}

- (NSString *)systemManagerURL
{
    assert(_serviceBaseURL);
    NSString *prefix = isSecureConnection == YES ? @"https://" : @"http://";
    NSRange range = [_serviceBaseURL rangeOfString:prefix];
    if (range.length == 0)
    {
        [_serviceBaseURL release];
        _serviceBaseURL = [[NSString stringWithFormat:@"%@%@:%d",prefix, _serviceBaseURL, port] retain];
    }
    return [NSString stringWithFormat:@"%@/mobile/statusManager", _serviceBaseURL];
}

- (NSString *)statusManagerURL
{
    assert(_serviceBaseURL);
    NSString *prefix = isSecureConnection == YES ? @"https://" : @"http://";
    NSRange range = [_serviceBaseURL rangeOfString:prefix];
    if (range.length == 0)
    {
        [_serviceBaseURL release];
        _serviceBaseURL = [[NSString stringWithFormat:@"%@%@:%d",prefix, _serviceBaseURL, port] retain];
    }
    return [NSString stringWithFormat:@"%@/mobile/statusManager", _serviceBaseURL];
}

- (NSString *)alertManagerURL
{
    assert(_serviceBaseURL);
    NSString *prefix = isSecureConnection == YES ? @"https://" : @"http://";
    NSRange range = [_serviceBaseURL rangeOfString:prefix];
    if (range.length == 0)
    {
        [_serviceBaseURL release];
        _serviceBaseURL = [[NSString stringWithFormat:@"%@%@:%d",prefix, _serviceBaseURL, port] retain];
    }
    return [NSString stringWithFormat:@"%@/mobile/alertManager", _serviceBaseURL];
}

- (NSString *)objectManagerURL
{
    //return @"http://192.168.102.201:8080/mobile/objectManager";
    assert(_serviceBaseURL);
    NSString *prefix = isSecureConnection == YES ? @"https://" : @"http://";
    NSRange range = [_serviceBaseURL rangeOfString:prefix];
    if (range.length == 0)
    {
        [_serviceBaseURL release];
        _serviceBaseURL = [[NSString stringWithFormat:@"%@%@:%d",prefix, _serviceBaseURL, port] retain];
    }
    return [NSString stringWithFormat:@"%@/mobile/objectManager", _serviceBaseURL];
}

- (NSString *)configManagerURL
{
    assert(_serviceBaseURL);
    NSString *prefix = isSecureConnection == YES ? @"https://" : @"http://";
    NSRange range = [_serviceBaseURL rangeOfString:prefix];
    if (range.length == 0)
    {
        [_serviceBaseURL release];
        _serviceBaseURL = [[NSString stringWithFormat:@"%@%@:%d",prefix, _serviceBaseURL, port] retain];
    }
    return [NSString stringWithFormat:@"%@/mobile/configManager", _serviceBaseURL];
}

- (NSString *)floorplanSchemeImageManagerURL:(NSString*)dcId
{
    assert(_serviceBaseURL);
    NSString *prefix = isSecureConnection == YES ? @"https://" : @"http://";
    NSRange range = [_serviceBaseURL rangeOfString:prefix];
    if (range.length == 0)
    {
        [_serviceBaseURL release];
        _serviceBaseURL = [[NSString stringWithFormat:@"%@%@:%d", prefix, _serviceBaseURL, port] retain];
    }
    return [NSString stringWithFormat:@"%@/mobile/getImage?id=%@", _serviceBaseURL, dcId];
}

- (void)initManagers
{
    [jsonRPCObjectManager release];
    [jsonRPCConfigManager release];
    [jsonRPCStatusManager release];
    [jsonRPCAlertManager release];
    [jsonRPCSystemManager release];
    jsonRPCObjectManager = [[DSJSONRPC alloc] initWithServiceEndpoint:[NSURL URLWithString:[self objectManagerURL]] andHTTPHeaders:[NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"JSESSIONID=%@", _userInfo.sessionId] forKey:@"Cookie"]];
    jsonRPCStatusManager = [[DSJSONRPC alloc] initWithServiceEndpoint:[NSURL URLWithString:[self statusManagerURL]] andHTTPHeaders:[NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"JSESSIONID=%@", _userInfo.sessionId] forKey:@"Cookie"]];
    jsonRPCAlertManager = [[DSJSONRPC alloc] initWithServiceEndpoint:[NSURL URLWithString:[self alertManagerURL]] andHTTPHeaders:[NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"JSESSIONID=%@", _userInfo.sessionId] forKey:@"Cookie"]];
    jsonRPCSystemManager = [[DSJSONRPC alloc] initWithServiceEndpoint:[NSURL URLWithString:[self systemManagerURL]] andHTTPHeaders:[NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"JSESSIONID=%@", _userInfo.sessionId] forKey:@"Cookie"]];
    jsonRPCConfigManager = [[DSJSONRPC alloc] initWithServiceEndpoint:[NSURL URLWithString:[self configManagerURL]] andHTTPHeaders:[NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"JSESSIONID=%@", _userInfo.sessionId] forKey:@"Cookie"]];
}


-(void)cancelRequests:(NSMutableArray *)requestIds
{
    jsonRPCUserManager.isCanceled = YES;
    for (id requestIdObject in requestIds)
    {
        [jsonRPCUserManager cancelConnectionById:[requestIdObject intValue]];
        [jsonRPCStatusManager cancelConnectionById:[requestIdObject intValue]];
        [jsonRPCObjectManager cancelConnectionById:[requestIdObject intValue]];
        [jsonRPCAlertManager cancelConnectionById:[requestIdObject intValue]];
        [jsonRPCSystemManager cancelConnectionById:[requestIdObject intValue]];
        [jsonRPCConfigManager cancelConnectionById:[requestIdObject intValue]];
    }
}

- (int)decodeBase64:(NSString* )input 
                                  p_output:(char *)decodedData
{
#define xx 65
    int input_length = [input length];
    char* input_array = (char*)malloc(input_length);

    int k;
    for(k=0; k < input_length; ++k)
    {
        input_array[k] =[input characterAtIndex:k];
    }
    
    int i=0;
    int j=0;
       

    unsigned char base64DecodeLookup[256] =
    {
        xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
        xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
        xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 62, xx, xx, xx, 63, 
        52, 53, 54, 55, 56, 57, 58, 59, 60, 61, xx, xx, xx, xx, xx, xx, 
        xx,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 
        15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, xx, xx, xx, xx, xx, 
        xx, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 
        41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, xx, xx, xx, xx, xx, 
        xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
        xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
        xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
        xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
        xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
        xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
        xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
        xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, xx, 
    };
    
    while (i < input_length)
    {
        unsigned char accumulated[BASE64_UNIT_SIZE];
        int accumulateIndex = 0;

		while (i < input_length)
		{
			unsigned char decode = base64DecodeLookup[input_array[i++]];
			if (decode != xx)
			{
				accumulated[accumulateIndex] = decode;
				accumulateIndex++;
				
				if (accumulateIndex == BASE64_UNIT_SIZE)
				{
					break;
				}
			}
		}
	
		// Store the 6 bits from each of the 4 characters as 3 bytes
		if(accumulateIndex >= 2)  
			decodedData[j] = (accumulated[0] << 2) | (accumulated[1] >> 4);  
		if(accumulateIndex >= 3)  
			decodedData[j + 1] = (accumulated[1] << 4) | (accumulated[2] >> 2);  
		if(accumulateIndex >= 4)  
			decodedData[j + 2] = (accumulated[2] << 6) | accumulated[3];
       
		j += accumulateIndex - 1;
    }
    free(input_array);
    return 0;
}

- (void)setLiveImagingSwitch:(LiveImagingTypeSwitch)state
{
    liveImagingTypeSwitch = state;
}

+ (NSString *)getUUID
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return [(NSString *)string autorelease];
}

- (int)getEmergencyContact: (NSString*)datacenterId  success:(void(^)(MMEmergencyContact *))success
                           failure:(void(^)(NSError *error))failure
{
    NSDictionary* emergencyContactDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"emergencyContact", @"type",
                                   [NSNull null], @"id",
                                   [NSNull null], @"parentsRequests",
                                   [NSNull null], @"linkRequests",
                                   [NSNull null], @"childrenRequests",
                                   [NSArray arrayWithObjects:@"EmergencyContact", nil], @"properties",
                                   [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                   [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                   [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                   [NSNumber numberWithInt:0], @"additionalInfo",
                                   nil];
    
    NSMutableArray *dcProperty = [NSMutableArray arrayWithObjects:@"emergencyName", @"emergencyTitle", @"emergencyMobilePhone", @"emergencyOfficePhone", @"emergencyEmail", nil];
    
    NSDictionary* dcDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                  @"DC", @"type",
                                  datacenterId, @"id",
                                  [NSNull null], @"parentsRequests",
                                  [NSNull null], @"linkRequests",
                                  dcProperty, @"properties",
                                  [NSArray arrayWithObjects:emergencyContactDictionary, nil], @"childrenRequests",
                                  [NSNumber numberWithBool:NO], @"requestDirectChildrenOnly",
                                  [NSNumber numberWithBool:YES], @"requestDirectParentsOnly",
                                  [NSNumber numberWithBool:NO], @"requestChildrenNumberOnly",
                                  [NSNumber numberWithInt:0], @"additionalInfo",
                                  nil];
     NSArray* requestArray = [NSArray arrayWithObject:[NSArray arrayWithObject:dcDictionary]];
    
    int requestId = [jsonRPCObjectManager callMethod:@"getObjectsData" withParameters:requestArray onCompletion:^(NSString *methodName, NSInteger callId, id methodResult, DSJSONRPCError *methodError, NSError *internalError)
     {
       MMEmergencyContact* ecInfo = [[[MMEmergencyContact alloc] init] autorelease];
         if (!internalError && methodResult)
         {
             //parsing
             id rootSubElements = [methodResult objectForKey:@"subElements"];
             for (id dataCenter in rootSubElements)
             {
                 if ([dataCenter objectForKey:@"subElements"] == [NSNull null])
                 {
                     continue;
                 }
                 for(id dataCenterIdAttribute in [dataCenter objectForKey:@"subElements"])
                 {
                     NSString *elementNodeName = [dataCenterIdAttribute objectForKey:@"name"];
                     if([elementNodeName compare:@"emergencyName"] == NSOrderedSame)
                     {
                         ecInfo.emergencyName = [self getAttributeStringValue:dataCenterIdAttribute];
                     }
                     else if([elementNodeName compare:@"emergencyTitle"] == NSOrderedSame)
                     {
                         ecInfo.emergencyTitle = [self getAttributeStringValue:dataCenterIdAttribute];
                     }
                     else if([elementNodeName compare:@"emergencyMobilePhone"] == NSOrderedSame)
                     {
                        ecInfo.emergencyMobilePhone = [self getAttributeStringValue:dataCenterIdAttribute];
                     }
                     else if([elementNodeName compare:@"emergencyOfficePhone"] == NSOrderedSame)
                     {
                        ecInfo.emergencyOfficePhone = [self getAttributeStringValue:dataCenterIdAttribute];
                     }
                     else if([elementNodeName compare:@"emergencyEmail"] == NSOrderedSame)
                     {
                        ecInfo.emergencyEmail = [self getAttributeStringValue:dataCenterIdAttribute];
                     }
                 }
             }
             success(ecInfo);
         }
         else
         {
             failure(internalError);
         }
     }];
    return requestId;
}

- (int)getUIPrivileges:(void(^)(MMUIPrivileges))success
{
    int requestId = [jsonRPCConfigManager callMethod:@"getUIPrivileges" withParameters:nil onCompletion:^(NSString *methodName, NSInteger callId, id methodResult, DSJSONRPCError *methodError, NSError *internalError)
     {
         MMUIPrivileges priveleges = {NO, NO};
         if (!internalError && methodResult)
         {
             for (id privelege in methodResult)
             {
                 if ([privelege isEqualToString:@"showSystemHealth"])
                 {
                     priveleges.showSystemHealth = YES;
                 }
                 else if ([privelege isEqualToString:@"showPUE"])
                 {
                     priveleges.showPUE = YES;
                 }
             }
         }
         success(priveleges);
     }];
    return requestId;
}

@end