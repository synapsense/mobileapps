//
//  MMFloorPlanInfo.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMFloorPlanInfo.h"

@implementation FloorPlanElement

@synthesize element_id;
@synthesize name;
@synthesize status;
@synthesize num_alerts;
@synthesize x;
@synthesize y;
@synthesize door;

- (void)dealloc
{
    self.element_id = nil;
    self.name = nil;
    [super dealloc];
}

@end


@implementation MMFloorPlanInfo

@synthesize width;
@synthesize height;
@synthesize RACK;
@synthesize CRAC;
@synthesize image;
@synthesize pressure;
@synthesize ion_wye;
@synthesize ion_delta;
@synthesize doors;
@synthesize ion_wireless;
@synthesize generic_temp;
@synthesize vert_temp;
@synthesize equipment_status;
@synthesize leak;

-(id)init
{
    self = [super init];
    self.width = 0.0;
    self.height = 0.0;
    self.RACK = [NSMutableArray array];
    self.CRAC = [NSMutableArray array];
    self.pressure = [NSMutableArray array];
    self.ion_delta = [NSMutableArray array];
    self.ion_wye = [NSMutableArray array];
    self.doors = [NSMutableArray array];
    self.ion_wireless = [NSMutableArray array];
    self.generic_temp = [NSMutableArray array];
    self.vert_temp = [NSMutableArray array];
    self.equipment_status = [NSMutableArray array];
    self.leak = [NSMutableArray array];
    return self;
}

-(void)dealloc
{
    if(self.RACK) [self.RACK release];
    if(self.CRAC) [self.CRAC release];
    if(self.pressure) [self.pressure release];
    if(self.ion_delta) [self.ion_delta release];
    if(self.ion_wye) [self.ion_wye release];
    if(self.image) [self.image release];
    if(self.doors) [self.doors release];
    if(self.ion_wireless) [self.ion_wireless release];
    if(self.generic_temp) [self.generic_temp release];
    if(self.vert_temp) [self.vert_temp release];
    if(self.equipment_status) [self.equipment_status release];
    if(self.leak) [self.leak release];
    [super dealloc];
}

@end
