//
//  MMDataCenterInfo.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMAlertInfo.h"

@implementation MMAlertInfo

@synthesize alertId,
            name,
            object,
            alertStatus,
            alertPriority,
            lastActionUser,
            location,
            lastActionTime,
            alertTimestamp,
            alertDescription,
            alertMessage,
            alertComment,
            objectName;

- (void)dealloc
{
    self.name = nil;
    self.alertDescription = nil;
    self.alertMessage = nil;
    self.alertComment = nil;
    self.objectName = nil;
    self.lastActionUser = nil;
    self.location = nil;
    self.alertTimestamp = nil;
    self.lastActionTime = nil;
    if (object.type) {
        [object.type release];
    }
    if (object.id) {
        [object.id release];
    }
    [super dealloc];
}

@end
