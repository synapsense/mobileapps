//
//  MMComponentStatus.m
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#import "MMComponentStatus.h"

@implementation MMComponentStatus

@synthesize component,
            status,
            message;
- (void)dealloc
{
    self.message = nil;
    [super dealloc];
}

@end
