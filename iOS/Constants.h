//
//  Constants.h
//  SynapSense DCM Mobile Platform
//
//  Created by Igor Botov on 7/31/12.
//  Copyright (c) 2012 SynapSense. All rights reserved.
//

#ifndef MobileMonitor_Constants_h
#define MobileMonitor_Constants_h


#define REQUEST_FREQUENCY       @"Request frequency"
#define REQUEST_TIME_PERIOD     @"Request time period"
#define POP_UP_NOTIFICATION     @"Popup Notification"
#define MAP_PRESENTATION        @"Map presentation"
#define STATUS_FILTER           @"Status filter"
#define PRIORITY_FILTER         @"Priority filter"
#define FLOORPLAN_FILTER        @"Floorplan filter"
#define ALERT_SORTING_CONDITION @"Alert sorting by"
#define USE_HTTPS               @"Use https"
#define SERVER_URL              @"Server URL"
#define UNSECURED_PORT          @"Unsecured port"
#define SECURED_PORT            @"Secured port"
#define UI_THEME                @"UITheme"
#define MAP_VIEW_REGION_LATITUDE      @"MAP_VIEW_REGION_LATITUDE"
#define MAP_VIEW__REGION_LONGITUDE      @"MAP_VIEW__REGION_LONGITUDE"
#define MAP_VIEW__REGION_DELTA_LATITUDE      @"MAP_VIEW__REGION_DELTA_LATITUDE"
#define MAP_VIEW__REGION_DELTA_LONGITUDE      @"MAP_VIEW__REGION_DELTA_LONGITUDE"
#define MAP_VIEW_REGION_UNKNOWN -1

#define VIEW_WIDTH              (self.view.frame.size.width)
#define VIEW_HEIGHT             (self.view.frame.size.height)

#define REAL_HIT_AREA_FOR_BUTTONS         45
#define HORIZONTAL_INDENT                 20
#define BADGE_COLOR_COMPONENTS            {1, 0, 0, 1, 1, 0, 0, 0.5}
#define SETTINGS_BUTTON_SIZE              35
#define STATUS_BAR_HEIGHT                 20
#define TAB_BAR_HEIGHT                    49
#define NAV_BAR_PORTRAIT_HEIGHT           44

#define PIECHART_RED_COLOR [UIColor colorWithRed:211.0f/255 green:37.0f/255 blue:41.0f/255 alpha:1]         //0xD32529
#define PIECHART_BLUE_COLOR [UIColor colorWithRed:31.0f/255 green:51.0f/255 blue:104.0f/255 alpha:1]        //0x1F3668
#define PIECHART_GREEN_COLOR [UIColor colorWithRed:0 green:121.0f/255 blue:62.0f/255 alpha:1]               //0x00793E
#define PIECHART_YELLOW_COLOR [UIColor colorWithRed:240.0f/255 green:153.0f/255 blue:2.0f/255 alpha:1]      //0xF099022
#define PIECHART_GRAY_COLOR [UIColor colorWithRed:149.0f/255 green:149.0f/255 blue:1 alpha:1]               //0x9595FF

#endif
