package com.synapsense.mobile.status;

import java.io.Serializable;

public class ComponentStatus implements Serializable {

	private static final long serialVersionUID = -5769461150719192210L;
	private int component;
	private int status;
	private String message;
	
	/**
	 * @param component
	 *            Number of the component
	 * @see Components
	 * @param status
	 *            Status of the component
	 * @see Status
	 */
	public ComponentStatus(int component, int status) {
		this.component = component;
		this.status = status;
	}

	public ComponentStatus() {}

	public String getMessage() {
    	return message;
    }

	public void setMessage(String message) {
    	this.message = message;
    }
	
	public int getComponent() {
		return component;
	}

	public void setComponent(int component) {
		this.component = component;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
