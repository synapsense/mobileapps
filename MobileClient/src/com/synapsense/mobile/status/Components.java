package com.synapsense.mobile.status;

// ESCA-JAVA0257:
/** Components of the system */
public interface Components {

	int DEVICE_MANAGER = 0;
	int IMAGING_SERVER = 1;
	int ENVIRONMENTAL_SERVER = 2;
	int CONTROL_PROCESS = 3;
}
