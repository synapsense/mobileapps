package com.synapsense.mobile.status;

// ESCA-JAVA0257:
/** Statuses of the components */
public interface Status {

	int UNKNOWN = 0;
	int STOPPED = 1;
	int RUNNING = 2;
	int PARTIAL = 3;
}
