package com.synapsense.mobile.user;

import java.io.Serializable;

/** Represents a preference of a user */
public class UserPreference implements Serializable {

	private static final long serialVersionUID = -7445299265335626134L;
	/**
	 * Preference name
	 */
	private String preferenceName;
	/**
	 * Preference value
	 */
	private String value;

	public UserPreference() {}

	public UserPreference(String preferenceName, String value) {
		this.preferenceName = preferenceName;
		this.value = value;
	}

	public String getPreferenceName() {
		return preferenceName;
	}

	public void setPreferenceName(String preferenceName) {
		this.preferenceName = preferenceName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder(100);
		sb.append("User preference : ");
		sb.append("name=" + this.preferenceName);
		sb.append(";");
		sb.append("value=" + this.value);
		sb.append(";");
		return sb.toString();
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((preferenceName == null) ? 0 : preferenceName.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final UserPreference other = (UserPreference) obj;
		if (preferenceName == null) {
			if (other.preferenceName != null) {
				return false;
			}
		} else if (!preferenceName.equals(other.preferenceName)) {
			return false;
		}
		if (value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!value.equals(other.value)) {
			return false;
		}
		return true;
	}
}
