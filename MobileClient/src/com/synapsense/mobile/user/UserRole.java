package com.synapsense.mobile.user;

// ESCA-JAVA0257:
/** Roles of a users */
public interface UserRole {

	int USER = 0;
	int ENGINEER = 1;
	int ADMIN = 2;
}
