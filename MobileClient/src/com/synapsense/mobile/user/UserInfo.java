package com.synapsense.mobile.user;

import java.io.Serializable;

public class UserInfo implements Serializable {

	private static final long serialVersionUID = -8123571176394407887L;
	private String userName;
	private int userRole;
	private String jSessionId;
	private UserPreference[] userPreferences;
	private int loginStatus = 0; // OK by default

	public UserInfo() {
	}

	/**
	 * @param userName
	 *            Name of user
	 * @param userRole
	 *            Role of user
	 * @see UserRole
	 * @param jSessionId
	 *            Id of session, which contains login and password of user
	 * @see UserPreference
	 */
	public UserInfo(String userName, int userRole, String jSessionId, UserPreference[] userPreferences) {
		this.userName = userName;
		this.userRole = userRole;
		this.jSessionId = jSessionId;
		this.userPreferences = userPreferences;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getUserRole() {
		return userRole;
	}

	public void setUserRole(int userRole) {
		this.userRole = userRole;
	}

	public String getjSessionId() {
		return jSessionId;
	}

	public void setjSessionId(String jSessionId) {
		this.jSessionId = jSessionId;
	}

	public UserPreference[] getUserPreferences() {
		return userPreferences;
	}

	public void setUserPreferences(UserPreference[] userPreferences) {
		this.userPreferences = userPreferences;
	}

	public int getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(int loginStatus) {
		this.loginStatus = loginStatus;
	}

}
