package com.synapsense.mobile.user;

public interface LoginStatus {
	int OK = 0;
	int FAILED = 1;
	int DEVICE_NOT_ALLOWED = 2;
	int DEVICE_REGISTRATION_REQUESTED = 3;
}
