package com.synapsense.mobile.alert;

// ESCA-JAVA0257:
/** Statuses of a alerts */
public interface AlertStatus {

	int OPENED = 1;
	int ACKNOWLEDGED = 2;
	int DISMISSED = 4;
	int RESOLVED = 8;
}