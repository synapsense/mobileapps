package com.synapsense.mobile.alert;

import java.io.Serializable;

import com.synapsense.mobile.tree.TreeObject;

/**
 * Alert
 */
public class Alert implements Serializable {

	private static final long serialVersionUID = -8919249904744076638L;
	private Integer id;
	private String name;
	private TreeObject object;
	private int status;
	private int priority;
	private String lastActionUser;
	private Long lastActionTime;
	private Long timestamp;
	private String description;
	private String message;
	private String comment;
	private String objectName;
	private String objectLocation;

	/**
	 * @param id
	 *            ID of the alert
	 * @param name
	 *            Name of the alert
	 * @param object
	 *            Object where the alert is happened
	 * @see TreeObject
	 * @param status
	 *            Status of the alert
	 * @see AlertStatus
	 * @param priority
	 *            Priority of the status
	 * @see AlertPriority
	 * @param lastActionUser
	 *            User, who changed status of the alert
	 * @param lastActionTime
	 *            Time, when the alert was changed his status
	 * @param timestamp
	 *            Time of creating alert
	 * @param description
	 *            Description of the alert
	 * @param message
	 *            Message of the alert
	 * @param comment
	 *            Comment of the alert. Comment will be written, when user changes the status of the alert.
	 */
	public Alert(Integer id, String name, TreeObject object, int status, int priority, String lastActionUser,
		Long lastActionTime, Long timestamp, String description, String message, String comment, String objectName, String objectLocation) {
		this.id = id;
		this.name = name;
		this.object = object;
		this.status = status;
		this.priority = priority;
		this.lastActionUser = lastActionUser;
		this.lastActionTime = lastActionTime;
		this.timestamp = timestamp;
		this.description = description;
		this.message = message;
		this.comment = comment;
		this.objectName = objectName;
		this.objectLocation = objectLocation;
	}

	public Alert() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TreeObject getObject() {
		return object;
	}

	public void setObject(TreeObject object) {
		this.object = object;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
		
	public String getObjectName() {
    	return objectName;
    }

	public void setObjectName(String objectName) {
    	this.objectName = objectName;
    }

	public String getObjectLocation() {
    	return objectLocation;
    }

	public void setObjectLocation(String objectLocation) {
    	this.objectLocation = objectLocation;
    }

	public String toString() {
		return "Alert [id=" + id + ", name=" + name + ", object=" + object + ", status=" + status + ", priority=" +
			priority + ", lastActionUser=" + lastActionUser + ", lastActionTime=" + lastActionTime + ", timestamp=" +
			timestamp + ", description=" + description + ", message=" + message + ", comment=" + comment + "]";
	}

	public String getLastActionUser() {
		return lastActionUser;
	}

	public void setLastActionUser(String lastActionUser) {
		this.lastActionUser = lastActionUser;
	}

	public Long getLastActionTime() {
		return lastActionTime;
	}

	public void setLastActionTime(Long lastActionTime) {
		this.lastActionTime = lastActionTime;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}
