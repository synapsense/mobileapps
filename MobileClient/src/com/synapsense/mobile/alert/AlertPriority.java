package com.synapsense.mobile.alert;

// ESCA-JAVA0257:
/** Priorities of a alerts */
public interface AlertPriority {

	int INFORMATIONAL = 1;
	int MINOR = 2;
	int MAJOR = 4;
	int CRITICAL = 8;
}
