package com.synapsense.mobile.tree;

import java.io.Serializable;


/**
 * Represents a node of the object tree.
 */
public class TreeObject implements Serializable {

	private static final long serialVersionUID = -7199567134973303167L;
	private String id;
	private String type;
	private int numAlerts;

	/**
	 * @param id
	 *            Id of the object
	 * @param type
	 *            Type of the object
	 * @param numAlerts
	 *            Numbers of alerts, which are on this object and on his child objects
	 */
	public TreeObject(String id, String type, int numAlerts) {
		this.id = id;
		this.type = type;
		this.numAlerts = numAlerts;
	}

	public TreeObject() {}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getNumAlerts() {
		return numAlerts;
	}

	public void setNumAlerts(int numAlerts) {
		this.numAlerts = numAlerts;
	}

	public String toString() {
		return "TreeObject [id=" + id + ", type=" + type + ", numAlerts=" + numAlerts + "]";
	}
}
