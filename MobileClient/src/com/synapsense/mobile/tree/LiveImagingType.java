package com.synapsense.mobile.tree;

import java.io.Serializable;

public class LiveImagingType implements Serializable {

	/**
	 * LiveImaging type wrap for sending to mobile application.
	 */
	private static final long serialVersionUID = -6278498565052641826L;

	private String name;
	private int dataClass;
	private int layer;

	public LiveImagingType() {

	}

	public LiveImagingType(String name, int dataClass, int layer) {
		this.name = name;
		this.dataClass = dataClass;
		this.layer = layer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDataClass() {
		return dataClass;
	}

	public void setDataClass(int dataClass) {
		this.dataClass = dataClass;
	}

	public int getLayer() {
		return layer;
	}

	public void setLayer(int layer) {
		this.layer = layer;
	}

	@Override
	public String toString() {
		return "LiveImagingType [name=" + name + ", dataclass=" + dataClass + ", layer=" + layer + "]";
	}

}
