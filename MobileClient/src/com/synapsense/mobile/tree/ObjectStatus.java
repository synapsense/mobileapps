package com.synapsense.mobile.tree;

// ESCA-JAVA0257:
/** Statuses of a objects */
public interface ObjectStatus {

	int NOT_RECEIVING_DATA = 0;
	int OK = 1;
	int DISABLED = 2;

	int GATEWAY_STATUS_ALIVE = 1;
	int GATEWAY_STATUS_MASTER = 2;
	int GATEWAY_STATUS_SYNC = 4;
}
