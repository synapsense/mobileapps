package com.synapsense.mobile.interaction;

public class DataRequestParam {
	protected String name;
	protected Object value;

	public DataRequestParam() {
		super();
	}

	public DataRequestParam(String name, Object value) {
		super();
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

}
