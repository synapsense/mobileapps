package com.synapsense.mobile.interaction;

public class ElementAttribute {
	protected String name;
	protected Object value;

	public ElementAttribute() {
		super();
	}

	public ElementAttribute(String name, Object value) {
		super();
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
}
