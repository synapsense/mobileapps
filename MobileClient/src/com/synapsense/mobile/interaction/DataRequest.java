package com.synapsense.mobile.interaction;

public class DataRequest {
	protected String requestType;
	protected DataRequestParam[] params;

	public DataRequest() {
		super();
	}

	public DataRequest(String requestType, DataRequestParam[] params) {
		super();
		this.requestType = requestType;
		this.params = params;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public DataRequestParam[] getParams() {
		return params;
	}

	public void setParams(DataRequestParam[] params) {
		this.params = params;
	}
}
