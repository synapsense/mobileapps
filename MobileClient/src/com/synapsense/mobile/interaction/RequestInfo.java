package com.synapsense.mobile.interaction;

public interface RequestInfo {
	int TIMESTAMP = 1;
	int UNITS = 2;
	int HISTORICAL = 4;
	int LINK = 8;
	int MAPPED_NAME = 16;
	int MAPPED_VALUE = 32;
	int FILTER_HIST_AND_TO = 64;
}
