package com.synapsense.mobile.interaction;

public class ObjectRequest {

	protected String[] properties = null;
	/**
	 * Type of objects to request Should be set only for not root requests
	 */
	protected String type = null;
	/**
	 * Id of objects to request Should be set only for root requests
	 */
	protected String id = null;
	/**
	 * Child objects to request
	 */
	protected ObjectRequest[] childrenRequests = null;
	/**
	 * Parent objects to request
	 */
	protected ObjectRequest[] parentsRequests = null;
	/**
	 * Objects links to request
	 */
	protected ObjectRequest[] linkRequests = null;
	/**
	 * Show which method to call: env.getChildren() or env.getRelatedObjects()
	 */
	protected boolean requestDirectChildrenOnly = true;
	/**
	 * Show which method to call: env.getParent() or env.getRelatedObjects()
	 */
	protected boolean requestDirectParentsOnly = true;

	/**
	 * Requests only children number
	 */
	protected boolean requestChildrenNumberOnly = false;

	/**
	 * Additional properties info bit mask (see RequestInfo class for details)
	 */
	protected int additionalInfo = 0;

	public ObjectRequest() {
		super();
	}

	public String[] getProperties() {
		return properties;
	}

	public void setProperties(String[] properties) {
		this.properties = properties;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ObjectRequest[] getChildrenRequests() {
		return childrenRequests;
	}

	public void setChildrenRequests(ObjectRequest[] childrenRequests) {
		this.childrenRequests = childrenRequests;
	}

	public ObjectRequest[] getParentsRequests() {
		return parentsRequests;
	}

	public void setParentsRequests(ObjectRequest[] parentsRequests) {
		this.parentsRequests = parentsRequests;
	}

	public ObjectRequest[] getLinkRequests() {
		return linkRequests;
	}

	public void setLinkRequests(ObjectRequest[] linkRequests) {
		this.linkRequests = linkRequests;
	}

	public boolean isRequestDirectChildrenOnly() {
		return requestDirectChildrenOnly;
	}

	public void setRequestDirectChildrenOnly(boolean requestDirectChildrenOnly) {
		this.requestDirectChildrenOnly = requestDirectChildrenOnly;
	}

	public boolean isRequestDirectParentsOnly() {
		return requestDirectParentsOnly;
	}

	public void setRequestDirectParentsOnly(boolean requestDirectParentsOnly) {
		this.requestDirectParentsOnly = requestDirectParentsOnly;
	}

	public boolean isRequestChildrenNumberOnly() {
		return requestChildrenNumberOnly;
	}

	public void setRequestChildrenNumberOnly(boolean requestChildrenNumberOnly) {
		this.requestChildrenNumberOnly = requestChildrenNumberOnly;
	}

	public int getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(int additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
}
