package com.synapsense.mobile.interaction;

public class ResponseElement {
	protected String name = "";
	protected ElementAttribute[] attributes;
	protected ResponseElement[] subElements;

	public ResponseElement() {
		super();
	}

	public ResponseElement(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ElementAttribute[] getAttributes() {
		return attributes;
	}

	public void setAttributes(ElementAttribute[] attributes) {
		this.attributes = attributes;
	}

	public ResponseElement[] getSubElements() {
		return subElements;
	}

	public void setSubElements(ResponseElement[] subElements) {
		this.subElements = subElements;
	}

}
