package com.synapsense.mobile.service;

import com.synapsense.mobile.user.UserInfo;
import com.synapsense.mobile.user.UserPreference;

public interface UserManager {

	/**
	 * Logs in user into the system.
	 * <p>
	 * If credentials are correct, then login and password is written to session. For follows requests, login and
	 * password will be got from session.
	 * 
	 * @param user
	 *            login
	 * @param password
	 *            password
	 * 
	 * @return UserInfo if credentials are correct, null otherwise
	 */
	public UserInfo login(String userName, String password, String deviceId, String deviceName);

	/**
	 * Logs in user into the system.
	 * <p>
	 * If credentials are correct, then login and password is written to session. For follows requests, login and
	 * password will be got from session.
	 * 
	 * @param user
	 * 			login
	 * @param password
	 * 			password
	 * @param preferenceNames
	 * 			user preference names to retrieve
	 * @return
	 */
	public UserInfo login(String userName, String password, String deviceId, String deviceName, String [] preferenceNames);
	
	/**
	 * Logs in user out of the system Session, which contain login and password, is destroyed
	 * 
	 * @return true
	 */
	boolean logout();

	/**
	 * Returns a user preference with specified name.
	 * 
	 * @param userPreferenceName
	 *            the user preference name
	 * @return the user preference if the user is logged in and the preference exists, otherwise {@code null}.
	 */
	UserPreference getPreference(String userPreferenceName);

	/**
	 * Returns an array of user preferences with specified names.
	 * 
	 * @param userPreferenceNames
	 *            the user preference names
	 * @return the user preference array if the user is logged in, otherwise null.
	 * NOTE: if a specified user preference doesn't exist then the respective item of the returned array is {@code null}.
	 */
	UserPreference[] getPreferences(String[] userPreferenceNames);
}
