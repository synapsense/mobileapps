package com.synapsense.mobile.service;

import com.synapsense.mobile.alert.Alert;

public interface AlertManager {

	/**
	 * Moves alert to "acknowledged" state. Add to the comments of alerts value "ACK:"+comment
	 * 
	 * @param alertId
	 *            Id of the alert
	 * @param comment
	 *            Comment to the alert
	 * 
	 * @return true if state of the alert was changed, false if transition to this state is prohibited
	 */
	boolean acknowledge(int alertId, String comment);

	/**
	 * Moves alert to "resolved" state.Add to the comments of alerts value "RES:"+comment
	 * 
	 * @param alertId
	 *            Id of the alert
	 * @param comment
	 *            Comment to the alert
	 * 
	 * @return true if state of the alert was changed, false if transition to this state is prohibited
	 */
	boolean resolve(int alertId, String comment);

	/**
	 * Moves alert to "dismissed" state.Add to the comments of alerts value "DIS:"+comment
	 * 
	 * @param alertId
	 *            Id of the alert
	 * @param comment
	 *            Comment to the alert
	 * 
	 * @return true if state of the alert was changed, false if transition to this state is prohibited
	 */
	boolean dismiss(int alertId, String comment);

	/**
	 * @param objId
	 *            specify object ID
	 *            <p>
	 *            If parameter is null, total number of the alerts in the system is returned
	 * @param period
	 *            period of time, for which alerts have been created
	 *            <p>
	 *            if parameter is null then returned number of alerts for all time
	 * @param priority
	 *            bitmap of priority
	 *            <p>
	 *            if parameter is null then returned only active alerts
	 * @see com.synapsense.mobile.alert.AlertPriority
	 * @param status
	 *            bitmap of status
	 *            <p>
	 *            if parameter is null then return number of alerts with every status
	 * @see com.synapsense.mobile.alert.AlertStatus
	 * 
	 * @return number of ActiveAlerts for given parameters
	 * 
	 */
	int getNumActiveAlerts(String objId, Long period, Integer priority, Integer status);

	/**
	 * @param objId
	 *            specify Data Center ID
	 *            <p>
	 *            If parameter is null,all alerts in the system is returned
	 * @param period
	 *            period of time, for which alerts have been created
	 *            <p>
	 *            if parameter is null, then returned alerts for all time
	 * @param priority
	 *            bitmap of priorities
	 *            <p>
	 *            if parameter is null, then returned alerts with all priorities
	 * @see com.synapsense.mobile.alert.AlertPriority
	 * @param status
	 *            bitmap of statuses
	 *            <p>
	 *            if parameter is null, then returned alerts with active statuses. Active statuses are open and
	 *            acknowledged.
	 * @see com.synapsense.mobile.alert.AlertStatus
	 * @param start
	 *            the start index
	 *            <p>
	 *            if parameter is null, alert starting from first item are returned
	 * @param count
	 *            count of Alerts
	 *            <p>
	 *            if parameter is null, all alert starting from start index are returned
	 * @return Array of Alerts according to specified parameters, alerts are sorted by timestamp
	 * @see Alert
	 */
	Alert[] getActiveAlerts(String objId, Long period, Integer priority, Integer status, Integer start, Integer count);
	
	/**
	 * Allows caller to dismiss all active alerts which are associated with the
	 * specified object.
	 * @param objId - object ID
	 * @param comment - acknowledgment message
	 */
	void dismissAllAlerts(String objId, String comment);	
}
