package com.synapsense.mobile.service;

import com.synapsense.mobile.interaction.DataRequest;
import com.synapsense.mobile.interaction.ResponseElement;

public interface DataManager {
	ResponseElement getObjectsData(DataRequest request);
}
