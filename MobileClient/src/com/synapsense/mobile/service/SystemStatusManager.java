package com.synapsense.mobile.service;

import com.synapsense.mobile.status.ComponentStatus;

public interface SystemStatusManager {

	/**
	 * @return statuses of server components
	 * @see com.synapsense.mobile.status.Components
	 * @see com.synapsense.mobile.status.Status
	 */
	ComponentStatus[] getEnvironmentalServerState();

	/**
	 * @return the total number of warnings
	 */
	int getNumWarnings();
}
