package com.synapsense.mobile.service;

public interface ConfigManager {
	/**
	 * Returns allowed objects types to show on floorplan
	 * @return an array on object type names
	 */
	String [] getFloorplanTypes();
	
	/**
	 * Retruns allowed UI privileges
	 * @return
	 */
	String [] getUIPrivileges();
}
