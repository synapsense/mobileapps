package com.synapsense.mobile.service;

import com.synapsense.mobile.interaction.ObjectRequest;
import com.synapsense.mobile.interaction.ResponseElement;
import com.synapsense.mobile.tree.LiveImagingType;
import com.synapsense.mobile.tree.TreeObject;

public interface ObjectManager {

	/**
	 * 
	 * @return array of tree objects
	 */
	TreeObject[] getObjectTree();

	/**
	 * Returns child tree objects with specified types of specified parent tree object.
	 * 
	 * @param objectId
	 *            the parent object id
	 * @param types
	 *            the child object types
	 * @param fullSubtree
	 *            if {@code true} then children are looked for in the full subtree of the parent otherwise under the
	 *            parent only
	 * @return an array of child tree objects if the specified parent object exists, otherwise {@code null}. NOTE: if
	 *         the child object types array is {@code null} then all children objects under the parent are returned (
	 *         {@code fullSubtree} parameter is ignored).
	 */
	TreeObject[] getChildren(String objectId, String[] types, boolean fullSubtree);

	/**
	 * Returns a specified property value of a specified object.
	 * 
	 * @param objectId
	 *            the object id
	 * @param propertyName
	 *            the property name
	 * @return the object property value if the specified property of the specified object exists, otherwise
	 *         {@code null}.
	 */
	Object getPropertyValue(String objectId, String propertyName);

	/**
	 * Returns an array of property values of a specified object.
	 * 
	 * @param objectId
	 *            the object id
	 * @param propertyNames
	 *            the property names
	 * @return the object property value array if the specified object exists, otherwise null. NOTE: if the specified
	 *         object doesn't have a specified object property then the respective item of the returned array is
	 *         {@code null}.
	 */
	Object[] getPropertyValues(String objectId, String[] propertyNames);

	/**
	 * Returns an array of an array of user preferences of a specified objects.
	 * 
	 * @param objectIds
	 *            the object identifiers
	 * @param propertyNames
	 *            the property names
	 * @return the array of object property value array. NOTE: if a specified object doesn't exist then the respective
	 *         item of the outer array is {@code null}. if a specified object doesn't have a specified object property
	 *         then the respective item of the inner array is {@code null}.
	 */
	Object[][] getManyObjectsPropertyValues(String[] objectIds, String[] propertyNames);

	/**
	 * Returns a specified floorplan picture as byte array.
	 * 
	 * @param objectId
	 *            the object id
	 * @param dataClassId
	 *            the live imaging data class
	 * @see com.synapsense.mobile.tree.LiveImagingType
	 * @param layer
	 *            the live imaging layer
	 * @see com.synapsense.mobile.tree.LiveImagingType
	 * @return the specified floorplan picture URL if the specified object exists, otherwise {@code null}.
	 */
	String getFloorplan(String objectId, int dataClassId, int layer);

	/**
	 * Return all LiveImaging types as an array of specified LiveImagingType objects.
	 * 
	 * @param locale
	 *            the localization
	 * 
	 * @return an array of specified LiveImagingType objects
	 */
	LiveImagingType[] getLiveImagingTypes();

	/**
	 * 
	 * @param objectId
	 *            the object id
	 * @return true if LiveImagingService available for object, otherwise false
	 */
	boolean isLiveImagingAvailable(String objectId);
	
	/**
	 * Get objects data by ObjectRequest array
	 * @param objRequests
	 * @return
	 */
	ResponseElement getObjectsData(ObjectRequest[] objRequests);
	
	/**
	 * Get objects data by ObjectRequest
	 * @param objRequests
	 * @return
	 */
	ResponseElement getObjectData(ObjectRequest objRequests);
	
	/**
	 * Get floorplan objects data by dc id
	 * @param objectId - id of dc or zone
	 * @param types - list of object types to request
	 * 
	 * @return array of objects. Each object is array of properies: ["status", "numAlerts",  "x", "y", "door"]
	 * "door" is optional and presents only for rack and door objects
	 */
	Object[] getFloorPlanData(String objectId, String[] types);
}
