package com.synapsense.mobile.service.func;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.googlecode.jsonrpc4j.ProxyUtil;
import com.synapsense.mobile.service.JsonRpcHttpCsrfClient;
import com.synapsense.mobile.service.ObjectManager;
import com.synapsense.mobile.service.TestUtils;
import com.synapsense.mobile.service.UserManager;
import com.synapsense.mobile.tree.TreeObject;
import com.synapsense.mobile.user.UserInfo;

public class ObjectManagerTest {

	/**
	 * 
	 * @param args
	 * @throws MalformedURLException
	 */

	public static void main(String[] args) throws MalformedURLException {
		URL url;
		url = new URL(Constants.BASE_URL + "/userManager");
		JsonRpcHttpClient userclient = new JsonRpcHttpCsrfClient(url);
		UserManager userManager = ProxyUtil.createClientProxy(Thread.currentThread().getContextClassLoader(),
		        UserManager.class, userclient);
		UserInfo userInfo = userManager.login(Constants.LOGIN,  Constants.PASSWORD, Constants.DEVICE_ID, Constants.DEVICE_NAME);

		if(!TestUtils.checkLoginStatus(userInfo)){
			return;
		}
		
		Map<String, String> header = new HashMap<String, String>();
		header.put("Cookie", "JSESSIONID=" + userInfo.getjSessionId());

		url = new URL(Constants.BASE_URL + "/objectManager");
		JsonRpcHttpClient objectclient = new JsonRpcHttpCsrfClient(url, header);
		ObjectManager objectmanager = ProxyUtil.createClientProxy(Thread.currentThread().getContextClassLoader(),
		        ObjectManager.class, objectclient);
		TreeObject[] tree = objectmanager.getObjectTree();
		for (int i = 0; i < tree.length; i++) {
			System.out.println(tree[i]);
		}

		System.out.println("*********************");

		tree = objectmanager.getChildren("ZONE:30", null, false);
		for (int i = 0; i < tree.length; i++) {
			System.out.println(tree[i]);
		}

		System.out.println("*********************");

		tree = objectmanager.getChildren("DC:28", new String[] { "RACK", "ZONE" }, true);
		for (int i = 0; i < tree.length; i++) {
			System.out.println(tree[i]);
		}

		System.out.println("*********************");

		tree = objectmanager.getChildren("DC:28", new String[] { "ZONE" }, false);
		for (int i = 0; i < tree.length; i++) {
			System.out.println(tree[i]);
		}

		System.out.println("*********************");

		Object name = objectmanager.getPropertyValue("ZONE:30", "name");
		System.out.println("ZONE name: " + name.toString());

		System.out.println("*********************");

		Object[] properties = objectmanager.getPropertyValues("ZONE:30", new String[] { "name", "x", "yy", "width",
		        "height", "nonexistprop" });
		System.out.println("ZONE properties:");
		for (Object obj : properties) {
			if (obj != null) {
				System.out.println(obj.toString() + "; ");
			} else {
				System.out.println("NULL; ");
			}
		}

		System.out.println("*********************");

		properties = objectmanager.getPropertyValues("ZONE:30", new String[] { "qwe", "www", "ywww" });
		System.out.println("ZONE properties:");
		for (Object obj : properties) {
			if (obj != null) {
				System.out.println(obj.toString() + "; ");
			} else {
				System.out.println("NULL; ");
			}
		}
		
		System.out.println("*********************");

		Object[][] manyObjProp = objectmanager.getManyObjectsPropertyValues(new String[] { "ZONE:30", "ZONE:189" },
		        new String[] { "name", "x", "y", "xxx" });
		System.out.println("ZONE properties:");
		for (Object[] props : manyObjProp) {
			for (Object prop : props) {
				if (prop != null) {
					System.out.println(prop.toString() + "; ");
				} else {
					System.out.println("NULL; ");
				}
			}
		}

		System.out.println("*********************");
		String image = objectmanager.getFloorplan("DC:28", 200, 131072);
		if (image != null) {
			System.out.println(image);
		} else {
			System.out.println("NULL");
		}

		url = new URL(Constants.BASE_URL + "/userManager");
		userclient = new JsonRpcHttpCsrfClient(url, header);
		userManager = ProxyUtil.createClientProxy(Thread.currentThread().getContextClassLoader(), UserManager.class,
		        userclient);
		userManager.logout();

		System.out.println("******Logout******");

	}
}
