package com.synapsense.mobile.service.func;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.googlecode.jsonrpc4j.ProxyUtil;

import com.synapsense.mobile.service.JsonRpcHttpCsrfClient;
import com.synapsense.mobile.service.TestUtils;
import com.synapsense.mobile.service.UserManager;
import com.synapsense.mobile.user.UserInfo;
import com.synapsense.mobile.user.UserPreference;

public class UserManagementTest {

	/**
	 * @param args
	 * @throws MalformedURLException
	 */
	public static void main(String[] args) throws MalformedURLException {
		URL url;
		url = new URL(Constants.BASE_URL + "/userManager");
		JsonRpcHttpClient userclient = new JsonRpcHttpCsrfClient(url);
		UserManager userManager = ProxyUtil.createClientProxy(Thread.currentThread().getContextClassLoader(),
		        UserManager.class, userclient);
		//UserInfo userInfo = userManager.login(Constants.LOGIN,  Constants.PASSWORD);
		UserInfo userInfo = userManager.login(Constants.LOGIN,  Constants.PASSWORD, Constants.DEVICE_ID, Constants.DEVICE_NAME, new String[]{"UnitsSystem"});
		System.out.println("******Login******");

		if(!TestUtils.checkLoginStatus(userInfo)){
			return;
		}
		System.out.println("UserPreferences:");
		UserPreference[] ups = userInfo.getUserPreferences();
		if(ups != null) {
			for (UserPreference up : ups) {
				if (up != null) {
					System.out.println(up.getPreferenceName() + ": " + up.getValue());
				} else {
					System.out.println("NULL");
				}
			}
		}else {
			System.out.println("UserPrefs is NULL");
		}
			
		
		Map<String, String> header = new HashMap<String, String>();
		header.put("Cookie", "JSESSIONID=" + userInfo.getjSessionId());
		userclient = new JsonRpcHttpCsrfClient(url, header);
		userManager = ProxyUtil.createClientProxy(Thread.currentThread().getContextClassLoader(), UserManager.class,
		        userclient);

		UserPreference pref = userManager.getPreference("TimeZone");
		System.out.println(pref.getPreferenceName() + ": " + pref.getValue());

		System.out.println("**********************");

		UserPreference[] prefs = userManager.getPreferences(new String[] { "TimeZone", "asdasd", "openTreeNodes",
		        "FloorPlanLayer" });
		for (UserPreference up : prefs) {
			if (up != null) {
				System.out.println(up.getPreferenceName() + ": " + up.getValue());
			} else {
				System.out.println("NULL");
			}
		}

		userManager.logout();

		System.out.println("******Logout******");

	}

}
