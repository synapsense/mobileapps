package com.synapsense.mobile.service.func;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.googlecode.jsonrpc4j.ProxyUtil;
import com.synapsense.mobile.alert.Alert;
import com.synapsense.mobile.service.AlertManager;
import com.synapsense.mobile.service.JsonRpcHttpCsrfClient;
import com.synapsense.mobile.service.TestUtils;
import com.synapsense.mobile.service.UserManager;
import com.synapsense.mobile.user.UserInfo;

public class AlertManagerTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws MalformedURLException {
		URL url;
		url = new URL(Constants.BASE_URL + "/userManager");
		JsonRpcHttpClient userclient = new JsonRpcHttpCsrfClient(url);
		UserManager userManager = ProxyUtil.createClientProxy(Thread.currentThread().getContextClassLoader(),
		        UserManager.class, userclient);
		UserInfo userInfo = userManager.login(Constants.LOGIN,  Constants.PASSWORD, Constants.DEVICE_ID, Constants.DEVICE_NAME);
		
		if(!TestUtils.checkLoginStatus(userInfo)){
			return;
		}
		System.out.println("************");
		url = new URL(Constants.BASE_URL + "/alertManager");
		Map<String, String> header = new HashMap<String, String>();
		header.put("Cookie", "JSESSIONID=" + userInfo.getjSessionId());
		JsonRpcHttpClient alertclient = new JsonRpcHttpCsrfClient(url, header);
		AlertManager manager = ProxyUtil.createClientProxy(Thread.currentThread().getContextClassLoader(),
		        AlertManager.class, alertclient);
		Alert[] alerts = null;
		alerts = manager.getActiveAlerts(null, null, null, null, null, null);
		System.out.println("Num active alerts" + manager.getNumActiveAlerts(null, null, null, null));
		for (Alert alert : alerts) {
			//System.out.println("alertID= "+alert.getId());
			System.out.println("alertID= "+alert.getId() + ", id is " + alert.getObject().getId() + ", type is " + alert.getObject().getType() + ", objName is " + alert.getObjectName() + ", mess: " + alert.getMessage() + ", lastActionTime: " + alert.getLastActionTime() + ", objLocation: " + alert.getObjectLocation() + ", descr: " + alert.getDescription() + ", priority: " + alert.getPriority());			
		}
		
		System.out.println("Num all alerts "+manager.getNumActiveAlerts(null, null, 0xF, 0xF));
		System.out.println("Num opened alerts "+manager.getNumActiveAlerts(null, null, 0xF, 1));
		System.out.println("Num ack alerts "+manager.getNumActiveAlerts(null, null, 0xF, 2));
		System.out.println("Num dismissed alerts "+manager.getNumActiveAlerts(null, null, 0xF, 4));
		System.out.println("Num resolved alerts "+manager.getNumActiveAlerts(null, null, 0xF, 8));
		
		url = new URL(Constants.BASE_URL + "/userManager");
		userclient = new JsonRpcHttpCsrfClient(url, header);
		userManager = ProxyUtil.createClientProxy(Thread.currentThread().getContextClassLoader(), UserManager.class,
		        userclient);
		userManager.logout();

		System.out.println("******Logout******");

	}

}
