package com.synapsense.mobile.service.func;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.googlecode.jsonrpc4j.ProxyUtil;
import com.synapsense.mobile.service.ConfigManager;
import com.synapsense.mobile.service.JsonRpcHttpCsrfClient;
import com.synapsense.mobile.service.TestUtils;
import com.synapsense.mobile.service.UserManager;
import com.synapsense.mobile.user.UserInfo;

public class ConfigManagerTest {

	/**
	 * @param args
	 * @throws MalformedURLException 
	 */
	public static void main(String[] args) throws MalformedURLException {
		URL url;
		url = new URL(Constants.BASE_URL + "/userManager");
		JsonRpcHttpClient userclient = new JsonRpcHttpCsrfClient(url);
		UserManager userManager = ProxyUtil.createClientProxy(Thread.currentThread().getContextClassLoader(),
		        UserManager.class, userclient);
		UserInfo userInfo = userManager.login(Constants.LOGIN,  Constants.PASSWORD, Constants.DEVICE_ID, Constants.DEVICE_NAME);

		if(!TestUtils.checkLoginStatus(userInfo)){
			return;
		}
		
		Map<String, String> header = new HashMap<String, String>();
		header.put("Cookie", "JSESSIONID=" + userInfo.getjSessionId());

		url = new URL(Constants.BASE_URL + "/configManager");
		JsonRpcHttpClient objectclient = new JsonRpcHttpCsrfClient(url, header);
		ConfigManager configManager = ProxyUtil.createClientProxy(Thread.currentThread().getContextClassLoader(),
				ConfigManager.class, objectclient);
		
		String [] types = configManager.getFloorplanTypes();
		for(String type : types){
			System.out.println("type: " + type);
		}
		
		System.out.println("-----------");
		
		String [] privs = configManager.getUIPrivileges();
		for(String priv : privs){
			System.out.println("priv: " + priv);
		}
	}

}
