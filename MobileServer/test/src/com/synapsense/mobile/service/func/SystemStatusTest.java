package com.synapsense.mobile.service.func;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import com.googlecode.jsonrpc4j.ProxyUtil;
import com.synapsense.mobile.service.JsonRpcHttpCsrfClient;
import com.synapsense.mobile.service.SystemStatusManager;
import com.synapsense.mobile.service.TestUtils;
import com.synapsense.mobile.service.UserManager;
import com.synapsense.mobile.status.ComponentStatus;
import com.synapsense.mobile.user.LoginStatus;
import com.synapsense.mobile.user.UserInfo;

public class SystemStatusTest {

	public static void main(String[] args) throws MalformedURLException {
		URL url;
		url = new URL(Constants.BASE_URL + "/userManager");		
		JsonRpcHttpCsrfClient userclient = new JsonRpcHttpCsrfClient(url);
		
		UserManager userManager = ProxyUtil.createClientProxy(Thread.currentThread().getContextClassLoader(),
		        UserManager.class, userclient);
		UserInfo userInfo = userManager.login(Constants.LOGIN,  Constants.PASSWORD, Constants.DEVICE_ID, Constants.DEVICE_NAME);
		System.out.println("******Login******");
		
		if(!TestUtils.checkLoginStatus(userInfo)){
			return;
		}
		
		Map<String, String> header = new HashMap<String, String>();
		header.put("Cookie", "JSESSIONID=" + userInfo.getjSessionId());		

		url = new URL(Constants.BASE_URL + "/statusManager");
		JsonRpcHttpCsrfClient statusClient = new JsonRpcHttpCsrfClient(url, header);
		SystemStatusManager statusManager = ProxyUtil.createClientProxy(Thread.currentThread().getContextClassLoader(),
		        SystemStatusManager.class, statusClient);
		System.out.println("Warnings: " + statusManager.getNumWarnings());		
		System.out.println("*********************");

		ComponentStatus[] statuses = statusManager.getEnvironmentalServerState();
		for (ComponentStatus status : statuses) {
			System.out.println(status.getComponent() + " " + status.getStatus() + ", " + status.getMessage());
		}

		url = new URL(Constants.BASE_URL + "/userManager");
		userclient = new JsonRpcHttpCsrfClient(url, header); //userclient = new JsonRpcHttpClient(url, header);
		
		userManager = ProxyUtil.createClientProxy(Thread.currentThread().getContextClassLoader(), UserManager.class,
		        userclient);
		
		userManager.logout();

		System.out.println("******Logout******");
	}
}
