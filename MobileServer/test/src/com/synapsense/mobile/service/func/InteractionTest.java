package com.synapsense.mobile.service.func;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.googlecode.jsonrpc4j.ProxyUtil;
import com.synapsense.mobile.dataAccess.Env;
import com.synapsense.mobile.interaction.ElementAttribute;
import com.synapsense.mobile.interaction.ObjectRequest;
import com.synapsense.mobile.interaction.RequestInfo;
import com.synapsense.mobile.interaction.ResponseElement;
import com.synapsense.mobile.lang.LangManager;
import com.synapsense.mobile.requestprocessing.ObjectRequestUtils;
import com.synapsense.mobile.service.JsonRpcHttpCsrfClient;
import com.synapsense.mobile.service.ObjectManager;
import com.synapsense.mobile.service.TestUtils;
import com.synapsense.mobile.service.UserManager;
import com.synapsense.mobile.tree.TreeObject;
import com.synapsense.mobile.user.UserInfo;

import com.synapsense.mobile.service.impl.ObjectManagerImpl;
import com.synapsense.mobile.service.impl.UserManagerImpl;

public class InteractionTest {
	public static void main(String[] args) throws MalformedURLException {
		URL url;
		url = new URL(Constants.BASE_URL + "/userManager");
		JsonRpcHttpClient userclient = new JsonRpcHttpCsrfClient(url);
		UserManager userManager = ProxyUtil.createClientProxy(Thread.currentThread().getContextClassLoader(),
		        UserManager.class, userclient);
		UserInfo userInfo = userManager.login(Constants.LOGIN,  Constants.PASSWORD, Constants.DEVICE_ID, Constants.DEVICE_NAME);
		//UserInfo userInfo = userManager.login("anna",  "anna");

		if(!TestUtils.checkLoginStatus(userInfo)){
			return;
		}
		
		Map<String, String> header = new HashMap<String, String>();
		header.put("Cookie", "JSESSIONID=" + userInfo.getjSessionId());

		url = new URL(Constants.BASE_URL + "/objectManager");
		JsonRpcHttpClient objectclient = new JsonRpcHttpCsrfClient(url, header);
		ObjectManager objectmanager = ProxyUtil.createClientProxy(Thread.currentThread().getContextClassLoader(),
		        ObjectManager.class, objectclient);
		TreeObject[] tree = objectmanager.getObjectTree();
		/*for (int i = 0; i < tree.length; i++) {
			System.out.println(tree[i]);
		}*/
		
		//objectmanager.getLiveImagingTypes(Locale.US);
		
		int a = 0;
		//DC map
		ObjectRequest dcMapReq = new ObjectRequest();
		//dcMapReq.setType("DC");
		dcMapReq.setId("DC:87");
		dcMapReq.setProperties(new String []{"longitude", "latitude"});
		dcMapReq.setAdditionalInfo(RequestInfo.TIMESTAMP | RequestInfo.UNITS);
		
		ResponseElement res = objectmanager.getObjectsData(new ObjectRequest []{dcMapReq});
		/*for(ResponseElement dc: res.getSubElements()){
			for (ElementAttribute dcAttr: dc.getAttributes()){
				System.out.println("attr name: " + dcAttr.getName() + ", val: " + dcAttr.getValue());
			}
			for (ResponseElement dcProperty : dc.getSubElements()){
				Object propVal = "";
				ElementAttribute [] attrs = dcProperty.getAttributes();
				if(attrs != null) for(ElementAttribute propertyAttr : attrs){
					if("v".equals(propertyAttr.getName())){
						propVal = propertyAttr.getValue();
						break;
					}
				}
				System.out.println("prop name: " + dcProperty.getName() + ", val: " + propVal);
			}
		}*/
		
		StringBuffer strBuffer = ObjectRequestUtils.printResponse(res);
		System.out.println(strBuffer);
		
		//System.out.println(objectmanager.getPropertyValue("DC:87", "image"));
		
		if(a == 1){
			System.out.println("!!! return");
			return;
		}
		
		//Object Props
		ObjectRequest rackReq = new ObjectRequest();
		rackReq.setId("RACK:294");
		rackReq.setLinkRequests(getSensorLinks());
		rackReq.setAdditionalInfo(RequestInfo.UNITS | RequestInfo.MAPPED_NAME | RequestInfo.HISTORICAL | RequestInfo.LINK);
		rackReq.setProperties(new String[]{});
		
		ResponseElement objResp = objectmanager.getObjectData(rackReq);
		
		StringBuffer objStrBuffer = ObjectRequestUtils.printResponse(objResp);
		System.out.println(objStrBuffer);
		
		System.out.println("!!! Rack props");
		
		//Object request for floorplan
		String dcId = "DC:6806";
		ObjectRequest floorplanReq = new ObjectRequest();
		String [] childrenTypes = new String []{"RACK", "CRAC", "PRESSURE"};
		String [] childrenProps = new String []{"x", "y", "name", "status", "numAlerts"};
		floorplanReq.setId(dcId);
		floorplanReq.setProperties(new String []{"width", "height", "name"});
		floorplanReq.setChildrenRequests(getRequests(childrenTypes, childrenProps, true, false, false));
		
		ResponseElement result = objectmanager.getObjectsData(new ObjectRequest []{floorplanReq});
		
		StringBuffer strBuf = ObjectRequestUtils.printResponse(result);
		System.out.println(strBuf);
		
		//Object request for facility dashboard
		ObjectRequest facilityDashReq = new ObjectRequest();
		String [] pueProps = new String []{"name", "pue", "itPower", "coolingPower", "powerLoss", "lightingPower"};
		facilityDashReq.setId("DC:87");
		facilityDashReq.setProperties(new String []{"name", "numAlerts"});
		
		ObjectRequest [] objReqsForNum = getRequests(new String []{"RACK", "CRAC"}, new String []{"whatever"}, true, false, true);		
		ObjectRequest [] objReqsForPUE = getRequests(new String []{"PUE"}, pueProps, true, false, false);		
		ObjectRequest [] concatReqs = concatArrays(objReqsForNum, objReqsForPUE);
				
		facilityDashReq.setChildrenRequests(concatReqs);
		
		result = objectmanager.getObjectsData(new ObjectRequest []{facilityDashReq});		
		
		strBuf = ObjectRequestUtils.printResponse(result);
		System.out.println(strBuf);
		System.out.println("!!! facility dashboard");
		// Object request for all RACK properties
		/*ObjectRequest rackReq = new ObjectRequest();
		rackReq.setId("RACK:294");
		//rackReq.setId("RACK:7890");
		//rackReq.setProperties(new String []{"name", "cTop"});
		rackReq.setLinkRequests(getSensorLinks());
		result = objectmanager.getObjectsData(new ObjectRequest []{rackReq});
		strBuf = ObjectRequestUtils.printResponse(result);
		System.out.println(strBuf);
		
		//Object request for test
		ObjectRequest [] objReqs = new ObjectRequest[2];
		String [] objProps = new String []{"name"};
		objReqs[0]= new ObjectRequest(); objReqs[0].setType("ROOT"); objReqs[0].setProperties(objProps);
		objReqs[1]= new ObjectRequest(); objReqs[1].setType("PRESSURE"); objReqs[1].setProperties(new String []{"x", "pressure"});
		objReqs[1].setLinkRequests(getSensorLinks());
		
		result = objectmanager.getObjectsData(objReqs);
		System.out.println("!!!");
		strBuf = ObjectRequestUtils.printResponse(result);
		System.out.println(strBuf);*/
		
		//First Screen
		ObjectRequest screen1 = new ObjectRequest();
		screen1.setType("DC");
		//ObjectRequest [] zoneForNum = getRequests(new String []{"ZONE"}, null, true, false, true);
		ObjectRequest zoneNumReq = new ObjectRequest();
		zoneNumReq.setType("ZONE");
		zoneNumReq.setRequestChildrenNumberOnly(true);
		zoneNumReq.setRequestDirectChildrenOnly(false);		
		
		screen1.setChildrenRequests(new ObjectRequest []{zoneNumReq});
		screen1.setProperties(new String []{"name", "numAlerts", "status"});
		
		result = objectmanager.getObjectsData(new ObjectRequest []{screen1});
		strBuf = ObjectRequestUtils.printResponse(result);
		System.out.println(strBuf);
		
		//Second Screen
		ObjectRequest screen2 = new ObjectRequest();
		screen2.setType("ROOT");
		//screen2.setProperties(new String []{"name"});
		
		ObjectRequest screen2DC = new ObjectRequest();
		screen2DC.setType("DC");
		screen2DC.setRequestChildrenNumberOnly(true);
		screen2DC.setRequestDirectChildrenOnly(false);
		
		ObjectRequest screen2RACK = new ObjectRequest();
		screen2RACK.setType("RACK");
		screen2RACK.setRequestChildrenNumberOnly(true);
		screen2RACK.setRequestDirectChildrenOnly(false);
		
		ObjectRequest screen2CRAC = new ObjectRequest();
		screen2CRAC.setType("CRAC");
		screen2CRAC.setRequestChildrenNumberOnly(true);
		screen2CRAC.setRequestDirectChildrenOnly(false);
		
		ObjectRequest screen2PUE = new ObjectRequest();
		screen2PUE.setType("PUE");
		screen2PUE.setRequestDirectChildrenOnly(false);
		screen2PUE.setProperties(new String []{"pue"});
		
		screen2.setChildrenRequests(new ObjectRequest []{screen2DC, screen2RACK, screen2CRAC, screen2PUE});
		
		result = objectmanager.getObjectsData(new ObjectRequest []{screen2});
		strBuf = ObjectRequestUtils.printResponse(result);
		System.out.println(strBuf);
		
		System.out.println("!!!! second Screen ended");
		
		//Facility Dashboard
		ObjectRequest facilityDashScreen = new ObjectRequest();
		facilityDashScreen.setId("DC:87");
		facilityDashScreen.setProperties(new String []{"name", "numAlerts", "racks"});
		//facilityDashScreen.setRequestDirectChildrenOnly(true);
		
		ObjectRequest facilityDashRACK = new ObjectRequest();
		facilityDashRACK.setType("RACK");
		facilityDashRACK.setRequestChildrenNumberOnly(true);
		facilityDashRACK.setRequestDirectChildrenOnly(false);
		
		ObjectRequest facilityDashCRAC = new ObjectRequest();
		facilityDashCRAC.setType("CRAC");
		facilityDashCRAC.setRequestChildrenNumberOnly(true);
		facilityDashCRAC.setRequestDirectChildrenOnly(false);
		
		ObjectRequest facilityDashPUE = new ObjectRequest();
		facilityDashPUE.setType("PUE");
		facilityDashPUE.setRequestDirectChildrenOnly(false);
		facilityDashPUE.setRequestDirectParentsOnly(true);
		facilityDashPUE.setProperties(new String []{"pue", "itPower", "coolingPower", "powerLoss", "lightingPower"});
		
		//facilityDashScreen.setChildrenRequests(new ObjectRequest []{facilityDashRACK, facilityDashCRAC, facilityDashPUE});
		facilityDashScreen.setChildrenRequests(new ObjectRequest []{facilityDashPUE});
		
		//result = objectmanager.getObjectsData(new ObjectRequest []{facilityDashScreen});
		result = objectmanager.getObjectData(facilityDashScreen);
		
		strBuf = ObjectRequestUtils.printResponse(result);
		System.out.println(strBuf);
		
		System.out.println("!!!!");
		if(a == 1){
			return;
		}
		
		//Floorplan
		ObjectRequest floorplanRequest = new ObjectRequest();
		floorplanRequest.setId("DC:87");
		floorplanRequest.setProperties(new String []{"width", "height", "name"});
		
		ObjectRequest floorplanRequestRACK = new ObjectRequest();
		floorplanRequestRACK.setType("RACK");
		floorplanRequestRACK.setRequestDirectChildrenOnly(false);
		floorplanRequestRACK.setProperties(new String []{"x", "y", "status", "numAlerts"});
		
		ObjectRequest floorplanRequestCRAC = new ObjectRequest();
		floorplanRequestCRAC.setType("CRAC");
		floorplanRequestCRAC.setRequestDirectChildrenOnly(false);
		floorplanRequestCRAC.setProperties(new String []{"x", "y", "status", "numAlerts"});
		
		floorplanRequest.setChildrenRequests(new ObjectRequest []{floorplanRequestRACK, floorplanRequestCRAC});
		
		result = objectmanager.getObjectsData(new ObjectRequest []{floorplanRequest});
		
		strBuf = ObjectRequestUtils.printResponse(result);
		System.out.println(strBuf);
		
		System.out.println("Done!");
	}
	
	private static ObjectRequest [] getRequests(String [] objTypes, String [] objProps, boolean expandSensorLinks, boolean directChildrenOnly, boolean childNumOnly){
		ObjectRequest [] result = new ObjectRequest[objTypes.length];
		for(int i = 0; i < objTypes.length; i++){
			result[i] = new ObjectRequest();
			result[i].setProperties(objProps);
			result[i].setType(objTypes[i]);
			if(expandSensorLinks){
				result[i].setLinkRequests(getSensorLinks());
			}			
			result[i].setRequestDirectChildrenOnly(directChildrenOnly);
			result[i].setRequestChildrenNumberOnly(childNumOnly);
		}
		return result;
	}
	
	private static ObjectRequest [] getSensorLinks(){
		String [] sensorProps = new String []{"lastValue"};
		String [] sensorTypes = new String []{"WSNSENSOR", "IPMISENSOR", "MODBUSPROPERTY"};
		ObjectRequest [] result = new ObjectRequest[sensorTypes.length];
		for(int i = 0; i < sensorTypes.length; i++){
			result[i] = new ObjectRequest();
			result[i].setProperties(sensorProps);
			result[i].setType(sensorTypes[i]);
			//result[i].setAdditionalInfo(RequestInfo.TIMESTAMP | RequestInfo.UNITS | RequestInfo.LINK);
			result[i].setAdditionalInfo(RequestInfo.UNITS | RequestInfo.MAPPED_VALUE);
		}
		return result;
	}
	
	private static ObjectRequest [] concatArrays(ObjectRequest [] arr1, ObjectRequest [] arr2){
		ObjectRequest [] concatReqs = new ObjectRequest[arr1.length + arr2.length];
		for(int i = 0; i < arr1.length; i++){
			concatReqs[i] = arr1[i];
		}
		int index = arr1.length;
		for (int i = 0; i < arr2.length; i++){
			concatReqs[index + i] = arr2[i];
		}		
		return concatReqs;
	}
}
