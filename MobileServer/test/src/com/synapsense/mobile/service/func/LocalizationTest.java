package com.synapsense.mobile.service.func;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.googlecode.jsonrpc4j.ProxyUtil;
import com.synapsense.mobile.service.JsonRpcHttpCsrfClient;
import com.synapsense.mobile.service.ObjectManager;
import com.synapsense.mobile.service.TestUtils;
import com.synapsense.mobile.service.UserManager;
import com.synapsense.mobile.tree.LiveImagingType;
import com.synapsense.mobile.user.UserInfo;

public class LocalizationTest {
	/**
	 * @param args
	 * @throws MalformedURLException
	 */
	public static void main(String[] args) throws MalformedURLException {
		URL url;
		url = new URL(Constants.BASE_URL + "/userManager");
		JsonRpcHttpClient userclient = new JsonRpcHttpCsrfClient(url);
		UserManager userManager = ProxyUtil.createClientProxy(Thread.currentThread().getContextClassLoader(),
		        UserManager.class, userclient);
		UserInfo userInfo = userManager.login(Constants.LOGIN,  Constants.PASSWORD, Constants.DEVICE_ID, Constants.DEVICE_NAME);

		if(!TestUtils.checkLoginStatus(userInfo)){
			return;
		}
		
		Map<String, String> header = new HashMap<String, String>();
		header.put("Cookie", "JSESSIONID=" + userInfo.getjSessionId());

		url = new URL(Constants.BASE_URL + "/objectManager");
		JsonRpcHttpClient objectclient = new JsonRpcHttpCsrfClient(url, header);
		ObjectManager objectmanager = ProxyUtil.createClientProxy(Thread.currentThread().getContextClassLoader(),
		        ObjectManager.class, objectclient);

		System.out.println("*********************");
		Locale loc = new Locale("en", "US");
		for (LiveImagingType type : objectmanager.getLiveImagingTypes()) {
			System.out.println(type.getName() + " " + type.getDataClass() + " " + type.getLayer());
		}

		System.out.println("*********************");
		loc = new Locale("ru", "RU");
		for (LiveImagingType type : objectmanager.getLiveImagingTypes()) {
			System.out.println(type.getName() + " " + type.getDataClass() + " " + type.getLayer());
		}

		url = new URL(Constants.BASE_URL + "/userManager");
		userclient = new JsonRpcHttpCsrfClient(url, header);
		userManager = ProxyUtil.createClientProxy(Thread.currentThread().getContextClassLoader(), UserManager.class,
		        userclient);
		userManager.logout();

		System.out.println("******Logout******");

	}
}
