package com.synapsense.mobile.service.func;

import java.util.Collection;
import java.util.Properties;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertFilter;
import com.synapsense.dto.AlertFilterBuilder;
import com.synapsense.dto.AlertStatus;
import com.synapsense.service.AlertService;
import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;

public class EnvironmentTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Properties p = new Properties();
		p.put(Context.PROVIDER_URL, "jnp://dendrite:1099");
		p.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		p.put(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");

		AlertService alertServ = null;
		try {
			alertServ = ServerLoginModule.getProxy(new InitialContext(p), JNDINames.ALERTING_SERVICE_JNDI_NAME,
			        AlertService.class);
		} catch (NamingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		AlertFilterBuilder filter = new AlertFilterBuilder();
		filter.active();
		filter.historical();

		Collection<Alert> alerts = alertServ.getAlerts(filter.build());
		for (Alert alert : alerts) {
			System.out.println(alert.getAlertId());
			System.out.println("**********");
		}
	}

}
