package com.synapsense.mobile.service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.googlecode.jsonrpc4j.JsonRpcHttpClient;

public class JsonRpcHttpCsrfClient extends JsonRpcHttpClient {
	protected static String responseCsrfNonce = null;

	public JsonRpcHttpCsrfClient(ObjectMapper mapper, URL serviceUrl, Map<String, String> headers) {
		super(mapper, serviceUrl, headers);
		// TODO Auto-generated constructor stub
	}

	public JsonRpcHttpCsrfClient(URL serviceUrl, Map<String, String> headers) {
		super(serviceUrl, headers);
		// TODO Auto-generated constructor stub
	}

	public JsonRpcHttpCsrfClient(URL serviceUrl) {
		super(serviceUrl);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object invoke(String methodName, Object argument, Type returnType, Map<String, String> extraHeaders)
	        throws Throwable {

		if (responseCsrfNonce != null) {
			Map<String, String> extExtraheaders = new HashMap<String, String>(extraHeaders);
			extExtraheaders.put("CSRF-Nonce", responseCsrfNonce);
			extraHeaders = extExtraheaders;
		}

		// create URLConnection
		HttpURLConnection con = openConnection(extraHeaders);

		// invoke it
		super.invoke(methodName, argument, con.getOutputStream());

		responseCsrfNonce = con.getHeaderField("CSRF-Nonce"); // update nonce
															  // for next invoke

		// read and return value
		return super.readResponse(returnType, con.getInputStream());
	}

	private HttpURLConnection openConnection(Map<String, String> extraHeaders) throws IOException {

		// create URLConnection
		HttpURLConnection con = (HttpURLConnection) getServiceUrl().openConnection(getConnectionProxy());
		con.setConnectTimeout(getConnectionTimeoutMillis());
		con.setReadTimeout(getReadTimeoutMillis());
		con.setAllowUserInteraction(false);
		con.setDefaultUseCaches(false);
		con.setDoInput(true);
		con.setDoOutput(true);
		con.setUseCaches(false);
		con.setInstanceFollowRedirects(true);
		con.setRequestMethod("POST");

		// add headers
		for (Entry<String, String> entry : getHeaders().entrySet()) {
			con.setRequestProperty(entry.getKey(), entry.getValue());
		}
		for (Entry<String, String> entry : extraHeaders.entrySet()) {
			con.setRequestProperty(entry.getKey(), entry.getValue());
		}
		con.setRequestProperty("Content-Type", "application/json-rpc");

		// open the connection
		con.connect();

		// return it
		return con;
	}
}
