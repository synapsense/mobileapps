package com.synapsense.mobile.service;

import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

import javax.naming.Context;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.mobile.dataAccess.Env;
import com.synapsense.mobile.dataAccess.FilteringAlertService;
import com.synapsense.mobile.dataAccess.FilteringEnvironment;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;

public class TestFilter {
		public static void user(){
			Properties c = new Properties();
			c.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
			c.put(Context.URL_PKG_PREFIXES,"org.jboss.naming:org.jnp.interfaces");
			c.put(Context.PROVIDER_URL, "jnp://axon:1099");
			Env.init(c);
			Env.login("user", "user");
			Environment env=Env.getEnv();
			FilteringEnvironment filterEnv=new FilteringEnvironment(env, "user", false);
			Environment fe=filterEnv.getProxy();
			AlertService alert=Env.getAlertService();
			FilteringAlertService filterAlert=new FilteringAlertService(alert, env, fe);
			AlertService fa=filterAlert.getProxy();
			
			Collection<TO<?>> root = env.getObjectsByType("ROOT");
			Iterator<TO<?>> itr = root.iterator();
			int num = fa.getNumActiveAlerts(itr.next(), true)
			        + fa.getNumActiveAlerts(TOFactory.EMPTY_TO, true);
			System.out.println(num);
			
		}
		public static void admin(){
			Properties c = new Properties();
			c.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
			c.put(Context.URL_PKG_PREFIXES,"org.jboss.naming:org.jnp.interfaces");
			c.put(Context.PROVIDER_URL, "jnp://axon:1099");
			Env.init(c);
			Env.login("admin", "admin");
			Environment env=Env.getEnv();
			FilteringEnvironment filterEnv=new FilteringEnvironment(env, "admin", true);
			Environment fe=filterEnv.getProxy();
			AlertService alert=Env.getAlertService();
			FilteringAlertService filterAlert=new FilteringAlertService(alert, env, fe);
			AlertService fa=filterAlert.getProxy();
			
			Collection<TO<?>> root = env.getObjectsByType("ROOT");
			Iterator<TO<?>> itr = root.iterator();
			int num = fa.getNumActiveAlerts(itr.next(), true)
			        + fa.getNumActiveAlerts(TOFactory.EMPTY_TO, true);
			System.out.println(num);
			
		}
		
	public static void main(String[] arqs){
		user();
		admin();
	}
}
