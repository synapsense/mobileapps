package com.synapsense.mobile.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;

import mockit.Expectations;

import org.junit.Ignore;
import org.junit.Test;

import com.synapsense.dto.AlertFilter;
import com.synapsense.dto.AlertStatus;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.mobile.service.impl.AlertManagerImpl;
import com.synapsense.mobile.utilities.EnvironmentTypes;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;

public class AlertManagerTest {

	final AlertManagerImpl alertManager = new AlertManagerImpl();

	@Test
	public void acknowledgeAlert(final AlertService alertServ, final com.synapsense.dto.Alert alert,
	        final AlertStatus status) throws Exception {

		alertManager.setAlertServ(alertServ);
		new Expectations() {
			{
				// return opened alert
				alertServ.getAlert(1);
				times = 1;
				returns(alert);

				alert.getStatus();
				times = 1;
				returns(status);

				status.ordinal();
				times = 1;
				returns(0);

				alertServ.acknowledgeAlert(1, "ack");
				times = 1;

				// return acknowledged alert
				alertServ.getAlert(2);
				times = 1;
				returns(alert);

				alert.getStatus();
				times = 1;
				returns(status);

				status.ordinal();
				times = 1;
				returns(1);
			}
		};
		assertTrue(alertManager.acknowledge(1, "ack"));
		assertFalse(alertManager.acknowledge(2, "ack"));
	}

	@Test
	public void resolveAlert(final AlertService alertServ, final com.synapsense.dto.Alert alert,
	        final AlertStatus status) throws Exception {

		alertManager.setAlertServ(alertServ);
		new Expectations() {
			{
				// return opened alert
				alertServ.getAlert(1);
				times = 1;
				returns(alert);

				alert.getStatus();
				times = 1;
				returns(status);

				status.ordinal();
				times = 1;
				returns(0);

				alertServ.resolveAlert(1, "opn");
				times = 1;

				// return acknowledged alert
				alertServ.getAlert(2);
				times = 1;
				returns(alert);

				alert.getStatus();
				times = 1;
				returns(status);

				status.ordinal();
				times = 1;
				returns(1);

				alertServ.resolveAlert(2, "ack");
				times = 1;

				// return dismissed alert
				alertServ.getAlert(3);
				times = 1;
				returns(alert);

				alert.getStatus();
				times = 1;
				returns(status);

				status.ordinal();
				times = 1;
				returns(4);
			}
		};
		assertTrue(alertManager.resolve(1, "opn"));
		assertTrue(alertManager.resolve(2, "ack"));
		assertFalse(alertManager.resolve(3, "dis"));
	}

	@Test
	public void dismissAlert(final AlertService alertServ, final com.synapsense.dto.Alert alert,
	        final AlertStatus status) throws Exception {

		alertManager.setAlertServ(alertServ);
		new Expectations() {
			{
				// return opened alert
				alertServ.getAlert(1);
				times = 1;
				returns(alert);

				alert.getStatus();
				times = 1;
				returns(status);

				status.ordinal();
				times = 1;
				returns(0);

				alertServ.dismissAlert(1, "opn");
				times = 1;

				// return acknowledged alert
				alertServ.getAlert(2);
				times = 1;
				returns(alert);

				alert.getStatus();
				times = 1;
				returns(status);

				status.ordinal();
				times = 1;
				returns(1);

				alertServ.dismissAlert(2, "ack");
				times = 1;

				// return resolved alert
				alertServ.getAlert(3);
				times = 1;
				returns(alert);

				alert.getStatus();
				times = 1;
				returns(status);

				status.ordinal();
				times = 1;
				returns(3);
			}
		};
		assertTrue(alertManager.dismiss(1, "opn"));
		assertTrue(alertManager.dismiss(2, "ack"));
		assertFalse(alertManager.dismiss(3, "dis"));
	}

	@Test
	public void getActiveAlerts(final AlertService alertServ, final Environment env) throws Exception {
		alertManager.setAlertServ(alertServ);
		alertManager.setEnv(env);

		final Collection<TO<?>> root = new LinkedList<TO<?>>();
		final TO<?> toRoot = TOFactory.getInstance().loadTO("ROOT", 1);
		root.add(toRoot);

		final Collection<com.synapsense.dto.Alert> alerts = new LinkedList<com.synapsense.dto.Alert>();
		final com.synapsense.dto.Alert alert =  new com.synapsense.dto.Alert();
		alert.setHostTO(toRoot);
		alerts.add(alert);
		alerts.add(new com.synapsense.dto.Alert());

		final Collection<AlertType> alTypes = new LinkedList<AlertType>();
		alTypes.add(new AlertType("name", "name", "alert", "MINOR", "header", "body", null, true, false, false, false));
		new Expectations() {
			{
				env.getObjectsByType(EnvironmentTypes.TYPE_ROOT);
				times = 1;
				returns(root);

				alertServ.getAlerts(withInstanceOf(AlertFilter.class));
				times = 1;
				returns(alerts);

				alertServ.getAlerts(withInstanceOf(AlertFilter.class));
				times = 1;
				returns(new LinkedList<com.synapsense.dto.Alert>());

				alertServ.getNumActiveAlerts(toRoot, true);
				times = 1;
				returns(0);
				
				env.getPropertyValue(toRoot, "name", String.class);
				times=1;
				returns("ROOT");
				
				alertServ.getAlertTypes(withInstanceOf(String[].class));
				times = 1;
				returns(alTypes);
				
				alertServ.getNumActiveAlerts(TOFactory.EMPTY_TO, true);
				times = 1;
				returns(0);

				alertServ.getAlertTypes(withInstanceOf(String[].class));
				times = 1;
				returns(alTypes);
			}
		};

		assertEquals(2, alertManager.getActiveAlerts(null, null, null, null, null, null).length);
	}
	
	@Test
	public void getZeroActiveAlerts(final AlertService alertServ, final Environment env) throws Exception {
		alertManager.setAlertServ(alertServ);
		alertManager.setEnv(env);

		final Collection<TO<?>> root = new LinkedList<TO<?>>();
		final TO<?> toRoot = TOFactory.getInstance().loadTO("ROOT", 1);
		root.add(toRoot);

		final Collection<com.synapsense.dto.Alert> alerts = new LinkedList<com.synapsense.dto.Alert>();

		new Expectations() {
			{
				env.getObjectsByType(EnvironmentTypes.TYPE_ROOT);
				times = 1;
				returns(root);

				alertServ.getAlerts(withInstanceOf(AlertFilter.class));
				times = 1;
				returns(alerts);

				alertServ.getAlerts(withInstanceOf(AlertFilter.class));
				times = 1;
				returns(new LinkedList<com.synapsense.dto.Alert>());

			}
		};

		assertEquals(0, alertManager.getActiveAlerts(null, null, null, null, null, null).length);
	}
	@Ignore
	@Test
	public void getActiveAlertsWithPriority(final AlertService alertServ, final Environment env) throws Exception {
		alertManager.setAlertServ(alertServ);
		alertManager.setEnv(env);

		final Collection<TO<?>> root = new LinkedList<TO<?>>();
		final TO<?> toRoot = TOFactory.getInstance().loadTO("ROOT", 1);
		root.add(toRoot);

		final Collection<com.synapsense.dto.Alert> alerts = new LinkedList<com.synapsense.dto.Alert>();
		final Collection<com.synapsense.dto.Alert> alerts1 = new LinkedList<com.synapsense.dto.Alert>();
		final Collection<com.synapsense.dto.Alert> alerts2 = new LinkedList<com.synapsense.dto.Alert>();
		final com.synapsense.dto.Alert alert =  new com.synapsense.dto.Alert();
		alert.setHostTO(toRoot);
		alert.setAlertTime(0L);
		alert.setAcknowledgementTime(new Date());
		alert.setName("ConsoleAlert_alert");
		alerts.add(alert);
		alerts.add(new com.synapsense.dto.Alert());
		alerts1.add(alert);
		alerts1.add(new com.synapsense.dto.Alert());
		alerts2.add(alert);
		alerts2.add(new com.synapsense.dto.Alert());

		final Collection<AlertType> alTypes = new LinkedList<AlertType>();
		alTypes.add(new AlertType("name", "name", "alert", "MINOR", "header", "body", null, true, false, false, false));
		
		final Collection<AlertType> alTypes1 = new LinkedList<AlertType>();
		alTypes1.add(new AlertType("name", "name", "alert", "INFORMATIONAL", "header", "body", null, true, false, false, false));
		
		final Collection<AlertType> alTypes2 = new LinkedList<AlertType>();
		alTypes2.add(new AlertType("name", "name", "alert", "MAJOR", "header", "body", null, true, false, false, false));
		
		final Collection<AlertType> alTypes3 = new LinkedList<AlertType>();
		alTypes3.add(new AlertType("name", "name", "alert", "CRITICAL", "header", "body", null, true, false, false, false));
		new Expectations() {
			{
				//get informational alert
				env.getObjectsByType(EnvironmentTypes.TYPE_ROOT);
				times = 1;
				returns(root);

				alertServ.getAlerts(withInstanceOf(AlertFilter.class));
				times = 1;
				returns(alerts);

				alertServ.getAlerts(withInstanceOf(AlertFilter.class));
				times = 1;
				returns(new LinkedList<com.synapsense.dto.Alert>());
				
				alertServ.getAlertTypes(withInstanceOf(String[].class));
				times = 1;
				returns(alTypes);
				
				alertServ.getAlertTypes(withInstanceOf(String[].class));
				times = 1;
				returns(alTypes1);
				
				alertServ.getNumActiveAlerts(TOFactory.EMPTY_TO, true);
				times = 1;
				returns(0);
				
				alertServ.getAlertTypes(withInstanceOf(String[].class));
				times = 1;
				returns(alTypes1);
				
				//get major alert
				
				env.getObjectsByType(EnvironmentTypes.TYPE_ROOT);
				times = 1;
				returns(root);

				alertServ.getAlerts(withInstanceOf(AlertFilter.class));
				times = 1;
				returns(alerts1);

				alertServ.getAlerts(withInstanceOf(AlertFilter.class));
				times = 1;
				returns(new LinkedList<com.synapsense.dto.Alert>());
				
				alertServ.getAlertTypes(withInstanceOf(String[].class));
				times = 1;
				returns(alTypes1);
				
				alertServ.getAlertTypes(withInstanceOf(String[].class));
				times = 1;
				returns(alTypes2);
				
				alertServ.getNumActiveAlerts(TOFactory.EMPTY_TO, true);
				times = 1;
				returns(0);
				
				alertServ.getAlertTypes(withInstanceOf(String[].class));
				times = 1;
				returns(alTypes2);
				
				//get minor alert
				
				env.getObjectsByType(EnvironmentTypes.TYPE_ROOT);
				times = 1;
				returns(root);

				alertServ.getAlerts(withInstanceOf(AlertFilter.class));
				times = 1;
				returns(alerts2);

				alertServ.getAlerts(withInstanceOf(AlertFilter.class));
				times = 1;
				returns(new LinkedList<com.synapsense.dto.Alert>());
				
				alertServ.getAlertTypes(withInstanceOf(String[].class));
				times = 1;
				returns(alTypes);
				
				alertServ.getAlertTypes(withInstanceOf(String[].class));
				times = 1;
				returns(alTypes3);
				
				alertServ.getNumActiveAlerts(TOFactory.EMPTY_TO, true);
				times = 1;
				returns(0);
				
				alertServ.getAlertTypes(withInstanceOf(String[].class));
				times = 1;
				returns(alTypes);
			}
		};

		assertEquals(1, alertManager.getActiveAlerts(null, null, 1, null, null, null).length);
		assertEquals(1, alertManager.getActiveAlerts(null, null, 4, null, null, null).length);
		assertEquals(1, alertManager.getActiveAlerts(null, null, 2, null, null, null).length);
	}
}
