package com.synapsense.mobile.service;

import com.synapsense.mobile.user.LoginStatus;
import com.synapsense.mobile.user.UserInfo;

public class TestUtils {
	public static boolean checkLoginStatus(UserInfo userInfo) {
		boolean result = true;
		String status = "OK";
		int loginStatus = userInfo.getLoginStatus();
		if (loginStatus != LoginStatus.OK) {
			result = false;
			status = "FAILED";
			if (loginStatus == LoginStatus.DEVICE_NOT_ALLOWED) {
				status = "DEVICE_NOT_ALLOWED";
			} else if (loginStatus == LoginStatus.DEVICE_REGISTRATION_REQUESTED) {
				status = "DEVICE_REGISTRATION_REQUESTED";
			}
			System.out.println("Login failed.");
		}
		System.out.println("Login Status: " + status);
		return result;
	}
}
