package com.synapsense.mobile.managers;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.mobile.dataAccess.Env;
import com.synapsense.mobile.utilities.EnvironmentTypes;
import com.synapsense.service.Environment;

public class MobileDeviceManager {
	private static final Logger LOG = Logger.getLogger(MobileDeviceManager.class);
	public static Integer INACTIVE_DEVICE_STATUS = 0;
	public static Integer ACTIVE_DEVICE_STATUS = 1;	
	
	public enum Status {
		OK,
		NOT_ALLOWED,
		NOT_REGISTERED
	}
	
	public static MobileDeviceManager.Status checkDeviceStatus(String deviceId, HttpServletRequest req){
		MobileDeviceManager.Status result = Status.NOT_ALLOWED;		
		Environment env = Env.getEnv(req);
		Collection<TO<?>> devices = env.getObjects(EnvironmentTypes.MOBILE_DEVICE, new ValueTO[]{new ValueTO("deviceId", deviceId)});		
		try {
			if(devices.isEmpty()){
				result = Status.NOT_REGISTERED;
			} else {
				TO<?> device = devices.iterator().next();
				Integer deviceStatus = env.getPropertyValue(device, "isActive", Integer.class);
				if(ACTIVE_DEVICE_STATUS.equals(deviceStatus)){
					result = Status.OK;
				} else {
					result = Status.NOT_ALLOWED;
				}				
			}
		}catch(Exception e){
			LOG.error(e.getLocalizedMessage(), e);
		}
		return result;
	}
	
	public static void requestRegistration(String deviceId, String deviceName, String userName, HttpServletRequest req) throws EnvException{
		Environment env = Env.getEnv(req);
		Collection<TO<?>> devices = env.getObjects(EnvironmentTypes.MOBILE_DEVICE, new ValueTO[]{new ValueTO("deviceId", deviceId)});
		if(devices.isEmpty()){
			ValueTO[] vtos = new ValueTO[]{new ValueTO("deviceName", deviceName),
					new ValueTO("deviceId", deviceId),
					new ValueTO("userName", userName),
					new ValueTO("isActive", INACTIVE_DEVICE_STATUS)};
			env.createObject(EnvironmentTypes.MOBILE_DEVICE, vtos);
		}
	}
}
