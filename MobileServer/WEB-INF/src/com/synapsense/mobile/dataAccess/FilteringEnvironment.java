package com.synapsense.mobile.dataAccess;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBException;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.mobile.conf.types.Property;
import com.synapsense.mobile.conf.types.Type;
import com.synapsense.mobile.conf.types.TypeUtil;
import com.synapsense.mobile.utilities.Constants;
import com.synapsense.mobile.utilities.EnvironmentTypes;
import com.synapsense.service.Environment;

/**
 * This class does the following:
 * 1. filters out zones, dcs and their children that user doesn't have permission for.
 * Following methods of the Environment are proxied:
 *     getChildren(TO<?> obj)
 *     getChildren(Collection<TO<?>> objs)
 *     getChildren(TO<?> obj, String type)
 *     getChildren(Collection<TO<?>> objs, String type)
 *     getRelatedObjects(TO<?> obj)
 *     getRelatedObjects(TO<?> obj, String type)
 *     getObjectsByType(String type)
 *     getObjectsByType(String type, ObjectTypeMatchMode mode)
 *     getObjects(String type, ValueTO [] vals)
 *     getObjects(String type, ObjectTypeMatchMode mode, ValueTO [] vals)
 *
 * 2. filters properties based on the configuration in conf/types.xml. 
 * Property is not filtered if it is explicitly requested by name.
 * Following methods of the Environment are proxied:
 *     getAllPropertiesValues(TO<?> obj)
 *     getAllPropertiesValues(Collection<TO<?>> objs)
 *     getObjectTypes(String[] typeNames)
 *     getObjectType(String typeName)
 *     updateObjectType(ObjectType objType)
 * 
 * 3. Locally calculates property value and history if "formula"
 * element for property is defined in types.xml. In this case method call
 * is not forwarded to Environment 
 *     
 * @author apahunov
 *
 */
public class FilteringEnvironment {


	private Environment env;
	private String userName;
	private boolean isRoot;

	public FilteringEnvironment(Environment env, String userName,
			boolean isRoot) {
		if (env == null) {
			throw new IllegalArgumentException("env can not be null");
		}
		if (userName == null) {
			throw new IllegalArgumentException("userName can not be null");
		}

		this.env = env;
		this.userName = userName;
		this.isRoot = isRoot;
	}
	
	/**
	 * Returns the original Environment proxied by this instance.
	 * @return the environment being proxied.
	 */
	public Environment getProxiedEnvironment() {
		return env;
	}

	public String getUserName() {
		return userName;
	}

	public boolean isRoot() {
		return isRoot;
	}
	
	/**
	 * Gets new proxy object which will perform filtering
	 * in a wrapped Environment interface. All returned proxies
	 * are linked with the current instance so any changes in this
	 * FilteringEnvironment instance will be reflected by all proxies
	 * crated from it.
	 * @return new Environment proxy instance.
	 */
	public Environment getProxy() {
		ChainProxy proxy1 = new PropertyFilterProxy();
		ChainProxy proxy2 = new PermissionsFilterProxy();
		proxy1.setNextProxy(proxy2);
			
		return (Environment)Proxy.newProxyInstance(
				Environment.class.getClassLoader(),
				new Class[]{Environment.class}, proxy1);
	}

	private class ChainProxy implements InvocationHandler {
		InvocationHandler nextProxy;

		@Override
		public Object invoke(Object proxy, Method method, Object[] args)
				throws Throwable {
			if (nextProxy == null) {
				try {
					return method.invoke(env, args);
				} catch (InvocationTargetException e) {
					//unwrap original cause
					throw e.getCause();
				}
			} else {
				return nextProxy.invoke(proxy, method, args);
			}
		}

		public void setNextProxy(InvocationHandler nextProxy) {
			this.nextProxy = nextProxy;
		}
		
	}

	/**
	 * This class filters out zones, dcs and their children that user doesn't have permission for.
	 * Following methods of the Environment are proxied:
	 *     getChildren(TO<?> obj)
	 *     getChildren(Collection<TO<?>> objs)
	 *     getChildren(TO<?> obj, String type)
	 *     getChildren(Collection<TO<?>> objs, String type)
	 *     getRelatedObjects(TO<?> obj)
	 *     getRelatedObjects(TO<?> obj, String type)
	 *     getObjectsByType(String type)
	 *     getObjectsByType(String type, ObjectTypeMatchMode mode)
	 *     getObjects(String type, ValueTO [] vals)
	 *     getObjects(String type, ObjectTypeMatchMode mode, ValueTO [] vals)
	 *
	 * @author apahunov
	 *
	 */
	private class PermissionsFilterProxy extends ChainProxy {
		@SuppressWarnings("unchecked")
		@Override
		public Object invoke(Object proxy, Method method, Object[] args)
				throws Throwable {
			if (isRoot) {
				return super.invoke(proxy, method, args);
			}
			
			if("getChildren".equals(method.getName())) {
				Object result =  super.invoke(proxy, method, args);;
				//dispatch on arguments
				if(args.length>0) {//just a sanity check
					if(TO.class.isAssignableFrom(args[0].getClass())) {
						if (args.length == 2
							&& !EnvironmentTypes.TYPE_DC.equals(args[1])
							&& !EnvironmentTypes.TYPE_ZONE.equals(args[1])) {
							return result;
						} else {					
							return filterTOs(result);
						}
					} else if(Collection.class.isAssignableFrom(args[0].getClass())) {
						if (args.length == 2
								&& !EnvironmentTypes.TYPE_DC.equals(args[1])
								&& !EnvironmentTypes.TYPE_ZONE.equals(args[1])) {
							return result;
						} else {					
							return filterCollectionTOs(result);
						}
					}
				}
			} else if("getRelatedObjects".equals(method.getName()) && args[2].equals(true)
					&&  (EnvironmentTypes.TYPE_DC.equals(((TO<?>)args[0]).getTypeName()) || EnvironmentTypes.TYPE_ROOT.equals(((TO<?>)args[0]).getTypeName()))){
				
				Collection<TO<?>> objs = (Collection<TO<?>>) super.invoke(proxy, method, args);
				TO<?> parent = (TO<?>)args[0];
				String childrenType = (String)args[1];
				
				if (EnvironmentTypes.TYPE_ZONE.equals(childrenType)) {
					if (EnvironmentTypes.TYPE_DC.equals(parent.getTypeName())) {
						//Zones in DC were requested, just remove prohibited zones
						objs = filterTOs(objs);
					} else if (EnvironmentTypes.TYPE_ROOT.equals(parent.getTypeName())){
						//Zones in root were requested, filter by dcs
						objs = filterRelatedByZonesOrDcs(objs, parent, childrenType, EnvironmentTypes.TYPE_DC);
						objs = filterTOs(objs);
					}
				} else if (EnvironmentTypes.TYPE_DC.equals(childrenType)) {
					//Just remove prohibited dcs
					objs = filterTOs(objs);
				} else {
					//Filter by both dcs and zones
					objs = filterRelatedByZonesOrDcs(objs, parent, childrenType, EnvironmentTypes.TYPE_DC);
					objs = filterRelatedByZonesOrDcs(objs, parent, childrenType, EnvironmentTypes.TYPE_ZONE);
				}
				return objs;
			} else if("getObjectsByType".equals(method.getName())
					|| ("getObjects".equals(method.getName()) && args[0] instanceof String)){
				Collection<TO<?>> objs = (Collection<TO<?>>) super.invoke(proxy, method, args);
				Collection<TO<?>> roots = env.getObjectsByType(EnvironmentTypes.TYPE_ROOT);
				if (!roots.isEmpty()) {
					TO<?> root = roots.iterator().next();
					String childrenType = (String) args[0];
					if (EnvironmentTypes.TYPE_ZONE.equals(childrenType)) {
						//Zones were requested, filter by dcs
						objs = filterRelatedByZonesOrDcs(objs, root, childrenType, EnvironmentTypes.TYPE_DC);
						objs = filterTOs(objs);
					} else if (EnvironmentTypes.TYPE_DC.equals(childrenType)) {
						//Just remove prohibited dcs
						objs = filterTOs(objs);
					} else {
						//Filter all ojbects by both zones and dcs
						objs = filterRelatedByZonesOrDcs(objs, root, childrenType, EnvironmentTypes.TYPE_DC);
						objs = filterRelatedByZonesOrDcs(objs, root, childrenType, EnvironmentTypes.TYPE_ZONE);
					}
				} 
				return objs;
			} else if("exists".equals(method.getName())) {
				TO<?> obj = (TO<?>) args[0];
				Collection<TO<?>> objs = new HashSet<TO<?>>(1);
				objs.add(obj);
				boolean filtered = filterTOs(objs).isEmpty();
				if (filtered) {
					return false;
				}
				//else forward call to implementation
			}
		
			return super.invoke(proxy, method, args);	
		}
		
		/**
		 * Removes not permitted zones and dcs from the collection
		 * of objects and returns new filtered collection
		 * 
		 * @param collection
		 * @return
		 */
		@SuppressWarnings("unchecked")
		private Collection<TO<?>> filterTOs(Object collection) {
			Collection<TO<?>> objs = (Collection<TO<?>>) collection;
			Collection<TO<?>> treeObjs = new HashSet<TO<?>>();
			
			for (TO<?> obj : objs) {
				if (EnvironmentTypes.TYPE_ZONE.equals(obj.getTypeName()) ||
					EnvironmentTypes.TYPE_DC.equals(obj.getTypeName())) {
					treeObjs.add(obj);
				}
			}
			
			if (!treeObjs.isEmpty()) {
				objs.removeAll(treeObjs);
				Collection<CollectionTO> props = env.getPropertyValue(treeObjs, new String[] {"permissions"});
				
				for (CollectionTO prop : props) {
					TO<?> treeObj = prop.getObjId();
					List<ValueTO> propValues = prop.getPropValues();
					String usersStr = null;
					//if prop exists
					if (!propValues.isEmpty()) {
						usersStr = (String) propValues.get(0).getValue();
					}
					
					//allow all
					if (usersStr == null || usersStr.contains(Constants.EVERYONE_ID)) {
						continue;
					}
					
					//allow no one
					if (usersStr.isEmpty()) {
						treeObjs.remove(treeObj);
						continue;
					}
					
					Collection<String> users = Arrays.asList(usersStr.toLowerCase().split(","));
					
					if (Env.SEMS_APP_NAME.equals(Env.appName)) {
						users = processSemsUsers(users);
					}
					
					if(!users.contains(userName.toLowerCase())) { //user login is case insensitive
						treeObjs.remove(treeObj);
					}
				}
				objs.addAll(treeObjs);
			}
		
			return objs;
		}
		
		@SuppressWarnings("unchecked")
		private Collection<ValueTO> filterValueTOs(Object collection) {
			Collection<ValueTO> values = new HashSet<ValueTO>((Collection<ValueTO>) collection);
			Map<TO<?> ,ValueTO> treeObjsMap = new HashMap<TO<?>, ValueTO>();
			
			for (ValueTO value : values) {
				TO<?> obj = (TO<?>) value.getValue();
				if (EnvironmentTypes.TYPE_ZONE.equals(obj.getTypeName()) ||
					EnvironmentTypes.TYPE_DC.equals(obj.getTypeName())) {
					treeObjsMap.put(obj, value);
				}
			}
			
			if (!treeObjsMap.isEmpty()) {
				values.removeAll(treeObjsMap.values());
				Collection<CollectionTO> props =  env.getPropertyValue(treeObjsMap.keySet(), new String[] {"permissions"});
				
				for (CollectionTO prop : props) {
					TO<?> treeObj = prop.getObjId();
					List<ValueTO> propValues = prop.getPropValues();
					String usersStr = null;
					//if prop exists
					if (!propValues.isEmpty()) {
						usersStr = (String) propValues.get(0).getValue();
					}
					
					//allow all
					if (usersStr == null || usersStr.contains(Constants.EVERYONE_ID)) {
						continue;
					}
					
					//allow no one
					if (usersStr.isEmpty()) {
						treeObjsMap.remove(treeObj);
						continue;
					}
					
					Collection<String> users = Arrays.asList(usersStr.toLowerCase().split(","));
					
					if (Env.SEMS_APP_NAME.equals(Env.appName)) {
						users = processSemsUsers(users);
					}
					
					if(!users.contains(userName.toLowerCase())) {
						treeObjsMap.remove(treeObj);
					}
				}
				values.addAll(treeObjsMap.values());
			}
		
			return values;
		}
		
		@SuppressWarnings("unchecked")
		private Collection<CollectionTO> filterCollectionTOs(Object collection) {
			Collection<CollectionTO> colTOs = new HashSet<CollectionTO>((Collection<CollectionTO>) collection);
			
			for (CollectionTO colTO : colTOs ) {
				List<ValueTO> values = colTO.getPropValues();
				filterValueTOs(values);
			}
			
			return colTOs;
		}
		
		/**
		 * Removes from objs objects those parent dcs or zones are not permitted
		 * 
		 * @param objs - collection of children to be filtered
		 * @param parent - parent object
		 * @param childrenType - this of the objects in objs
		 * @param treeObjectType - zone or dc
		 * @return
		 */
		private Collection<TO<?>> filterRelatedByZonesOrDcs(Collection<TO<?>> objs, TO<?> parent, String childrenType, String treeObjectType) {
			Collection<TO<?>> res = new HashSet<TO<?>>(objs);
			Collection<TO<?>> treeObjects = env.getRelatedObjects(parent, treeObjectType, true);
			Collection<TO<?>> allowed = filterTOs(new HashSet<TO<?>>(treeObjects));
			treeObjects.removeAll(allowed);
			Collection<TO<?>> prohibited = treeObjects;
			for (TO<?> treeObject : prohibited) {
				res.removeAll(env.getRelatedObjects(treeObject, childrenType, true));
			}
			return res;
		}
		
		private Collection<String>  processSemsUsers(Collection<String> users) {
			String distinctServerName = System.getProperty("jboss.server.name");
			Collection<String> res = new LinkedList<String>();
			if (distinctServerName != null && !distinctServerName.isEmpty()) {
				for (String user : users) {
					if (user.startsWith(distinctServerName + "\\")) {
						res.add(user.substring(distinctServerName.length() + 1));
					} else {
						res.add(user);
					}
				}
			}
			return res;
		}
	
	}

	/**
	 * This class filters out properties that have visible="false" attribute in types.xml file.
	 * Property is not filtered if it is requested explicitly, that is property name is passed to the method.
	 * Following methods of the Environment are proxied:
	 *     getAllPropertiesValues(TO<?> obj)
	 *     getAllPropertiesValues(Collection<TO<?>> objs)
	 *     getObjectTypes(String[] typeNames)
	 *     getObjectType(String typeName)
	 *     updateObjectType(ObjectType objType)
	 *     
	 * @author apahunov
	 */
	private class PropertyFilterProxy extends ChainProxy {

		@SuppressWarnings("unchecked")
		@Override
		public Object invoke(Object proxy, Method method, Object[] args)
				throws Throwable {
			if ("getAllPropertiesValues".equals(method.getName())) {
				if (args[0] instanceof TO) {
					TO<?> obj = (TO<?>) args[0];
					Collection<ValueTO> colVto = (Collection<ValueTO>) super.invoke(proxy, method, args);
					Collection<ValueTO> excludedVto = new HashSet<ValueTO>();
					for (ValueTO vto : colVto) {
						Property property = TypeUtil.getInstance().getProperty(obj.getTypeName(), vto.getPropertyName());
						if (property  != null && !property.isVisible()) {
							excludedVto.add(vto);
						}
					}
					colVto.removeAll(excludedVto);
					return colVto;
				} else if (args[0] instanceof Collection) {
					Collection<CollectionTO> colColTo = (Collection<CollectionTO>) super.invoke(proxy, method, args);
					for (CollectionTO colTo : colColTo) {
						for(ValueTO vto : colTo.getPropValues()) {
							Property property = TypeUtil.getInstance().getProperty(colTo.getObjId().getTypeName(), vto.getPropertyName());
							if (property  != null && !property.isVisible()) {
								colTo.remove(vto);
							}
						}
					}
				}
			} else if ("getObjectTypes".equals(method.getName())) {
				if (args != null && args.length == 1) {
					ObjectType[] ots = (ObjectType[]) super.invoke(proxy, method, args);
					for (ObjectType ot : ots) {
						filterObjectType(ot);
						//add virtual properties calculated in console
						ot.getPropertyDescriptors().addAll(getVirtualDescrs(ot.getName()));
					}
					return ots;
				}
			} else if ("getObjectType".equals(method.getName())) {
				ObjectType ot = (ObjectType) super.invoke(proxy, method, args);
				if (ot == null) {
					return null;
				}
				//remove properties with visible="false"
				ot = filterObjectType(ot);
				//add virtual properties calculated in console
				ot.getPropertyDescriptors().addAll(getVirtualDescrs(ot.getName()));
				return ot;
			} else if ("updateObjectType".equals(method.getName())) {
				ObjectType ot = (ObjectType) args[0];
				//add properties that were removed above, so they are not deleted after update
				ot.getPropertyDescriptors().addAll(getFilteredDescrs(ot, true));
				//remove virtual properties calculated in console
				ot.getPropertyDescriptors().removeAll(getVirtualDescrs(ot.getName()));
				super.invoke(proxy, method, args);
				return null;
			} else if ("getPropertyDescriptor".equals(method.getName())) {
				String typeName = (String) args[0];
				String propName = (String) args[1];
				
				//search in virtual properties first
				Set<PropertyDescr> descrs = getVirtualDescrs(typeName);
				for (PropertyDescr descr : descrs) {
					if (descr.getName().equals(propName)) {
						return descr;
					}
				}
				//PropertyDescr for propName is requested explicitly
				//so return even those that have visible="false"
				return env.getPropertyDescriptor(typeName, propName);
			}

			return super.invoke(proxy, method, args);
		}
		
		private ObjectType filterObjectType(ObjectType objType) throws JAXBException {
			objType.getPropertyDescriptors().removeAll(getFilteredDescrs(objType, false));
			return objType;
		}
		
		private Set<PropertyDescr> getFilteredDescrs(ObjectType objType, boolean isFiltered) {
			
			if (isFiltered) {
				objType = env.getObjectType(objType.getName());
			}
			
			Set<PropertyDescr> descrs = objType.getPropertyDescriptors();
			Set<PropertyDescr> filterderDescrs = new HashSet<PropertyDescr>();
			for (PropertyDescr descr : descrs) {
				Property property = TypeUtil.getInstance().getProperty(objType.getName(), descr.getName());
				if (property  != null && !property.isVisible()) {
					filterderDescrs.add(descr);
				}
			}
			return filterderDescrs;
		}
		
		private Set<PropertyDescr> getVirtualDescrs(String typeName) {
			Set<PropertyDescr> descrs = new HashSet<PropertyDescr>();
			Type type = TypeUtil.getInstance().getType(typeName);
			if(type != null){
				List<Property> properties = type.getProperty();
				for (Property property : properties) {
					if (property.getFormula() != null) {
						descrs.add(new PropertyDescr(property.getName(),property.getClassname()));
					}
				}
			}
			return descrs;
		}
	}
}
