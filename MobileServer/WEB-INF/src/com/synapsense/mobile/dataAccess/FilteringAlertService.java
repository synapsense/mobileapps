package com.synapsense.mobile.dataAccess;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;

import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertFilter;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.mobile.utilities.EnvironmentTypes;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;



/**
 * This FilteringAlertService filters result of methods of the 
 * AlertService based on "permissions" property of ZONE and DC objects
 * The following methods are proxied:
 *     getAlerts(AlertFilter alertFilter)
 *     getNumActiveAlerts(TO<?> obj, true)
 *     getActiveAlerts(TO<?> obj, true)
 *     getActiveAlerts(Collection<TO<?>> objs, true)
 *     getAllAlerts(TO<?> obj, true)
 *     getAllAlerts(Collection<TO<?>> objs, true)
 *     getAlertsHistory(TO<?> obj, true)
 *     getAlertsHistory(Collection<TO<?>> objs, true)
 *     getDismissedAlerts(TO<?> obj, true)
 *     getDismissedAlerts(Collection<TO<?>> objs, true)
 *     dismissAlerts(TO<?> obj, true)
 *     dismissAlerts(Collection<TO<?>> objs, true)
 *     dismissAlerts(TO<?> obj, true, String msg)
 *     dismissAlerts(Collection<TO<?>> objs, true, String msg)
 *     
 * @author pahunov
 *
 */
public class FilteringAlertService implements InvocationHandler {

	private AlertService alertService;
	private Environment fEnv;
	private Environment env;
	
	//Maps get method names into index of "deep" argument
	private static Map<String, Integer> getMethodsMap = new HashMap<String, Integer>();
	static {
		getMethodsMap.put("getActiveAlerts", 1);
		getMethodsMap.put("getAllAlerts", 1);
		getMethodsMap.put("getDismissedAlerts", 1);
		getMethodsMap.put("getAlertsHistory", 3);
	}
	
	/**
	 * Returns the original AlertService proxied by this instance.
	 * @return the AlertService being proxied.
	 */
	public AlertService getProxiedAlertService() {
		return alertService;
	}
	public  FilteringAlertService(AlertService alertService, Environment envinonment, Environment filteringEnvironment) {
		if (alertService == null) {
			throw new IllegalArgumentException("alertService can not be null");
		}
		if (envinonment == null) {
			throw new IllegalArgumentException("envinonment can not be null");
		}
		if (filteringEnvironment == null) {
			throw new IllegalArgumentException("filteringEnvironment can not be null");
		}
		
		this.alertService = alertService;
		this.env = envinonment;
		this.fEnv = filteringEnvironment;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {
		
		if (getMethodsMap.containsKey(method.getName()) && (Boolean) args[getMethodsMap.get(method.getName())]) {
			// All get methods return Collection<Alert> and have 1st param either
			// TO<?> or Collection<TO<?>> 
			//Proxy method only if "deep" argument is true
			Collection<Alert> alerts = (Collection<Alert>) method.invoke(alertService, args);
			Collection<TO<?>> filteredObjects = new HashSet<TO<?>>();
			
			if (TO.class.isAssignableFrom(args[0].getClass())){
				filteredObjects.addAll(getFilteredChildren((TO<?>) args[0]));
			} else if (Collection.class.isAssignableFrom(args[0].getClass())) {
				Collection<TO<?>> objs = (Collection<TO<?>>) args[0];
				for (TO<?> obj : objs) {
					filteredObjects.addAll(getFilteredChildren(obj));
				}
			}
			
			if (!filteredObjects.isEmpty()) {
				Class<?>[] paramTypes = new Class[args.length];
				paramTypes[0] = Collection.class;
				for (int i = 1; i < paramTypes.length; i++) {
					paramTypes[i] = args[i].getClass();
					//Boolean and boolean are considered to be different by getMethod
					if (paramTypes[i].equals(Boolean.class)) {
						paramTypes[i] = boolean.class;
					}
					
				}
				//make sure we call method with Collection param
				Method m = alertService.getClass().getMethod(method.getName(), paramTypes);
				args[0] = filteredObjects;
				//Remove from the result alerts on the filtered objects
				alerts.removeAll((Collection<Alert>) m.invoke(alertService, args));
			}
			
			return alerts;
		} else if ("getAlerts".equals(method.getName())) {
			Collection<Alert> alerts = alertService.getAlerts((AlertFilter) args[0]);;
			
			Collection<TO<?>> roots = env.getObjectsByType(EnvironmentTypes.TYPE_ROOT);
			if (roots.isEmpty()) {
				return alerts;
			}
			
			Collection<TO<?>> filteredObjects = new HashSet<TO<?>>();
			filteredObjects.addAll(getFilteredChildren(roots.iterator().next()));
			Collection<Alert> filteredList = new LinkedList<Alert>();
			
			for (Alert alert: alerts) {
				TO<?> to = alert.getHostTO();
				
				if (!TOFactory.EMPTY_TO.equals(to)) {
					Collection<TO<?>> parents = new HashSet<TO<?>>();
					parents.addAll(env.getRelatedObjects(to, EnvironmentTypes.TYPE_DC, false));
					parents.addAll(env.getRelatedObjects(to, EnvironmentTypes.TYPE_ZONE, false));
					
					for (TO<?> parent : parents) {
						if (filteredObjects.contains(parent)) {
							filteredList.add(alert);
							break;
						}
					}
				}
			}
			
			alerts.removeAll(filteredList);
			
			return alerts;
		} else if ("dismissAlerts".equals(method.getName()) && (Boolean) args[1]) {
			Collection<Alert> alerts = null;
			Collection<TO<?>> filteredObjects = new HashSet<TO<?>>();
			
			if (TO.class.isAssignableFrom(args[0].getClass())){
				TO<?> obj = (TO<?>) args[0];
				alerts = alertService.getActiveAlerts(obj, true);
				filteredObjects.addAll(getFilteredChildren(obj));
			} else if (Collection.class.isAssignableFrom(args[0].getClass())) {
				Collection<TO<?>> objs = (Collection<TO<?>>) args[0];
				alerts = alertService.getActiveAlerts(objs, true);
				for (TO<?> obj : objs) {
					filteredObjects.addAll(getFilteredChildren(obj));
				}
			}
			
			if (!filteredObjects.isEmpty()) {
				//Remove from the result alerts on the filtered objects
				alerts.removeAll(alertService.getActiveAlerts(filteredObjects, true));
			}
			
			for (Alert alert : alerts) {
				alertService.dismissAlert(alert, (String) args[2]);
			}
			
			return null;
		} else if ("getNumActiveAlerts".equals(method.getName()) && (Boolean) args[1]) {
			
			TO<?> obj = (TO<?>) args[0];
			
			int numAlerts = alertService.getNumActiveAlerts(obj, true);

			if (EnvironmentTypes.TYPE_ROOT.equals(obj.getTypeName())) {
				
				Collection<TO<?>> allDcs = env.getChildren(obj, EnvironmentTypes.TYPE_DC);
				Collection<TO<?>> visibleDcs = fEnv.getChildren(obj, EnvironmentTypes.TYPE_DC);
			
				for (TO<?> dc : allDcs) {
					if (visibleDcs.contains(dc)) {
						//try to find invisible zones
						Collection<TO<?>> filteredZones = getFilteredChildren(dc);
						for (TO<?> zone : filteredZones) {
							//subtract number of invisible alerts on zone
							numAlerts -= alertService.getNumActiveAlerts(zone, true);
						}
					} else {
						//subtract number of invisible alerts on dc
						numAlerts -= alertService.getNumActiveAlerts(dc, true);
					}
				}
			} else if (EnvironmentTypes.TYPE_DC.equals(obj.getTypeName())) {
				//try to find invisible zones
				Collection<TO<?>> filteredZones = getFilteredChildren(obj);
				for (TO<?> zone : filteredZones) {
					//subtract number of invisible alerts on zone
					numAlerts -= alertService.getNumActiveAlerts(zone, true);
				}
			}
			
			return numAlerts;
		}

		return method.invoke(alertService, args);
	}
	
	/**
	 * Returns prohibited child dcs and zones for supplies obj
	 * 
	 * @param obj
	 * @return
	 * @throws ObjectNotFoundException
	 */
	private Collection<TO<?>> getFilteredChildren(TO<?> obj) throws ObjectNotFoundException {
		Collection<TO<?>> res;
		//As "permissions" property is defined for ZONEs and DCs
		//only we request only that objects
		if (EnvironmentTypes.TYPE_ROOT.equals(obj.getTypeName())) {
			res = env.getChildren(obj, EnvironmentTypes.TYPE_DC);
			res.addAll(env.getRelatedObjects(obj, EnvironmentTypes.TYPE_ZONE, true));
			res.removeAll(fEnv.getChildren(obj, EnvironmentTypes.TYPE_DC));
			res.removeAll(fEnv.getRelatedObjects(obj, EnvironmentTypes.TYPE_ZONE, true));
		} else if (EnvironmentTypes.TYPE_DC.equals(obj.getTypeName())) {
			res = env.getChildren(obj, EnvironmentTypes.TYPE_ZONE);
			res.removeAll(fEnv.getChildren(obj, EnvironmentTypes.TYPE_ZONE));
		} else {
			res = new HashSet<TO<?>>();
		}
		return res;
	}
	
	/**
	 * Gets new proxy object which will perform filtering
	 * in a wrapped Environment interface. All returned proxies
	 * are linked with the current instance so any changes in this
	 * AlertService instance will be reflected by all proxies
	 * created from it.
	 * @return new AlertService proxy instance.
	 */
	public AlertService getProxy() {
		return (AlertService)Proxy.newProxyInstance(
				AlertService.class.getClassLoader(),
				new Class[]{AlertService.class}, this);
	}

}
