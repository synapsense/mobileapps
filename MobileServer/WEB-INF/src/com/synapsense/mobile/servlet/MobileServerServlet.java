package com.synapsense.mobile.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.googlecode.jsonrpc4j.JsonRpcServer;
import com.synapsense.mobile.dataAccess.Env;
import com.synapsense.mobile.service.impl.AlertManagerImpl;
import com.synapsense.mobile.service.impl.ConfigManagerImpl;
import com.synapsense.mobile.service.impl.ObjectManagerImpl;
import com.synapsense.mobile.service.impl.SystemStatusManagerImpl;
import com.synapsense.mobile.service.impl.UserManagerImpl;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.LocalizationService;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.impl.audit.AuditorSvc;

public class MobileServerServlet extends HttpServlet {
	private static final Logger LOG = Logger.getLogger(MobileServerServlet.class);
	/**
     * 
     */
	private static final long serialVersionUID = -1613536589326361963L;
	private JsonRpcServer server;

	public MobileServerServlet() {

	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LOG.debug("doPost enter");
		
		String servletPath = req.getServletPath();
		// trim '/' symbol
		servletPath = servletPath.substring(1);

		Object manager = null;
		if (servletPath.equals("alertManager")) {
			AlertService alertServ = Env.getAlertingServ(req);
			Environment env = Env.getEnv(req);
			AlertManagerImpl alertManager = new AlertManagerImpl();
			alertManager.setAlertServ(alertServ);
			alertManager.setEnv(env);
			manager=alertManager;
		} else if (servletPath.equals("objectManager")) {
			AlertService alertServ = Env.getAlertingServ(req);
			Environment env = Env.getEnv(req);
			LocalizationService localizationServ = Env.getLocalizationService();
			ObjectManagerImpl objectManager = new ObjectManagerImpl();
			objectManager.setAlertServ(alertServ);
			objectManager.setEnv(env);
			objectManager.setLocalizationServ(localizationServ);
			objectManager.setHttpRequest(req);
			manager=objectManager;
		} else if (servletPath.equals("statusManager")) {
			AuditorSvc auditor = Env.getAuditorSvc();
			Environment env = Env.getEnv(req);
			SystemStatusManagerImpl statusManager = new SystemStatusManagerImpl();
			statusManager.setAuditors(auditor);
			statusManager.setEnv(env);
			manager=statusManager;
		} else if (servletPath.equals("userManager")) {			
			UserManagementService userService = Env.getUserDAO();
			UserManagerImpl userManager = new UserManagerImpl();
			HttpSession session = req.getSession(true);
			userManager.setUserManagement(userService);
			userManager.setSession(session);
			userManager.setHttpRequest(req);
			manager=userManager;
		} else if (servletPath.equals("configManager")){
			ConfigManagerImpl configManager = new ConfigManagerImpl();
			manager = configManager;
		}
		
		server = new JsonRpcServer(manager);
		
		LOG.debug("Service '" + servletPath + "' handle started");
		server.handle(req, resp);
		LOG.debug("Service '" + servletPath + "' handle finished");
		LOG.debug("doPost exit");
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
}
