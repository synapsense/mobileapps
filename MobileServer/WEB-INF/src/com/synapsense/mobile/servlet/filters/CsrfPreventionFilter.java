package com.synapsense.mobile.servlet.filters;

import java.io.IOException;
import java.io.Serializable;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.log4j.Logger;

public class CsrfPreventionFilter extends BaseFilter {
	private static final Logger log = Logger.getLogger(CsrfPreventionFilter.class);

	final static String CSRF_NONCE_CACHE_SESSION_ATTR = "CSRF_NONCE_CACHE";
	final static String CSRF_NONCE_HEADER = "CSRF-Nonce";

	private String randomClass = SecureRandom.class.getName();
	private Random randomSource = null;
	private int nonceCacheSize = 10;

	@Override
	public void destroy() {
	}

	public void setNonceCacheSize(int nonceCacheSize) {
		this.nonceCacheSize = nonceCacheSize;
	}

	public void init(FilterConfig filterConfig) throws ServletException {
		super.init(filterConfig);

		// Set the parameters
		/*
		 * Enumeration<?> paramNames = filterConfig.getInitParameterNames();
		 * 
		 * while (paramNames.hasMoreElements()) { String paramName = (String)
		 * paramNames.nextElement(); try { BeanUtils.setProperty(this,
		 * paramName, filterConfig.getInitParameter(paramName)); } catch
		 * (Exception e) { log.warn("cannot set property:" + paramName, e); } }
		 */

		// Set the parameters
		try {
			String cacheSize = filterConfig.getInitParameter("nonceCacheSize");
			if (cacheSize != null) {
				setNonceCacheSize(Integer.parseInt(cacheSize));
			}
		} catch (Exception e) {
			log.warn("cannot set property nonceCacheSize", e);
		}

		try {
			Class<?> clazz = Class.forName(randomClass);
			randomSource = (Random) clazz.newInstance();
		} catch (ClassNotFoundException e) {
			ServletException se = new ServletException("Invalid random class", e);
			throw se;
		} catch (InstantiationException e) {
			ServletException se = new ServletException("Invalid random class", e);
			throw se;
		} catch (IllegalAccessException e) {
			ServletException se = new ServletException("Invalid random class", e);
			throw se;
		}
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
	        ServletException {

		ServletResponse wResponse = null;

		if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {

			HttpServletRequest req = (HttpServletRequest) request;
			HttpServletResponse res = (HttpServletResponse) response;

			String path = req.getServletPath();
			if (req.getPathInfo() != null) {
				path = path + req.getPathInfo();
			}
			boolean skipNonceCheck = isNotCheckedUrl(path);

			/*
			 * if (Constants.METHOD_GET.equals(req.getMethod())) { String path =
			 * req.getServletPath(); if (req.getPathInfo() != null) { path =
			 * path + req.getPathInfo(); }
			 * 
			 * if (entryPoints.contains(path)) { skipNonceCheck = true; } }
			 */

			@SuppressWarnings("unchecked")
			LruCache<String> nonceCache = (LruCache<String>) req.getSession(true).getAttribute(
			        CSRF_NONCE_CACHE_SESSION_ATTR);

			if (!skipNonceCheck) {
				String previousNonce = req.getHeader(CSRF_NONCE_HEADER);

				if (nonceCache != null && !nonceCache.contains(previousNonce)) {
					res.sendError(HttpServletResponse.SC_FORBIDDEN);
					return;
				}

				if (nonceCache == null) {
					nonceCache = new LruCache<String>(nonceCacheSize);
					req.getSession().setAttribute(CSRF_NONCE_CACHE_SESSION_ATTR, nonceCache);
				}

				String newNonce = generateNonce();

				nonceCache.add(newNonce);

				res.setHeader(CSRF_NONCE_HEADER, newNonce);

				wResponse = new CsrfResponseWrapper(res, newNonce);
			} else {
				wResponse = response;
			}
		} else {
			wResponse = response;
		}

		chain.doFilter(request, wResponse);
	}

	protected String generateNonce() {
		byte random[] = new byte[16];

		// Render the result as a String of hexadecimal digits
		StringBuilder buffer = new StringBuilder();

		randomSource.nextBytes(random);

		for (int j = 0; j < random.length; j++) {
			byte b1 = (byte) ((random[j] & 0xf0) >> 4);
			byte b2 = (byte) (random[j] & 0x0f);
			if (b1 < 10) {
				buffer.append((char) ('0' + b1));
			} else {
				buffer.append((char) ('A' + (b1 - 10)));
			}
			if (b2 < 10) {
				buffer.append((char) ('0' + b2));
			} else {
				buffer.append((char) ('A' + (b2 - 10)));
			}
		}

		return buffer.toString();
	}

	protected static class CsrfResponseWrapper extends HttpServletResponseWrapper {
		// private final String nonce;

		public CsrfResponseWrapper(HttpServletResponse response, String nonce) {
			super(response);
			// this.nonce = nonce;
		}
	}

	protected static class LruCache<T> implements Serializable {
		private static final long serialVersionUID = 1L;

		// Although the internal implementation uses a Map, this cache
		// implementation is only concerned with the keys.
		private final Map<T, T> cache;

		public LruCache(final int cacheSize) {
			cache = Collections.synchronizedMap(new LinkedHashMap<T, T>() {
				private static final long serialVersionUID = 1L;

				@Override
				protected boolean removeEldestEntry(Map.Entry<T, T> eldest) {
					if (size() > cacheSize) {
						return true;
					}
					return false;
				}
			});
		}

		public void add(T key) {
			cache.put(key, null);
		}

		public boolean contains(T key) {
			return cache.containsKey(key);
		}
	}
}
