package com.synapsense.mobile.servlet.filters;

import java.util.Collection;
import java.util.HashSet;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;

/**
 * @author pahunov This filter supports a list of URLSs that don't require
 *         security.
 */
public abstract class BaseFilter implements Filter {
	protected final Collection<String> notCheckedUrls = new HashSet<String>();
	protected final Collection<String> entryPoints = new HashSet<String>();

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// Read it from context (not filter config), so all concrete filter
		// could share it
		setNotCheckedUrls(filterConfig.getServletContext().getInitParameter("notCheckedUrls"));
		setEntryPoints(filterConfig.getServletContext().getInitParameter("entryPoints"));
	}

	public void setNotCheckedUrls(String notCheckedUrls) {
		if (notCheckedUrls == null) {
			return;
		}
		String values[] = notCheckedUrls.split(";");
		for (String value : values) {
			value = value.trim();
			if (!value.isEmpty()) {
				this.notCheckedUrls.add(value);
			}
		}
	}

	protected boolean isNotCheckedUrl(String url) {
		for (String u : notCheckedUrls) {
			if (url.contains(u)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Entry points are URL parts that will not be tested for the presence of a
	 * valid nonce. They are used to provide a way to navigate back to a
	 * protected application after navigating away from it. Entry points will be
	 * limited to HTTP GET requests and should not trigger any security
	 * sensitive actions.
	 * 
	 * @param entryPoints
	 *            Semi separated list of URLs to be configured as entry points.
	 */
	public void setEntryPoints(String entryPoints) {
		if (entryPoints == null) {
			return;
		}

		String values[] = entryPoints.split(";");
		for (String value : values) {
			this.entryPoints.add(value.trim());
		}
	}

	protected boolean isEntryPoint(String url) {
		for (String u : entryPoints) {
			if (url.contains(u)) {
				return true;
			}
		}
		return false;
	}

}
