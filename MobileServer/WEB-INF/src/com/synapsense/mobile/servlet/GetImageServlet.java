package com.synapsense.mobile.servlet;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.BinaryData;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.mobile.dataAccess.Env;

public class GetImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Log LOG = LogFactory.getLog(GetImageServlet.class);

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String id = request.getParameter("id");
		LOG.debug("getting bacckground image for object: " + id);
		TO<?> to = TOFactory.getInstance().loadTO(id);
		String propName = request.getParameter("propName");
		if (propName == null || propName.isEmpty()) {
			propName = "image";
		}
		getFromTO(to, propName, request, response);

	}

	private void getFromTO(TO<?> to, String propName, HttpServletRequest request, HttpServletResponse response)
	        throws ServletException, IOException {
		Object imgSource = null;
		Long imageTimestamp = null;
		ValueTO imageVTO = null;
		byte[] imageRawData = null;

		try {
			imageVTO = Env.getEnv(request).getPropertyValue(to, propName, ValueTO.class);
			imgSource = imageVTO.getValue();

			if (imgSource != null) {
				imageRawData = ((BinaryData) imgSource).getValue();
				imageTimestamp = imageVTO.getTimeStamp();
			}

		} catch (ObjectNotFoundException e) {
			throw new ServletException(e.getLocalizedMessage(), e);
		} catch (PropertyNotFoundException e) {
			throw new ServletException(e.getLocalizedMessage(), e);
		} catch (UnableToConvertPropertyException e) {
			throw new ServletException(e.getLocalizedMessage(), e);
		}
		handleResponse(request, response, imageTimestamp, imageRawData);
	}

	private void handleResponse(HttpServletRequest request, HttpServletResponse response, Long imageTimestamp,
	        byte[] imageRawData) throws IOException {
		response.setHeader("Cache-Control", "public,must-revalidate");
		response.setDateHeader("Expires", 0);
		long isModifiedSince = request.getDateHeader("if-modified-since");
		if (isModifiedSince != -1) {
			if (imageTimestamp <= isModifiedSince) {
				response.sendError(HttpServletResponse.SC_NOT_MODIFIED);
				return;
			}
		}
		response.setDateHeader("Last-Modified", imageTimestamp);

		if (imageRawData != null) {
			ImageInputStream input = ImageIO.createImageInputStream(new ByteArrayInputStream(imageRawData));
			if (ImageIO.getImageReaders(input).hasNext()) {
				String formatName = ImageIO.getImageReaders(input).next().getFormatName();
				String mimeType = getServletContext().getMimeType("." + formatName);
				response.setContentType(mimeType);
			}
			response.setContentLength(imageRawData.length);
			response.getOutputStream().write(imageRawData);
		} else {
			String requestUri = request.getRequestURI();
			response.sendError(HttpServletResponse.SC_NOT_FOUND, requestUri);
		}
	}

}
