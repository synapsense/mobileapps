package com.synapsense.mobile.servlet.listener;

import java.util.Properties;

import javax.naming.Context;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.synapsense.mobile.conf.AdminConfig;
import com.synapsense.mobile.conf.MobileConfig;
import com.synapsense.mobile.conf.types.TypeUtil;
import com.synapsense.mobile.dataAccess.Env;
import com.synapsense.mobile.lang.LangManager;
import com.synapsense.mobile.lang.ResBundleConverter;

public class ApplicationWatch implements ServletContextListener {

	private static String path = "./";
	
	public static String getPath() {
		return path;
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

	}

	@Override
	public void contextInitialized(ServletContextEvent ce) {
		ServletContext servletContext = ce.getServletContext();
		path = servletContext.getRealPath("/");
		Properties p = getConnectionProperties(servletContext);
		Env.loginAsSynapUserIfRemote(); // for development version. Does nothing in production version.
		Env.init(p);
		Env.loginAsSynapUser();
		ResBundleConverter.init();
		LangManager.init(servletContext);
		TypeUtil.getInstance(); //initialize typeUtil
		MobileConfig.init();
		AdminConfig.init();
	}


	private Properties getConnectionProperties(ServletContext c){
		final String URL_PKG_PREFIXES = c.getInitParameter("URL_PKG_PREFIXES");
		Properties environment = new Properties();
        environment.put(Context.URL_PKG_PREFIXES,URL_PKG_PREFIXES);
        return environment;
	}

}
