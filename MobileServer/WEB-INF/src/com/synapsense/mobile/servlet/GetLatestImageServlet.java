package com.synapsense.mobile.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.mobile.dataAccess.Env;

public class GetLatestImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Log LOG = LogFactory.getLog(GetLatestImageServlet.class);
	private static final String HOST_OPEN_TAG = "<li:webserver-ip>";
	private static final String HOST_CLOSE_TAG = "</li:webserver-ip>";
	private static final String PORT_OPEN_TAG = "<li:webserver-port>";
	private static final String PORT_CLOSE_TAG = "</li:webserver-port>";
	private String liHost;
	private String liPort;
	private CloseableHttpClient httpClient;
	
	@Override
	public void init() throws ServletException {
		super.init();
		Env.loginAsSynapUser();
		Collection<TO<?>> TOs = Env.getEnv().getObjects("CONFIGURATION_DATA",
		        new ValueTO[] { new ValueTO("name", "LiveImaging") });
		String liTempHost = null;
		String liTempPort = null;
		if (!TOs.isEmpty()) {
			try {
				String config = Env.getEnv().getPropertyValue(TOs.iterator().next(), "config", String.class);
				liTempHost = config.substring(config.indexOf(HOST_OPEN_TAG) + HOST_OPEN_TAG.length(),
				        config.indexOf(HOST_CLOSE_TAG));
				liTempPort = config.substring(config.indexOf(PORT_OPEN_TAG) + PORT_OPEN_TAG.length(),
				        config.indexOf(PORT_CLOSE_TAG));
			} catch (EnvException e) {
				LOG.warn("Error while getting liveimaging configuration from env", e);
			}
			liHost = liTempHost == null ?  "localhost" : liTempHost;
			liPort = liTempPort == null ?  "9091" : liTempPort;
		}
		httpClient = HttpClients.createDefault();
	}
	@Override
	public void destroy() {
		try {
	        httpClient.close();
        } catch (IOException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
	    super.destroy();
	}
	
	@Override
	public void service(HttpServletRequest request, HttpServletResponse servletResponse) throws IOException, ServletException {
		URI uri = null;
		try {
	        uri = new URIBuilder()
	        .setScheme("http")
	        .setHost(liHost).setPort(Integer.parseInt(liPort))
	        .setPath(request.getServletPath())
	        .setCustomQuery(request.getQueryString())
	        .build();
        } catch (URISyntaxException e) {
        	LOG.warn("URL is mailformed", e);
        }
		
		HttpGet httpget = new HttpGet(uri);
	    CloseableHttpResponse proxyResponse = httpClient.execute(httpget);
	    HttpEntity entity = proxyResponse.getEntity();
	    OutputStream servletOutputStream = servletResponse.getOutputStream();
	    try {
	        servletResponse.setStatus(proxyResponse.getStatusLine().getStatusCode());
	        for (Header header : proxyResponse.getAllHeaders()) {
	            servletResponse.addHeader(header.getName(), header.getValue());
	          }
	        entity.writeTo(servletOutputStream);
	    } finally {
	    	EntityUtils.consume(entity);
	    	servletOutputStream.close();
	    }	
	    
	}
}
