package com.synapsense.mobile.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.synapsense.dto.User;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.mobile.dataAccess.Env;
import com.synapsense.mobile.managers.MobileDeviceManager;
import com.synapsense.mobile.service.UserManager;
import com.synapsense.mobile.user.LoginStatus;
import com.synapsense.mobile.user.UserInfo;
import com.synapsense.mobile.user.UserPreference;
import com.synapsense.mobile.user.UserRole;
import com.synapsense.mobile.utilities.SessionVariableNames;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.util.LocalizationUtils;

public class UserManagerImpl implements UserManager {
	private static final Logger LOG = Logger.getLogger(UserManagerImpl.class);
	UserManagementService userManagement;
	HttpSession session;
	HttpServletRequest httpRequest;

	private static final String MODULE_USER_MANAGEMENT				= "UserManagement";
	private static final String ACTION_USER_LOGIN					= "UserLogin";
	private static final String ACTION_USER_LOGOUT					= "UserLogout";
	private static final String ACTION_USER_LOGIN_FAILED			= "UserLoginFailed";

	public UserManagementService getUserManagement() {
		return userManagement;
	}

	public void setUserManagement(UserManagementService userManagement) {
		this.userManagement = userManagement;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public HttpServletRequest getHttpRequest() {
		return httpRequest;
	}

	public void setHttpRequest(HttpServletRequest httpRequest) {
		this.httpRequest = httpRequest;
	}

	private UserInfo loginUser(User user, String password) {
		if (user != null) {
			String userName = user.getUserName();
			String jSessionId = session.getId();

			com.synapsense.mobile.utilities.UserPreference.updateDefaultPrefs(userName, userManagement);

			session.setAttribute(SessionVariableNames.SESSION_USER, userName);
			session.setAttribute(SessionVariableNames.SESSION_PASSWORD, password);

			UserInfo info = new UserInfo(userName, 0, jSessionId, null);
			return info;
		} else {
			return null;
		}
	}

	@Override
	public UserInfo login(String userName, String password, String deviceId, String deviceName) {
		boolean isLoggedIn = false;
		ActivityRecord record;
		User user = null;
		UserInfo userInfo = new UserInfo(userName, 0, null, null);
		userInfo.setLoginStatus(LoginStatus.FAILED);

		try {
			httpRequest.login(userName, password);
			Env.login(userName, password);
			isLoggedIn = true;
			
			user = userManagement.getUserInformation(userName);
			if(Env.hasPrivilege(user.getUserName(), "ACCESS_WEBCONSOLE")) {
				userInfo = loginUser(user, password);
			} else {
				isLoggedIn = false;
			}
		} catch (Exception e) {
			isLoggedIn = false;
		}
		
		if (isLoggedIn) {			
			MobileDeviceManager.Status res = MobileDeviceManager.checkDeviceStatus(deviceId, httpRequest);

			if (!MobileDeviceManager.Status.OK.equals(res)) {				
				userInfo.setLoginStatus(LoginStatus.FAILED);				
				if (MobileDeviceManager.Status.NOT_ALLOWED.equals(res)) {
					userInfo.setLoginStatus(LoginStatus.DEVICE_NOT_ALLOWED);
				} else if (MobileDeviceManager.Status.NOT_REGISTERED.equals(res)) {
					try {
						MobileDeviceManager.requestRegistration(deviceId, deviceName, userName, httpRequest);
						userInfo.setLoginStatus(LoginStatus.DEVICE_REGISTRATION_REQUESTED);
					} catch (ObjectNotFoundException e){
						LOG.warn(e.getLocalizedMessage() + ". Looks like system was not properly configured. Update is required.");
					}
					catch (Exception e){
						LOG.error(e.getLocalizedMessage(), e);
					}
				}
				logout();
			}
		}
		
		if (userInfo.getLoginStatus() == LoginStatus.OK) {
			record = new ActivityRecord(userName, MODULE_USER_MANAGEMENT, ACTION_USER_LOGIN, LocalizationUtils.getLocalizedString("activity_log_control_user_authenticated_into_the_application"));
		} else {
			record = new ActivityRecord(userName, MODULE_USER_MANAGEMENT, ACTION_USER_LOGIN_FAILED, LocalizationUtils.getLocalizedString("activity_log_control_user_authentication_refused", userName));
		}
		Env.getActivityLogServ().addRecord(record);

		return userInfo;
	}

	@Override
	public UserInfo login(String userName, String password, String deviceId, String deviceName, String[] preferenceNames) {
		UserInfo userInfo = login(userName, password, deviceId, deviceName);
		if (userInfo != null && userInfo.getLoginStatus() == LoginStatus.OK) {
			userInfo.setUserPreferences(getPreferences(preferenceNames));
		}
		return userInfo;
	}

	@Override
	public boolean logout() {
		String username = (String) session.getAttribute(SessionVariableNames.SESSION_USER);
		if (username != null) {
			Env.getActivityLogServ(httpRequest).addRecord(new ActivityRecord(username, MODULE_USER_MANAGEMENT, ACTION_USER_LOGOUT, LocalizationUtils.getLocalizedString("activity_log_control_user_logged_off")));
		}
		session.invalidate();
		return true;

	}

	private int getRole(String role) {
		if (role.equals("admin")) {
			return UserRole.ADMIN;
		} else if (role.equals("engineer")) {
			return UserRole.ENGINEER;
		} else {
			return UserRole.USER;
		}
	}

	@Override
	public UserPreference getPreference(String userPreferenceName) {
		UserPreference[] userPref = getPreferences(new String[] { userPreferenceName });
		if (userPref != null) {
			return userPref[0];
		}

		return null;
	}

	@Override
	public UserPreference[] getPreferences(String[] userPreferenceNames) {
		Collection<com.synapsense.dto.UserPreference> userPreferencesCol = userManagement
		        .getUserPreferences((String) session.getAttribute(SessionVariableNames.SESSION_USER));

		List<UserPreference> result = new ArrayList<UserPreference>();
		Map<String, String> existPrefNames = new HashMap<String, String>();
		for (com.synapsense.dto.UserPreference pref : userPreferencesCol) {
			existPrefNames.put(pref.getPreferenceName(), pref.getValue());
		}

		for (String prefName : userPreferenceNames) {
			if (existPrefNames.containsKey(prefName)) {
				result.add(new UserPreference(prefName, existPrefNames.get(prefName)));
			} else {
				result.add(null);
			}
		}

		return result.toArray(new UserPreference[result.size()]);
	}
}
