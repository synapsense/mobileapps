package com.synapsense.mobile.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.synapsense.dto.AlertFilterBuilder;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.mobile.alert.Alert;
import com.synapsense.mobile.alert.AlertPriority;
import com.synapsense.mobile.alert.AlertStatus;
import com.synapsense.mobile.service.AlertManager;
import com.synapsense.mobile.tree.TreeObject;
import com.synapsense.mobile.utilities.EnvironmentTypes;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;

public class AlertManagerImpl implements AlertManager {

	private static final Logger log = Logger.getLogger(AlertManagerImpl.class);
	AlertService alertServ;
	Environment env;

	public AlertService getAlertServ() {
		return alertServ;
	}

	public void setAlertServ(AlertService alertServ) {
		this.alertServ = alertServ;
	}

	public Environment getEnv() {
		return env;
	}

	public void setEnv(Environment env) {
		this.env = env;
	}

	@Override
	public void dismissAllAlerts(String objId, String comment){
		TO<?> object = TOFactory.getInstance().loadTO(objId);
		alertServ.dismissAlerts(object, true, comment);
	}
	
	@Override
	public boolean acknowledge(int alertId, String comment) {
		try {
			alertServ.acknowledgeAlert(alertId, comment);
			return true;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			return false;
		}
	}

	@Override
	public boolean resolve(int alertId, String comment) {
		try {
			alertServ.resolveAlert(alertId, comment);
			return true;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			return false;
		}
	}

	@Override
	public boolean dismiss(int alertId, String comment) {		
		try {
			alertServ.dismissAlert(alertId, comment);
			return true;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
			return false;
		}
	}

	@Override
	public int getNumActiveAlerts(String objId, Long period, Integer priority, Integer status) {
		return getAlerts(objId, period, priority, status).size();
	}

	@Override
	public Alert[] getActiveAlerts(String objId, Long period, Integer priority, Integer status, Integer start,
	        Integer count) {

		Collection<com.synapsense.dto.Alert> alerts = getAlerts(objId, period, priority, status);
		alerts = startCountFilter(sortCollection(alerts), start, count);

		if (alerts.isEmpty()) {
			return new Alert[0];
		}

		List<Alert> activeAlerts = new ArrayList<Alert>();
		for (com.synapsense.dto.Alert alert : alerts) {
			activeAlerts.add(createMobAlert(alert));
		}

		return activeAlerts.toArray(new Alert[activeAlerts.size()]);
	}

	private Collection<com.synapsense.dto.Alert> getAlerts(String objId, Long period, Integer priority, Integer status) {
		AlertFilterBuilder filterBuilder = createAlertFilterBuilder(objId, period, status);
		Collection<com.synapsense.dto.Alert> alerts = alertServ.getAlerts(filterBuilder.build());
		// getting null object's alerts separately
//		if (objId == null) {
//			filterBuilder.onSingleHostOnly((TOFactory.EMPTY_TO));
//			alerts.addAll(alertServ.getAlerts(filterBuilder.build()));
//		}
		alerts = priorityFilter(alerts, priority);
		return alerts;
	}

	private Collection<com.synapsense.dto.Alert> sortCollection(Collection<com.synapsense.dto.Alert> alerts) {
		final Comparator<com.synapsense.dto.Alert> comparatorAlert = new Comparator<com.synapsense.dto.Alert>() {
			public int compare(com.synapsense.dto.Alert a1, com.synapsense.dto.Alert a2) {
				return a2.getAlertTime().compareTo(a1.getAlertTime());
			}
		};
		Collections.sort(((List<com.synapsense.dto.Alert>) alerts), comparatorAlert);
		return alerts;

	}

	private Collection<com.synapsense.dto.Alert> priorityFilter(Collection<com.synapsense.dto.Alert> alerts,
	        Integer priority) {
		if (priority == null || priority == 0xF) {
			return alerts;
		}
		Collection<com.synapsense.dto.Alert> originAerts = new LinkedList<com.synapsense.dto.Alert>();
		originAerts.addAll(alerts);
		for (com.synapsense.dto.Alert alert : originAerts) {
			String priorityOfAlert = alert.getPriority();
			Integer integerPriority = null;
			if (priorityOfAlert.equals("INFORMATIONAL")) {
				integerPriority = AlertPriority.INFORMATIONAL;
				if (integerPriority != (priority & AlertPriority.INFORMATIONAL)) {
					alerts.remove(alert);
				}
			}
			if (priorityOfAlert.equals("MINOR")) {
				integerPriority = AlertPriority.MINOR;
				if (integerPriority != (priority & AlertPriority.MINOR)) {
					alerts.remove(alert);
				}
			}
			if (priorityOfAlert.equals("MAJOR")) {
				integerPriority = AlertPriority.MAJOR;
				if (integerPriority != (priority & AlertPriority.MAJOR)) {
					alerts.remove(alert);
				}
			}
			if (priorityOfAlert.equals("CRITICAL")) {
				integerPriority = AlertPriority.CRITICAL;
				if (integerPriority != (priority & AlertPriority.CRITICAL)) {
					alerts.remove(alert);
				}
			}
		}
		return alerts;
	}

	private AlertFilterBuilder createAlertFilterBuilder(String objId, Long period, Integer status) {
		AlertFilterBuilder filterBuilder = new AlertFilterBuilder();

		// always get all hierarchy
		filterBuilder.propagate(true);

		// check status
		if (status == null) {
			filterBuilder.active();
		} else {
			if ((status & AlertStatus.OPENED) == AlertStatus.OPENED) {
				filterBuilder.opened();
			}
			if ((status & AlertStatus.ACKNOWLEDGED) == AlertStatus.ACKNOWLEDGED) {
				filterBuilder.acknowledged();
			}
			if ((status & AlertStatus.DISMISSED) == AlertStatus.DISMISSED) {
				filterBuilder.dismissed();
			}
			if ((status & AlertStatus.RESOLVED) == AlertStatus.RESOLVED) {
				filterBuilder.resolved();
			}
		}

		// check objId
		if (objId != null) {
			TO<?> host = TOFactory.getInstance().loadTO(objId);
			filterBuilder.onHost(host);
		} else {
			Collection<TO<?>> root = env.getObjectsByType(EnvironmentTypes.TYPE_ROOT);
			filterBuilder.onHost(root.iterator().next());
			filterBuilder.onHost(TOFactory.EMPTY_TO);
		}

		// check period
		Date end;
		Date begin;
		if (period != null && period != 0l) {
			Long currentTime = System.currentTimeMillis();
			end = new Date(currentTime);
			begin = new Date(currentTime - period);
		} else {
			begin = new Date(0);
			end = new Date();
		}
		filterBuilder.between(begin, end);

		return filterBuilder;
	}

	private Alert createMobAlert(com.synapsense.dto.Alert alert) {
		// trim consoleAlert name
		String name = alert.getName();
		if (name.startsWith("ConsoleAlert_")) {
			name = name.substring("ConsoleAlert_".length());
		}

		// Prepare parameters for alertObject
		TO<?> host = alert.getHostTO();
		String hostIdStr = TOFactory.getInstance().saveTO(host);
		String objectName = "";
		String objectLocation = null;
		int objectNumAlerts = alertServ.getNumActiveAlerts(host, true);
		if (!TOFactory.EMPTY_TO.equals(host)) {
			try {				
				objectName = env.getPropertyValue(host, "name", String.class);
			} catch (ObjectNotFoundException e) {
				log.debug("Object not found", e);
			} catch (PropertyNotFoundException e) {
				log.debug("Property not found", e);
			} catch (UnableToConvertPropertyException e) {
				log.debug("Unable to convert property", e);
			}			
			try {
				objectLocation = env.getPropertyValue(host, "location", String.class);
			} catch(Exception e){
				log.debug(e.getLocalizedMessage(), e);
			}
		} else {
			hostIdStr = "-1";
		}

		TreeObject object = new TreeObject(hostIdStr, host.getTypeName(), objectNumAlerts);

		// Prepare parameters for alert
		Integer alertstatus = null;
		switch (alert.getStatus().ordinal()) {
		case 0:
			alertstatus = AlertStatus.OPENED;
			break;
		case 1:
			alertstatus = AlertStatus.ACKNOWLEDGED;
			break;
		case 2:
			alertstatus = AlertStatus.DISMISSED;
			break;
		case 3:
			alertstatus = AlertStatus.RESOLVED;
			break;

		}

		Integer currentpriority = null;
		String alpr = alert.getPriority();
		if (alpr.equals("INFORMATIONAL")) {
			currentpriority = AlertPriority.INFORMATIONAL;
		}
		if (alpr.equals("MINOR")) {
			currentpriority = AlertPriority.MINOR;
		}
		if (alpr.equals("MAJOR")) {
			currentpriority = AlertPriority.MAJOR;
		}
		if (alpr.equals("CRITICAL")) {
			currentpriority = AlertPriority.CRITICAL;
		}

		String lastActionUser = "";
		Long lastActionTime = null;
		if (!(alertstatus == 0)) {
			lastActionUser = alert.getUser();
			if (alert.getAcknowledgementTime() != null) {
				lastActionTime = alert.getAcknowledgementTime().getTime();
			}
		}

		return new Alert(alert.getAlertId(), name, object, alertstatus, currentpriority, lastActionUser,
		        lastActionTime, alert.getAlertTime(), alert.getTypeDescr(), alert.getFullMessage(),
		        alert.getAcknowledgement(), objectName, objectLocation);
	}

	private Collection<com.synapsense.dto.Alert> startCountFilter(Collection<com.synapsense.dto.Alert> alerts,
	        Integer start, Integer count) {
		int end;
		if (start != null) {
			if (start >= alerts.size()) {
				return new ArrayList<com.synapsense.dto.Alert>();
			}
			if (start < 0) {
				start = 0;
			}
		} else {
			start = 0;
		}

		if (count != null) {
			if (count > alerts.size()) {
				count = alerts.size();
			}
			if (count <= 0) {
				return new ArrayList<com.synapsense.dto.Alert>();
			}
			end = start + count;
			if (end > alerts.size()) {
				end = alerts.size();
			}
		} else {
			end = alerts.size();
		}

		return ((List<com.synapsense.dto.Alert>) alerts).subList(start, end);
	}

}
