package com.synapsense.mobile.service.impl;

import com.synapsense.mobile.conf.AdminConfig;
import com.synapsense.mobile.service.ConfigManager;

public class ConfigManagerImpl implements ConfigManager {

	@Override
	public String[] getFloorplanTypes() {
		return AdminConfig.getAllowedFloorplanTypes();
	}

	@Override
	public String[] getUIPrivileges() {
		return AdminConfig.getUiPrivileges();
	}
}
