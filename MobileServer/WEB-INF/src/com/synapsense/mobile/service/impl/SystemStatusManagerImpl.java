package com.synapsense.mobile.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.mobile.service.SystemStatusManager;
import com.synapsense.mobile.status.ComponentStatus;
import com.synapsense.mobile.status.Components;
import com.synapsense.mobile.status.Status;
import com.synapsense.mobile.tree.ObjectStatus;
import com.synapsense.mobile.utilities.EnvironmentTypes;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.audit.Auditor;
import com.synapsense.service.impl.audit.AuditorSvc;

public class SystemStatusManagerImpl implements SystemStatusManager {
	private static final Logger log = Logger.getLogger(SystemStatusManagerImpl.class);
	private static final long CP_UPDATE_PERIOD = 2 * 60 * 1000;
	private static final String STATUS_OK = "OK";
	private static final String STATUS_UNDEFINED = "Undefined";
	AuditorSvc auditors;
	Environment env;

	public Environment getEnv() {
		return env;
	}

	public void setEnv(Environment env) {
		this.env = env;
	}

	public AuditorSvc getAuditors() {
		return auditors;
	}

	public void setAuditors(AuditorSvc auditors) {
		this.auditors = auditors;
	}

	@Override
	public ComponentStatus[] getEnvironmentalServerState() {
		Map<String, Auditor> mapauditor = auditors.getAuditors();
		List<ComponentStatus> componentStatus = new ArrayList<ComponentStatus>();

		ComponentStatus rmiStatus;
		Auditor auditor = mapauditor.get("com.synapsense.impl.auditsvc.auditor.RmiServiceAuditor");
		if (auditor == null) {
			rmiStatus = new ComponentStatus(Components.DEVICE_MANAGER, Status.UNKNOWN);
		} else {
			String status = auditor.getLastStatus();
			rmiStatus = new ComponentStatus(Components.DEVICE_MANAGER, Status.STOPPED);
			if (status.equalsIgnoreCase(STATUS_OK)) {
				rmiStatus.setStatus(Status.RUNNING);
			} else if (status.equalsIgnoreCase(STATUS_UNDEFINED)) {
				rmiStatus.setStatus(Status.UNKNOWN);
			}
			String msg = STATUS_OK.equals(status) ? "" : status;
			rmiStatus.setMessage(msg);
		}
		componentStatus.add(rmiStatus);

		ComponentStatus synapVizStatus;
		auditor = mapauditor.get("com.synapsense.impl.auditsvc.auditor.LiveImagingAuditor");
		if (auditor == null) {
			synapVizStatus = new ComponentStatus(Components.IMAGING_SERVER, Status.UNKNOWN);
		} else {
			String status = auditor.getLastStatus();
			synapVizStatus = new ComponentStatus(Components.IMAGING_SERVER, Status.STOPPED);
			if (status.equalsIgnoreCase(STATUS_OK)) {
				synapVizStatus.setStatus(Status.RUNNING);
			} else if (status.equalsIgnoreCase(STATUS_UNDEFINED)) {
				synapVizStatus.setStatus(Status.UNKNOWN);
			}
			String msg = STATUS_OK.equals(status) ? "" : status;
			synapVizStatus.setMessage(msg);
		}
		componentStatus.add(synapVizStatus);

		componentStatus.add(new ComponentStatus(Components.ENVIRONMENTAL_SERVER, Status.RUNNING));

		ComponentStatus controlStatus = getActiveControlStatus(mapauditor);
		if (controlStatus != null) {
			componentStatus.add(controlStatus);
		}
		return componentStatus.toArray(new ComponentStatus[0]);
	}

	@Override
	public int getNumWarnings() {
		int totalWarnings = 0;
		try {
			Collection<TO<?>> allNodes = env.getObjectsByType("WSNNODE");
			if (!allNodes.isEmpty()) {
				String[] nodesProps = { "status" };
				Collection<CollectionTO> props = env.getPropertyValue(allNodes, nodesProps);
				for (CollectionTO p : props) {
					ValueTO v = p.getPropValues().get(0);
					if (v != null) {
						Integer status = (Integer) v.getValue();
						if (status != null && (status == ObjectStatus.NOT_RECEIVING_DATA)) {
							totalWarnings++;
						}
					}
				}
			}

			Collection<TO<?>> allGateways = env.getObjectsByType("WSNGATEWAY");
			if (!allGateways.isEmpty()) {
				String[] gatewayProps = { "status" };
				Collection<CollectionTO> gwProps = env.getPropertyValue(allGateways, gatewayProps);
				for (CollectionTO p : gwProps) {
					ValueTO v = p.getPropValues().get(0);
					if (v != null) {
						Integer status = (Integer) v.getValue();
						if (status != null) {
							totalWarnings += ((status & ObjectStatus.GATEWAY_STATUS_ALIVE) + (status & ObjectStatus.GATEWAY_STATUS_SYNC)) < 2 ? 1
							        : 0;
						}
					}
				}
			}
			return totalWarnings;
		} catch (Exception e) {
			log.error("Exeption during warning number retrieving.", e);
			return 0;
		}

	}

	private ComponentStatus getActiveControlStatus(Map<String, Auditor> auditorsMap) {
		ComponentStatus result = null;

		Auditor auditor = auditorsMap.get("com.synapsense.impl.auditsvc.auditor.ControlProcessAuditor");
		if (auditor != null) {
			Collection<TO<?>> controllers = env.getObjectsByType(EnvironmentTypes.TYPE_CONTROLLER_STATUS);
			if (controllers.size() > 1) {
				result = new ComponentStatus(Components.CONTROL_PROCESS, Status.PARTIAL);

				result.setMessage(auditor.getLastStatus());

				int numControllersOffline = 0;
				int index = 0;
				// check statuses of all 'controller status' objects
				Collection<CollectionTO> properties = env.getPropertyValue(controllers,
				        new String[] { "name", "active" });
				for (CollectionTO cto : properties) {
					ValueTO vto = cto.getSinglePropValue("active");
					long lastUpdate = vto.getTimeStamp();
					if (new Date().getTime() - lastUpdate < CP_UPDATE_PERIOD) {
					} else {
						numControllersOffline++;
					}
					index++;
				}
				if (numControllersOffline == 0) {
					result.setStatus(Status.RUNNING);
				} else if (numControllersOffline == index) {
					result.setStatus(Status.STOPPED);
				}
			}
		}
		return result;
	}

}
