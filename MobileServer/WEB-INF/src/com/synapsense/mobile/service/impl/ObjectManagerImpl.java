package com.synapsense.mobile.service.impl;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.synapsense.dto.BinaryData;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.mobile.conf.MobileConfig;
import com.synapsense.mobile.interaction.ObjectRequest;
import com.synapsense.mobile.interaction.ResponseElement;
import com.synapsense.mobile.lang.LangManager;
import com.synapsense.mobile.requestprocessing.RequestProcessor;
import com.synapsense.mobile.service.ObjectManager;
import com.synapsense.mobile.tree.LiveImagingType;
import com.synapsense.mobile.tree.TreeObject;
import com.synapsense.mobile.utilities.EnvironmentTypes;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.LocalizationService;
import com.synapsense.service.impl.dao.to.BinaryDataImpl;

public class ObjectManagerImpl implements ObjectManager {
	private static final Logger log = Logger.getLogger(ObjectManagerImpl.class);
	Environment env;
	AlertService alertServ;
	LocalizationService localizationServ;
	HttpServletRequest httpRequest;

	RequestProcessor requestProcessor = new RequestProcessor();

	public HttpServletRequest getHttpRequest() {
		return httpRequest;
	}

	public void setHttpRequest(HttpServletRequest httpRequest) {
		this.httpRequest = httpRequest;
	}

	public Environment getEnv() {
		return env;
	}

	public void setEnv(Environment env) {
		this.env = env;
	}

	public AlertService getAlertServ() {
		return alertServ;
	}

	public void setAlertServ(AlertService alertServ) {
		this.alertServ = alertServ;
	}

	public LocalizationService getLocalizationService() {
		return localizationServ;
	}

	public void setLocalizationServ(LocalizationService localizationServ) {
		this.localizationServ = localizationServ;
	}

	@Override
	public TreeObject[] getObjectTree() {
		Collection<TO<?>> rootcollections = env.getObjectsByType(EnvironmentTypes.TYPE_ROOT);
		TO<?> root = rootcollections.iterator().next();
		List<TreeObject> objectList = new ArrayList<TreeObject>();
		try {
			Collection<TO<?>> datacenters = env.getChildren(root, EnvironmentTypes.TYPE_DC);
			for (TO<?> datacenter : datacenters) {
				String id = "DC:" + datacenter.getID().toString();
				int numAlerts = alertServ.getNumActiveAlerts(datacenter, true);
				TreeObject dc = new TreeObject(id, datacenter.getTypeName(), numAlerts);
				objectList.add(dc);
			}
			objectList = sortCollection(objectList);
		} catch (ObjectNotFoundException e) {
			log.debug("Object not found", e);
		}
		for (TreeObject treeObj : objectList) {
			treeObj.setType(EnvironmentTypes.TYPE_DC);
		}

		return objectList.toArray(new TreeObject[objectList.size()]);
	}

	@Override
	public TreeObject[] getChildren(String objectId, String[] types, boolean fullSubtree) {

		if (objectId == null) {
			return null;
		}

		List<String> typesList = null;
		if (types != null) {
			typesList = Arrays.asList(types);
		}

		TO<?> objTo = TOFactory.getInstance().loadTO(objectId);
		List<TreeObject> childrenObj = new ArrayList<TreeObject>();
		Collection<TO<?>> children = new LinkedList<TO<?>>();

		if (fullSubtree && typesList != null && !typesList.isEmpty()) {
			for (String type : typesList) {
				children.addAll(env.getRelatedObjects(objTo, type, true));
			}
		} else {
			try {
				children.addAll(env.getChildren(objTo));
			} catch (ObjectNotFoundException e) {
				log.warn("Object not found", e);
			}
		}

		for (TO<?> to : children) {
			if (typesList != null) {
				if (!typesList.contains(to.getTypeName())) {
					continue;
				}
			}
			String id = to.getTypeName() + ":" + to.getID().toString();
			childrenObj.add(new TreeObject(id, to.getTypeName(), alertServ.getNumActiveAlerts(to, true)));
		}
		if (!childrenObj.isEmpty()) {
			childrenObj = sortCollection(childrenObj);
			return childrenObj.toArray(new TreeObject[childrenObj.size()]);
		}
		return new TreeObject[0];
	}

	@Override
	public Object getPropertyValue(String objectId, String propertyName) {

		Object obj = null;
		try {
			obj = env.getPropertyValue(TOFactory.getInstance().loadTO(objectId), propertyName, Object.class);
			if (obj instanceof BinaryData) {
				byte[] image = ((BinaryDataImpl) obj).getValue();
				return DatatypeConverter.printBase64Binary(image);
			}
			return obj;
		} catch (ObjectNotFoundException e) {
			log.warn("Object not found", e);
		} catch (PropertyNotFoundException e) {
			log.warn("Property not found", e);
		} catch (UnableToConvertPropertyException e) {
			log.warn("Unable to convert property", e);
		}

		return null;
	}

	@Override
	public Object[] getPropertyValues(String objectId, String[] propertyNames) {
		Collection<TO<?>> cto = new LinkedList<TO<?>>();
		cto.add(TOFactory.getInstance().loadTO(objectId));
		Collection<CollectionTO> properties = env.getPropertyValue(cto, propertyNames);

		if (properties.isEmpty()) {
			return new Object[propertyNames.length];
		}
		CollectionTO to = properties.iterator().next();
		LinkedList<Object> objects = new LinkedList<Object>();
		for (String s : propertyNames) {
			List<ValueTO> prop = to.getPropValue(s);
			if (prop != null) {
				objects.add(to.getPropValue(s).iterator().next().getValue());
			} else {
				objects.add(null);
			}

		}

		return objects.toArray();
	}

	private List<TreeObject> sortCollection(List<TreeObject> treeobject) {
		final Comparator<TreeObject> comparator = new Comparator<TreeObject>() {
			public int compare(TreeObject a1, TreeObject a2) {
				return a2.getType().compareTo(a1.getType());
			}
		};
		Collections.sort(treeobject, comparator);
		Collections.reverse(treeobject);
		return treeobject;

	}

	@Override
	public String getFloorplan(String objectId, int dataClassId, int layer) {
		URL url;
		try {
			url = new URL(httpRequest.getRequestURL().toString());
			return url.getProtocol() + "://" + url.getHost() + ":" + url.getPort() + "/mobile/getLatestImage?objId="
			        + objectId + "&dataclass=" + dataClassId + "&layer=" + layer;
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public LiveImagingType[] getLiveImagingTypes() {
		LiveImagingType[] returnTypes = MobileConfig.getLiveImangingTypes();
		for (int i = 0; i < returnTypes.length; i++) {
			String localizedName = LangManager.getInstance().getLocaleString(
			        LangManager.getCurrentLocale(this.httpRequest), returnTypes[i].getName());
			returnTypes[i].setName(localizedName);
		}
		return returnTypes;
	}

	private String getLiveImagingConfString() {
		Collection<TO<?>> TOs = env.getObjects("CONFIGURATION_DATA",
		        new ValueTO[] { new ValueTO("name", "LiveImaging") });
		if (!TOs.isEmpty()) {
			try {
				String config = env.getPropertyValue(TOs.iterator().next(), "config", String.class);
				return config;
			} catch (EnvException e) {
				log.warn("Error while getting liveimaging configuration from env", e);
			}
		}

		return null;
	}

	private Document convertStringToXML(String str) {
		InputStream inputStream = null;
		try {
			inputStream = new ByteArrayInputStream(str.getBytes("UTF-8"));
		} catch (IOException e) {
			log.warn("Invalid input string", e);
		}

		return convertInputStreamToXML(inputStream);
	}

	private Document convertInputStreamToXML(InputStream inputStream) {
		Document conf = null;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			DocumentBuilder builder = factory.newDocumentBuilder();
			builder.setEntityResolver(new EntityResolver() {

				@Override
				public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
					return new InputSource(new StringReader(""));
				}
			});

			conf = builder.parse(inputStream);
		} catch (IOException e) {
			log.warn("Invalid input stream", e);
		} catch (SAXException e) {
			log.warn("SAX error while converting string to XML", e);
		} catch (ParserConfigurationException e) {
			log.warn("Can't parse input string", e);
		}

		return conf;
	}

	private String getRequestedHost(HttpServletRequest request) {
		try {
			URL url = new URL(request.getRequestURL().toString());
			return url.getHost();
		} catch (MalformedURLException e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return null;
	}

	private String sendResponseToLIServer(URL url) {
		HttpURLConnection conn = null;
		BufferedReader rd = null;
		StringBuilder result = null;
		try {
			conn = (HttpURLConnection) url.openConnection();
			rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			result = new StringBuilder();
			if (conn.getResponseCode() == HttpServletResponse.SC_OK) {
				while ((line = rd.readLine()) != null) {
					result.append(line);
				}
			}

		} catch (IOException e) {
			log.warn("Could not get response from LiveImaging server");
			return null;
		} finally {
			try {
				if (rd != null) {
					rd.close();
				}
				if (conn != null) {
					conn.disconnect();
				}
			} catch (Exception e) {
				log.error("Could not close connection with LI server", e);
			}
		}
		return result.toString();
	}

	@Override
	public boolean isLiveImagingAvailable(String objectId) {
		String confStr = getLiveImagingConfString();
		if (confStr == null) {
			log.warn("Configuration not found");
			return false;
		}

		Document conf = convertStringToXML(confStr);
		if (conf == null) {
			log.warn("Unable convert string to XML");
			return false;
		}

		NodeList hostsList = conf.getElementsByTagName("li:webserver-ip");
		NodeList portsList = conf.getElementsByTagName("li:webserver-port");

		String host = (hostsList.getLength() > 0) ? hostsList.item(0).getTextContent() : "localhost";
		if ("localhost".equals(host)) {
			host = getRequestedHost(httpRequest);
		}
		String port = (portsList.getLength() > 0) ? portsList.item(0).getTextContent() : "9091";
		URL url = null;
		try {
			url = new URL("http://" + host + ":" + port + "/hasImages?objId=" + objectId);
		} catch (MalformedURLException e) {
			log.warn("URL is mailformed", e);
			return false;
		}
		String result;

		result = sendResponseToLIServer(url);

		if (result != null) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				JsonNode json_res = mapper.readTree(result);
				JsonNode bool_res = json_res.get("result");
				return bool_res.asBoolean();
			} catch (Exception e) {
				log.warn("Could not parse JSON response from LI server", e);
				return false;
			}

		}
		return false;
	}

	@Override
	public Object[][] getManyObjectsPropertyValues(String[] objectIds, String[] propertyNames) {
		int i = 0;
		Object[][] objects = new Object[objectIds.length][propertyNames.length];
		for (String objId : objectIds) {
			int j = 0;
			for (Object propObj : getPropertyValues(objId, propertyNames)) {
				objects[i][j] = propObj;
				j++;
			}
			i++;
		}
		return objects;
	}

	@Override
	public ResponseElement getObjectsData(ObjectRequest[] objRequests) {
		return requestProcessor.getObjectsData(objRequests, env, httpRequest);
	}

	@Override
	public ResponseElement getObjectData(ObjectRequest objRequests) {
		return requestProcessor.getObjectData(objRequests, env, httpRequest);
	}

	@Override
	public Object[] getFloorPlanData(String objectId, String[] types) {

		List<Object> res = new LinkedList<Object>();
		String[] propNames = new String[] { "status", "numAlerts", "x", "y" };
		String[] rackPropNames = new String[] { "status", "numAlerts", "x", "y", "door" };

		TO<?> parent = TOFactory.getInstance().loadTO(objectId);
		for (String type : types) {
			// need special handling for rack's door sensor
			String[] props = "RACK".equals(type) || "DOOR".equals(type) ? rackPropNames : propNames;
			Collection<TO<?>> tos = env.getRelatedObjects(parent, type, true);
			Collection<CollectionTO> colColTo = env.getPropertyValue(tos, props);
			for (CollectionTO colTo : colColTo) {
				// 1 for id
				Object[] objs = new Object[props.length + 1];
				int i = 0;
				objs[i++] = TOFactory.getInstance().saveTO(colTo.getObjId());
				for (String propName : props) {
					Object value;
					if ("door".equals(propName)) {
						value = 0;
						TO<?> door = (TO<?>) colTo.getSinglePropValue(propName).getValue();
						if (door != null) {
							try {
								value = env.getPropertyValue(door, "lastValue", Double.class).intValue();
							} catch (ObjectNotFoundException e) {
								log.error("Object not found", e);
							} catch (PropertyNotFoundException e) {
								log.error("Property not found", e);
							} catch (UnableToConvertPropertyException e) {
								log.error("Unable to convert property", e);
							}
						}
					} else {
						value = colTo.getSinglePropValue(propName).getValue();
					}
					objs[i++] = value;
				}
				res.add(objs);

			}
		}

		return res.toArray(new Object[res.size()]);
	}
}
