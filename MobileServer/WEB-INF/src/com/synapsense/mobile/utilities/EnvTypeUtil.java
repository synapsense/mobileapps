package com.synapsense.mobile.utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.service.Environment;

public class EnvTypeUtil {
	static private final String TYPE_WILDCARD = "*";
	private static Map<String, Map<String, Class<?>>> invisiblePropsMap = new HashMap<String, Map<String, Class<?>>>();
	private static Map<String, Map<String, Class<?>>> propertyTypeMap = new HashMap<String, Map<String, Class<?>>>();
	private static HashMap<String, ArrayList<String>> histProps = new HashMap<String, ArrayList<String>>();

	static {
		Map<String, Class<?>> commonPropsMAp = new HashMap<String, Class<?>>();
		commonPropsMAp.put("numAlerts", Integer.class);
		commonPropsMAp.put("objectId", Integer.class);
		invisiblePropsMap.put(EnvTypeUtil.TYPE_WILDCARD, commonPropsMAp);
	}

	public static Class<?> getPropertyClass(String propName, String typeName, Environment environment)
	        throws PropertyNotFoundException, ObjectNotFoundException {
		Map<String, Class<?>> objMap = invisiblePropsMap.get(EnvTypeUtil.TYPE_WILDCARD);
		if (objMap != null) {
			Class<?> c = objMap.get(propName);
			if (c != null) {
				return c;
			}
		}

		Map<String, Class<?>> types = new HashMap<String, Class<?>>();
		if (!propertyTypeMap.containsKey(typeName)) {
			propertyTypeMap.put(typeName, types);
		} else {
			types = propertyTypeMap.get(typeName);
		}
		if (!types.containsKey(propName)) {
			PropertyDescr descr = environment.getPropertyDescriptor(typeName, propName);
			Class<?> clazz = descr.getType();
			types.put(propName, clazz);
			return clazz;
		} else {
			return types.get(propName);
		}
	}

	@SuppressWarnings("unchecked")
	static public ArrayList<String> getHistoricalPropertyNames(String objTypeName, Environment env) {
		ArrayList<String> propNames;

		if (EnvTypeUtil.histProps.containsKey(objTypeName)) {
			propNames = EnvTypeUtil.histProps.get(objTypeName);
		} else {
			propNames = new ArrayList<String>();
			ObjectType[] obArr = env.getObjectTypes(new String[] { objTypeName });
			if (obArr.length == 0) {
				return propNames;
			}
			// Get property descriptors of type
			Set<PropertyDescr> propDescrSet = obArr[0].getPropertyDescriptors();

			// Get historic property names
			for (PropertyDescr pDesc : propDescrSet) {
				// Currently we chart only historical double properties
				if (pDesc.isHistorical() && pDesc.getType().isAssignableFrom(Double.class)) {
					propNames.add(pDesc.getName());
				}
			}
			EnvTypeUtil.histProps.put(objTypeName, propNames);
		}
		return (ArrayList<String>) propNames.clone();
	}

	public static boolean isHistorical(String type, String propName, Environment environment) {
		List<String> propNames = EnvTypeUtil.getHistoricalPropertyNames(type, environment);
		return propNames.contains(propName);
	}
}
