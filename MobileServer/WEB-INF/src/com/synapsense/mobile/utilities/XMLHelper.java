package com.synapsense.mobile.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Writer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.ProcessingInstruction;
import org.xml.sax.SAXException;

public class XMLHelper {
	private static final Logger LOG = Logger.getLogger(XMLHelper.class);

	public static Document createNewXmlDocument(String rootNodeName) throws ParserConfigurationException {
		// Create the XML Document
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(true);
		DocumentBuilder docBuilder = factory.newDocumentBuilder();
		Document xmlDoc = docBuilder.newDocument();

		Node rootNode = xmlDoc.createElement(rootNodeName);
		xmlDoc.appendChild(rootNode);

		return xmlDoc;
	}

	/**
	 * Writes XML document directly to the writer
	 * 
	 * @param xmlDoc
	 * @param writer
	 * @throws TransformerException
	 */
	public static void writeXml(Document xmlDoc, Writer writer) throws TransformerException {
		TransformerFactory transfac = TransformerFactory.newInstance();
		Transformer trans = transfac.newTransformer();
		trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		trans.setOutputProperty(OutputKeys.INDENT, "yes");

		StreamResult result = new StreamResult(writer);
		DOMSource source = new DOMSource(xmlDoc);

		trans.transform(source, result);
	}

	/**
	 * Return org.w3c.dom.Node object. If value is null - it is considered as
	 * empty string ("") to avoid exceptions while XML transformations.
	 * 
	 * @param doc
	 * @param name
	 * @param value
	 * @return
	 */

	public static Node createXmlTextNode(Document doc, String name, String value) {
		Node root = doc.createElement(name);
		if (value == null) {
			value = "";
		}

		if (value.indexOf("\n") == -1) {
			root.appendChild(doc.createTextNode(value));
		} else {
			// workaround: wrap text as CDATA; otherwise "\n" will became "\r\n"
			// after 'XML document->string' transformation
			addTextValueAsCDATA(doc, root, value);
		}

		return root;
	}

	private static void addTextValueAsCDATA(Document doc, Node xmlNode, String value) {
		// disable output escaping
		ProcessingInstruction pi = doc.createProcessingInstruction(Result.PI_DISABLE_OUTPUT_ESCAPING, "");
		xmlNode.appendChild(pi);

		// add CDATA section
		xmlNode.appendChild(doc.createCDATASection(value));

		// enable output escaping
		pi = doc.createProcessingInstruction(Result.PI_ENABLE_OUTPUT_ESCAPING, "");
		xmlNode.appendChild(pi);
	}

	public static Document loadXmlFromFile(String fileAbsolutePath) throws IOException {
		Document xmlDoc = null;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			xmlDoc = builder.parse(new FileInputStream(fileAbsolutePath));
		} catch (ParserConfigurationException pcE) {
			LOG.error(pcE.getMessage());
		} catch (SAXException sE) {
			LOG.error(sE.getMessage());
		}

		return xmlDoc;
	}
	
	/**
	 * Gets standard document builder
	 * @return
	 * @throws ParserConfigurationException
	 */
	public static DocumentBuilder getStandardDocBuilder() throws ParserConfigurationException{
		//Get XML-configuration document				
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setIgnoringComments(true);
		factory.setCoalescing(true); // Convert CDATA to Text nodes
		factory.setNamespaceAware(false); // No namespaces: this is default
		factory.setValidating(false); // Don't validate DTD: also default
	    DocumentBuilder docBuilder = factory.newDocumentBuilder();	    
	    return docBuilder;
	}

}
