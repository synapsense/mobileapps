package com.synapsense.mobile.utilities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;


import com.synapsense.dto.Preference;
import com.synapsense.exception.UserManagementException;
import com.synapsense.service.UserManagementService;

public class UserPreference {
	
	public static final String TIME_ZONE = "TimeZone"; 

	private static final Logger LOG = Logger.getLogger(UserPreference.class);

	//prefName->setting name in standalone.xml
	private static Map<String, String> defaultPrefs = new HashMap<String, String>();
	private static List<String> notStringTypePrefs = new ArrayList<String>(5);
	
	static {
		defaultPrefs.put("SavedLanguage", "com.synapsense.defaultLanguage");
		defaultPrefs.put("UnitsSystem", "com.synapsense.defaultUnitSystem");
		defaultPrefs.put("DateFormat", "com.synapsense.defaultDateFormat");
		defaultPrefs.put("DecimalSeparator", "com.synapsense.defaultDecimalSeparator");
		defaultPrefs.put("DecimalFormat", "com.synapsense.defaultDecimalFormat");
		
		notStringTypePrefs.add("DecimalFormat");
	}
	
	public static void set(String userName, String prefName, String prefValue, UserManagementService userDAO) {
		Collection<Preference> prefTypes = userDAO.getAllPreferences();
		//check if preference exists
		boolean nameExists = false;
		for (Preference prefType : prefTypes) {
			if (prefType.getPreferenceName().equals(prefName)) {
				nameExists = true;
				break;
			}
		}
		//if preference not exists create new one
		if(!nameExists) {
			Preference prefType = new Preference(prefName, "");
			userDAO.addPreference(prefType);
		}
		
        try {
			userDAO.setUserPreferenceValue(userName, new  com.synapsense.dto.UserPreference(prefName, prefValue));
		} catch (UserManagementException e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}

	public static String get(String userName, String prefName, UserManagementService userDAO) {
		Collection<com.synapsense.dto.UserPreference> prefs = userDAO.getUserPreferences(userName);
		for (com.synapsense.dto.UserPreference pref : prefs) {
			if (pref.getPreferenceName().equals(prefName)) {
				return pref.getValue();
			}
		}
		return null;
	}
	
	public static void removeAll(String userName, UserManagementService userDAO) {
	    try {
            Collection<com.synapsense.dto.UserPreference> prefs = userDAO.getUserPreferences(userName);
            for (com.synapsense.dto.UserPreference pref : prefs) {
                userDAO.removeUserPreference(userName, pref);
            }
	    } catch (UserManagementException e) {
	        LOG.error(e.getLocalizedMessage(), e);
	    }
	}
	
	/**
	 * Since we store json objects in the user preferences all strings are quoted.
	 * This method returns unquoted string.  
	 * 
	 * @param userName
	 * @param prefName
	 * @param userDAO
	 * @return
	 */
	public static String getString(String userName, String prefName, UserManagementService userDAO) {
		String str = UserPreference.get(userName, prefName, userDAO);
		
		if (str != null && str.startsWith("\"") && str.endsWith("\"") && str.length() > 1) {
			str = str.substring(1, str.length() - 1);
		}
		
		return str;
	}
	
	public static void updateDefaultPrefs(String userName, UserManagementService userDAO) {
		for (Map.Entry<String, String> entry : defaultPrefs.entrySet()) {
			String prefName = entry.getKey();
			String prefValue = com.synapsense.mobile.utilities.UserPreference.get(userName, prefName, userDAO);
			if (prefValue == null) {
				//Default values are defined in standalone.xml and are loaded 
				//into system properties by Registrar service
				prefValue = System.getProperty(entry.getValue());
				if (prefValue != null) {
					if(!notStringTypePrefs.contains(prefName)){
						prefValue = "\"" + prefValue + "\"";
					}
					com.synapsense.mobile.utilities.UserPreference.set(userName, prefName, prefValue, userDAO);
				}
			}
			
		}
	}

}
