/**
 * 
 */
package com.synapsense.mobile.utilities;

/**
 * @author dinniss
 *
 */
public class Constants {
	public static final int ALERT_TYPE_STATUS_ALL						= -1;
	public static final int ALERT_TYPE_STATUS_DEACTIVE					= 0;
	public static final int ALERT_TYPE_STATUS_ACTIVE  					= 1;
	
	public static final int NUM_CONTROLLERS = 2;

	public static final String CONSOLE_ALERT_PREFIX						= "ConsoleAlert_";
	public static final String ALERT_RULES_PACKAGE						= "ConditionalAlerts";
	public static final String COMPONENT                  		= "component";
	
	// TYPE DESCRIPTION CONSTANTS
	public static final String TYPE_GATEWAY_X_PROPERTY = "x";
	public static final String TYPE_GATEWAY_Y_PROPERTY = "y";
	public static final String TYPE_GATEWAY_PHYSICAL_ID_PROPERTY = "mac";
	public static final String TYPE_GATEWAY_STATUS_PROPERTY = "status";
	public static final String TYPE_GATEWAY_ADDRESS_PROPERTY = "address";
	public static final String TYPE_GATEWAY_LOCATION_PROPERTY = "location";
	
	public static final String TYPE_NETWORK_ID_PROPERTY = "networkId";
	public static final String TYPE_NETWORK_PANID_PROPERTY = "panId";
	
	public static final String TYPE_PUE_IT_PROPERTY = "itPower";
	public static final String TYPE_PUE_INFRA_PROPERTY = "infrastructurePower";
	public static final String TYPE_PUE_COOLING_PROPERTY = "coolingPower";
	public static final String TYPE_PUE_LIGHTING_PROPERTY = "lightingPower";
	public static final String TYPE_PUE_POWERLOSS_PROPERTY = "powerLoss";
	public static final String TYPE_PUE_PUE_PROPERTY = "pue";
	public static final String TYPE_PUE_DCIE_PROPERTY = "dcie";
	public static final String TYPE_PUE_DCE_PROPERTY = "dce";
	public static final String TYPE_PUE_ABATEDINDEX_PROPERTY = "co2AbatedIndex";
	public static final String TYPE_PUE_ITDESIGN_PROPERTY = "itPowerDesign";
	public static final String TYPE_PUE_COOLINGDESIGN_PROPERTY = "coolingPowerDesign";
	
	//GATEWAY OPERATIONAL STATUS CONSTANTS
	public static final int GATEWAY_STATUS_ALIVE = 1;
	public static final int GATEWAY_STATUS_MASTER = 2;
	public static final int GATEWAY_STATUS_SYNC = 4;
	
	public static final String EVERYONE_ID	= "__everyone";
	public static final String EVERYONE_NAME	= "Everyone";

	//User role constants
	public static final String USER_ROLE_ADMIN	= "admin";
	public static final String USER_ROLE_ENGINEER= "engineer";
	public static final String USER_ROLE_USER	= "user";
	
	// Power Rack platform constants
	public static final String POWER_RACK_PLATFORM_SC	= "SC";
}
