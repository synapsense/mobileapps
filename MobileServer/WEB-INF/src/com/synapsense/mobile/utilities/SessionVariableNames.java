/**
 * 
 */
package com.synapsense.mobile.utilities;

/**
 * @author dinniss
 *
 */
public final class SessionVariableNames {

	// Session Variable Names
	public static final String SESSION_USER						= "SessionUser";
	public static final String SESSION_PASSWORD						= "SessionPassword";
	public static final String SESSION_LANGUAGE						= "SessionLanguage";
	public static final String SESSION_AUTOLOGIN						= "SessionAutoLogin";
	public static final String SESSION_ENVIRONMENT					= "SessionEnvironment";
	public static final String SESSION_LOCAL_ENVIRONMENT					= "SessionLocalEnvironment";
	public static final String SESSION_TRANSLATING_ENVIRONMENT					= "SessiontranslatingEnvironment";
	public static final String SESSION_ALERTING_SERVICE					= "SessionAlertingService";
	public static final String SESSION_DRULE_TYPE_DAO					= "SessionDruleTypeDAO";
	public static final String SESSION_DRULES_ENGINE					= "SessionDRulesEngine";
	public static final String SESSION_USERDAO					= "SessionUserDAO";
	public static final String SESSION_JSP_DATA_MAP					= "SessionJspDataMap";
	public static final String SESSION_LIVEIMAGING_SERVICE                  = "SessionLiveImagingService";
	public static final String SESSION_AUDITOR_SVC_SERVICE                  = "SessionAuditorSvcService";
	
	public static final String CSRF_NONCE = "CSRF_NONCE";
}
