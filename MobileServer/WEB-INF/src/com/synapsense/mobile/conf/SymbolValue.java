package com.synapsense.mobile.conf;

public class SymbolValue {	
	public String label;
	public Number value;
	
	public SymbolValue() {
	    super();
    }

	public SymbolValue(String label, Number value) {
	    super();
	    this.label = label;
	    this.value = value;
    }
	
}
