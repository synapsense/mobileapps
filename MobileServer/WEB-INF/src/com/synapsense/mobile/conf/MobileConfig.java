package com.synapsense.mobile.conf;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.synapsense.mobile.servlet.listener.ApplicationWatch;
import com.synapsense.mobile.tree.LiveImagingType;
import com.synapsense.mobile.utilities.XMLHelper;

public class MobileConfig {
	private static final Logger LOG = Logger.getLogger(MobileConfig.class);
	private static String configFileName = "conf/mobileconf.xml";
	private static Map<Integer, Collection<SymbolValue>> classId_symbolValues = new HashMap<Integer, Collection<SymbolValue>>();
	private static List<LiveImagingType> liTypes = new ArrayList<LiveImagingType>();

	private MobileConfig() {
	};

	public static void init() {
		File confFile = new File(ApplicationWatch.getPath() + configFileName);
		Document confDoc = null;

		try {
			confDoc = XMLHelper.getStandardDocBuilder().parse(confFile);
			XPathFactory factoryX = XPathFactory.newInstance();
			XPath xPath = factoryX.newXPath();

			// Set classId to symbolValues mapping
			XPathExpression xPathDataClasses = xPath.compile("mobileconfig/sensorValueMapping/dataClass");
			XPathExpression xPathClassId = xPath.compile("@id");
			XPathExpression xPathValues = xPath.compile("value");
			XPathExpression xPathVal = xPath.compile("@val");
			XPathExpression xPathMapVal = xPath.compile("@prompt");

			NodeList classIds = (NodeList) xPathDataClasses.evaluate(confDoc, XPathConstants.NODESET);
			if (classIds != null) {
				for (int i = 0; i < classIds.getLength(); i++) {
					List<SymbolValue> vals = new ArrayList<SymbolValue>();
					Integer classId = Integer.parseInt(xPathClassId.evaluate(classIds.item(i)));

					NodeList symbolVals = (NodeList) xPathValues.evaluate(classIds.item(i), XPathConstants.NODESET);
					if (symbolVals != null) {
						for (int j = 0; j < symbolVals.getLength(); j++) {
							vals.add(new SymbolValue(xPathMapVal.evaluate(symbolVals.item(j)), (Number) Double
							        .parseDouble(xPathVal.evaluate(symbolVals.item(j)))));
						}
						classId_symbolValues.put(classId, vals);
					}
				}
			}

			// Set dataclass to layerValues mapping
			XPathExpression xpDataClasses = xPath.compile("mobileconfig/feature[@name='LiveImaging']/dataclass");
			XPathExpression xpClassId = xPath.compile("@id");
			XPathExpression xpTapes = xPath.compile("type");
			XPathExpression xpMapLayer = xPath.compile("@layer");
			XPathExpression xpPrompt = xPath.compile("@prompt");

			NodeList dataClassIds = (NodeList) xpDataClasses.evaluate(confDoc, XPathConstants.NODESET);
			if (dataClassIds != null) {
				for (int i = 0; i < dataClassIds.getLength(); i++) {
					Integer classId = Integer.parseInt(xpClassId.evaluate(dataClassIds.item(i)));

					NodeList symbolVals = (NodeList) xpTapes.evaluate(dataClassIds.item(i), XPathConstants.NODESET);
					if (symbolVals != null) {
						for (int j = 0; j < symbolVals.getLength(); j++) {
							Integer layer = Integer.parseInt(xpMapLayer.evaluate(symbolVals.item(j)));
							String prompt = xpPrompt.evaluate(symbolVals.item(j));
							liTypes.add(new LiveImagingType(prompt, classId, layer));
						}
					}
				}
			}
		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}

	public static LiveImagingType[] getLiveImangingTypes() {
		LiveImagingType[] result = new LiveImagingType[liTypes.size()];
		for (int i = 0; i < liTypes.size(); i++) {
			LiveImagingType currType = liTypes.get(i);
			result[i] = new LiveImagingType(currType.getName(), currType.getDataClass(), currType.getLayer());
		}
		return result;
	}

	public static Collection<SymbolValue> getSymbolValuesByClassId(Integer classId) {
		return classId_symbolValues.get(classId);
	}

	public static String getSymbolValueByClassId(Integer classId, Number value) {
		String result = null;
		Collection<SymbolValue> sVals = classId_symbolValues.get(classId);
		if (sVals != null) {
			for (SymbolValue sv : sVals) {
				if (value.equals(sv.value)) {
					result = sv.label;
					break;
				}
			}
		}
		return result;
	}
}
