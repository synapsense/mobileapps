package com.synapsense.mobile.conf;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.synapsense.mobile.servlet.listener.ApplicationWatch;
import com.synapsense.mobile.utilities.XMLHelper;

public class AdminConfig {
	private static final Logger LOG = Logger.getLogger(AdminConfig.class);
	private static String configFileName = "conf/adminconf.xml";
	private static String[] floorplanTypes = new String[0];
	private static String[] uiPrivileges = new String[0];

	private AdminConfig() {
	};

	public static void init() {
		File confFile = new File(ApplicationWatch.getPath() + configFileName);
		Document confDoc = null;

		try {
			confDoc = XMLHelper.getStandardDocBuilder().parse(confFile);
			XPathFactory factoryX = XPathFactory.newInstance();
			XPath xPath = factoryX.newXPath();

			// Set allowed object types on the floorplan
			XPathExpression xPathAllowedTypes = xPath
			        .compile("adminconfig/feature[@name='floorplanObjects']/setting[@name='allowed']");
			XPathExpression xPathValue = xPath.compile("@value");
			NodeList allowedObjs = (NodeList) xPathAllowedTypes.evaluate(confDoc, XPathConstants.NODESET);
			if (allowedObjs != null) {
				String objsStr = xPathValue.evaluate(allowedObjs.item(0));
				if (objsStr != null) {
					floorplanTypes = objsStr.split(",");
				}
			}

			// Set UI Privileges
			XPathExpression xPathUiPrivs = xPath.compile("adminconfig/feature[@name='uiprivileges']/setting");
			XPathExpression xPathPrivName = xPath.compile("@name");
			XPathExpression xPathIsAllowed = xPath.compile("@value");
			NodeList privs = (NodeList) xPathUiPrivs.evaluate(confDoc, XPathConstants.NODESET);
			if (privs != null) {
				List<String> privList = new ArrayList<String>(privs.getLength());
				for (int i = 0; i < privs.getLength(); i++) {
					if ("true".equals(xPathIsAllowed.evaluate(privs.item(i)))) {
						String privName = xPathPrivName.evaluate(privs.item(i));
						if (privName != null) {
							privList.add(privName);
						}
					}
				}
				uiPrivileges = privList.toArray(new String[0]);
			}
		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}

	public static String[] getAllowedFloorplanTypes() {
		return floorplanTypes;
	}

	public static String[] getUiPrivileges() {
		return uiPrivileges;
	}
}
