package com.synapsense.mobile.requestprocessing;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.mobile.dataAccess.Env;
import com.synapsense.mobile.interaction.ObjectRequest;
import com.synapsense.mobile.lang.LangManager;
import com.synapsense.service.Environment;

/**
 * 
 * @author ahorosh This class is needed for Annual Energy Usage patch.
 *         FakePropertyAppender using should be removed when "annualEnergyUsage"
 *         property will be added to the PUE
 */
public class FakePropertyAppender {
	private static final String ANNUAL_ENERGY_USAGE_PROPERTY = "estimatedAnEnergyUsageToDate";

	public static void addFakeProperty(TO<?> envObj, CollectionTO colTo, ObjectRequest objectRequest, Environment env) {
		boolean addProperty = false;
		if (envObj.getTypeName().equals("PUE")) {
			String[] dcProps = objectRequest.getProperties();
			if (dcProps == null){
				addProperty = true;
			} else {
				for (int i = 0; i < dcProps.length; i++) {
					if (ANNUAL_ENERGY_USAGE_PROPERTY.equals(dcProps[i])) {
						addProperty = true;
						break;
					}
				}
			}
		}
		
		if(addProperty){						
			if(colTo.getPropValue(ANNUAL_ENERGY_USAGE_PROPERTY) == null){ // if PUE property doesn't exists 
				Double value = getEnergyUsage(envObj, env);
				ValueTO vto = new ValueTO(ANNUAL_ENERGY_USAGE_PROPERTY, value);
				colTo.addValue(vto);
			}
		}
	}

	private static Double getEnergyUsage(TO<?> pueTO, Environment env) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.DATE, 1);
		cal.set(Calendar.AM_PM, Calendar.AM);
		Double energyUsage = 0.0;
		for (int i = 0; i < 11; i++) {

			Date start = cal.getTime();
			try {
				// if (i != 0) {
				energyUsage += getMonthValue(pueTO, "energyUsage", start, env);
				// }
			} catch (EnvException e) {
				System.out.println(e);
				return null;
			}

			cal.add(Calendar.MONTH, -1);

		}

		Double currentEU;
		try {
			currentEU = env.getPropertyValue(pueTO, "energyUsage", Double.class);
		} catch (EnvException e) {
			return null;
		}

		if (currentEU == null) {
			currentEU = 0.0;
		}
		energyUsage += currentEU;

		return energyUsage;
	}

	// very ugly
	private static Double getMonthValue(TO<?> pue, String property, Date date, Environment env) throws EnvException {
		Collection<ValueTO> startVals = env.getHistory(pue, property, date, 2, Double.class);

		// Two values always
		if (startVals.size() != 2) {
			return 0.0;
		}

		// Second must be always zero
		ValueTO[] svArr = startVals.toArray(new ValueTO[startVals.size()]);
		if (!svArr[1].getValue().equals(0.0)) {
			return 0.0;
		}

		// Both from the same month
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int month = cal.get(Calendar.MONTH);
		for (ValueTO val : svArr) {
			cal.setTimeInMillis(val.getTimeStamp());
			if (month != cal.get(Calendar.MONTH)) {
				return 0.0;
			}
		}

		return (Double) svArr[0].getValue();
	}

	public static String getFakePropertyUnit(TO<?> obj, String property, HttpServletRequest req) {
		String unit = null;
		if (obj.getTypeName().equals("PUE") && ANNUAL_ENERGY_USAGE_PROPERTY.equals(property)) {
			unit = Env.getLocalizationService().getString("unit_kwh", LangManager.getCurrentLocale(req));
		}
		return unit;
	}
}
