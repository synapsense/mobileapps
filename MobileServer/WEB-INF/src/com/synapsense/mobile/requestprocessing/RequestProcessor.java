package com.synapsense.mobile.requestprocessing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;

import com.synapsense.dto.BinaryData;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.mobile.conf.MobileConfig;
import com.synapsense.mobile.conf.types.TypeUtil;
import com.synapsense.mobile.dataAccess.Env;
import com.synapsense.mobile.interaction.ObjectRequest;
import com.synapsense.mobile.interaction.RequestInfo;
import com.synapsense.mobile.interaction.ResponseElement;
import com.synapsense.mobile.lang.LangManager;
import com.synapsense.mobile.utilities.EnvTypeUtil;
import com.synapsense.mobile.utilities.EnvironmentTypes;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.BinaryDataImpl;

public class RequestProcessor {
	protected ThreadLocal<HttpServletRequest> request = new ThreadLocal<HttpServletRequest>();
	protected ThreadLocal<Environment> environment = new ThreadLocal<Environment>();
	private static final Logger LOG = Logger.getLogger(RequestProcessor.class);

	public RequestProcessor() {
	}

	public ResponseElement getObjectData(ObjectRequest objectRequest, Environment env, HttpServletRequest req) {
		environment.set(env);
		request.set(req);
		InternalResponseElement rootElem = new InternalResponseElement("root");
		addObjectRequestToElem(objectRequest, rootElem);

		return ObjectRequestUtils.convertElement(rootElem);
	}

	public ResponseElement getObjectsData(ObjectRequest[] objRequests, Environment env, HttpServletRequest req) {
		environment.set(env);
		request.set(req);
		InternalResponseElement rootElem = new InternalResponseElement("root");
		for (ObjectRequest objectRequest : objRequests) {
			addObjectRequestToElem(objectRequest, rootElem);
		}
		return ObjectRequestUtils.convertElement(rootElem);
	}

	protected void addObjectRequestToElem(ObjectRequest objectRequest, InternalResponseElement rootElem) {
		Environment env = environment.get();

		Collection<TO<?>> objColl = null;
		if (objectRequest.getId() == null) {
			objColl = env.getObjectsByType(objectRequest.getType());
		} else {
			objColl = Arrays.asList(new TO<?>[] { TOFactory.getInstance().loadTO(objectRequest.getId()) });
		}

		for (TO<?> envObj : objColl) {
			addObjectToElement(envObj, rootElem, objectRequest, env);
		}
	}

	protected void addObjectToElement(TO<?> envObj, InternalResponseElement rootElem, ObjectRequest objectRequest,
	        Environment env) {
		try {
			InternalResponseElement objElem = new InternalResponseElement(envObj.getTypeName());
			addCommonAttrsToObjectElem(envObj, objElem, objectRequest);
			rootElem.addSubElement(objElem);

			CollectionTO colTo = getProperties(objectRequest.getProperties(), envObj, env);

			addPropValuesToResponse(envObj, colTo, objElem, objectRequest, env);

			addRelatedObjects(envObj, objElem, objectRequest.getChildrenRequests(), true);
			addRelatedObjects(envObj, objElem, objectRequest.getParentsRequests(), false);
		} catch (Exception e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}

	protected void addRelatedObjects(TO<?> obj, InternalResponseElement elem, ObjectRequest[] objectRequests,
	        boolean isChild) throws ObjectNotFoundException, PropertyNotFoundException, ServletException,
	        NamingException {
		if (objectRequests == null || objectRequests.length == 0) {
			return;
		}
		Environment env = environment.get();
		for (ObjectRequest objectRequest : objectRequests) {
			Collection<TO<?>> objects = null;

			String objectsType = objectRequest.getType();
			String[] objectProps = objectRequest.getProperties();

			String elemName = isChild ? "children" : "parents";
			InternalResponseElement objElem = new InternalResponseElement(elemName);
			objElem.addAttribute("typeName", objectsType);
			elem.addSubElement(objElem);

			if ("all".equals(objectsType)) {
				if (isChild) {
					objects = env.getChildren(obj);
				} else {
					objects = env.getParents(obj);
				}
			} else {
				if (isChild) {
					if (objectRequest.isRequestDirectChildrenOnly()) {
						objects = env.getChildren(obj, objectsType);
					} else {
						objects = env.getRelatedObjects(obj, objectsType, isChild);
					}
				} else {
					if (objectRequest.isRequestDirectParentsOnly()) {
						objects = env.getParents(obj, objectsType);
					} else {
						objects = env.getRelatedObjects(obj, objectsType, isChild);
					}
				}
			}

			if (objectRequest.isRequestChildrenNumberOnly()) {
				int totalNum = objects == null ? 0 : objects.size();
				objElem.addAttribute("totalNumber", totalNum);
				continue;
			}

			if (objects == null || objects.isEmpty()) {
				continue;
			}

			Collection<CollectionTO> valueArray = null;

			if (objectProps == null) {
				valueArray = env.getAllPropertiesValues(objects);
			} else if (objectProps.length == 0) {
				valueArray = new HashSet<CollectionTO>();
				for (TO<?> objTO : objects) {
					valueArray.add(new CollectionTO(objTO, Collections.<ValueTO> emptyList()));
				}
			} else {
				valueArray = env.getPropertyValue(objects, objectProps);
			}

			if (valueArray == null) {
				return;
			}

			for (CollectionTO colTo : valueArray) {
				TO<?> object = colTo.getObjId();

				InternalResponseElement subObjElem = new InternalResponseElement(object.getTypeName());
				addCommonAttrsToObjectElem(object, subObjElem, objectRequest);
				objElem.addSubElement(subObjElem);

				addPropValuesToResponse(object, colTo, subObjElem, objectRequest, env);

				addRelatedObjects(object, subObjElem, objectRequest.getParentsRequests(), false);
				addRelatedObjects(object, subObjElem, objectRequest.getChildrenRequests(), true);
			}
		}
	}

	protected void addPropertyToResponseElem(TO<?> obj, ValueTO prop, InternalResponseElement responseElem,
	        ObjectRequest objectRequest, Environment env) throws PropertyNotFoundException, ObjectNotFoundException,
	        ServletException, NamingException {
		if (prop == null) {
			return;
		}

		Object o = prop.getValue();
		Class<?> clazz = EnvTypeUtil.getPropertyClass(prop.getPropertyName(), obj.getTypeName(), env);
		boolean isHistorical = EnvTypeUtil.isHistorical(obj.getTypeName(), prop.getPropertyName(), env);
		
		if((objectRequest.getAdditionalInfo() & RequestInfo.FILTER_HIST_AND_TO) == RequestInfo.FILTER_HIST_AND_TO){
			if(!isHistorical && !TO.class.isAssignableFrom(clazz)){
				LOG.debug("Property '" + prop.getPropertyName() + "' was filtered");
				return;
			}
		}
		
		InternalResponseElement propertyElem = new InternalResponseElement(prop.getPropertyName());

		if ((objectRequest.getAdditionalInfo() & RequestInfo.MAPPED_NAME) == RequestInfo.MAPPED_NAME) {
			String propNamePrompt = TypeUtil.getInstance().getDisplayablePropertyName(obj.getTypeName(),
			        prop.getPropertyName());
			String mappedPropName = LangManager.getInstance().getLocaleString(
			        LangManager.getCurrentLocale(request.get()), propNamePrompt);
			propertyElem.addAttribute("mn", mappedPropName);
		}		

		if ((objectRequest.getAdditionalInfo() & RequestInfo.LINK) == RequestInfo.LINK) {
			if (TO.class.isAssignableFrom(clazz)) {
				propertyElem.addAttribute("isLink", true);
			}
		}

		if (o != null) {
			// Class<?> clazz = o.getClass();
			if (TO.class.isAssignableFrom(clazz)) {
				ObjectRequest linkRequest = ObjectRequestUtils.getLinkRequestByType(objectRequest,
				        ((TO<?>) o).getTypeName());
				if (linkRequest == null) {
					propertyElem.addAttribute("v", TOFactory.getInstance().saveTO((TO<?>) o));
					responseElem.addSubElement(propertyElem);
					return;
				} else {
					TO<?> sensor = (TO<?>) o;
					CollectionTO colTo = getProperties(linkRequest.getProperties(), sensor, env);
					addCommonAttrsToObjectElem(sensor, propertyElem, linkRequest);

					addPropValuesToResponse(sensor, colTo, propertyElem, linkRequest, env);

					addRelatedObjects(sensor, propertyElem, linkRequest.getParentsRequests(), false);
					addRelatedObjects(sensor, propertyElem, linkRequest.getChildrenRequests(), true);
				}
			} else if (Number.class.isAssignableFrom(clazz)) {
				propertyElem.addAttribute("v", o);

				// hack to add mapped values
				if ((objectRequest.getAdditionalInfo() & RequestInfo.MAPPED_VALUE) == RequestInfo.MAPPED_VALUE) {
					if ("lastValue".equals(prop.getPropertyName())) {
						if (EnvironmentTypes.isSensorType(obj.getTypeName())) {
							try {
								Integer dataclass = env.getPropertyValue(obj, "dataclass", Integer.class);
								String label = MobileConfig.getSymbolValueByClassId(dataclass, (Number) o);
								if (label != null) {
									String mappedLabel = LangManager.getInstance().getLocaleString(
									        LangManager.getCurrentLocale(request.get()), label);
									propertyElem.addAttribute("mv", mappedLabel);
								}
							} catch (Exception e) {
								// just skip
								LOG.error(e.getLocalizedMessage(), e);
							}
						}
					}
				}

				if ((objectRequest.getAdditionalInfo() & RequestInfo.UNITS) == RequestInfo.UNITS) {
					String units = Env.getUnits(obj, prop.getPropertyName(), request.get());
					if (units == null) {
						units = FakePropertyAppender.getFakePropertyUnit(obj, prop.getPropertyName(), request.get()); // TODO: should be removed
						if(units == null){
							units = "";
						}
					}
					propertyElem.addAttribute("u", units);
				}

				if ((objectRequest.getAdditionalInfo() & RequestInfo.TIMESTAMP) == RequestInfo.TIMESTAMP) {
					propertyElem.addAttribute("t", prop.getTimeStamp());
				}

				if ((objectRequest.getAdditionalInfo() & RequestInfo.HISTORICAL) == RequestInfo.HISTORICAL) {
					if (isHistorical) {
						propertyElem.addAttribute("h", true);
					}
				}

			} else if (o instanceof BinaryData) {
				byte[] image = ((BinaryDataImpl) o).getValue();
				propertyElem.addAttribute("v", DatatypeConverter.printBase64Binary(image));
			} else {
				propertyElem.addAttribute("v", o);
			}
		} else {
			if (Number.class.isAssignableFrom(clazz)) {
				if ((objectRequest.getAdditionalInfo() & RequestInfo.TIMESTAMP) == RequestInfo.TIMESTAMP) {
					propertyElem.addAttribute("t", prop.getTimeStamp());
				}
				if ((objectRequest.getAdditionalInfo() & RequestInfo.UNITS) == RequestInfo.UNITS) {
					String units = Env.getUnits(obj, prop.getPropertyName(), request.get());
					if (units == null) {
						units = FakePropertyAppender.getFakePropertyUnit(obj, prop.getPropertyName(), request.get()); // TODO: should be removed
						if(units == null){
							units = "";
						}
					}					
					propertyElem.addAttribute("u", units);
				}
				if ((objectRequest.getAdditionalInfo() & RequestInfo.HISTORICAL) == RequestInfo.HISTORICAL) {
					if (isHistorical) {
						propertyElem.addAttribute("h", true);
					}
				}
			}
		}
		responseElem.addSubElement(propertyElem);
	}

	protected CollectionTO getProperties(String[] properties, TO<?> envObj, Environment env) {
		CollectionTO colTo = null;
		try {
			if (properties == null) {
				colTo = new CollectionTO(envObj, new LinkedList<ValueTO>(env.getAllPropertiesValues(envObj)));
			} else if (properties.length > 0) {
				Collection<TO<?>> col = new HashSet<TO<?>>();
				col.add(envObj);
				colTo = env.getPropertyValue(col, properties).iterator().next();
			}
		} catch (ObjectNotFoundException e) {

		}

		return colTo;
	}

	protected void addPropValuesToResponse(TO<?> envObj, CollectionTO colTo, InternalResponseElement objElem,
	        ObjectRequest objectRequest, Environment env) throws PropertyNotFoundException, ObjectNotFoundException,
	        ServletException, NamingException {

		if (colTo == null || colTo.getPropValues().isEmpty()) {
			return;
		}

		FakePropertyAppender.addFakeProperty(envObj, colTo, objectRequest, env); // TODO: should be removed

		List<ValueTO> otherProps = appendOrderedPropertiesToDoc(envObj, objElem, colTo, objectRequest);

		for (ValueTO prop : otherProps) {
			addPropertyToResponseElem(envObj, prop, objElem, objectRequest, env);
		}
	}

	protected List<ValueTO> appendOrderedPropertiesToDoc(TO<?> obj, InternalResponseElement objElem,
	        CollectionTO listProp, ObjectRequest objectRequest) throws ObjectNotFoundException, ServletException,
	        NamingException, PropertyNotFoundException {
		ArrayList<ValueTO> arr = new ArrayList<ValueTO>();
		arr.addAll(listProp.getPropValues());

		List<String> propNames = TypeUtil.getInstance().listPropertyNames(obj.getTypeName());

		for (int i = 0; i < propNames.size(); i++) {
			ValueTO currProp = null;
			List<ValueTO> colVto = listProp.getPropValue(propNames.get(i));
			if (colVto != null && !colVto.isEmpty()) {
				currProp = colVto.get(0);
				addPropertyToResponseElem(obj, currProp, objElem, objectRequest, environment.get());
			}
			if (currProp != null) {
				arr.remove(currProp);
			}
		}

		return arr;
	}

	private void addCommonAttrsToObjectElem(TO<?> object, InternalResponseElement objElement,
	        ObjectRequest objectRequest) {
		objElement.addAttribute("id", TOFactory.getInstance().saveTO(object));
		if ((objectRequest.getAdditionalInfo() & RequestInfo.MAPPED_NAME) == RequestInfo.MAPPED_NAME) {
			String typeNamePrompt = TypeUtil.getInstance().getDisplayableTypeName(object.getTypeName());
			String mappedTypeName = LangManager.getInstance().getLocaleString(
			        LangManager.getCurrentLocale(request.get()), typeNamePrompt);
			objElement.addAttribute("mn", mappedTypeName);
		}
	}
}
