package com.synapsense.mobile.requestprocessing;

import java.util.ArrayList;
import java.util.List;

import com.synapsense.mobile.interaction.ElementAttribute;

public class InternalResponseElement {
	protected String name = "";
	protected List<ElementAttribute> attributes = null;
	protected List<InternalResponseElement> subElements = null;

	public InternalResponseElement() {
		super();
	}

	public InternalResponseElement(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ElementAttribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<ElementAttribute> attributes) {
		this.attributes = attributes;
	}

	public List<InternalResponseElement> getSubElements() {
		return subElements;
	}

	public void setSubElements(List<InternalResponseElement> subElements) {
		this.subElements = subElements;
	}

	public void addAttribute(ElementAttribute attr) {
		createAttrsIfNull();
		attributes.add(attr);
	}

	public void addAttribute(String attrName, Object attrValue) {
		createAttrsIfNull();
		attributes.add(new ElementAttribute(attrName, attrValue));
	}

	public void addSubElement(InternalResponseElement elem) {
		createSubElementIfNull();
		subElements.add(elem);
	}

	protected void createAttrsIfNull() {
		if (attributes == null) {
			attributes = new ArrayList<ElementAttribute>(1);
		}
	}

	protected void createSubElementIfNull() {
		if (subElements == null) {
			subElements = new ArrayList<InternalResponseElement>(1);
		}
	}

}
