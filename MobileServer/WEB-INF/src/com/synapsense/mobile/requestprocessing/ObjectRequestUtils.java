package com.synapsense.mobile.requestprocessing;

import java.util.List;

import com.synapsense.mobile.interaction.ElementAttribute;
import com.synapsense.mobile.interaction.ObjectRequest;
import com.synapsense.mobile.interaction.ResponseElement;

public class ObjectRequestUtils {

	public static ResponseElement convertElement(InternalResponseElement intResponse) {
		ResponseElement result = new ResponseElement(intResponse.getName());

		List<ElementAttribute> attrs = intResponse.getAttributes();
		if (attrs != null) {
			result.setAttributes(attrs.toArray(new ElementAttribute[0]));
		}

		List<InternalResponseElement> intSubElems = intResponse.getSubElements();
		if (intSubElems != null) {
			ResponseElement[] subElements = new ResponseElement[intSubElems.size()];
			for (int i = 0; i < subElements.length; i++) {
				subElements[i] = convertElement(intSubElems.get(i));
			}
			result.setSubElements(subElements);
		}
		return result;
	}

	public static ObjectRequest getLinkRequestByType(ObjectRequest objRequest, String type) {
		ObjectRequest[] linkRequests = objRequest.getLinkRequests();
		if (linkRequests != null && type != null) {
			for (ObjectRequest linkRequest : linkRequests) {
				if (type.equals(linkRequest.getType())) {
					return linkRequest;
				}
			}
		}
		return null;
	}

	public static StringBuffer printResponse(ResponseElement response) {
		StringBuffer result = new StringBuffer();
		addElement(response, result);
		return result;
	}

	private static void addAttrs(ResponseElement response, StringBuffer buff) {
		if (response.getAttributes() == null)
			return;

		for (ElementAttribute attr : response.getAttributes()) {
			buff.append(attr.getName() + "=\"" + attr.getValue() + "\" ");
		}
	}

	private static void addElement(ResponseElement response, StringBuffer buff) {
		buff.append("<" + response.getName() + " ");
		addAttrs(response, buff);
		buff.append(">");

		if (response.getSubElements() != null) {
			for (ResponseElement elem : response.getSubElements()) {
				addElement(elem, buff);
			}
		}
		buff.append("</" + response.getName() + ">");
	}
}
