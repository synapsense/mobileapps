package com.synapsense.mobile.lang;

public class LangManagerInitializationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LangManagerInitializationException(String message, Throwable cause) {
		super(message, cause);
	}

	public LangManagerInitializationException(String message) {
		super(message);
	}

	public LangManagerInitializationException() {
		super();
	}

}
